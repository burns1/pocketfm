PFM has an updating mechanism to automatically update configuration files via pluging in an USB storage medium.
Pen drive or other media needs to be formated with a pfm readable file system FAT works. EXT{2,3,4} should work too but untested.

Files on the media could be e.g.: channels.conf, pfm.conf, sat.playlist, update.license


pfm.conf is the main configuration file. This will be installed without checks.

Other files needs explicitly be mentioned in update.license.

Herein a sha1 hash of the configuration file is made aswell as the filname. This two values are obstructed while converting the line to binary.

The structure of the file is:
Filename|SHA1_hash <-- converted to bin

licenser.py depends on an older version of SimpleCrypt: https://pypi.python.org/pypi/simple-crypt

**easy_install on mac os 10.10.5 installs a to recent version!**


mplayer channels.conf syntax (once again):

Exampel: Syrnet:10721:h:0:22000:0:402:0

means:

Channel name:Frequenz:Parameter:Signalquelle:Symbolrate:VPID:APID:TPID

TPID = Teletext.

Best to use is the script: pfmlicense <filename> <filename>

Like copying the editet files to /tmp

e.g. channels.conf and pfmlicense

then issuing:

./pfmlicense channels.conf

which generates update.license. The valid key file for the update.

All modified files including update.license must be present in the
root directory of a usb pen.

For updates the OK button needs to be presse while booting.
The USB pen drive needs to be inserted after boot and in update mode.
