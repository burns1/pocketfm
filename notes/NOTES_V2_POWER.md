# Power Consumption

When SAT not in can it be powered off? And does it help? Are there other improvements that can be made?


## Configuration

Measure:

    vcgencmd measure_temp && vcgencmd measure_volts

    temp=45.5'C
    volt=1.20V

Turn HDMI off. Unclear level of improvement:

    vcgencmd display_power 0

Turn SAT card off. Unload drivers:

    rmmod dvb_usb_ttusb2 dvb_usb dvb_core

Turn SAT card off. Turn off USB, also turns off eth:

    /etc/init.d/networking stop
    echo 0 > /sys/devices/platform/bcm2708_usb/buspower

Underclock CPU. Modify config.txt on the FAT partition of microsd
wiki.stocksy.co.uk/wiki/Raspberry_Pi_Setup
There may also be a way via proc?

    force_turbo=0
    arm_freq_min=250
    #arm_freq=1000 # option allow scale up
    core_freq_min=100
    sdram_frew_min=150
    over_voltage_min=4

## Code

The original version of the code would keep mplayer on the SAT when the tuner gets switched to AUX. This means it is techically still tuned to and decoding the sat signal. To remove this I changed pfmd.py:


    def buttons_src(button):
    ...
        if button == BTN_SET:
        ...
        # prepare playlist for sat
        if new_src_str == 'SAT':
            mpl.play('SAT')
        # uncomment if you wish to kill SAT mplayer on change to AUX
        # this has the disadvantage of adding delay between switch
        # but may improve power?
        # TODO: test if there are any power benifits
        elif new_src_str == 'AUX':
            mpl.play('AUX')
