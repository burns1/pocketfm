# Sat notes

These are my notes concerning getting challes and tuning the dish.

* Linux channel lists: http://www.linowsat.de/settings/vdr.html
* Transponder Details: http://www.linowsat.de/ http://www.lyngsat.com/freeradio/Germany.html
http://www.lyngsat.com/Astra-3B.html
* Getting azimuth: http://www.lyngsat-maps.com/ http://www.dishpointer.com/
* Other resources: http://satbeams.com


# Azimuth Astra

    Satellite: 23.5E ASTRA 3B
    Address: Berlin
    Latitude: 52.5200°
    Longitude: 13.4050°
    Elevation: 29.2°
    Azimuth (true): 167.4°
    Azimuth (magn.): 164.0° <--

    Satellite: 19.2E ASTRA 1KR | ASTRA 1L | ASTRA 1M | ASTRA 1N
    Address: Berlin
    Latitude: 52.5200°
    Longitude: 13.4050°
    Elevation: 29.7°
    Azimuth (true): 172.7°
    Azimuth (magn.): 169.3° <--

    Satellite: 28.5E EUTELSAT 28A
    Address: Berlin
    Latitude: 52.5200°
    Longitude: 13.4050°
    Elevation: 28.3°
    Azimuth (true): 161.2°
    Azimuth (magn.): 157.8° <--

Connect dish, tune PocketFM and wait for a LOCK.
or LinuxDVB dvbtune to known transponder then monitor with ``femon -H`` and wait for a signal lock.


# Get channel list

w_scan:
w_scan uses a satellites.dat compiled into code which has it's transponders lists.

    w_scan -fs -E0 -s S19E2 -X > channels.conf
    # -fs frontend type (s sat)
    # -s sat (? for list)
    # -X format output list for zap/czap/xine channels.conf
    #    compatible with mplayer and the PocketFM
    # -R 0 1 (search radio) or -T tv chans, -E cond acc


scan:
has some initial transponder lists in /usr/share/dvb/dvb-s/

    scan -x0 -t1 -s1 /usr/share/dvb/dvb-s/Astra-19.2E


# Testing / channels.conf

On Linux. Using a channels.conf from working local with

    szap -r "radioeins(ARD rbb)" -c ./channels.conf

    mplayer /dev/dvb/adapter0/dvr0 -vo null


Again the format of that channel in channels.conf:

    radioeins(ARD rbb):12265:  h  :  0  : 27500 :  0   : 841 : 0
    name               freq  param  src  symrate  vpid  apid tpid


name = Service Name  
freq = Frequency in MHz (what if frequency with komma)  
param = Polarization (v/h)  
src = Signal source of the channel, as defined in sources.conf (Sxxxx). Likely for DiSEC motors. See 
      [Linux-TV](http://linuxtv.org/vdrwiki/index.php/Syntax_of_channels.conf) and 
      [VDR man page](http://www.vdr-portal.de/board16-video-disk-recorder/board5-vdr-konfiguration/14469-aufbau-channels-conf/) In our case always 0. 
symrate = Symbol rate  
vpid = Video PID (Program Identification) (0 for Radio channels)  
apid = Audio PID. Either one or more numbers separated by a comma.  
tpid = Teletext PID (In our case always 0)

According to the VDR man page (man channels.conf) freq (Frequency)
needs always to be set as INT. It is unclear how float values should
be handled. According to VDR frequency should be set in MHz. Float
values like: 12728.75 MHz needs to rounded?

**There seems to be also a special handling of frequency data needed
when using it with mplayer in the Ku- and C-Band:**


**MPlayer and Satellite Dishes**

If you are using MPlayer with a satellite dish that has a C-Band lnb LO: 5150 or standard Ku Band lnb LO: 10750 such as are commonly used in North America, MPlayer will tune the channel if you use the L Band frequency in your .mplayer/channels.conf file instead of the C or Ku Band frequency. To calculate the L Band frequency for C Band, 5150 minus frequency in Mhz equals L Band frequency in Mhz. For Ku Band, frequency in Mhz minus 10750 equals L Band frequency in Mhz.

For example to tune C Band 4.060 Ghz

5150 - 4060 = 1090

Use 1090 for the frequency in channels.conf instead of 4060.

For Ku Band 11.799 Ghz

11799 - 10750 = 1049 

http://www.linuxtv.org/wiki/index.php/MPlayer


On PocketFM you could place the channels.conf entry into /home/pfm/channels.conf and change sat.playlist OR you can from the console kill current mplayer instances and run as you would on linux.

Status:

    femon -H

The output provides healt and is the same used on device to determine if there
is a signal. Status might be S, SC or SCVYL... From code:

    https://github.com/openpli-arm/dvb-apps/blob/master/util/femon/femon.c
    fe_info.signal ? 'S' : ' ',   //have signal
    fe_info.carrier ? 'C' : ' ',  //have carrier
    fe_info.viterbi ? 'V' : ' ',  //FEC Forward Error Correction good
    fe_info.sync ? 'Y' : ' ',     //Sync bytes found
    fe_info.lock ? 'L' : ' ',     //lock



# Lyngsat Entry Translate

Example translation of details from Lyngsat to something that could be used, for example, with dvbtune:

                    Frequency   SR-FEC                ONID-TID
                    Beam        SID-VPID              C/N lock
                    EIRP (dBW)                        APID Lang.

    Deutschlandfunk 12639.7 V   940-1/2 (SR-FEC)      0-1 (ONID-TID)
                    tp 20       1000-   (SID-VPID)    2.7 (C/N lock)
                                                      101 G (APID Lang)
    Radio POS AC    11973 V     -                     -
                    tp 214      -                     -
                                                      6631 G

    dvbtune -f 12639700 -p V -s 9400 -v 110 -a 101 -t 130 -tone 1


    http://www.lyngsat.com/Astra-1KR-1L-1M-1N.html

    ZDF Vision    11954 H  27500-3/4 (SR-FEC)       1-1079 (ONID-TID)
                                                    5.5 (C/N lock)
            ch:            28006 110 (SID-VPID)     120 G (APID Lang)


    dvbtune -f 11954000 -p H -s 27500 -v 110 -a 120 -t 130 -tone 1
    #          freq      pol.   symb     vpid  apid   tpid
    # tpid=telitextpid apid=audo vpid=video
    #       -x        Attempt to auto-find other transponders (experimental)
    #       -m        Monitor the reception quality





# Scan Log

## w_scan -fs -s S19E2 -X     Astra 1F/1G/1H/1KR/1L


http://www.linuxtv.org/vdrwiki/index.php/Syntax_of_channels.conf


dumping lists (384 services)
tagesschau24(ARD):10743:h:0:22000:101:102:28721
Einsfestival(ARD):10743:h:0:22000:201:206:28722
EinsPlus(ARD):10743:h:0:22000:301:306:28723
arte(ARD):10743:h:0:22000:401:402:28724
PHOENIX(ARD):10743:h:0:22000:501:502:28725
Test-R(ARD):10743:h:0:22000:501:502:28726
VH1 Classic.(MTV Networks Europe):11739:v:0:27500:3071:3072:28667
iTELE(CSAT):11856:v:0:27500:210:221:8202
iTELE(CSAT):11856:v:0:27500:210:221:8222
service_id 8225:11856:v:0:27500:510:521:8225
rbb Brandenburg(ARD):12109:h:0:27500:601:602:28205
rbb Berlin(ARD):12109:h:0:27500:601:602:28206
ARD-TEST-1(ARD):12109:h:0:27500:601:602:28221
NDR FS MV(ARD):12109:h:0:27500:2601:2602:28224
NDR FS HH(ARD):12109:h:0:27500:2601:2602:28225
NDR FS NDS(ARD):12109:h:0:27500:2601:2602:28226
NDR FS SH(ARD):12109:h:0:27500:2601:2602:28227
MDR Sachsen(ARD):12109:h:0:27500:2901:2902:28228
MDR S-Anhalt(ARD):12109:h:0:27500:2901:2902:28229
MDR Thüringen(ARD):12109:h:0:27500:2901:2902:28230
SWR Fernsehen RP(ARD):12109:h:0:27500:3101:3106:28231
service_id 8106:11934:v:0:27500:610:621:8106
TV5MONDE(CSAT):11934:v:0:27500:1010:1021:8110
service_id 8112:11934:v:0:27500:1210:0:8112
Comedy Central/VIVA(MTV Networks Europe):11973:v:0:27500:4061:4062:28676
Nickelodeon(MTV Networks Europe):11973:v:0:27500:4101:4102:28680
ZDF(ZDFvision):11953:h:0:27500:110:125:28006
3sat(ZDFvision):11953:h:0:27500:210:225:28007
KiKA(ZDFvision):11953:h:0:27500:310:325:28008
ZDFinfo(ZDFvision):11953:h:0:27500:610:625:28011
DKULTUR(ZDFvision):11953:h:0:27500:0:711:28012
DLF(ZDFvision):11953:h:0:27500:0:810:28013
zdf_neo(ZDFvision):11953:h:0:27500:660:675:28014
DRadio DokDeb(ZDFvision):11953:h:0:27500:0:510:28015
zdf.kultur(ZDFvision):11953:h:0:27500:1110:1125:28016
DRadio Wissen(ZDFvision):11953:h:0:27500:0:410:28017
ProSieben Schweiz(ProSiebenSat.1):12051:v:0:27500:289:292:20001
ProSieben Austria(ProSiebenSat.1):12051:v:0:27500:161:85:20002
Kabel 1 Schweiz(ProSiebenSat.1):12051:v:0:27500:162:163:20003
Kabel 1 Austria(ProSiebenSat.1):12051:v:0:27500:166:167:20004
SAT.1 A(ProSiebenSat.1):12051:v:0:27500:800:803:20005
SAT.1 CH(ProSiebenSat.1):12051:v:0:27500:1535:1539:20006
SAT.1 HH/SH(ProSiebenSat.1):12051:v:0:27500:2047:2051:20008
SAT.1 NS/Bremen(ProSiebenSat.1):12051:v:0:27500:2047:2051:20009
SAT.1 RhlPf/Hessen(ProSiebenSat.1):12051:v:0:27500:2047:2051:20010
RTL Television(RTL World):12187:h:0:27500:163:106:12003
RTL Regional NRW(RTL World):12187:h:0:27500:163:104:12004
RTL HB NDS(RTL World):12187:h:0:27500:163:104:12005
RTL FS(RTL World):12187:h:0:27500:163:104:12006
RTL2(RTL World):12187:h:0:27500:166:128:12020
SUPER RTL(RTL World):12187:h:0:27500:165:120:12040
SUPER RTL CH(RTL):12187:h:0:27500:172:145:12041
VOX(RTL World):12187:h:0:27500:167:136:12060
RTLNITRO(RTL):12187:h:0:27500:173:146:12061
Channel 21(RTL World):12187:h:0:27500:168:137:12080
n-tv(RTL World):12187:h:0:27500:169:73:12090
Channel 21 ALT(CBC):12187:h:0:27500:168:137:12095
RTL Austria(RTL):12226:h:0:27500:201:202:28800
VOX Austria(RTL):12226:h:0:27500:301:302:28805
RTL2 Austria(RTL):12226:h:0:27500:401:402:28810
SUPER RTL A(RTL):12226:h:0:27500:501:502:28815
Eurosport Deutschland(SES Astra):12226:h:0:27500:101:103:31200
HSE24 EXTRA(SES Astra):12226:h:0:27500:512:660:31210
EuroNews(Globecast):12226:h:0:27500:2432:2433:31220
VOX CH(RTL):12226:h:0:27500:601:602:28820
RTL CH(CBC):12226:h:0:27500:611:613:28825
Bayern 1(ARD BR):12265:h:0:27500:0:101:28400
Bayern 2(ARD BR):12265:h:0:27500:0:111:28401
BAYERN 3(ARD BR):12265:h:0:27500:0:121:28402
BR-KLASSIK(ARD BR):12265:h:0:27500:0:132:28403
B5 aktuell(ARD BR):12265:h:0:27500:0:141:28404
BAYERN plus(ARD BR):12265:h:0:27500:0:151:28405
PULS(ARD BR):12265:h:0:27500:0:161:28406
BR Heimat(ARD BR):12265:h:0:27500:0:171:28407
B5 plus(ARD BR):12265:h:0:27500:0:181:28408
hr1(ARD HR):12265:h:0:27500:0:401:28419
hr2(ARD HR):12265:h:0:27500:0:412:28420
hr3(ARD HR):12265:h:0:27500:0:421:28421
hr4(ARD HR):12265:h:0:27500:0:431:28422
YOU FM(ARD HR):12265:h:0:27500:0:451:28423
hr-iNFO(ARD HR):12265:h:0:27500:0:461:28424
MDR1 SACHSEN(ARD MDR):12265:h:0:27500:0:501:28428
MDR S-ANHALT(ARD MDR):12265:h:0:27500:0:511:28429
MDR THÜRINGEN(ARD MDR):12265:h:0:27500:0:521:28430
MDR FIGARO(ARD MDR):12265:h:0:27500:0:532:28431
MDR JUMP(ARD MDR):12265:h:0:27500:0:541:28432
MDR SPUTNIK(ARD MDR):12265:h:0:27500:0:551:28433
MDR INFO(ARD MDR):12265:h:0:27500:0:561:28434
MDR KLASSIK(ARD MDR):12265:h:0:27500:0:571:28435
NDR 2(ARD NDR):12265:h:0:27500:0:601:28437
NDR Kultur(ARD NDR):12265:h:0:27500:0:611:28438
NDR Info(ARD NDR):12265:h:0:27500:0:621:28439
N-JOY(ARD NDR):12265:h:0:27500:0:631:28440
NDR 90,3(ARD NDR):12265:h:0:27500:0:641:28441
NDR1WelleNord(ARD NDR):12265:h:0:27500:0:651:28442
NDR 1 Radio MV(ARD NDR):12265:h:0:27500:0:661:28443
NDR 1 Nieders.(ARD NDR):12265:h:0:27500:0:671:28444
NDR Info Spez.(ARD NDR):12265:h:0:27500:0:681:28445
NDR Blue(ARD NDR):12265:h:0:27500:0:691:28446
Bremen Eins(ARD RB):12265:h:0:27500:0:701:28448
Nordwestradio(ARD RB):12265:h:0:27500:0:711:28449
Bremen Vier(ARD RB):12265:h:0:27500:0:721:28450
Inforadio(ARD rbb):12265:h:0:27500:0:801:28452
Kulturradio(ARD rbb):12265:h:0:27500:0:811:28453
Antenne Brandenburg(ARD rbb):12265:h:0:27500:0:821:28454
radioBERLIN 88,8(ARD rbb):12265:h:0:27500:0:831:28455
radioeins(ARD rbb):12265:h:0:27500:0:841:28456
Fritz(ARD rbb):12265:h:0:27500:0:851:28457
SR 1 Europawelle(ARD SR):12265:h:0:27500:0:901:28461
SR 2 KulturRadio(ARD SR):12265:h:0:27500:0:911:28462
SR 3 Saarlandwelle(ARD SR):12265:h:0:27500:0:921:28463
SWR1 BW(ARD SWR):12265:h:0:27500:0:1001:28465
SWR1 RP(ARD SWR):12265:h:0:27500:0:1011:28466
SWR2(ARD SWR):12265:h:0:27500:0:1022:28467
SWR3(ARD SWR):12265:h:0:27500:0:1031:28468
SWR4 BW(ARD SWR):12265:h:0:27500:0:1041:28469
SWR4 RP(ARD SWR):12265:h:0:27500:0:1051:28470
DASDING(ARD SWR):12265:h:0:27500:0:1061:28471
SWRinfo(ARD SWR):12265:h:0:27500:0:1071:28472
1LIVE(ARD WDR):12265:h:0:27500:0:1101:28475
WDR 2(ARD WDR):12265:h:0:27500:0:1111:28476
WDR 3(ARD WDR):12265:h:0:27500:0:1122:28477
WDR 4(ARD WDR):12265:h:0:27500:0:1131:28478
WDR 5(ARD WDR):12265:h:0:27500:0:1141:28479
WDR Funkhaus Europa(ARD WDR):12265:h:0:27500:0:1151:28480
1LIVE diGGi(ARD WDR):12265:h:0:27500:0:1161:28481
KIRAKA(ARD WDR):12265:h:0:27500:0:1171:28482
WDR Event(ARD WDR):12265:h:0:27500:0:1181:28483
SR Fernsehen(ARD):12265:h:0:27500:1301:1302:28486
ARD-alpha(ARD):12265:h:0:27500:1401:1406:28487
CANALSAT RADIOS(CSAT):12363:v:0:27500:0:1910:9118
RADIOS 3(CSAT):12363:v:0:27500:0:1943:9116
RADIOS 2(CSAT):12363:v:0:27500:0:1976:9113
RADIOS 1(CSAT):12363:v:0:27500:0:1908:9114
RF(CSAT):12363:v:0:27500:0:1901:9115
RADIO COURTOISIE(CSAT):12363:v:0:27500:0:1952:9141
AUDIO PRIMO:12363:v:0:27500:0:1971:9102
.(CSAT):12363:v:0:27500:0:2046:9117
D8(CSAT):12402:v:0:27500:110:122:8701
LCP(CSAT):12402:v:0:27500:710:721:8707
ARTE(CSAT):12402:v:0:27500:1210:1221:8712
D8(IMEDIA):12402:v:0:27500:110:122:8721
ARTE(CSAT):12402:v:0:27500:1210:1221:8732
service_id 8763:12402:v:0:27500:4910:0:8763
service_id 8764:12402:v:0:27500:4910:0:8764
Das Erste(ARD):11836:h:0:27500:101:106:28106
Bayerisches FS Süd(ARD):11836:h:0:27500:201:206:28107
hr-fernsehen(ARD):11836:h:0:27500:301:302:28108
Bayerisches FS Nord(ARD):11836:h:0:27500:201:206:28110
WDR Köln(ARD):11836:h:0:27500:601:602:28111
SWR Fernsehen BW(ARD):11836:h:0:27500:801:806:28113
service_id 4002:12515:h:0:22000:510:82:4002
service_id 4003:12515:h:0:22000:511:83:4003
service_id 4011:12515:h:0:22000:517:88:4011
service_id 4012:12515:h:0:22000:518:92:4012
service_id 4013:12515:h:0:22000:519:96:4013
service_id 4016:12515:h:0:22000:525:120:4016
service_id 4044:12515:h:0:22000:534:74:4044
service_id 4045:12515:h:0:22000:535:75:4045
service_id 4047:12515:h:0:22000:537:77:4047
service_id 4035:12515:h:0:22000:0:120:4035
service_id 4060:12515:h:0:22000:0:119:4060
service_id 4062:12515:h:0:22000:0:132:4062
service_id 4058:12515:h:0:22000:558:128:4058
service_id 4064:12515:h:0:22000:564:90:4064
service_id 4065:12515:h:0:22000:564:90:4065
QVC PLUS(SES ASTRA):12551:v:0:22000:168:144:3394
QVC Deutschland(SES ASTRA):12551:v:0:22000:165:166:12100
Bibel TV(SES ASTRA):12551:v:0:22000:33:34:12122
eUrotic(SES ASTRA):12551:v:0:22000:460:470:12123
NT1:12551:v:0:22000:128:228:12164
NT1:12551:v:0:22000:128:228:12165
BFM TV(SES ASTRA):12551:v:0:22000:2171:2172:12171
BFM Business(SES ASTRA):12551:v:0:22000:39:38:12180
CashTV(SES ASTRA):12551:v:0:22000:1071:1072:12185
Radio Horeb(Eurociel):12603:h:0:22000:0:1289:7289
Sky News Intl(SES ASTRA):12603:h:0:22000:1290:2290:7290
Radio neue Hoffnung(RADIO Neue Hoffnung):12603:h:0:22000:0:1292:7292
RTL RADIO:12603:h:0:22000:0:7932:7931
WDR Aachen(ARD):12603:h:0:22000:3401:3402:28534
WDR Wuppertal(ARD):12603:h:0:22000:3401:3402:28535
WDR Bonn(ARD):12603:h:0:22000:3401:3402:28536
WDR Duisburg(ARD):12603:h:0:22000:3401:3402:28537
WDR HD Aachen(ARD):12603:h:0:22000:5601:5606:28544
WDR HD Wuppertal(ARD):12603:h:0:22000:5601:5606:28545
WDR HD Bonn(ARD):12603:h:0:22000:5601:5606:28546
WDR HD Duisburg(ARD):12603:h:0:22000:5601:5606:28547
MEDIA BROADCAST - Test 7(MEDIA BROADCAST):12633:h:0:22000:1113:1114:12600
K-TV(MEDIA BROADCAST):12633:h:0:22000:202:302:12601
Deutsches Musik Fernsehen(MEDIA BROADCAST):12633:h:0:22000:53:54:12604
Lustkanal24 TV(MEDIA BROADCAST):12633:h:0:22000:206:306:12606
MEDIA BROADCAST - Test 6(MEDIA BROADCAST):12633:h:0:22000:1113:1114:12607
Sexy Club TV(MEDIA BROADCAST):12633:h:0:22000:212:312:12612
rhein main tv(MEDIA BROADCAST):12633:h:0:22000:208:308:12614
Deutsche Girls 24 TV (MEDIA BROADCAST):12633:h:0:22000:215:315:12615
Juwelo TV(MEDIA BROADCAST):12633:h:0:22000:1041:1042:12616
Dreamgirls24 TV(MEDIA BROADCAST):12633:h:0:22000:218:318:12618
Erotiksat24 TV(MEDIA BROADCAST):12633:h:0:22000:219:319:12619
123-Damenwahl(MEDIA BROADCAST):12633:h:0:22000:220:320:12620
MEDIA BROADCAST - Test 5(MEDIA BROADCAST):12633:h:0:22000:1113:1114:12621
Maennersache TV(MEDIA BROADCAST):12633:h:0:22000:222:322:12622
Date Line(MEDIA BROADCAST):12633:h:0:22000:223:323:12623
Fotohandy(MEDIA BROADCAST):12633:h:0:22000:224:324:12624
Mobile Sex(MEDIA BROADCAST):12633:h:0:22000:225:325:12625
SEX-Kontakte(MEDIA BROADCAST):12633:h:0:22000:226:326:12626
Eros TV(MEDIA BROADCAST):12633:h:0:22000:227:327:12627
Achtung Sexy TV(MEDIA BROADCAST):12633:h:0:22000:228:328:12628
Traumfrauen TV(MEDIA BROADCAST):12633:h:0:22000:229:329:12629
Heiss und Sexy TV(MEDIA BROADCAST):12633:h:0:22000:230:330:12630
Shop24Direct(MEDIA BROADCAST):12633:h:0:22000:233:333:12633
nice(MEDIA BROADCAST):12633:h:0:22000:242:342:12634
Babestation24(MEDIA BROADCAST):12633:h:0:22000:235:335:12635
Fundorado TV(MEDIA BROADCAST):12633:h:0:22000:236:336:12636
EROTIKA TV - NEU!(MEDIA BROADCAST):12633:h:0:22000:239:339:12639
BunnyClub24(MEDIA BROADCAST):12633:h:0:22000:240:340:12640
Clipmobile(MEDIA BROADCAST):12633:h:0:22000:241:341:12641
MEDIA BROADCAST - Test 4(MEDIA BROADCAST):12633:h:0:22000:1113:1114:12642
ALT - SUCHLAUF STARTEN(MEDIA BROADCAST):12633:h:0:22000:53:54:12643
multithek (Internet)(MEDIA BROADCAST):12633:h:0:22000:244:0:12644
Inselradio(MEDIA BROADCAST):12633:h:0:22000:0:702:12651
radio top40(MEDIA BROADCAST):12633:h:0:22000:0:353:12653
ffn digital(MEDIA BROADCAST):12633:h:0:22000:0:354:12654
Radio Paloma(MEDIA BROADCAST):12633:h:0:22000:0:355:12655
Radio HBR(MEDIA BROADCAST):12633:h:0:22000:0:356:12656
Radio HCJB(MEDIA BROADCAST):12633:h:0:22000:0:357:12657
SCHLAGERPARADIES(MEDIA BROADCAST):12633:h:0:22000:0:358:12658
Radio Gloria(MEDIA BROADCAST):12633:h:0:22000:0:359:12659
HIT RADIO FFH(MEDIA BROADCAST):12633:h:0:22000:0:1024:12660
planet radio(MEDIA BROADCAST):12633:h:0:22000:0:1030:12661
harmony.fm(MEDIA BROADCAST):12633:h:0:22000:0:1036:12662
Radio Regenbogen(MEDIA BROADCAST):12633:h:0:22000:0:363:12663
radio B2(MEDIA BROADCAST):12633:h:0:22000:0:364:12664
RTL NITRO A(RTL):12662:h:0:22000:1020:1021:13102
Welt der Wunder(-):12662:h:0:22000:1030:1031:13103
LT1-OOE(-):12662:h:0:22000:1040:1041:13104
sixx Austria(sevenonemedia):12662:h:0:22000:1060:1061:13106
VISIT-X.tv(-):12662:h:0:22000:1070:1071:13107
ServusTV Deutschland(ServusTV):12662:h:0:22000:1110:1111:13110
ServusTV Oesterreich(ServusTV):12662:h:0:22000:2110:2111:13111
L-TV/TVM(-):12662:h:0:22000:2130:2131:13113
OE1(ORF):12662:h:0:22000:0:421:13121
OE2 W(ORF):12662:h:0:22000:0:423:13123
OE2 N(ORF):12662:h:0:22000:0:424:13124
OE2 B(ORF):12662:h:0:22000:0:425:13125
OE2 O(ORF):12662:h:0:22000:0:426:13126
OE2 S(ORF):12662:h:0:22000:0:427:13127
OE2 T(ORF):12662:h:0:22000:0:428:13128
OE2 V(ORF):12662:h:0:22000:0:429:13129
OE2 St(ORF):12662:h:0:22000:0:430:13130
OE2 K(ORF):12662:h:0:22000:0:431:13131
OE3(ORF):12662:h:0:22000:0:433:13133
FM4(ORF):12662:h:0:22000:0:434:13134
U1 Tirol(U1 Tirol):12662:h:0:22000:0:436:13136
RADIO MARIA(Radio Maria sterreich):12662:h:0:22000:0:440:13140
BTV(-):12662:h:0:22000:2410:2411:13141
HITRADIO OE3(ORF):12692:h:0:22000:130:131:13013
ORF2E(ORF):12692:h:0:22000:170:171:13014
Mei Musi TV(-):12692:h:0:22000:150:151:13015
Folx TV(-):12692:h:0:22000:180:181:13018
RiC(-):12692:h:0:22000:190:191:13019
gotv(GoTV):12692:h:0:22000:3020:3021:13021
WDR Bielefeld(ARD):12421:h:0:27500:501:502:28306
WDR Dortmund(ARD):12421:h:0:27500:501:502:28307
WDR Düsseldorf(ARD):12421:h:0:27500:501:502:28308
WDR Essen(ARD):12421:h:0:27500:501:502:28309
WDR Münster(ARD):12421:h:0:27500:501:502:28310
WDR Siegen(ARD):12421:h:0:27500:501:502:28311
WDR HD Köln(ARD):12421:h:0:27500:5501:5506:28325
WDR HD Bielefeld(ARD):12421:h:0:27500:5501:5506:28326
WDR HD Dortmund(ARD):12421:h:0:27500:5501:5506:28327
WDR HD Düsseldorf(ARD):12421:h:0:27500:5501:5506:28328
WDR HD Essen(ARD):12421:h:0:27500:5501:5506:28329
WDR HD Münster(ARD):12421:h:0:27500:5501:5506:28330
WDR HD Siegen(ARD):12421:h:0:27500:5501:5506:28331
Radio Bremen TV(ARD):12421:h:0:27500:1201:1202:28385
WDR Test A(ARD):12421:h:0:27500:501:502:28395
Sonnenklar TV(BetaDigital):12480:v:0:27500:2303:2304:32
HSE24(BetaDigital):12480:v:0:27500:1279:1280:40
mediasparTV Homeshopping(BetaDigital):12480:v:0:27500:1791:1792:46
REGIO TV(BetaDigital):12480:v:0:27500:2047:2048:47
TELE 5(BetaDigital):12480:v:0:27500:1535:1536:51
DMAX(BetaDigital):12480:v:0:27500:3327:3328:63
QLAR(BetaDigital):12480:v:0:27500:2815:2816:76
HSE24 TREND(BetaDigital):12480:v:0:27500:3071:3072:77
AstroTV(BetaDigital):12480:v:0:27500:2559:2560:661
e8 television(BetaDigital):12480:v:0:27500:767:768:897
MediaShop- Neuheiten(BetaDigital):12480:v:0:27500:255:256:898
meinTVshop(BetaDigital):12480:v:0:27500:511:512:899
SPORT1(BetaDigital):12480:v:0:27500:1023:1024:900
Sky Select(SKY):12031:h:0:27500:2815:2816:18
ManouLenz.tv(BetaDigital):12460:h:0:27500:511:512:48
EWTN katholisches TV(BetaDigital):12460:h:0:27500:1023:1024:62
QVC BEAUTY&STYLE(BetaDigital):12460:h:0:27500:1791:1792:64
Innovation TV(BetaDigital):12460:h:0:27500:1279:1280:74
domradio(BetaDigital):12460:h:0:27500:0:368:171
egoFM(BetaDigital):12460:h:0:27500:0:384:172
Klassik Radio(BetaDigital):12460:h:0:27500:0:336:173
HOPE Channel Radio(BetaDigital):12460:h:0:27500:0:400:175
JAM FM(BetaDigital):12460:h:0:27500:0:528:177
Sparhandy TV(BetaDigital):12460:h:0:27500:3071:3072:659
1-2-3.tv(BetaDigital):12460:h:0:27500:2815:2816:662
ANIXE SD(BetaDigital):12460:h:0:27500:3311:3312:764
TLC(BetaDigital):12460:h:0:27500:1535:1536:772
SIXX(ProSiebenSat.1):12460:h:0:27500:767:768:776
Disney Channel(BetaDigital):12460:h:0:27500:2047:2051:1793
QVC(BetaDigital):12460:h:0:27500:255:256:1794
N24 Austria(BetaDigital):12148:h:0:27500:255:256:53
Beauty TV(BetaDigital):12148:h:0:27500:3071:3072:54
Comedy Central / VIVA AT(MTV Networks):12148:h:0:27500:1535:1536:60
NICKELODEON AT(MTV Networks):12148:h:0:27500:1791:1792:61
DELUXE MUSIC(BetaDigital):12148:h:0:27500:3327:3328:65
BB-MV Lokal-TV(BetaDigital):12148:h:0:27500:2559:2560:70
HOPE Channel deutsch(BetaDigital):12148:h:0:27500:511:512:71
DMAX Austria(BetaDigital):12148:h:0:27500:3583:3584:73
ROCK ANTENNE(BetaDigital):12148:h:0:27500:0:304:160
ERF Plus(BetaDigital):12148:h:0:27500:0:320:161
ERF Pop(BetaDigital):12148:h:0:27500:0:368:162
Life Channel CH(BetaDigital):12148:h:0:27500:0:384:163
sunshine live(BetaDigital):12148:h:0:27500:0:336:169
ANTENNE BAYERN(BetaDigital):12148:h:0:27500:0:352:170
JML Shop(BetaDigital):12148:h:0:27500:2303:2304:514
pearl.tv Shop(BetaDigital):12148:h:0:27500:2047:2048:765
RNF(BetaDigital):12148:h:0:27500:1104:1105:768
Channel21(BetaDigital):12148:h:0:27500:1023:1024:769
GOD Channel(BetaDigital):12148:h:0:27500:767:768:774
MediaShop- Meine Einkaufswelt(BetaDigital):12148:h:0:27500:1279:1280:775
Volksmusik(-):11243:h:0:22000:2220:2221:13222
Bibel TV HD(ORS):11243:h:0:22000:2240:2241:13224
Schau TV(-):11243:h:0:22000:2250:2251:13225
Starparadies AT(-):11243:h:0:22000:2260:2261:13226
HOPE Channel HD(-):11243:h:0:22000:2270:0:13227
MELODIE TV(ORS):11243:h:0:22000:2290:2291:13229
RTL 2 CH(BetaDigital):11391:h:0:22000:255:256:12400
Cubavision Internacional(GLOBECAST):11508:v:0:22000:708:728:7008
Al Jazeera Channel(GLOBECAST):11508:v:0:22000:709:729:7009
RedeRecord(GLOBECAST):11508:v:0:22000:710:730:7010
Arirang TV(GLOBECAST):11508:v:0:22000:711:731:7011
Al Jazeera English(GlobeCast):11508:v:0:22000:712:732:7012
RT Esp(GlobeCast):11508:v:0:22000:1450:1550:7013
NHK World TV(GLOBECAST):11508:v:0:22000:714:734:7014
a.tv(BetaDigital):11523:h:0:22000:255:256:4600
Franken Fernsehen(BetaDigital):11523:h:0:22000:511:512:4601
intv(BetaDigital):11523:h:0:22000:767:768:4602
Ulm-Allgäu(BetaDigital):11523:h:0:22000:1023:1024:4603
münchen.tv(BetaDigital):11523:h:0:22000:1279:1280:4604
rfo Regional Oberbayern(BetaDigital):11523:h:0:22000:1535:1536:4605
Mainfranken(BetaDigital):11523:h:0:22000:1791:1792:4606
TV Oberfranken(BetaDigital):11523:h:0:22000:2047:2048:4607
TVA-OTV(BetaDigital):11523:h:0:22000:2303:2304:4608
Niederbayern(BetaDigital):11523:h:0:22000:2559:2560:4609
Lokal TV Portal(BetaDigital):11523:h:0:22000:4607:4608:4690
Russia Today(GLOBECAST):11538:v:0:22000:604:624:6904
France 24 (en Français)(GLOBECAST):11538:v:0:22000:605:625:6905
France 24 (in English)(GLOBECAST):11538:v:0:22000:606:626:6906
France 24 (in Arabic)(GLOBECAST):11538:v:0:22000:610:630:6910
CCTV9 Documentary(GLOBECAST):11538:v:0:22000:612:632:6912
CCTV F(GLOBECAST):11538:v:0:22000:613:633:6913
CCTV NEWS(GLOBECAST):11538:v:0:22000:614:634:6914
TV5MONDE EUROPE(GlobeCast):11538:v:0:22000:615:635:6915
ASTRA 3D demo(SES ASTRA):11611:h:0:22000:33:36:5101
TRT Turk(SES ASTRA):11611:h:0:22000:2011:4004:5113
TSR Turkce(SES ASTRA):11611:h:0:22000:0:1615:5116
ALICE(Persidera):11611:h:0:22000:2064:2068:5119
TWOJ(BetaDigital):11641:h:0:22000:255:256:32000
service_id 30404:11038:v:0:22000:164:96:30404
TV Record SD(Telefonica Servicios Audiovisuales):11067:v:0:22000:33:32:31301
Iberalia(Harmonic):11067:v:0:22000:37:36:31302
TELESUR(Telefonica Servicios Audiovisuales):11067:v:0:22000:2101:2102:31304
TBN Enlace(TSA):11067:v:0:22000:8002:8102:31305
TVGA(TSA):11067:v:0:22000:1701:1702:31306
I24 NEWS(TSA):11067:v:0:22000:1081:1082:31307
L EQUIPE 21(Telefonica Servicios Audiovisuales):11067:v:0:22000:311:312:31309
SEXY SAT(Telefonica Servicios Audiovisuales):11067:v:0:22000:1901:1902:31310
L EQUIPE 21(Telefonica Serviciosn Audiovisuales):11067:v:0:22000:311:312:31409
service_id 30075:11097:v:0:22000:4090:4091:30075
CANAL SUR A.(CANAL +):11156:v:0:22000:166:104:30505
FASHION TV(PRISA TV):11156:v:0:22000:165:100:30511
TV TRWAM(SES ASTRA):11185:v:0:22000:33:3606:4310
Radio Maryja:11185:v:0:22000:0:3607:4311
BVN TV(SES ASTRA):11185:v:0:22000:525:122:4320
LTC(PRISA TV):10758:v:0:22000:163:92:29853
service_id 30352:10788:v:0:22000:171:124:30352
service_id 30355:10788:v:0:22000:173:132:30355
