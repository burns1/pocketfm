Porting PFM to BeagleBoneBlack
================================

Hardware- and softwarewise there could be following main obstacles:
* I2S: The obstacle is here probably more on the hardware side. Since eletrconics differ. Probably there needs to be some modifications for adapting frequencies. See below for further information.
* I2C: Adapting pins and according changes in the scripts.
* Serial I/O: Adapting connections from the front panel to the board. Renaming TTY-device in the script. Probably enabling another TTY-device, since the one enabled is used for serial communication with the system?
* GPIO: The RPi-GPIO library needs to be changed to a BBB special one: https://learn.adafruit.com/setting-up-io-python-library-on-beaglebone-black/gpio

## GPIO: ##

Some changes in the software need to happen here, so kind of real porting.

RPi.GPIO needs to be changed to Adafruit_BBIO.GPIO. The Adafruit library differs from the RPi one.

Got as far as GPIO.setmode. GPIO.setmode is completly missing in the Adafruit library, but is not needed for further functionality, since it just sets a special pin out naming scheme.

**Naming of the GPIO pins needs to be adjusted which is currently:**

BTN\_UP  = 13

BTN\_DWN = 26

BTN_SET = 5

LED\_TX = 12
**This needs to be changed to e.g.:**

BTN\_UP = "P8_14"


Hardware wise there must be 5 pins identified for the 3 buttons + 2 LEDs on the front panel which are using GPIO. This is the same for the serial port. Easiest would be to construct an adapter bar for converting RPi pins to BBB.


**From ixds-workbench.md we get this:**

Blende

1 GND 5V 2

3 LCD B3 4

5 L4  L1 6

7 B2  L2 8

9 B1  L3 10


09 -> B1

11 -> B2

17 -> B3

27 -> L1

22 -> L2

10 -> L3

07 -> L4


In PFMstatics there is only one LED addressed, but we have 2 running
in V2.

Description from ixds-workbench could translate into:

Pins on connector:

PIN1: GND

PIN2: 5V

PIN3: LCD? some serial line?

PIN4 : Button3

PIN5: LED4?

PIN6: LED1?

PIN7: Button2

PIN8: LED2?

PIN9: Button1

PIN10: LED4?

Needs to be tested!



