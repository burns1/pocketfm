##SD-Card Optimization##

Several topics needs to be covered for sd-card optimization which is similar to all flash based storage media.<br />
For sd-cards though it seems more relevant since they have a more simple controller.

<http://lwn.net/Articles/428584/><br />
<https://thelastmaimou.wordpress.com/2013/05/04/magic-soup-ext4-with-ssd-stripes-and-strides/>

**FAT32**

<http://3gfp.com/wp/2014/07/formatting-sd-cards-for-speed-and-lifetime/>


**Partition alignment**

To make the story short: The minimal block size or erasure block size
on most SD cards is 4096MiB today. So whith a sector size of 512 Bytes we
need a sector count of at least of 8192 for optimal partition alignment with
parted.

4*1024^2/512 = 8192

Also optimal alignment check should be switched on while starting
parted.

To get the next aligned block do something like:
2861 * 8192 = 23437312s

To get the start sector for the next partition.
