Notes New designs.


# Transmitter

The new transmitter from PCS Electronics will be similar to the MaxPro6015 but
it will also contain a Si4713 FM Tuner chip. Also on the 6015 configuration
happens over RS232 Serial via the LCD module. Then this module handles
communication with main board over I2C. On the new version control will
happen over directly to the main board through I2C. We have been told the protocol
over I2C will be similar to the protocol over Serial seen on the 6015.

Also the new transmitter will have the Si4713 that the V2 currently has on the
breakout board. As with the PFM V2 this will be controllable over I2C and audio
will be streamed to it over I2S pins. For details on how I2S and I2C are
utilized with Si4713 consult notes for V2.

## PCS MaxPro Serial

Reviewed documentation for MaxPro6015 and there is no mention of I2C. PCS
Electronics have clarified that the new 3015 will use I2C only, and not the
Serial protocpl found on the 6015 LCD. The LCD of 6015 communicates with rest
of board over I2C and the new 3015 will have a similar protocol to this interface.
PCS Electronics have provided a pinmap for the 6015 showing the location of the
i2c pins. They have also provided some documentation of this protocol. See the
[docs/pcselectronics] directory.


### 6015 Serial control via LCD

For historical reference a brief overview of the Serial protocol of the 6015.

Protocol for Serial is described in Appendix 1:

* 9600 baud
* Command format:  
  Put 10ms delay between sending of each part  
  <StartByte>Command<EndCommandByte>Value<EndByte>  
    - StartByte:        0x00 signals command/parameter incoming
    - Command:          Ascii formatted
    - EndCommandByte:   0x01
    - Value:            The method varies a bit from parameter to parameter, but
                        usually you can simply send the command/parameter value
                        in ASCII form.
    - EndByte:          0x02

Example commands:

* 0x00 "PS00" 0x01 "8CHARMAX" 0x02 = Set RDS Chann name to "8CHARMAX"
* 0x00 "FO" = Set FM Transmitter Power
* 0x00 "FF" = Set FM Transmitter Frequency
* 0x00 "FDA" = Set FM Transmitter Attack
* 0x00 "FDC" = Set FM Transmitter Compression
* 0x00 "FDH" = Set FM Transmitter Threshold
* 0x00 "FW" = Store Changes

Pins found on LCD Module. Both the 5 Pin header and RS232 connection provide
the same Serial pins. The pins on the header are labeled. However this port is
5v and we do not have a cable for this yet.




# New Sat

Standard dvb-s card. We might consider researching if there is a consumer card
that has better support for ``blindscaning`` as tmbinc mentions in his advice
found in https://gitlab.com/pocketfm/pocketfm/issues/2



# Main Processor

## CompuLab SBC FX6

This board provides everything one needs to create a cell phone, plus some.

The board likely supports I2S in some form. Currently the Eval Board describes
only support S/PDIF which is not directly compatible with the Si4713. However
the block diagram for the processor module does indicate support for I2S. Upon
examination it appears the P18 / ESAI port on the eval board might expose
something compatible with I2S but these pins have multiple functions and the
configuration of the processor should be determined. For more details see
https://gitlab.com/pocketfm/pocketfm/issues/27

* Specs: http://www.compulab.co.il/products/sbcs/sbc-fx6/#specs
* Getting Started: http://www.compulab.co.il/workspace/mediawiki/index.php5/CM-FX6:_Linux:_Getting_started
* More: http://www.compulab.co.il/products/sbcs/sbc-fx6/#devres

### Connecting to Serial

The preinstalled UBoot image can be connected to via Serial. At least this is
what is inidicated in the Linux Getting Started Guide
http://www.compulab.co.il/workspace/mediawiki/index.php5/CM-FX6:_Linux:_Getting_started


> Start the terminal emulation software on your PC. Set baud rate to 115200 bps,
> 8 bit per frame, 1 stop bit, no parity, no flow control.

Connect to the P60 RJ11 Serial port. It is not clear what the voltage is on these
lines. Some inidicate 5V, others float. I used a 5V USB to Serial cable: https://www.sparkfun.com/products/9718

I connected the Orange TX of USB to Serial cable to the middle pin of 5 pins on top.
I connected the Yellow RX of USB to Serail cable to the pin to right of the Orange.
Then exec: ``screen  /dev/ttyUSB0 115200,cs8,-parenb,-cstopb,-crtscts,-cdtrdsr``

It failed but got this on output which at least indicates the pins were in right
order.

```
�6v�tT���T��
            ���{�3%�#=#7������Q#�m%=15�s!%=�3!�;!!%�9!%%=#7��u[[A[�

```




### Making SD image

http://compulab.co.il/workspace/mediawiki/index.php5/CM-FX6:_U-Boot:_Creating_a_bootable_SD_card
