The following is a summary of the work conducted by Nathan during two weeks
that spanned August and September.

# Summary

Nathan documented the current V2 PFM system from the hardware to software level.
This documentation is helpful to understanding how much work would be required
with different hardware changes being planned for V3. He created minor
to UI for Satellite input. He documented and tested Wifi integration. Documented
and tested the various data buses between Raspberry Pi and Sat Tuner.

What he could not complete in a sufficient manner and is left hanging open for
the future is: Preparing the PCS Electronics 6015 FM tuner for development
over i2c. Setup of potential Raspberry Pi replacement from compulab.


# Sat

He explored how improvements could be made to V2 to assist with Satellite dish
orientation and setup. This included added minor additions to the V2 LCD UI to
show signal details and satellite channel name. Further improvements would
require more intense investigation of the satellite hardware. He could not
reproduce the setup bug that was experienced in the past by MiCT and suspects
that if the satellite dish is installed properly the system as it is currently
should have no issues with locking on to the MiCT Sat channel.

Relevant documents:

* Documentation of blindscan support with our tuner, or lack of support:  
  https://gitlab.com/pocketfm/pocketfm/issues/2
* Documentation on how to use and test DVB-S cards on PFM or Linux workstation:  
  https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_SAT.md


# FM Radio Tuner

Documented how the Pi connects and controls the Tuner. Explored what additional
information could be gathered from the Tuner or the SiLabs FM chip. While this
information has not been integrated into the UI he has left a function in the
code that shows all the information that can be gathered and how to gather it.
Also made clear that SiLabs chip planned for new PCS Electronics board is the
same currently found on V2 Pi Breakout Shield.

* Function that shows all information that can be obtained from SiLabs si4713:  
  the status() function of the RadioControl class in PFMtools.py  
  https://gitlab.com/pocketfm/pocketfm/blob/master/v2/pfm/PFMtools.py#L144
* On how Audio gets from Pi to Tuner, including hardware pins,
  protocols and linux drivers:  
  https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_V2_AUDIO-INTERCHANGE.md
* Some discussion of PCS Electronics 6015 (dev board we have) and comparison to
  new 3015 that we will use in the future, found here:
  https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_V3.md

# Compulab board

This board was examined and has yet to be fully setup. It was left with the need
to connect the serial cable properly to gain access to the UBoot console so that
linux firmware could be installed.

Importantly some work was done documenting how I2S/Digital Audio could be connected
to this evaluation board. This still appears challenging and may require additional
work. It likely will not just work out of the box when linux is installed.

* Documentation of setup:  
  https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_V3.md
* Documentatoin of I2S pins and questions:  
  https://gitlab.com/pocketfm/pocketfm/issues/27


# Etc

Nathan documented how Wifi could be setup. Including testing both broadcast
of own Wifi from PFM device, as well as connecting to an existing wifi. This
still requires thinking how a UI can be created for configuration.

*   https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_V2_WIFI.md


Also created a module so that testing of code can be conducted locally on a
Linux workstation. This includes creating a Mock LCD, GPIO and SiLabs interface.

*  https://gitlab.com/pocketfm/pocketfm/commit/13735ae173a19b1686e3eef2abd8927257ecfb69
