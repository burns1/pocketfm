
This document describes the startup procedure of V2 PocketFM. It may or may not be helfpull. Feel free to consolodate and make more understandable.

# rc.local

Init:

    # Fix rights for /dev/dvb that are not set properly until user is logged in
    chown -R pfm:pfm /dev/dvb

    # Set up hardware clock (RTC)
    echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device

    # Start pulseaudio in background
    pulseaudio -D --disallow-exit --disallow-module-loading
    python /home/pfm/splash.py "$errors"

$errors is a state flag that splash will turn into a LCD message
that is it from splash.py

    # Check for updates
    python /home/pfm/updater.py

in updater.py:
if BTN_UP=0 will run debug.py on USB
if BTN_SET=0 will run update
updater.py get pfm.conf and parses license file
license file is an encrypted blob with key 'powerplay'
 which contains lines of: filename|sha1

    # Check if device is locked then start pfmd.py (from lock.py)
    python /home/pfm/lock.py "$errors"

if static config has locked true then get pin
lock.py also starts ``python /home/pfm/pfmd.py $errors``


# pfmd.py

    sat_quality = SATQuality()

used by monitor_sat_signal(). The object itself uses: ``femon -H -c 1`` output on device is:

```
$ femon -H -c 1
FE: Philips TDA10086 DVB-S (DVBS)
Problem retrieving frontend information: Resource temporarily unavailable
status       | signal  62% | snr  71% | ber 0 | unc -1096239540 |
```

Status might be S, SC or SCVYL... From code:
https://github.com/openpli-arm/dvb-apps/blob/master/util/femon/femon.c

```
fe_info.signal ? 'S' : ' ',   //have signal
fe_info.carrier ? 'C' : ' ',  //have carrier
fe_info.viterbi ? 'V' : ' ',  //FEC Forward Error Correction good
fe_info.sync ? 'Y' : ' ',     //Sync bytes found
fe_info.lock ? 'L' : ' ',     //lock
```

    usb_daemon = USBDaemon()

Object can query /media/usb0/ for playlist, mp3 wavs m4as

    mpl = MPlayerControl(silence_file='/home/pfm/silence.mp3', usb_playlist='/media/usb0/usb.playlist', sat_playlist='/home/pfm/sat.playlist')

Object will run mplayer for media on init. play(mode) determines
mode (USB, AUX, SAT)

    tx = RadioControl(rst_pin=RST_LINE, led_pin=LED_TX, src=tx_input, fm_freq=tx_freq, fm_rds=tx_rds_id, fm_power=tx_power)

Object init's Adafruit_Si4713 which is based on I2C
it is a subclass of this class as well

Statics:
LED_TX    = 12
RST_LINE  = 24
pfmd.conf:
tx_input  = input        = SAT
tx_freq   = freq         = 97.7
tx_rds_id = station_name = SYRNET
fm_power  = power        = 118

https://github.com/adafruit/Adafruit-Si4713-Library
https://www.silabs.com/Support%20Documents/TechnicalDocs/Si47xxEVB.pdf
https://www.adafruit.com/product/1958


Main loop:

    while True:

        # check if usb drive is connected
        if usb_daemon.is_mounted() and not is_usb_present: 		usb_attached()

Gets audio file list
Changes mplayer to play that list
Changes tx radio to switch to USB

        # check if usb drive is disconnected
        elif is_usb_present and not usb_daemon.is_mounted():	usb_removed()

Changes to mplayer and tx to previous input type

        # check for sat signal quality
        if context == 'main' and not sat_disabled:	monitor_sat_signal()

just runs sat_quality.query(['signal', 'snr'])

        time.sleep(0.5)


# DONE
