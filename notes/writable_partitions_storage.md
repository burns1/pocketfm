**Proposal (RFC) for writable storage space for pfm**

PFM needs some writeable storage for 2 purposes:
* Store configuartion data in case of poweroff/reboot
* Storage for downloadable media for later playback

*Proposed solution:*

After evaluating several possibilities like binding the storage to a special port (USB) the most flexibel solution seems to be marking the needed partitions via EXT2-labels.

Proposed values are:

* pfm\_conf
* pfm\_store

This solution is file system based and could be applied media neutral, even on the same storage medium.
For security reasons there should be some check for config files e.g. signed with a GPG key or similar.

For evaluating if a writable medium is present following node should be parsed on FS level:

/dev/disk/by-label/

If pfm\_conf and pfm\_store is present it would look like:


* /dev/disk/by-label/pfm\_conf
* /dev/disk/by-label/pfm\_store

A general function which checks for these "files" should be called before any write config operation.

TODO:

Usbmount needs to be evaluated if really 2 partitions on a medium are automatically mounted, also on boot up.

*Mount is not persistend on boot. USB devices are not mounted on boot.*

Probably a special configuration for usbmount needs to be created for symlinking the 2 partitions to a special place like it´s already done with different models/manufacturers.
Check: /etc/usbmount/mount.d/00createmodel_symlink

*Already done, and works: 01createlabel_symlink in mount.d*

Automount versus static mount at boot
-------------------------------------

There is a general problem with automount maybe due to the read only file
system. 
For some reason automount does not mounts the device or partition at
boot time (/etc/mtab is currently a link to /proc/mounts).
Tried different udev-quirks.
This would be preferable in a hotplug scenario but since the
config and storage space is hardwired to the device, -- e.g. part of an
embededded flash solution --, it could be mounted statically via fstab.
Following configuration needs to be added to /etc/fstab:


For configuration space:

> LABEL=pfm_conf	/home/pfm/pfm_conf	ext2	rw,noatime	0 0

For writeable storage space:

> LABEL=pfm_store	/home/pfm/pfm_store	ext2	rw,noatime	0 0

If externally plugged in while running, the device could be rebooted to make the
writeable configuration space work (mounted).

Filesystem should be ext4 without a journal or low frequency journal.


The magic tools are: e2label and tune2fs but mostly called with mkfs.ext4 -L "label".


For alignment partitions needs to be aligned to "erasure block size"

which is mostly 4MB.


To replicate "formatting". Without "raid stars and stripes ;)":

sudo mkfs.ext4 -O ^has_journal -L pfm_store /dev/mmcblk0p4

^has_journal disables journaling.

