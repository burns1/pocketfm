I2S Audio connection
====================

The provided requierments of the Silabs Si4712/13 needs to be met. Bitrates, Sampling Frequency etc.:
From the Silabs Si4712/13 data sheet http://www.adafruit.com/datasheets/Si4712-13-B30.pdf


5.4.  Digital Audio Interface
The digital audio interface operates in slave mode and
supports 3 different audio data formats:
1.  I2S
2.  Left-Justified
3.  DSP Mode

5.4.1. Audio Data Formats
In I2S mode, the MSB is captured on the second rising edge of DCLK following each DFS transition. The remaining bits of the word are sent in order, down to the LSB. The Left Channel is transferred first when the DFS is low, and the Right Channel is transferred when the DFS is high.
In Left-Justified mode, the MSB is captured on the first rising edge of DCLK following each DFS transition. The remaining bits of the word are sent in order, down to the LSB. The Left Channel is transferred first when the DFS is high, and the Right Channel is transferred when the DFS is low.
In DSP mode, the DFS becomes a pulse with a width of 1 DCLK period. The Left Channel is transferred first, followed right away by the Right Channel. There are two options in transferring the digital audio data in DSP mode: the MSB of the left channel can be transferred on the first rising edge of DCLK following the DFS pulse or on the second rising edge. 
In all audio formats, depending on the word size, DCLK frequency and sample rates, there may be unused DCLK cycles after the LSB of each word before the next DFS transition and MSB of the next word.
The number of audio bits can be configured for 8, 16, 20, or 24 bits.

**The BBB I2S-Port supports 24bit/192kHz (128fs clock):**

http://www.element14.com/community/community/designcenter/single-board-computers/next-gen_beaglebone//blog/2013/05/28/bbb--audio-notes

http://www.element14.com/community/community/designcenter/single-board-computers/next-gen_beaglebone/blog/2013/07/06/bbb--building-a-dac

Same is true for the BananaPro:
http://www.lemaker.org/thread-11807-2-1.html

Compatibilty SI4713 <-> BBB and SI4713 <-> BananaPro needs to be checked in this regard.
