Bandwith Testing available USB hub on RaspberryPi
-------------------------------------------------

### Structure USB connections ###
root@pfm:~# lsusb -t  
/:  Bus 01.Port 1: Dev 1, Class=root\_hub, Driver=dwc\_otg/1p, 480M  
----|\_\_ Port 1: Dev 2, If 0, Class=hub, Driver=hub/5p, 480M  
----|\_\_ Port 1: Dev 3, If 0, Class=vend., Driver=smsc95xx, 480M  
-------|\_\_ Port 2: Dev 8, If 0, Class=vend., Driver=dvb\_usb  


### Testing results ###
mplayer -ao alsa -srate 48000 -vo null sndin.fifo  
arecord -f cd --device='default:CARD=Audio' --vumeter='stereo'
sndin.fifo  


[  4] local 192.168.10.240 port 5001 connected with 192.168.10.109
port 51294  
[ ID] Interval       Transfer     Bandwidth  
[  4]  0.0-10.1 sec  5.25 MBytes  4.38 Mbits/sec  
[  5] local 192.168.10.240 port 5001 connected with 192.168.10.109
port 51297  
[  5]  0.0-10.1 sec  4.62 MBytes  3.83 Mbits/sec  
[  4] local 192.168.10.240 port 5001 connected with 192.168.10.109
port 51300  
[  4]  0.0-10.3 sec  3.88 MBytes  3.16 Mbits/sec  


**with stream:**  
[  5]  0.0-10.4 sec  6.50 MBytes  5.26 Mbits/sec  
[  5]  0.0-10.2 sec  6.88 MBytes  5.64 Mbits/sec  

With USB-Audio-In as replacement for the sat receiver.

Also tested with syrnet as mp3 stream over network connected via the wireless dongle.
