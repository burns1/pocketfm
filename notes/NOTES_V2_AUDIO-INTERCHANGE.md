See NOTES_V2_COMPONENTS.md for current hardware and system diagram.

Example scenarios:

                   Stage 1         Stage 2            Stage 3

                   Pi Linux        Pi Linux           Breaout
    SAT  --usb->   standard   ->   I2S       --I2S->  Si4713   --RF->
                   dvb-s           drivers            Tuner

                                                      Breaout
    AUX  - - - - - - - - - AUX_IN - - - - - - - - ->  Si4713   --RF->
                                                      Tuner

                   Pi Linux        Pi Linux           Breaout
    USB  --usb->   standard   ->   I2S       --I2S->  Si4713   --RF->
                   filesystm       drivers            Tuner




# Stage 1.

pfmd.py executes Mplayer to play USB or SAT radio.


# Stage 2.

USB file access is standard.

I2S is supported through Raspberry Pi Specific drivers. These drivers provide
Linux support for I2S audio on certain GPIO pins. These can then be connected
to the I2S capable Si4713 and used through standard linux sound stacks.

For a history of I2S issues with PocketFM perhaps review Hans Forum threads:

* https://www.raspberrypi.org/forums/viewtopic.php?f=29&t=87118&p=613950
* https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=86645&p=611167


### Digital Audio pins

I2S DLCK, DFS and DIN pins mapped using multimeter.

                                PI Pin

                                11  12 GPIO18 <-> Si4713_pin17_DLCK
                                 ....
    Si4713_pin14_DFS <-> GPIO19 35  36
                                37  38
                         GND    39  40 GPIO21 <-> Si4713_pin13_DIN

NOTE: Based on Stefan's document appears I missed a pin. He also provides this
pin description:  
https://gitlab.com/pocketfm/pocketfm/issues/6

    gpio | # | desrc
    ----------------
      18 | 12 | bitlock
      19 | 35 | lrclock
      20 | 38 | data in
      21 | 40 | data out

### snd_soc_bcm2708_i2s linux module

The I2S pins are defined in the linux driver ``snd_soc_bcm2708_i2s``
(bcm2708 is the Pi's processor):  
https://github.com/raspberrypi/linux/blob/rpi-4.1.y/sound/soc/bcm/bcm2708-i2s.c

    /* ALSA SoC I2S Audio Layer for Broadcom BCM2708 SoC */
    static void bcm2708_i2s_setup_gpio(void) {
        ...
        /* SPI is on different GPIOs on different boards */
        /* for Raspberry Pi B+, this is pin GPIO18-21, for original on 28-31 */


(NOTE: I think the comment might be off about SPI? Regardless, Pins are defined
in this driver)

This, I suppose, makes ALSA i2s play nicely over the GPIO's. This driver may also
make I2S support depend on ALSA (pulse audio) stack over other stacks?


### Other System Files

#### /etc/modules

The following modules can be found configured on the system.

    #enable i2c
    i2c-bcm2708
    i2c-dev
    #enable general b+ i2s output
    snd_soc_core
    snd_soc_bcm2708_i2s
    bcm2708_dmaengine
    # working soundcard
    snd_soc_pcm1794a
    snd_soc_rpi_dac
    # real time clock
    rtc-ds1307



# Stage 3.

pfmd.py uses Adafruit_I2C.py and Adafruit_Si4713.py to Tune FM and control the
Si4713 chip. Pfmd.py will create a ``RadioControl`` object that uses the I2C
and Si4713 python modules to control the input type (analog AUX or digital i2s).
For digital/i2s types standard Mplayer is then used to send audio to the chip and
out the RF.


### RadioControl

Initialized in pfmd.py:

    tx = RadioControl(rst_pin=RST_LINE, led_pin=LED_TX, src=tx_input, fm_freq=tx_freq, fm_rds=tx_rds_id, fm_power=tx_power)
    # PFMstatics.py defines:     25               12
    # pfmd.conf defines:                                      "SAT"             97.7           "SYRNET"            118

When initialized this will set the frequency, power and rds_id over I2C. The
tx_input string maps to the ``src_tx_pairs`` list from PFMstatic that defines
if an input will be digital or analog. Based on this value RadioControl will
tell Si4713 over i2c to switch to analog input when "AUX" is set.

Source type to Si4713 input type in PFMstatics.py:

    # input sources and their specific tx modes
    src_tx_pairs = collections.OrderedDict([
        ("AUX", 'analog'),
        ("SAT", 'digital'),
        ("USB", 'digital')
    ])

On initialization or change (which causes a reinitializatoin) this mode is
passed to the Adafruit_Si4713 begin() function which passes it to the powerUp()
which will send a different I2C command based on its value:

    def powerUp(self, mode='analog'):
        if mode is 'digital':
            self.sendCommand(self.SI4710_CMD_POWER_UP, [0xC2, 0x0F])
        else:
            self.sendCommand(self.SI4710_CMD_POWER_UP, [0xC2, 0x50])
