This describes how to configure the device for development (enable ssh,etc) and
how to use a DVB-S tuner card locally.

# On PocketFM V2

Setup SSH:

* Remove SD card and mount on a linux host.
  - Partition 1 is the FAT which contains the Pi's config.txt, which we will not
    need.
  - Partition 2 is EXT which contains the primary linux system and our code.
* Add your SSH key to /root/.ssh/authorized_keys and /home/pfm/.ssh/authorized_keys.  
  If these directories or files do not exist be sure the dir .ssh has perms 700
  and the authorize_keys file 600.

Setup network (showing DHCP config):

* Modify /etc/network/interfaces and uncomment the lines for eth0 and eth1
* Unmount the SD card and place it back into PocketFM
* Run a DHCP server on your local linux system to give PocketFM an IP on boot:

    ETH=eth0
    sudo ufw disable

    # kill dhclient on if $ETH
    PIDS=$(ps -aux | grep dhclient | grep $ETH | awk "{print \$2}" | xargs)
    test "$PIDS" && sudo kill $PIDS

    sudo ifconfig $1 192.168.1.1 netmask 255.255.255.0

    mkdir /tmp/$ETH
    DIR=/tmp/$ETH

    cat >$DIR/dnsmasq.conf <<EOF
    dhcp-option=option:router,192.168.1.1
    dhcp-range=192.168.1.10,192.168.1.254,255.255.255.0,96h
    EOF

    sudo dnsmasq -d -i $ETH \
      --conf-file="$DIR/dnsmasq.conf" \
      --dhcp-leasefile="$DIR/leases" \
      --pid-file="$DIR/pid"

    sudo ufw enable

* dnsmasq will print to the console the IP assigned to PocketFM when it is given one

Once on the device you can execute ``/home/pfm/rw`` to change the main parititon
to r/w so that you can modify files during runtime.

# On Linux with DVB-S Card

The DVB-S card we used was Technotrend TT-connect S-2400. This requires a driver
``dvb-usb-tt-s2400-01.fw`` (https://github.com/OpenELEC/dvb-firmware/blob/master/firmware/dvb-usb-tt-s2400-01.fw)
placed in ``/lib/firmware``. After this you can connect the USB cable and utilize
dvb tools.

Refer to the ``NOTES_SAT.md`` for notes on scanning and using these tools.

It might also possible to execute much of the PFM code or at least use it from the
python REPL.
