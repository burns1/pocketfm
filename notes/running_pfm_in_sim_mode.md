# howto run pfm in simulation mode on different systems #


Apple (Mac OS X):
-----------------


Tested with 10.10.5


PySerial needs to be installed first:

> sudo easy_install pyserial


Since the scripts are running with "/usr/bin/python2" a link needs to be created in /usr/bin/ to link to the python wrapper script:

> cd /usr/bin/
> 
> sudo ln -s python2 python


In the scripts home dir the directory pfm_conf needs to be present and a dummy file like label_dummy.
In the pfm_conf dir a symbolic link with the name: label_dummy needs to be created to the dummy file.
Finally a configuration file called pfm.conf needs to be present in pfm_conf. Just copy the original one from the working directory.


Run pfm with:

> ./pfmd.py


**FIXME: Simulation does not run with sat as input. Loop just quits. **


Not tested on Windows
---------------------



On linux:
---------


PySerial needs to be installed.
