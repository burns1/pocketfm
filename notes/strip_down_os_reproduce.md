Steps to reproduce building the stripped down ro-os.
====================================================

The system is now based on: minbian: https://minibianpi.wordpress.com/

Current package list is as provided in sys/debian: package-selections-minbian_231215.txt

## Reproduce selected packages: ##

apt-get install dctrl-tools
sync-available
dpkg --clear-selections
dpkg --set-selections < package-selections-minbian_231215.txt
apt-get dselect-upgrade

