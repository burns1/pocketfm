Schedule handling via cron
==========================

The raspbian cron deamon is installed and started at boot.
Current package version is: 3.0pl1-124


User crontab configuration according to:
https://www.raspberrypi.org/documentation/linux/usage/cron.md


m h  dom mon dow   command

> \* * * * *  command to execute
> 
> ┬ ┬ ┬ ┬ ┬  
> │ │ │ │ │  
> │ │ │ │ │  
> │ │ │ │ └───── day of week (0 - 7) (0 to 6 are Sunday to Saturday, or use names; 7 is Sunday, the same as 0)  
> │ │ │ └────────── month (1 - 12)  
> │ │ └─────────────── day of month (1 - 31)  
> │ └──────────────────── hour (0 - 23)  
> └───────────────────────── min (0 - 59)


This is the user crontab editable via: crontab -e. The system crontab looks probably different.


Replacing the default crond with anacron, bcron or fcron could be an option:

http://anacron.sourceforge.net/

http://fcron.free.fr/

