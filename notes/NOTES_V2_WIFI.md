Notes on getting Wifi up and supporting

Tested with wifi dongle which worked out of the box (no special drivers required)

# Client

## Setup

https://www.maketecheasier.com/setup-wifi-on-raspberry-pi/
http://raspberrypihq.com/how-to-add-wifi-to-the-raspberry-pi/
https://raspberrypi.stackexchange.com/questions/9257/whats-the-difference-between-wpa-roam-and-wpa-conf-in-the-etc-network-inte
wpa-roam means when wireless connects eth goes off and wlan is main
with conf both stay on. but actually would still like wlan to take def route
must test

    $ sudo apt-get install wpa-supplicant wireless-tools

might not need wireless-tools but we could not find another way to scan ssids (iwlist)


    $ cat >> /etc/network/interfaces <<EOM
    allow-hotplug wlan0
    iface wlan inet dhcp
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
    iface default inet dhcp
    EOM

Not sure if we need the iface default or not

    killall wpa_supplicant
    wpa_passphrase "mict" "mict-international" > /etc/wpa_supplicant/wpa_supplicant.conf
    wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
    iwconfig wlan0
    dhclient -1 -v -cf /etc/dhcp/dhclient.conf  -pf /run/dhclient.wlan0.pid -lf /var/lib/dhcp/dhclient.wlan0.leases wlan0

## Next Steps

* Tested with WPA but should check with a WPA2 and WEP network.
* Write script to get and sort list of available networks
* Write script to configure for a selected network. For now only need to save last
saved network, not list of all historical networks.
* Integrate with UI somehow




# Own Wifi network

Broadcasting our own network

## Setup

https://learn.adafruit.com/setting-up-a-raspberry-pi-as-a-wifi-access-point?view=all

Most tutorials recommend having the hostap daemon. But we would like to avoid
daemons and will go the adhoc route. If stability issues arrise consider switching
to hostap. But by avoiding it for now it lets us more clearly manage the whole
setup from easy scripts, which is considered more friendly on future developers
(everything in one place).

https://wiki.archlinux.org/index.php/Ad-hoc_networking

    $ cat > /etc/wpa_supplicant/wpa_supplicant-adhoc.conf <<EOF
    ctrl_interface=DIR=/run/wpa_supplicant GROUP=daemon
    # use 'ap_scan=2' on all devices connected to the network
    ap_scan=2
    network={
        ssid="MySSID"
        mode=1
        frequency=2432
        proto=WPA
        key_mgmt=WPA-NONE
        pairwise=NONE
        group=TKIP
        psk="secret passphrase"
    }
    EOF

    wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant-adhoc.conf -D nl80211,wext

    ifconfig wlan0 192.168.44.1 netmask 255.255.255.0

Connect to the MySSID network and supply yourself with an IP in the same .44
network and then SSH to the device. Works

To add DHCP:

    apt-get install dnsmasq
    killall dnsmasq
    sed -i 's/ENABLED=1/ENABLED=0/' /etc/default/dnsmasq
    # we will start it on our own, as part of the all in one place principle

    cp /etc/dnsmasq.conf /etc/dnsmasq.conf.dist

    cat > /etc/dnsmasq.conf <<EOF
        dhcp-option=option:router,192.168.44.1
        dhcp-range=192.168.44.10,192.168.44.254,255.255.255.0,96h
    EOF

    dnsmasq -d -i wlan0 \
      --conf-file="/etc/dnsmasq.conf" \
      --dhcp-leasefile="/var/lib/misc/dnsmasq.leases" \
      --pid-file="/var/run/dnsmasq/dnsmasq.pid"

At this point we may want to setup a firewall (ufw is a friendly iptables wraper)

    # make sure we dont loose network during this setup
    nohup $(while [ 1 ]; do ufw allow 22/tcp; sleep 300; done) &
    apt-get install ufw
    sed -i 's/IPV6=yes/IPV6=no/' /etc/default/ufw

    # Allow ssh
    ufw allow 22/tcp
    # allow dnsmasq and dhcp server
    ufw allow 53
    ufw allow in from any port 68 to any port 67 proto udp

    ufw status verbose



If you want the user to be able to access the internet through the RPI
(when it is connected to internet itself via eth1) then add the following.
Note, this should not be needed as this is not the purpose of this wifi on
the device isnt about providing internet. It is highly unlikely this would
ever be desired. In any case, to enable:

    echo 1 > /proc/sys/net/ipv4/ip_forward


## Next Steps

* Write script to start this network on UI request, or on boot if configured for that.
This only requires running the wpa-supplicant daemon and dnsmasq
* Get network password from file that can be randomly and uniquely defined at build time
  OR create determinative password that is based off of MAC or other details as seed.
* Decide if we want user to be able to change wifi password.
* Decide if we wish to broadcast secret SSID as backdoor recovery?
