Description of components. Important details.

# Raspberry Pi

Model B+.

Pins:

               3v3      1   2   5v
         SDA1  GPIO2    3   4   5v
         SCL1  GPIO3    5   6   GND
    GPIO_GCLK  GPIO4    7   8   GPIO14  TXD0
               GND      9  10   GPIO15  RXD0
    GPIO_GEN0  GPIO17  11  12   GPIO18  GPIO_GEN1
    GPIO_GEN2  GPIO27  13  14   GND
    GPIO_GEN3  GPIO22  15  16   GPIO23  GPIO_GEN4
               3v3     17  18   GPIO24  GPIO_GEN5
     SPI_MOSI  GPIO10  19  20   GND
     SPI_MISO  GPIO9   21  22   GPIO25  GPIO_GEN6
     SPI_SCLK  GPIO11  23  24   GPIO8   SPI_CE0_N
               GND     25  26   GPIO7   SPI_CE1_N
               ID_SD   27  28   ID_SC
               GPIO5   29  30   GND
               GPIO6   31  32   GPIO12
               GPIO13  33  34   GND
               GPIO19  35  36   GPIO16
               GPIO26  37  38   GPIO20
               GND     39  40   GPIO21

i2c: SDA1 (data signal), SCL1 (clock signal)



# Raspberry Pi Shield

custom board for Pi B+.

    .-------------. .----------. .--------.
    | DVB-S Tuner | | USB File | | AUX In |--.                       ((.))
    '-------------' '----------' '--------'  |                         |
           ^              |                  |                        /_\  
           |              |       .----------|-----------.           /___\
           '-----USB------'       | Breakout |           |          /     \
                  |               | Board    |           |             ^
                  |               |          v           |             |
                  v               |   .-------------.    |      .-------------.
          .---------------.       |   |   Si4713    |    |      |  PCS Elec   |
          | Raspberry Pi  |----i2s--->|   Tune FM   |-----RF--->| AMP+Antenna |
          '---------------'       |   '-------------'    |      '-------------'
                  |               |          ^           |
                  '------>i2c----------------'           |
                           |      |                      |
                           |      |  .------------.      |
                           |-------->| DS1307 RTC |      |
                           |      |  '------------'      |
                           |      |  .-----------------. |
                           '-------->| INA219 PowerMon | |
                                  |  '-----------------' |
                                  '----------------------'

* HW: Board contains...
    - SiLabs Si4713 - I2C and I2S
    - TI INA219 power monitor - I2C
    - DS1307 realtime clock with 32khz crystal - I2C
      https://www.adafruit.com/products/264
    - ATTINY45 micro-controller  
      purpose unclear, perhaps unimportant at the moment.
* SW: various locations effect hardware...
    - Si4713 control:  
      Adafruit_I2C.py and Adafruit_Si4713.py
    - DS1307 RTC control:  
      Driver: rtc-ds1307 (/etc_org/modules)  
      Driver support i2c: i2c-bcm2708 i2c-dev (/etc_org/modules)  
      Set HW clock is done over i2c with:  
        echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device


Pi Pins to Components on breakout board (mapped with multimeter):

                                 Raspberry PI

                                     1   2
    Si4713_pin7_SDIO    <->  SDA1    3   4
    & INA219_pin3_SDA
    Si4713_pin8_SLCK    <->  SCL1    5   6
    & INA219_pin4_SCL
                                     7   8   TXD0     <->   LCD Breakout
                             GND     9  10   RXD0     nc
                                    11  12   GPIO18   <->   Si4713_pin17_DLCK
                                    13  14   GND
                                    15  16
                             3v3    17  18
                                    19  20   GND
                                    21  22
                                    23  24
                             GND    25  26
                                    27  28
                                    29  30   GND
                                    31  32
                                    33  34   GND
    Si4713_pin14_DFS    <->  GPIO19 35  36
                                    37  38
                             GND    39  40   GPIO21   <->   Si4713_pin13_DIN


    Si4713_pin16_LIN <-> Audio Jack
    Si4713_pin15_RIN <-> Audio Jack


I2C communication requires Pi pins: SDA1 and SCL1  
I2S communication requires Si473 pins: DFS, DIN, DLCK  
these pins are connected to Pi GPIO pins: 18, 19, 21


NOTE: i2C connected to both the Si4713 and INA219  
DONE: Mapped SiLabs, INA219, LCD.  
TODO: Map LED's and RTC (likely i2c)



# LCD

* HW: Off the shelf 30EU. Still in stock.
      https://www.sparkfun.com/products/9568
      https://www.sparkfun.com/datasheets/LCD/SerLCD_V2_5.PDF
      https://www.sparkfun.com/datasheets/LCD/HD44780.pdf (char/symbol table)  
      There is a button module developed by IXDS that handles combining the buttons,
      LEDs, and LCD uart into one cable for the Pi.

* SW: Using serlcd.py library via ttyAMA0. Library indicates protocol supports
      special commands such as center, blink, etc. based on
      https://github.com/scogswell/ArduinoSerLCD/blob/master/SerLCD.h



# Sat Tuner

* HW: Off the shelf 40eu.  Technotrend TT-connect S-2400.
  http://www.technotrend.eu/2713/TT-connect__S-2400.html
* SW: Standard linux /dev/dvb
  with driver dvb-usb-tt-s2400-01.fw copied to /lib/firmware/
  https://github.com/OpenELEC/dvb-firmware/blob/master/firmware/dvb-usb-tt-s2400-01.fw

Controlled via mplayer. See NOTES_SAT.md for more examples

    mplayer -ao pulse -srate 48000 -really-quiet -vo null -nolirc -framedrop \
      -loop 0 -playlist /home/pfm/sat.playlist
    $ cat /home/pfm/sat.playlist
    dvb://MICT
    $ cat /home/pfm/channels.conf
    MICT:11033:v:0:27500:0:7044:7015

Status and signal quality checked with femon command. See NOTES_SAT.md


# TX Radio Transmitter

It is now clear the PCS Electronics boards only provide filtering and amp. All
tuning and clocks, etc are handled by custom breakout board. Hence, there
isn't much to document here.

The TX board only gets power from breakout board.  
And an RF single from the Si4713 chip on the breakout board.
