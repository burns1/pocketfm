#!/usr/bin/python

"""
Display all visible SSIDs
Extended to get also BSSIDs
"""

import NetworkManager

for dev in NetworkManager.NetworkManager.GetDevices():
    if dev.DeviceType != NetworkManager.NM_DEVICE_TYPE_WIFI:
        continue
    for ap in dev.SpecificDevice().GetAccessPoints():
        print '%-30s %dMHz %d%% %s' % (ap.Ssid, ap.Frequency, ap.Strength, ap.HwAddress)

