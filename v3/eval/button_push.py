#!/usr/bin/python


import serial
import time
import sys
import datetime
import string
import os
import re
import ConfigParser

__master_file = "/home/pfm/pfm_master.ini"
__working_file = "/home/pfm/pfm_conf/pfm_working_test.ini"

__master_config = ConfigParser.RawConfigParser()
__master_config.optionxform = str
__working_config = ConfigParser.RawConfigParser()
__working_config.optionxform = str

def configuration_update(data, signal):
    __working_config.set("SIGNALS", signal, str(data))
    updater = time.strftime("%Y-%m-%d-%H:%M:%S") + " by firmware reset"
    __working_config.set("SIGNALS", signal + " LAST UPDATE", updater)
    #Disable buffering explicitly. Added ,0 while openening the
    #file handler for the working configuration file
    with open(__working_file, 'wb', 0) as tmp:
        __working_config.write(tmp)
        tmp.close()

def init_conf():
    if os.path.isfile(__master_file):
        print("reading master conf")
        __master_config.read(__master_file)
    else:
        print("could not read master conf")
        
    if os.path.isfile(__working_file):
        __working_config.read(__working_file)
    else:
        if os.path.isfile(__master_file):
            with open(__working_file, 'wb') as tmp:
                __master_config.write(tmp)
                tmp.close()
            __working_config.read(__working_file)
        else:
            __working_config.add_section("SIGNALS")

def read_masterconf(signal):
    val = ""
    if __master_config.has_option('SIGNALS', signal):
        val = __master_config.get('SIGNALS', signal)
    else:
        print("no value: "+str(signal))
    return val


def parse_angle(str_retval):
    enc_seq = str_retval.split('\\x1b')
    enc_angles = []
    enc_pushes = []
    i = 0
    rl_set = 0
    push_set = 0
    if enc_seq[0] == "\'":
        del enc_seq[0]
    if len(enc_seq) > 0:
        for i,enc_pos in enumerate(enc_seq):
            if enc_pos != "\'":
                #print("Encoder Positions: "+str(enc_seq))
                enc_angle = enc_pos.split(",")
                print("Encoder angle: "+str(enc_angle[0])+" "+str(i))
                enc_push_val = enc_angle[1].split("E")
                print("Encoder push: "+str(hex(int(enc_push_val[0])))+" "+str(i))
                #logging.info("Encoder angle: "+str(enc_angle[0])+" "+str(i))
                enc_angles.append(enc_angle[0])
                enc_pushes.append(enc_push_val[0])
        if len(enc_angles) > 1:
            #flip back on 0
            if int(enc_angles[0]) == 0 and int(enc_angles[1]) == 255:
                rl_set = -1
            elif int(enc_angles[1]) < int(enc_angles[0]):
                #print("Encoder turn left success!")
                rl_set = -1
            elif int(enc_angles[1]) > int(enc_angles[0]):
                #print("Encoder turned right?")
                rl_set = 1
                
        #check for push
        if len(enc_pushes) > 1:
            #we have at least a push and release
            push1 = int(enc_pushes[0]) & 0xc0
            push2 = int(enc_pushes[1]) & 0xc0
            print("push values: "+str(push1)+"/"+str(push2))
            
            if (push1 != 0) or (push2 != 0):
                print("got push and release")
                push_set = 2
            elif (push1 == 0) and (push2 == 0):
                print("releases only?")
                push_set = 0
            #check for release after push
            if len(enc_pushes) > 3:
                push2 = int(enc_pushes[2]) & 0xc0
                push3 = int(enc_pushes[3]) & 0xc0
                print("second push values: "+str(push2)+"/"+str(push3))
                #hex repr for release is always 0x30
                #is dec 48
                #if int(enc_pushes[3]) == 48:
                if push2 == 192 and push3 == 0: 
                    push_set = 3
        elif len(enc_pushes) == 1:
            #push only
            push1 = int(enc_pushes[0]) & 0xc0
            print("push value: "+str(push1))
            if (push1 != 0):
                print("encoder push only set")
                push_set = 1
            if (push1 == 0):
                print("got release only")
                push_set = 0
        else:
            print("not enough push values")

    return push_set


port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)

port.write("\x1b\x3fE")
time.sleep(1)

timeout_wait = 3
first_wait = 2
angle_check = 0
push_check2 = 0

#port.write("\r\nPush for reset")

for waitfor in range(0,first_wait):
    try:
        rcv = port.read(128)
    except serial.SerialException as e:
        rcv = None
        print("\nSerial Data Error 0.1")
        port.close()
        sys.exit(-1)
    except TypeError as e:
        rcv = None
        print("\nSerial Data Error 0.2")
        port.close()
        sys.exit(-1)

    print("Encoder Value: "+repr(rcv))
    if repr(rcv) != "":
        break
    time.sleep(1)

if repr(rcv) == "" or waitfor >= first_wait:
    port.write("\r\nTime out Enc: "+repr(rcv))
    print("Encoder got no data: "+str(waitfor))
else:
    print("Got data on encoder: "+repr(rcv))
    enc_retvals = str(repr(rcv))
    if len(enc_retvals) > 2:
        print("char count: "+str(len(enc_retvals)))
        angle_check = parse_angle(enc_retvals)
    else:
        print("emtpy values")

if angle_check >= 1:
    port.write("\r\nButton pushed")
    print("Got Push, need confirmation")

    port.flush()

    port.close()
    time.sleep(1)

    port.open()
    port.write("\x1b\x3fE")
    time.sleep(1)

    port.write("\r\nPush again")
    port.write("\r\nfor reset!")
    waitfor = 0

    for waitfor in range(0,timeout_wait+1):
        try:
            rcv = port.read(128)
        except serial.SerialException as e:
            rcv = None
            print("\nSerial Data Error 0.1")
            port.close()
            sys.exit(-1)
        except TypeError as e:
            rcv = None
            print("\nSerial Data Error 0.2")
            port.close()
            sys.exit(-1)

        print("Encoder Value: "+repr(rcv))
        if repr(rcv) != "":
            break
        time.sleep(1)

    if repr(rcv) == "" or waitfor >= timeout_wait:
        port.write("\r\nTime out Enc: "+repr(rcv))
        print("Encoder got no data: "+str(waitfor))
    else:
        print("Got data on encoder: "+repr(rcv))
        enc_retvals = str(repr(rcv))
        if len(enc_retvals) > 2:
            push_check2 = parse_angle(enc_retvals)
        else:
            print("emtpy values")

    if push_check2 == 3:
        port.write("\r\nButton pushed again")
        port.write("\r\nand released")
        port.write("\r\nSystem reseting!")
        print("System reseting!")

        #read/write config
        if os.path.isfile(__working_file):
            os.remove(__working_file)
        init_conf()
        pfm_sup_pin = read_masterconf("PFM Support PIN")
        configuration_update(pfm_sup_pin, "Panel PIN")
    else:
        port.write("\r\nNo confirmation")
        print("No reset!")
else:
    port.write("\r\nNo push for reset")

print("\nCheck reset finished")

port.flush()
port.close()

sys.exit(0)

