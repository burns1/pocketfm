Bill of Material for Connect.Bom

Used Part Type                                        Designator Footprint       Description
==== ================================================ ========== =============== ================================================
1    0ZCJ0050AF2E                                     J305       1210
2    1,55SMC6,8A                                      D201 D302  SMC             Diode Shottky
2    1A/20V MBRS120T3                                 D200 D303  SMB             Diode Shottky
5    1M                                               R115 R116  0805
                                                      R325 R326
                                                      R327
2    1k2                                              R134 R135  0805
1    1k                                               R109       0805
7    1n                                               C337 C338  0805            Capacitor
                                                      C339 C340
                                                      C341 C342
                                                      C343
9    1uF 1�/25V=GRM31MR71E105KA01L                    C105 C113  1206            Capacitor
                                                      C115 C117
                                                      C119 C221
                                                      C222 C223
                                                      C329
1    2k2                                              R121       0805
1    2n2                                              C230       0805            Capacitor
1    3A/40V MBRS340T3                                 D104       SMC             Diode Shottky
1    3k                                               R217       0805
1    4.7uH/SER1360-472KLD                             L203       SER1360         Coil-Shield
1    4x22k                                            R338       RPACK2X3        4xResistor
3    4x100R                                           R123 R124  RPACK2X3        4xResistor
                                                      R339
4    4x100k                                           R120 R222  RPACK2X3        4xResistor
                                                      R223 R328
1    4x220k                                           R344       RPACK2X3        4xResistor
1    5mR/1W                                           R119       2512
1    6.3AT/3101.0050                                  F100       3101.0050
1    10A,14mR,1.8mH/42H42A000                         L100       RSD42H42        Coil 3x2pin
2    10R                                              R117 R118  0805
9    10k                                              R101 R102  0805
                                                      R103 R104
                                                      R107 R108
                                                      R213 R323
                                                      R324
1    10n                                              C336       0805            Capacitor
2    10uF/16V                                         C200 C201  1206            Capacitor
3    10uH/1A/SRR6038-100Y                             L200 L201  L4_6.8X6.8SMD   Coil
                                                      L302
1    12MHz                                            Q301       QUARZSMD-GND    Quarz
2    18k                                              R218 R219  0805
2    18p                                              C334 C335  0805            Capacitor
1    20k                                              R201       0805
9    22k                                              R105 R106  0805
                                                      R110 R111
                                                      R112 R113
                                                      R130 R131
                                                      R216
11   22uF/10V GRM31CR71A226ME15L                      C126 C127  1206            Capacitor
                                                      C128 C226
                                                      C227 C228
                                                      C229 C330
                                                      C331 C332
                                                      C333
6    22uF/25V  22�F/25V=GRM32ER71E226KE15L            C202 C203  1210            Capacitor
                                                      C206 C217
                                                      C218 C328
1    32.768kHz                                        Q100       MC-146          Quarz
10   47k                                              R114 R122  0805
                                                      R133 R330
                                                      R331 R332
                                                      R333 R334
                                                      R335 R336
2    47n                                              C224 C225  0805            Capacitor
1    74HC08D                                          U105       SO14            4 x AND
1    75k                                              R221       0805
3    100R                                             R173 R174  0805
                                                      R175
2    100k                                             R128 R329  0805
24   100n                                             C100 C101  0805            Capacitor
                                                      C102 C103
                                                      C104 C106
                                                      C107 C108
                                                      C109 C110
                                                      C111 C120
                                                      C121 C122
                                                      C123 C204
                                                      C216 C323
                                                      C324 C325
                                                      C326 C327
                                                      C344 C345
3    100p                                             C205 C219  0805            Capacitor
                                                      C220
1    130k                                             R345       0805
4    180k                                             R340 R341  0805
                                                      R342 R343
3    220k                                             R100 R200  0805
                                                      R212
2    220uF/35V                                        C112 C118  C10RM5
2    470p                                             C124 C125  0805            Capacitor
2    680k                                             R220 R337  0805
1    1206L200SLYR                                     J107       1210
1    1010027182/SIM-SLOT                              J102       1010027182      Mini-SIM Slot Push Push 1010027182
1    686 116 144 22 WR-FPC SMT ZIF Horiz.             J304       68611614422     ZIF FPC 16x1mm
2    742792096                                        L103 L303  L0805           Coil
1    744233121                                        L205       744233121       Dual Coil
1    ATMEGA324P-20AU                                  U315       TQFP44A         Atmel AVR Microcontroller
1    BAR43C                                           D106       SOT23           Doppeldiode
4    BAT54S                                           D107 D108  SOT23           Doppeldiode in Reihe SOT23
                                                      D109 D110
2    BC846                                            T103 T104  SOT23           NPN Transistor SMD
3    BSS123                                           T102 T201  SOT23           N-Channel FET 100V 0.17A 1.2R 360mW
                                                      T302
4    BSS138                                           T105 T106  SOT23           N-Channel FET 100V 0.17A 1.2R 360mW
                                                      T107 T117
2    CON2                                             J104 J200  CON2_RM5.08     2 steckbare Klemme
1    CON2                                             J103       CON2_RM5.08V    2 steckbare Klemme
1    CON2X3                                           J111       CON2X3
1    CR2032_122-2420-GR                               B100       122-2420-GR     Lithium 3V
1    DMP3056L-7                                       T303       SOT23           P-Channel FET 30V3A 0.1R 3W
1    FH12-6S-1SH(55) 6x1mm                            J100       FH12-6S-1SH     ZIF Connector 6 Pin 0.5mm
1    GY-DPS6MV2                                       U314       GY-DPS6MV2      GPS-Modul NEO-6M-0-001
1    INA219AIDR                                       U102       SO8             I2C Current and Voltage ADC
7    IP4256CZ5-W                                      U100 U101  SOT665          2 CH EMI Filter with 100R
                                                      U309 U310
                                                      U311 U312
                                                      U313
2    IPD50P04P4L-11                                   T100 T101  TO252AA         P-Channel FET 40V 50A 0.01R 55W
1    LP2985-33DBVR                                    U317       SOT23-5         Voltage Regulator 16V 0.25A
1    MCP6002-E/SN                                     U103       SO8             Dual OP-AMP SMD
1    MK2302S-01LFTR                                   U106       SO8             Clock Multiplier 0.5,1,2,4,8,16 Out 10MHz-168MHz
4    MM3Z5V6T1G                                       D100 D101  D1206           Diode Z
                                                      D102 D103
1    Mini-USB                                         J207       USB-Mini-5P-SMD
1    NJW4132                                          U200       SOT89-5         Step Up PWM DC-DC Regulator
1    NUP4201MR6/NUP2201MR6                            U205       SOT23-6         ESD-Schutzdioden
1    PCF8523T                                         U104       SO8             RTC mit TrickleCharge
1    PCM1808PWR                                       U107       TSSOP14         I2S Audio ADC
1    RCA 161-4220-E                                   J110       161-4220-E      dual RCA Connector
1    Raspberry-PI2-Buchsenleiste                      J106       RASPI2          Buchsenleiste, 2reihig 2.54mm gedreht
2    SPBH14S-300Y                                     J108 J109  FKV14SN
1    TPS62175DQCT                                     U316       WSON10          Step Down 28V 0.5A
1    TPS65286RHD                                      U203       QFN28-P         4.5-V to 28-V 6A Dual Switches
1    USB_A/USB_2A                                     J206       USB_2A          USB_A used
1    spare                                            J303       HDR1X5_2MM
1    spare/100n                                       C231       0805            Capacitor
1    spare/744233121                                  L204       744233121       Dual Coil
1    spare/Mini-USB                                   J204       USB-Mini-5P-SMD
1    spare/NUP4201MR6/NUP2201MR6                      U204       SOT23-6         ESD-Schutzdioden
1    spare/Raspberry-PI2-Flachbandkabel               J101       FKV40SN         Pfostenleiste, 2reihig
1    spare/USB_A                                      J205       USB_A