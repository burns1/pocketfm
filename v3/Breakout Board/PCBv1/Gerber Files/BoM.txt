Bill of Material for Breakout-Board

Used Part Type                                        Designator Footprint       Description
==== ================================================ ========== =============== =====================================
2    18p                                              C334 C335  0805            Capacitor
1    82p                                              C425       0805            Capacitor
3    100p                                             C116 C219  0805            Capacitor
                                                      C220
10   1n                                               C337 C338  0805            Capacitor
                                                      C339 C340
                                                      C341 C342
                                                      C343 C419
                                                      C420 C421
1    2n2                                              C230       0805            Capacitor
1    10n                                              C336       0805            Capacitor
2    47n                                              C224 C225  0805            Capacitor

24   100n                                             C100 C101  0805            Capacitor
                                                      C102 C103
                                                      C104 C105
                                                      C106 C107
                                                      C108 C109
                                                      C110 C111
                                                      C216 C323
                                                      C324 C325
                                                      C326 C327
                                                      C344 C345
                                                      C413 C414
                                                      C415 C416

6    1�/25V GRM31MR71E105KA01L                        C117 C119  1206            Capacitor
                                                      C221 C222
                                                      C223 C329
5    10�/16V GRM31CR71C106MA12L                       C422 C423  1206            Capacitor
                                                      C424 C200 C201
10   22�/10V GRM31CR71A226ME15L                       C226 C227  1206            Capacitor
                                                      C228 C229
                                                      C330 C331
                                                      C332 C333
                                                      C417 C418
6    22�/25V GRM32ER71E226KE15L                       C115 C217  1210            Capacitor
                                                      C113 C114  1210            Capacitor
                                                      C218 C328

2    220�/35V                                         C112       C10RM5

----------------
4    0R                                               R122 R123  0603
                                                      R124 R427

1    5mR/1W   PA2512FKF070R005E                       R119       2512
2    10R                                              R117 R118  0805
3    100R                                             R173 R174  0805
                                                      R175
1    130R                                             R445       1206
1    220R                                             R444       1206
1    390R                                             R443       1206
4    1k                                               R109 R110  0805
                                                      R111 R112
2    1k                                               R113 R114  1206
2    1k2                                              R134 R135  0805
1    2k2                                              R121       0805
1    3k                                               R217       0805
11   10k                                              R101 R102  0805
                                                      R103 R104
                                                      R105 R106
                                                      R107 R108
                                                      R213 R323
                                                      R324
2    11k                                              R132 R439  0805
2    18k                                              R218 R219  0805
3    22k                                              R130 R131  0805
                                                      R216
1    27k                                              R442       0805
9    47k                                              R133 R330  0805
                                                      R331 R332
                                                      R333 R334
                                                      R335 R336
                                                      R440
1    75k                                              R221       0805
5    100k                                             R128 R129  0805
                                                      R329 R437
                                                      R438
1    130k                                             R345       0805
4    180k                                             R340 R341  0805
                                                      R342 R343
4    220k                                             R100 R212  0805
2    680k                                             R220 R337  0805
1    910k                                             R441       0805
                                                      R422 R423
5    1M                                               R115 R116  0805
                                                      R325 R326
                                                      R327
-----
1    4x100R    MNR14E0APJ101                         R339       RPACK2X3        4xResistor
1    4x22k     MNR14E0APJ223                         R338       RPACK2X3        4xResistor
5    4x100k    MNR14E0APJ104                         R120 R328  RPACK2X3        4xResistor
                                                      R424 R425
                                                      R426
1    4x220k    MNR14E0APJ224                         R344       RPACK2X3        4xResistor
2    4x470k    MNR14E0APJ474                          R222 R223  RPACK2X3        4xResistor
---------------------
5    150141M173100                                    U411 U412  3528            LEDRGB WE
                                                      U413 U414
                                                      U415
1    742792096                                        L303       L0805           Coil
1    744233121                                        L205       744233121       Dual Coil
1    ATMEGA324P-20AU                                  U315       TQFP44A         Atmel AVR Microcontroller
1    74HC08D                                          U105       SO14            4 x AND
1    74HC165ADTR2G                                    U416       SSOP16            parallel in serial out shift register
1    74HCT595                                         U410       SO16            Serial in, parallel out 8Bit register

6    BAT54S                                           D106 D107  SOT23           Doppeldiode in Reihe SOT23
                                                      D108 D109
                                                      D110 D111
2    BC846                                            T103 T104  SOT23           NPN Transistor SMD
1    BSS84P                                           T405       SOT23           P-Channel FET 60V 8R 0.36W
4    BSS123                                           T102 T201  SOT23           N-Channel FET 100V 0.17A 1.2R 360mW
                                                      T302 T403
4    BSS138                                           T105 T106  SOT23           N-Channel FET 100V 0.17A 1.2R 360mW
                                                      T107 T117
1    CR2032                                           B100       122-2420-GR     Lithium 3V
2    DMP3056L-7                                       T303 T404  SOT23           P-Channel FET 30V3A 0.1R 3W
1    GY-DPS6MV2                                       U314       GY-DPS6MV2      GPS-Modul NEO-6M-0-001
1    INA219AIDR                                       U102       SO8             I2C Current and Voltage ADC
7    IP4256CZ5-W                                      U100 U101  SOT665          2 CH EMI Filter with 100R
                                                      U309 U310
                                                      U311 U312
                                                      U313
2    IPD50P04P4L-11                                   T100 T101  TO252AA         P-Channel FET 40V 50A 0.01R 55W
1    LM2733YMF                                        U417       SOT23-5         Step Up PWM DC-DC Regulator
1    NJW4132                                          U103       SOT23-6         Step Up PWM DC-DC Regulator
2    LP2985-33DBVR                                    U317 U409  SOT23-5         Voltage Regulator 16V 0.25A
1    MM3Z3V3T1G                                       D125       3216[1206]      Diode Z
4    MM3Z5V6T1G                                       D100 D101  3216[1206]      Diode Z
                                                      D102 D103
1    NUP4201MR6                                       U205       SOT23-6         ESD-Schutzdioden
1    PCF8523T                                         U104       SO8             RTC mit TrickleCharge
1    PEC12R-3220F-S0024                               D407                      Bourns Rotary-Encoder with Pushbutton

1    TPS62175DQCT                                     U316       WSON10          Step Down 28V 0.5A

1    TPS65286RHD                                      U203       VQFN28-P        4.5-V to 28-V 6A Dual Switches

2    1,55SMC6,8A                                      D201 D302  SMC             Diode Shottky
5    1A/20V MBRS120T3                                 D105 D303  SMB             Diode Shottky
                                                      D404 D405
                                                      D406
1    3A/40V MBRS340T3                                 D104       SMC             Diode Shottky

---------------------

1    32kHz                                            Q100       QUARZCSMD       Quarz
1    12MHz                                            Q301       QUARZSMD-GND    Quarz

---------------------
1    4�7/SER1360-472KLD                               L203       SER1360         Coil
4    10uH/1A/SRR6038-100Y                             L101 L102  L4_6.8X6.8SMD   Coil
                                                       L302 L401
1    10A,14mR,1.8mH/42H42A000                         L100       RSD42H42        Coil 3x2pin

---------------------

1    1010027182/SIM-SLOT                              J102       1010027182      Mini-SIM Slot Push Push 1010027182

1    FH12-6S-1SH(55) 6x1mm                            J100       1734592-6       ZIF Connector 6 Pin 1mm
      
2    686 116 144 22 WR-FPC SMT ZIF Horiz.             J304 J404  68611614422     ZIF FPC 16x1mm
2    SFW4R-3STAE1LF                                   J406 J407  SFW4R-3STAE1LF  ZIF FPC 4x1mm
1    2-1734592-2                                      J405       2-1734592-2     ZIF FPC 22x0.5mm
1		 ZIF Kabel 4x1mm 10cm 
1	 	 ZIF Kabel 16x1mm 20cm 686616200001 W�RTH
1    RCA 161-4220-E                                   J110       161-4220-E      dual RCA Connector
1    0ZCJ0050AF2E                                     J305       1210			      POLY 0.5A/24V 0.75R
1    6.3AT/3101.0050                                  F100       3101.0050

3    CON2                                             J103 J104  2_RM508         2 steckbare Klemme
                                                      J105

1    Mini-USB                                         J207       USB-Mini-5P-SMD
2    SPBH14S-300Y                                     J108 J109  FKV14SN
1    Raspberry-PI2                                    J106       BKLEV40         Pfostenleiste, 2reihig
1    USB_A                                            J205       USB_A
1    USB_A/USB_2A                                     J206       USB_2A

4    M2.6                                             m100 m101  M2.6            2.5mm Abstandsbolzen f�r PI
                                                      m102 m103
4    M3x10mm                                         m200 m201  M3              3mm Abstandsbolzen f�r Platine
                                                      m202 m203
---------------------
Not Mounted:
6    spare 0R                                         R125 R126  0603
                                                      R127 R214
                                                      R215 R432
1    spare                                            J303       HDR1X5_2MM
1    spare/100n                                       C231       0805            Capacitor
1    spare/1206L200SLYR                               J107       1210
1    spare/744233121                                  L204       744233121       Dual Coil
1    spare/CON2X4                                     J101       FKV8SN
1    spare/Mini-USB                                   J204       USB-Mini-5P-SMD
1    spare/NUP4201MR6                                 U204       SOT23-6         ESD-Schutzdioden