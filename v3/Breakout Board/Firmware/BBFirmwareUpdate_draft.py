#!/usr/bin/python
#
# MICT Pocket FM
# Breakout Board Firmware Upgrader
#
# To be run on PFM RPi2
# Requires PySerial package or equivalent pip install
# 2016 Adam Burns
#
#
#

import sys
import serial
from time import sleep

# filename = "Pocket-FM-0.1-ausgeliefert.hex"
filename = sys.argv[1]


# change device for testing (under Linux)
# 
# socat -d -d pty,raw,echo=0 pty,raw,echo=0
# This will create 2 virtual terminals (pty/# printed on socat output).
# eg. use
# serialDevice = "/dev/pts/8"
# and cat </dev/pts/9 for response
serialDevice = "/dev/ttyAMA0"

serialBaudRate = 115200
serialTimeout = 0.1


# Read Firmware file into memory
fh = open(filename, 'r')
all_lines = fh.readlines()
fh.close()

# Prepare serial device
ser = serial.Serial(serialDevice, baudrate=serialBaudRate, timeout=serialTimeout)

# WRITE FIRMWARE UPDATE COMMAND
ser.write("\x1b4711U")
print("Sent Firmware Update command")

for line in all_lines:
    BBLineSuccess = False
    while BBLineSuccess == False:
        print line
        ser.write(line)
        BBResponse = ser.read(2)
        if BBResponse == "O" or BBResponse == "p" or BBResponse == "s":  
            BBLineSuccess = True
        if BBResponse == "F" or BBResponse == "f" or BBResponse == "i":
            BBLineSuccess = False
        if BBResponse == "":
            # BBLineSuccess = True 
            # pass # (for Linux debug with no response codes)
            BBLineSuccess = False         
            print "Error: No response from BBoard"
            sys.exit(1)
            

