All changes regarding the system, packages configuration files goes into this file

Latest go at the bottom -> Last line. Thanks.

Christian, 8.1.2016
-------------------

/boot/config.txt
================
gpu_mem=16

interfaces             (<- added support for i2c, i2s, spi)
dtparam=i2c_arm=on
dtparam=i2s=on
dtparam=spi=on

kernel modules
==============

modbrobe i2c_dev

lsmod -> ...

i2c_bcm2708
i2c_dev
...

packages to be installed
========================

apt-get install i2c-tools
apt-get install tmux
apt-get install python
apt-get install vim



Zsolt, 13.1.2016
----------------

Packages installed
===============

pulseaudio
alsa-utils

added debian-multimedia in sources
https://www.deb-multimedia.org

mplayer

Adam 18.1.2016
--------------

Packages installed:
usbutils

Adam 19.1.2016
--------------
Packages installed:

iptables-persistent
firmware-atheros
hostapd
dnsmasq

Files changed:

#build/root/.. is git repo path

scp -P 2525 build/root/etc/iptables/rules.v4  root@mict.no-ip.org:/etc/iptables/

scp -P 2525 build/root/etc/network/interfaces  root@mict.no-ip.org:/etc/network
scp -P 2525 build/root/etc/sysctl.conf  root@mict.no-ip.org:/etc/
scp -P 2525 build/root/etc/default/hostapd root@mict.no-ip.org:/etc/default
scp -P 2525 build/root/hostapd/hostapd.conf root@mict.no-ip.org:/etc/hostapd
scp -P 2525 build/root/etc/dnsmasq.conf root@mict.no-ip.org:/etc/
scp -P 2525 build/root/etc/hosts  root@mict.no-ip.org:/etc/

System services:

systemctl enable hostapd
systemctl enable dnsmasq

Adam 19.1.2016
--------------

added line "auto eth0" to /etc/network/interfaces
updated build/root/etc/network/interfaces to reflect

Adam 24.1.2016
--------------
Packages installed:
gammu
gammu-smsd
usb-modeswitch

scp -P 2525 v3/sys/etc/gammu-smsdrc   root@mict.no-ip.org:/etc
scp -P 2525 v3/sys/etc/udev/rules.d/   root@mict.no-ip.org:/etc/udev/rules.d/
scp -P 2525 v3/etc/udev/rules.d/70-huawei_e352.rules    root@mict.no-ip.org:/etc/udev/rules.d/
scp -P 2525 v3/usr/sbin/gammu-smsd_run_on_receive.sh     root@mict.no-ip.org:/usr/sbin/
scp -P 2525 v3/usr/sbin/sms_command_parse_functions.sh   root@mict.no-ip.org:/usr/sbin/

Nathan, 2.2.2016
----------------
support for ``femon`` used to check dvb/sat tuning status

    apt-get install apt-cache
    apt-file update
    apt-get install dvb-apps

Nathan, 3.2.2016
----------------
support for tuner driver. working myself backward from what I know is required.
Currently this only partially works. See gitlab issue.

    wget https://home.andre-weidemann.de/tt-firmware/dvb-usb-tt-s2400-01.fw
    mv dvb-usb-tt-s2400-01.fw /lib/firmware/.

Zsolt, 7.2.2016
---------------
Installed: libsensors4, bzip2, sysstat

For monitoring sd-card health.


Adam 19.2.2016
--------------

changed /etc/modules to:
#/etc/modules: kernel modules to load at boot time.
#
#This file contains the names of kernel modules that should be loaded
#at boot time, one per line. Lines beginning with "#" are ignored.

snd-bcm2835

#enable i2c
i2c-bcm2708 
i2c-dev

#enable general b+ i2s output
snd_soc_core
snd_soc_bcm2708_i2s
bcm2708_dmaengine

#working soundcard
snd_soc_pcm1794a
snd_soc_rpi_dac

to test audio path mplayer->alsa->i2s->si47xx

note backup in modules.orig

Adam + Zsolt, 25.2.2016
--------------------
Changed Baudrate on device tree level in:

/boot/config.txt

dtparam=i2c_baudrate=56000

25.04.2016
----------

apt-get install lsof
apt-get install wireless-tools
apt-get install iw

Adam.
Adam, 11.5.2016
--------------------
 /etc/hostapd/hostapd.conf

ctrl_interface=/var/run/hostapd
ctrl_interface_group=0

as per https://www.raspberrypi.org/forums/viewtopic.php?f=36&t=63045
to get hostapd_cli to work

added to /etc/sudoers
pfm     ALL = (root) NOPASSWD: /sbin/iwconfig, /sbin/iw, /bin/systemctl

Adam, 12.5.2016
--------------------
Installed Python MPlayer module
# pip install mplayer

# pip list
argparse (1.2.1)
chardet (2.3.0)
colorama (0.3.2)
html5lib (0.999)
mercurial (3.1.2)
mplayer.py (0.7.0)
ndg-httpsclient (0.3.2)
pip (1.5.6)
pyasn1 (0.1.7)
PyDispatcher (2.0.5)
pyOpenSSL (0.13.1)
pyserial (2.6)
requests (2.4.3)
serbus (1.0.4)
setuptools (5.5.1)
six (1.8.0)
smbus (1.1)
urllib3 (1.9.1)
wheel (0.24.0)
wsgiref (0.1.2)

Adam 14.5.2016
--------------
Installed pyudev for USB Stick/Audio detection in python
pip install pyudev

Adam 16.5.2016
--------------
Installed pulsectl for experimental research on controlling pulse from python ...
pip install pulsectl

Adam, 20.5.2016
--------------------
Uninstalled Python MPlayer module
# pip uninstall mplayer

Adam, 22.5.2016
--------------------
Installed usbmount for trials to use with USB stick insertions
Note this software package is currently unmaintained!
# apt-get install usbmount

Adam, 23.5.2016
--------------------
Installed python-magic for detection of file types
# pip install python-magic

Adam, 27.5.2016
--------------------
Installed dosfstools package for vfat file system checking
# apt-get install dosfstools


Adam, 30.5.2016
--------------------
Installed RPi.GPIO python module for SIM card checking on GPIO pin 5
# pip install RPi.GPIO


Adam, 01.06.2016
--------------------

For wifi client mode we need wpa_supplicant
# apt-get install wpasupplicant
# apt-get install network-manager
For a virtual hostap mode & client station, create

/etc/udev/rules.d/70-persistent-net.rules:

SUBSYSTEM=="ieee80211", ACTION=="add|change", ATTR{macaddress}=="10:fe:ed:1c:cb:fd", KERNEL=="phy0", \
    RUN+="/sbin/iw dev wlan0 del", \
    RUN+="/sbin/iw phy phy0 interface add ap0 type __ap", \
    RUN+="/sbin/iw phy phy0 interface add sta0 type station", \
    RUN+="/bin/ip link set sta0 address 00:11:22:33:44:56"

--

** Important note for configuration & provisioning: MAC address here must be configured to the MAC address of the WiFi dongle for udev to pick it up.
--

/etc/network/interfaces:

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

#allow-hotplug wlan0
#iface wlan0 inet static
#    address 192.168.86.1
#    network 255.255.255.0

allow-hotplug ap0
iface ap0 inet static
    address 192.168.86.1
    netmask 255.255.255.0
    hostap /home/pfm/pfm_conf/hostapd.conf

allow-hotplug sta0
iface sta0 inet dhcp
    wpa-conf /home/pfm/pfm_conf/wpa_supplicant.conf
    
--
And modify dmsmasq.conf to bind to ap0 instead of old wlan0

diff /etc/dnsmasq.conf /etc/dnsmasq.conf.pfm 
107c107
< interface=ap0
---
> interface=wlan0

Adam, 05.06.2016
--------------------
Installed python-dbus package for dbus-python dependency
# apt-get install python-dbus


Zsolt, 08.06.2016
--------------------
pip install python-networkmanager

Was already installed. Reinstall because of image change by Adam.

(Note from Adam: python-networkmanager was orginally a broken dist install using apt-get (python-networkmanager (0.9.10))
installing vi pip gives newer version (python-networkmanager (1.0.1)). pip must be used!)

On the old image python-MPlayer-module was removed.

pip-list 10.6.2016:
argparse (1.2.1)
chardet (2.3.0)
colorama (0.3.2)
html5lib (0.999)
mercurial (3.1.2)
ndg-httpsclient (0.3.2)
pip (1.5.6)
pulsectl (16.5.0)
pyasn1 (0.1.7)
pydbus (0.5.1)
PyDispatcher (2.0.5)
pygobject (3.14.0)
pyOpenSSL (0.13.1)
pyserial (2.6)
python-magic (0.4.11)
python-networkmanager (1.0.1)
pyudev (0.20.0)
requests (2.4.3)
RPi.GPIO (0.6.2)
serbus (1.0.4)
setuptools (5.5.1)
six (1.8.0)
smbus (1.1)
urllib3 (1.9.1)
wheel (0.24.0)
wsgiref (0.1.2)

Adam, 27.06.2016
----------------

Experimental: created new policykit file to allow pfm user to make changes using networkmanager.
created:
/etc/polkit-1/localauthority/50-local.d/org.freedesktop.NetworkManager.pkla

[nm-applet]
Identity=unix-group:netdev
Action=org.freedesktop.NetworkManager.*
ResultAny=yes
ResultInactive=yes
ResultActive=yes



# Must add pfm to netdev group
gpasswd -a pfm netdev

# (Note NM will not work with dist installed python-network-manager see note at Zsolt, 08.06.2016)

apt-get remove python-network-manager
pip install python-network-manager

Adam 02.07.2016
---------------

added tmpfs read/write for NetworkManager

tmpfs     /var/lib/NetworkManager  tmpfs  noatime,nodiratime,size=4M,mode=1755 0 0

to /etc/fstab

/dev/mmcblk0p1 /boot vfat defaults 0 2

#/dev/mmcblk0p2 / ext4 errors=remount-ro,noatime,nodiratime,commit=120 0 1
#/dev/sda2 / ext4 errors=remount-ro,noatime,nodiratime,commit=120 0 1

#running for raspi
/dev/mmcblk0p2 / ext4 ro,noatime,nodiratime 0 1

#running for qemu
#/dev/sda2 /          ext4   ro,noatime,nodiratime      0 1

tmpfs     /var/log    tmpfs  noatime,nodiratime,size=16M,mode=1755 0 0
tmpfs     /var/spool  tmpfs  noatime,nodiratime,size=4M,mode=1755 0 0
tmpfs     /var/tmp    tmpfs  noatime,nodiratime,size=8M,mode=1755 0 0
tmpfs     /tmp        tmpfs  noatime,nodiratime,size=8M,mode=1755 0 0
tmpfs     /var/lib/systemd  tmpfs  noatime,nodiratime,size=4M,mode=1755 0 0
tmpfs     /var/lib/pulse  tmpfs  noatime,nodiratime,size=4M,mode=1755 0 0
tmpfs     /var/lib/NetworkManager  tmpfs  noatime,nodiratime,size=4M,mode=1755 0 0

LABEL=pfm_conf /home/pfm/pfm_conf ext4 rw,noatime,nodiratime,commit=120,nofail      0 2
LABEL=pfm_store /home/pfm/pfm_store ext4 rw,noatime,nodiratime,commit=120,nofail      0 2

#there was also nobootwait added.


Zsolt 18.07.2016
---------------
Moved local .mplayer/conf to ~/pfm_conf/mplayer.
Added also channels.conf.sat in ~/pfm_conf/mplayer/ for configuring
channels in a local writable partition.

Added vc=null to config there, to disable video decoding in case a
stream with video in it is choosen.

Adam 19.07.2016
---------------
NOTE: Reverted udev split of seperate virtual wifi networks (planned for one AP & one client).

rm /etc/udev/rules.d/70-persistent-network-rules

IMPORTANT: this also is linked to a repo change in the app! 

In v3/app/connectionmanager.py, 

self.WiFiClientDevice = 'wlan0' # was ap0 for virtual device defined in /etc/udev/rules.d/70-persistent-network-rules


Adam 26.07.2016
----------------

Full migration to NetworkManager

1. eth0 definition in /etc/network/interfaces is commented out

$ cat /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

#auto eth0
#iface eth0 inet dhcp

#allow-hotplug wlan0
#iface wlan0 inet static
#    address 192.168.86.1
#    network 255.255.255.0

allow-hotplug ap0
iface ap0 inet static
    address 192.168.86.1
    netmask 255.255.255.0
    hostapd /home/pfm/pfm_conf/hostapd.conf

# allow-hotplug wlan1
# iface wlan1 inet manual
#     wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

2. dnsmasq service is disabled

systemctl disable dnsmasq

3. In order for resolver to dynamically update, /etc/resolv.conf must be writable by NM

mv /etc/resolv.conf /home/pfm/pfm_conf/NetworkManager

ln -s /home/pfm/pfm_conf/NetworkManager/resolv.conf /etc/resolv.vonf

Adam 22.08.2016
----------------

MPlayer looping fix

In order to achieve looping playlists in the mplayer.py module, the slave mode is invoked with a default playlist.

So, mplayer.py expects the following files in the read only root partition:

/home/pfm/pfm_silence_playlist

/home/pfm/silence.mp3

where /home/pfm/pfm_silence_playlist contains the single line of:

/home/pfm/silence.mp3

and /home/pfm/silence.mp3 is the silence file from pfm v2

Adam 25.08.2016
---------------

/etc/sudoers changed to allow gammu user to sudo as pfm user and invoke /home/pfm/app/tools/send_signal.py as pfm user

/etc/sudoers:

#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

pfm     ALL = (root) NOPASSWD: /sbin/iwconfig, /sbin/iw, /bin/systemctl
gammu   ALL = (pfm) NOPASSWD: /home/pfm/app/tools/send_signal.py
#includedir /etc/sudoers.d


Adam 29.08.2016
---------------

apt-get upgrade

required for installation of vsftpd

apt-get install vsftpd
apt-get install db-util

(db-util is for virtual user control with vsftpd - see https://help.ubuntu.com/community/vsftpd)

added /etc/pam.d/vsftpd.virtual

# as per https://help.ubuntu.com/community/vsftpd for virtual users
#%PAM-1.0
auth       required     pam_userdb.so db=/home/pfm/pfm_conf/vsftpd/vsftpd-virtual-user
account    required     pam_userdb.so db=/home/pfm/pfm_conf/vsftpd/vsftpd-virtual-user
session    required     pam_loginuid.so


For avahi mdns service pulishing, see https://wiki.archlinux.org/index.php/Avahi


Zsolt 27.9.2016
---------------
changed local time to Europe/Berlin in:

/etc/timezone

/etc/localtime -> /usr/share/zoneinfo/Europe/Berlin


Adam 29.09.2016
---------------

apt-get install minimodem

for experimental data transmission

Adam 04.10.2016
---------------

FTP provisioning fix to ensure virtual ftp user has read/write access to /home/pfm/pfm_store.
As per 29.08.2016 note, vsftp user ftp must have read/write access to ~pfm owned filestore.

1. add configuration to /etc/vsftpd.conf:

allow_writeable_chroot=YES

making full file:

# grep -v "^#" /etc/vsftpd.conf
listen=NO
listen_ipv6=YES
anonymous_enable=NO
local_enable=YES
dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
secure_chroot_dir=/var/run/vsftpd/empty
pam_service_name=vsftpd
rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
ssl_enable=NO

anonymous_enable=NO
local_enable=YES
virtual_use_local_privs=YES
write_enable=YES
allow_writeable_chroot=YES
pam_service_name=vsftpd.virtual

guest_enable=YES

user_sub_token=

local_root=/home/pfm/pfm_store

chroot_local_user=YES

hide_ids=YES

local_umask=002
chown_upload_mode=0777  # it is local development server only, that's why I have
file_open_mode=0777     # set it to 777, you can change accordingly

2. add user ftp to the pfm group in /etc/group to allow rw group access to directories and files

edit line 

pfm:x:1000:dialout,video,ftp

making full group file:
root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mail:x:8:
news:x:9:
uucp:x:10:
man:x:12:
proxy:x:13:
kmem:x:15:
dialout:x:20:gammu,pfm
fax:x:21:
voice:x:22:
cdrom:x:24:
floppy:x:25:
tape:x:26:
sudo:x:27:pfm
audio:x:29:pulse,pfm
dip:x:30:
www-data:x:33:
backup:x:34:
operator:x:37:
list:x:38:
irc:x:39:
src:x:40:
gnats:x:41:
shadow:x:42:
utmp:x:43:
video:x:44:pfm
sasl:x:45:
plugdev:x:46:
staff:x:50:
games:x:60:
users:x:100:
nogroup:x:65534:
input:x:101:
systemd-journal:x:102:
systemd-timesync:x:103:
systemd-network:x:104:
systemd-resolve:x:105:
systemd-bus-proxy:x:106:
crontab:x:107:
ntp:x:108:
netdev:x:109:pfm
ssh:x:110:
messagebus:x:111:
pfm:x:1000:dialout,video,ftp
i2c:x:112:pfm
pulse:x:113:
pulse-access:x:114:root,pfm
rtkit:x:115:
gammu:x:116:
utempter:x:117:
avahi:x:118:
bluetooth:x:119:
Debian-exim:x:120:
ssl-cert:x:121:
ftp:x:122:

3. change group write permissions of ~pfm/pfm_store and below (owned by pfm)

# chmod -R g+w /home/pfm/pfm_store


Zsolt 07.10.2016
---------------
Integrated proposed changes into root image. Changing /etc/group
adding ftp tp pfm group
Added a script which checks for the right permissions on startup and
changes all files and dirs to pfm

Added check_owner_pfm_store.sh to /home/pfm/app/tools/
And /etc/rc.local

Adam 02.12.2016
---------------
Deleted /etc/udev/rules.d/70-huawei_e352.rules
to allow NetworkManager to cooperate with gammu-smsd
3G card comes up as device wwan0, instead of ttyUSB3 & ppp0 ... far cleaner!!!

Adam 25.02.2017
---------------
Niqash Project: To allow WLAN (wlan1) external USB port to come up

Change default configuration for hostapd to conf file under pfm_conf

cat /etc/default/hostapd 
# Defaults for hostapd initscript
#
# See /usr/share/doc/hostapd/README.Debian for information about alternative
# methods of managing hostapd.
#
# Uncomment and set DAEMON_CONF to the absolute path of a hostapd configuration
# file and hostapd will be started during system boot. An example configuration
# file can be found at /usr/share/doc/hostapd/examples/hostapd.conf.gz
#
DAEMON_CONF="/etc/hostapd/hostapd.conf"
DAEMON_CONF="/home/pfm/pfm_conf/hostapd.conf"

# Additional daemon options to be appended to hostapd command:-
#       -d   show more debug messages (-dd for even more)
#       -K   include key data in debug messages
#       -t   include timestamps in some debug messages
#
# Note that -B (daemon mode) and -P (pidfile) options are automatically
# configured by the init.d script and must not be added to DAEMON_OPTS.
#
#DAEMON_OPTS=""

Change dnsmasq default conf file to be in pfm_conf:

cat /etc/default/dnsmasq
# This file has five functions: 
# 1) to completely disable starting dnsmasq, 
# 2) to set DOMAIN_SUFFIX by running `dnsdomainname` 
# 3) to select an alternative config file
#    by setting DNSMASQ_OPTS to --conf-file=<file>
# 4) to tell dnsmasq to read the files in /etc/dnsmasq.d for
#    more configuration variables.
# 5) to stop the resolvconf package from controlling dnsmasq's
#    idea of which upstream nameservers to use.
# For upgraders from very old versions, all the shell variables set 
# here in previous versions are still honored by the init script
# so if you just keep your old version of this file nothing will break.

#DOMAIN_SUFFIX=`dnsdomainname`
DNSMASQ_OPTS="--conf-file=/home/pfm/pfm_conf/dnsmasq.conf"

# Whether or not to run the dnsmasq daemon; set to 0 to disable.
ENABLED=1

# By default search this drop directory for configuration options.
# Libvirt leaves a file here to make the system dnsmasq play nice.
# Comment out this line if you don't want this. The dpkg-* are file
# endings which cause dnsmasq to skip that file. This avoids pulling
# in backups made by dpkg.
CONFIG_DIR=/etc/dnsmasq.d,.dpkg-dist,.dpkg-old,.dpkg-new

# If the resolvconf package is installed, dnsmasq will use its output 
# rather than the contents of /etc/resolv.conf to find upstream 
# nameservers. Uncommenting this line inhibits this behaviour.
# Note that including a "resolv-file=<filename>" line in 
# /etc/dnsmasq.conf is not enough to override resolvconf if it is
# installed: the line below must be uncommented.
#IGNORE_RESOLVCONF=yes


Make sure wlan1 is ignored by network manager by adding explicit unmanaged-devices line

cat /etc/NetworkManager/NetworkManager.conf
[main]
plugins=ifupdown,keyfile

[ifupdown]
managed=false

[keyfile]
unmanaged-devices=interface-name:wlan1


Add wlan1 definition to /etc/network/interfaces:

cat /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

#allow-hotplug wlan0
#iface wlan0 inet static
#    address 192.168.86.1
#    network 255.255.255.0

allow-hotplug wlan1
auto wlan1
iface wlan1 inet static
    address 172.16.16.1
    network 255.255.255.0
    post-up systemctl start hostapd || /bin/true
    post-up systemctl start dnsmasq || /bin/true
    post-down systemctl stop dnsmasq || /bin/true
    post-down systemctl stop hostapd || /bin/true
# iface wlan1 inet manual
#   wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

Adam 09.03.2017
---------------
A new implementation of a SIM GPIO watcher means that /etc/sudoers needs to be updated with 
pfm     ALL = (root) NOPASSWD: /sbin/iwconfig, /sbin/iw, /bin/systemctl, /bin/date, /home/pfm/app/tools/gpio5monitor.sh

(note that iw, iwconfig & I think date (for setting system date) may be out of date!)

Adam 09.03.2017
---------------
We currently do not require email services & exim4 was likely pulled in as a dependency. It errors looking for RW spool etc.

Disable exim4 service

systemctl disable exim4

Adam 09.03.2017
---------------
Dial back DHCP timeout for faster boot

add to /etc/dhcp/dhclient.conf:

timeout 10;

