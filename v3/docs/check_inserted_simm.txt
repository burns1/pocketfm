Test with the wiring library:
http://wiringpi.com/

apt-get install wiringpi

SIMM-Card insert check is populated via: 
GPIO-21 (wiring scheme) (Physical PIN 29) (GPIO 5 BCM)

Prints table of available GPIOs:
gpio readall 

Gets logical value of particular PIN:
gpio read 21

- 0: while SIMM-card is inserted
- 1: no SIM-card in slot detected

Python binding?:
https://pypi.python.org/pypi/wiringpi

