Following partition scheme should be used for provisioning, upgrades and in the first place testing.

boot | root | pfm_conf | pfm_store | testing
          8GB                         8GB           8GB

Testing would be an additional partition, the same system and the same size as root, but with only the testing and later upgrading system on it.

Booting the different partitions would be done via defining root on the kernel command line.
The system boots by default into root and could be switched to boot from testing.

Some more information about the boot sequence and bootloaders
regarding the RaspberryPi:

https://github.com/raspberrypi/noobs/wiki/NOOBS-partitioning-explained

http://raspberrypi.stackexchange.com/questions/10489/how-does-raspberry-pi-boot

https://github.com/dwelch67/raspberrypi
