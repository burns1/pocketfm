## Standard Boot Procedure (Draft)

* Device get's connected to Power Source
* Atmel on Breakout Board starts
* Atmel powers up Raspberry Pi (incl. Dongles) and the TX (maybe delayed, TBC)
* Raspberry Pi boots up
* When booted, Raspberry Pi looks for configuration file in `/conf`
* If configuration file is present and readable, system boots up into last saved operation mode
* If no configuration file is present or file is not readable, system boots into default mode as stored in `/root`
* boot process ends in SCREEN LOCK, even if last saved operational mode is broadcasting

Note: The user should have the possibility to hard-boot the device into factory mode, e.g. by keeping the button pressed during start-up.


Atmel Standard Power Scheme: (v0.4)
- Once the Atmel is powered up, it powers up the Raspi
- The Raspi then can decide if t stays on or goes back to sleep, waiting for an RTC-Alarm
- Alarm gets rest when Raspi sends the first ESC sequence
- When the Raspi is sleeping and the Encoder button is pressed, the Atmel powers up the Raspi

<br />


**Default Boot Mode:**

* Raspberry Pi is active
* TX is active with frequency below FM range and directional power = 0W
* WiFi module not active (but driver loaded)
* GSM/3G module not active (but driver loaded)
* Sat-module deactivated (powered off)
* LED1 and LED2 on (yellow)


<br />

> **LED functionality for reference**
> 
> LED numbering is from down to up from 1 to 5.
> 
> LED1 = Yellow when device connected to power source and Atmel is up, Red if Alert
> 
> LED2 = Yellow when Raspberry Pi & TX are powered up and running
> 
> LED3-5 = Yellow visually displaying when the TX actually transmits sound
