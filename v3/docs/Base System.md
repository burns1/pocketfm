**BASE SYSTEM DOCUMENTATION**


Everything runs from a 16GB microSD Card by SanDisk (Model "Ultra")


The card carries 4 logical partitions

- boot
- root
- conf
- storage

 
**1.) BOOT**

`/boot` hosts everything the system needs to boot up.

 


**2.) ROOT**

`/root` hosts the operation system and all operational scripts.

OS: Minibian Jessie 2015-11-12
`https://minibianpi.wordpress.com/2015/11/12/minibian-jessie-2015-11-12-is-out/`
`https://minibianpi.wordpress.com/features/`

The "root" partition is set-up as read-only.

It includes a "Factury Settings" defaults file.

From "root" run the OS and all other scripts.

 


**3.) CONF**

`/conf` is used to store all changes the user or the system or any script is making.
It is also used for all temporary files and log files that need to be written.

At boot, the system uses the stored information on "conf" to boot into the last operational state.


 

**4.) USER**

This is the partition where the user can store audio files for playback.
For the user, this partition appears as an internal hard drive and is labeled HD.


