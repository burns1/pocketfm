Naming convention for naming the release images and tarballs.

Root image:
Consists of the operating system and pfm home dir. 
dd image is made of the root partition:
#FIXME dd cmd line of creating the image: e.g. dd if=/mmbclp2 of=???.img bs=4M
pfm_root_RELEASENR_YYYYMMDD.img
The newest should be always linked as pfm_root_current.img.

Conf image:
Consists of the conf partition which is linked into /home/pfm_conf
#FIXME dd cmd line of creating the image.
pfm_conf_RELEASENR_YYYYMMDD.img
The newest should be always linked as pfm_conf_current.img.

App tarball:
Consists of latest tar.gz packed app dir tagged as release.
#FIXME tar.gz cmd line here
pfm_app_RELEASENR_YYYYMMDD.tar.gz
The newest should be always linked as pfm_app_current.tar.gz.

