## Main Loop Architecture (Draft)

"Main Loop" describes the framework which enables all the component to work and properly interact with each other.
To realize that, we use a set of (a) event driven control scripts and (b) query based processes.
  
-


**1. ––  Control Scripts/Shells**

We have certain event driven shells whereas each controls one component or one set of functions.
<br />
<br />

**1.1  ––  input-ctrl**

`input-ctrl` is the instance that deals with the source selection. This involves:

- Switching the hardware input of the Silabs chip on the MaxPro between analog and digital by means of writing over I2C to the Atmel on the MaxPro Board
- Playing audio files from a USB stick or from the internal storage and routing this to the Silabs over I2S
- Routing the audio stream from the sat tuner to the Silabs chip over I2S
- Playing/receiving web-based audio streams (over Wifi or mobile data connection) and routing this to the Silabs over I2S
- Reading the analog audio levels from the Silabs chip when playing analog audio (VU voltage meter over I2C, routed to the Breakout Board Atmel for analog-digital conversion)

After completion, the status is written to `/conf`.

<br />
<br />
**1.2  ––  tx-ctrl**
 
`tx-ctrl` is the instance that interacts with the fm transmitter board (MaxPro 3015) over I2C:
 
This includes:

- Set frequency
- Set broadcast power (directional power)
- Set RDS
- Set Stereo/Mono Mode
- Set DSP audio processor

After completion, the status is written to `/conf`.


<br />
<br />
**1.3  ––  sat-ctrl**

`sat-ctrl` is the instance that handles the control of the satellite tuner.

This includes:

- Set frequency
- Set V/H
- Set Symbol Rate
- (unclear: set APID?)
- (unclear: set Signal Source for DiSEC?)

After completion, the status is written to `/conf`.

<br />
<br />
**1.4  ––  sms-ctrl**
 
`sms-ctrl` is the instance that enables remote control of certain features of the device, as described in [Documentation_SMS commands.md](https://gitlab.com/pocketfm/pocketfm/blob/master/v3/docs/Documentation_SMS%20commands.md) and in [issue #21](https://gitlab.com/pocketfm/pocketfm/issues/21).

This process does not write to `/conf` itself.
 


<br />
<br />
**1.5  ––  network-ctrl**
 
`wifi-ctrl` is the instance that enables network functionality of the device. This includes establishing data connections to allow the device to play web-based audio sources and allow remote users to connect to the `webUI` of the device. This includes a) the **WIFI** module as described in [issue #34](https://gitlab.com/pocketfm/pocketfm/issues/34) and b) the **GPRS/UMTS/HSDPA/HSDPU**-module of the device.



<br />
<br />
**1.6  ––  gps-ctrl**
 
`gps-ctrl` can query the current GPS position from the Atmel on the Breakout Board.


<br />
<br />
**1.7  ––  rtc-ctrl**
 
`rtc-ctrl` handles all time and scheldung related events by talking to the battery buffered real time clock with alarm function. The communications is realized through the Atmel on the Breakout Board.

<br />
<br />
**1.8  ––  power-ctrl**
 
`power-ctrl` can tell the Atmel on the Breakout Board to power-switch on/off certain components of the system (Sat, TX, Raspi).



-
-
<br />
**2.1  ––  frontshield-ui**
 
`display-ui` is the instance that handles the communication with the Atmel on the Breakout Board for displaying information on the LCD and the LEDs and reading input from the Rotary Encoder (Button).



<br />
**2.2  ––  web-ui**
 
`web-ui` is the instance that provides a browser-based UI to a local or remote user. It is able to call all the processes under (1.) (event-based) and displays all their values/parameters in real-time (query-based).

<br />

***Both UI processes involve the following queries:***


TX

- Read actual emitted broadcast power (directional power)
- Read reflected power
- Read exciter temperature
- Read exciter voltage
- Read alarm status


<br />
SAT

- Read Signal Status (see [NOTES_SAT](https://gitlab.com/pocketfm/pocketfm/blob/master/notes/NOTES_SAT.md)) ("Lock", "Sync found", "FEC", "Have Carrier", "Have Channel")
- Read Signal Quality ("Signal Strenght" in %, "Signal Quality")

<br />
FRONTSHIELD

- LCD - display real-time information (event- and query based)
- LEDs - display real-time information (query based)
- Butten (Rotary Encoder) - read events (inputs)



-
-
<br />
**3.0  ––  other processes**

**`watchdog`** is a functionality of the Atmel on our Brakout Board. It queries the Raspberry Pi in a certain rhytem to make sure it is up and running. In case of no answer it will try to soft- or hard-reboot the Raspberry Pi.

**`power`** is the instance that interacts with the Atmel on the Breakout Board to realize power-switching. The Atmel has the possibility to switch the following:

- Power on/off the TX Board
- Power on/off the sat board
- Power on/off the Raspberry Pi
- Switch off the LCD

The Atmel on the Breakout Board can also set the system in a "sleep mode" where everything is switched off except the Atmel itself. Pressing the Rotary Encoder on the frontshield will wake the system. This can also be scheduled.

