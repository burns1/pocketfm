##SMS commands for reomote control


The device only accepts SMS that begin with the `PINCODE`.
If the pincode is missing, the SMS gets deleted right away.

If the pincode is incorrect, the device repeats with the sms `INVALID PIN`.

If the device recieves an SMS with the correct pincode, it will look for a command to interpret.

If the device receives the SMS and can execute the command, it responds with the SMS `DONE` respectivaly with the requested value(s).

If the device accepts the SMS (correct PINCODE) but cannot interpret or execute the command, it responds with the SMS `ERROR`.

The SMS commands are not case-sensitive.
<br />
<br />


 



**Set TX power to any full value between 0 and 100%**

`PINCODE PWR "DIGIT"`

e.g.
`123456 PWR 50` will set the broadcasting power to 50%.
<br />
<br />




**Set frequency**

`PINCODE FRQ "DIGIT.DIGIT"`

e.g.
`123456 FRQ 98.7` will set the broadcasting frequency to 98.7MHz.
<br />
<br />



**Switch input source**

`PINCODE SRC "SOURCE"`

e.g.
`123456 SRC INT` will switch the input source to automatic playback from the internal user storage.
"SOURCES" are defined as follows:

`INT` = Internal user storage

`USB` = Attached USB stick

`ANALOG` = Analog in

`SAT` = Satellite Tuner

`STREAM` = Internet Stream

<br />
<br />


**Get system status**

`PINCODE STATUS` will generate an SMS with certain status informtation of the device, e.g.:
`SID=PocketFM__PWR=50%__FQ=98.7__SOURCE=SAT__WIFI=Off__GPS=38.1234N045.1234W
<br />
<br />



**Activate/Deactivate Wifi Modes**

`PINCODE WIFI CLIENT` will activate the Wifi module in client mode.

`PINCODE WIFI AP` will activate the Wifi module in client mode.

`PINCODE WIFI OFF` will deactivate the Wifi module.

<br />
<br />



**Darkmode On/Off**

`PINCODE DARK ON/OFF` will call the Atmel to power-off / power-on the MaxPro board and switch off the Wifi.
<br />
<br />


**Shutdown device**

`PINCODE SHUTDOWN` will activate sleep mode (as in the Breakout Board documentation).
