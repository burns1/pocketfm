#!/usr/bin/python

import sqlite3
import time
import argparse
import general

"""
Created stolle 2016

Tools and the whole sqlite code is highly experimental. I basically copied them from
other projects and did not get the time to make it nice ...

"""


NOSIGYET = "nosigyet"


############################################################################################
#
#  you can define as many functions as you want. The can be executed using -F functionname
#


def max_pro_off():
    send_signal_by_name("SIG_SET_POWER_MAXPROBOARD", False)


def nullify_power_screen():
    send_signal_by_name("SIG_UPDATE_MAXPRO_OUTPUT_POWER", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_MAXPRO_REFLECTED_POWER", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_MAXPRO_TEMPERATURE", NOSIGYET)

def nullify_main_screen():
    send_signal_by_name("SIG_UPDATE_MAXPRO_STATIONID", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_MAXPRO_FREQUENCY", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_MAXPRO_OUTPUT_POWER", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_AUDIOMANAGER_INPUT", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_AUDIOMANAGER_INPUT", NOSIGYET)
    send_signal_by_name("SIG_UPDATE_WIFI_STATUS", NOSIGYET)

def example():
    print "sending 10 times SIG_ENCODER_ROTATOR_ANALOG_LEFT:"

    for i in xrange(10):
        str = ''
        for ii in xrange (i):
            str += '*'

        send_signal_by_name ('SIG_SET_MAXPRO_STATIONID', str)
        send_signal_by_name ('SIG_ENCODER_ROTATOR_ANALOG_LEFT', 0)
        time.sleep (0.5)

def send_null():
    send_signal_by_name('SIG_ENCODER_ROTATOR_ANALOG_LEFT', None)
    time.sleep(1)
    send_signal_by_name('SIG_ENCODER_ROTATOR_ANALOG_LEFT', None)
    time.sleep(1)
    send_signal_by_name('SIG_ENCODER_ROTATOR_ANALOG_LEFT', None)



##########################################################################################
#
#


__DB = general.DEFAULT_DB       # will be overwritten by -d

def send_signal_by_name(sig_name, data, sender="send_signal.py"):

    conn = general.connect_safely(__DB, True, True)

    c = conn.cursor()
    sstr = "SELECT sig_string FROM SignalState where sig_name = ?"

    c.execute(sstr, (sig_name,))
    row = c.fetchone()

    if row is None:
        print "'{}' is no valid signal".format(sig_name)
        print "Use -l to get a list of valid signals"
        exit()

    sig_string = str(row[0])  # no str - unicode, no match ...
    print ("send {:<30} of type  {}  as  '{}'".format(data, type(data), sig_string))

    sstr = "INSERT INTO SignalQueue (sig_string, sender, val_pickle) VALUES (?,?,?)"
    c.execute(sstr, (sig_string, sender, sqlite3.Binary(general.sqlite_pickle(data))))
    conn.commit()
    conn.close()


def list_signals (frg=None):
    conn = sqlite3.connect(__DB)
    c = conn.cursor()
    sstr = "SELECT sig_name, sig_string FROM SignalState"

    if frg:
        sstr += " WHERE sig_string LIKE '%{}%'".format(frg)

    for row in c.execute(sstr):
        print "{:50} - {}".format(str(row[0]), row[1])




parser = argparse.ArgumentParser(description='Sends a Signal to the running pfm',
                                 usage="signal value")

parser.add_argument("-d", "--database", help="the database, defaults to " + __DB)
parser.add_argument("-l", "--list", help="list of valid signals", action="store_true")
parser.add_argument("-L", "--filter", help="list of valid signals filtered")
parser.add_argument("-i", "--int", help="forces the value to be int", action="store_true")
parser.add_argument("-f", "--float", help="forces the value to be float", action="store_true")
parser.add_argument("-s", "--signal", help="the signal to be send")
parser.add_argument("-v", "--value", help="the value to be set, not given -> None is sent")
parser.add_argument("-F", "--function", help="execute function ")

args = parser.parse_args()

if args.function:
    globals()[args.function]()
    exit()

if args.database:
    __DB = args.database

if args.list:
    list_signals()
    exit()

if args.filter:
    list_signals(args.filter)
    exit()

if not args.signal:
    print "you have to give a signal"
else:
    if not args.value:
        send_signal_by_name(args.signal, None)
    else:
        value = args.value

        if args.int:
            value = int(args.value)

        if args.float:
            value = float(args.value)

        send_signal_by_name(args.signal, value)

