#!/usr/bin/python

import serial
import time
import sys
import datetime
import string

port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)

#if len(sys.argv) > 1:
#try:
#    sys.argv[1]
#except NameError:
#    print("\n Wrong arg name")
#else:
#    curdate = sys.argv[1]
#    print curdate

#get rtc time
port.write("\x1b?T")
time.sleep(2)
#port.flushInput()
rcv = port.read(21)
rcv = rcv.lstrip("\x1b0")

print("\nTime from RTC")
print repr(rcv)

sysdate=datetime.datetime.strftime(datetime.datetime.utcnow(), '%u %d/%m/%y %H:%M:%S')
print("\nSystem Time")
print repr(sysdate)

#set the rtc
port.write("\x1b"+str(sysdate)+"T")
time.sleep(2)
port.write("\x1b99,99,99,99A")
time.sleep(2)

#read rtc again
port.flushInput()
port.write("\x1b?T")
time.sleep(2)
rcv = port.read(21)
print("\nNew RTC Date")
print repr(rcv)
