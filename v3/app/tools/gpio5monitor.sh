#!/usr/bin/env python

from time import sleep
import RPi.GPIO as GPIO
import subprocess

PFM_SIM_GPIO_PIN = 29

PFM_SEND_SIG = "/home/pfm/send_signal.py"

GPIO.setmode(GPIO.BOARD)
GPIO.setup(PFM_SIM_GPIO_PIN, GPIO.IN)

def my_callback(channel):
    callback_sim_gpio = GPIO.input(PFM_SIM_GPIO_PIN)
    if callback_sim_gpio == 0:
        pfm_signal = 'SIG_UPDATE_3G_SIM_INSERTED'
    else:
        pfm_signal = 'SIG_UPDATE_3G_SIM_REMOVED'

    callback_command = ['sudo', '-u', 'pfm', PFM_SEND_SIG, '-s', pfm_signal, '-i', '-v',str(callback_sim_gpio)]
    callback_output = subprocess.check_output(callback_command)

    #print('SIM GPIO callback: send signal command is {} with result {}'.format(callback_command, callback_output))

# get initial SIM GPIO state
sim_gpio = GPIO.input(PFM_SIM_GPIO_PIN)
#  print('SIM watcher: initial state is {}'.format(sim_gpio))

# send initial SIM GPIO state to app
send_signal_command = ['sudo', '-u', 'pfm', PFM_SEND_SIG, '-s', 'SIG_UPDATE_3G_SIM_GPIO_INITIAL_STATE', '-i', '-v',str(sim_gpio)]
send_signal_output = subprocess.check_output(send_signal_command)

#print('SIM watcher: initial send signal command is {} with result {}'.format(send_signal_command, send_signal_output))

GPIO.add_event_detect(PFM_SIM_GPIO_PIN, GPIO.BOTH, callback=my_callback, bouncetime=300)

while True:
    #print("blah")
    sleep(10)
