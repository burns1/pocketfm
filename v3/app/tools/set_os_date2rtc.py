#!/usr/bin/python

import serial
import time
import sys
import datetime
import string
import os
import re

port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)

#get rtc time
port.write("\x1b?T")
time.sleep(2)

try:
    rcv = port.read(21)
except serial.SerialException as e:
    rcv = None
    print("\nSerial Data Error 0.1")
    port.close()
    sys.exit(-1)
except TypeError as e:
    rcv = None
    print("\nSerial Data Error 0.2")
    port.close()
    sys.exit(-1)

print("\nRead1")
print repr(rcv)
port.flush()
port.close()
time.sleep(2)

port.open()
port.write("\x1b?T")
time.sleep(2)

try:
    rcv = port.read(21)
except serial.SerialException as e:
    rcv = None
    print("\nSerial Data Error")
    port.close()
    sys.exit(-1)
except TypeError as e:
    rcv = None
    print("\nSerial Data Error2")
    port.close()
    sys.exit(-1)    
    
rcv = re.sub("^.*\x1b0. ", "", rcv)

print("\nTime from RTC")
print repr(rcv)

try:
    systime2set = datetime.datetime.strptime(str(rcv), '%d/%m/%y %H:%M:%S')
except ValueError:
    print("\nWrong value recieved from RTC")
    port.close()
    sys.exit(-1)
    
try:
    time2set = datetime.datetime.strftime(systime2set, "%m%d%H%M%y")
except ValueError:
    time2set = "0101000000"

print("\nTime to set: "+str(time2set))

#set the os datetime
os.system("date -u " + time2set)
port.flush()
port.close()
