#!/bin/bash
#

STOREPATH="/home/pfm/pfm_store"
OWNER=`stat -c '%U' ${STOREPATH}`

#echo "owner: ${OWNER}"

if [ ${OWNER} != "pfm" ]; then
    echo "change owner"
    #mount -o rw,remount /
    chown pfm:pfm ${STOREPATH}
    #mount -o ro,remount /
else
    echo "owner already pfm"
fi

chmod g+w ${STOREPATH}
FILES="${STOREPATH}/*"
#echo "files: $FILES"

for file in $FILES; do
    #echo "check: ${file}"
    if [ "${file}" != "${STOREPATH}/lost+found" ]; then
	#echo "changing2: ${file}"
	chown -R pfm:pfm "${file}"
	chmod -R g+w "${file}"
    fi
done
