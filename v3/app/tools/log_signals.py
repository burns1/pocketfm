#!/usr/bin/python

import curses.textpad
import sqlite3
import sys
import time
from datetime import datetime
import cPickle
import general

"""
Created stolle 2016

Tools and the whole sqlite code is highly experimental. I basically copied them from
other projects and did not get the time to make it nice ...

"""


__DB = general.DEFAULT_DB       # will be overwritten by -d


stdscr = curses.initscr()
curses.noecho()
curses.halfdelay(1)           # How many tenths of a second are waited, from 1 to 255
curses.noecho()               # Wont print the input

__marker = 0
__maxrow = 0

def print_log (limit):
    conn = general.connect_safely()
    if conn is None:
        return

    c = conn.cursor()
    c.execute(__select)

    y = limit
    global __maxrow
    __maxrow = 0
    for row in c.execute(__select):
        if __maxrow == 0:
            __maxrow = row[0]

        if __marker == row[0]:
            y -= 1

        tstamp = datetime.fromtimestamp (float(row[1])).strftime("%H:%M:%S.%f")[:-2]
        sig_string = row[2]
        sender = row[3]

        val = general.sqlite_unpickle(row[4])


        line = "{:<5} {:<16} {:<40}  {:<10.10}  {:<12}  {:42.42}".format(
            row[0], tstamp , sig_string, sender[:20], type(val), str(val))

        #    __lines.append(line)

        if row[0] % 10:
            stdscr.addstr(y, 1,line)
        else:
            stdscr.addstr(y, 1,line, curses.A_BOLD)


        #stdscr.addstr(y, 1, "{:<40}  {:<10}    {:<20}".format(sig_name, updates, row[2]))
        y -= 1

    conn.close()
    stdscr.refresh()



__select = 'SELECT rowid, tstamp, sig_name, sender, val_pickle FROM SignalLog'


if len(sys.argv) > 1:
    __select += ' WHERE sig_name like "%{}%"'.format(sys.argv[1])

    for arg in sys.argv[2:]:
        __select += ' OR sig_name like "%{}%"'.format(arg)


__select += " ORDER BY ROWID DESC LIMIT 42"



while True:

    print_log(42)
    char = stdscr.getch()
    if char != curses.ERR:    # This is true if the user pressed something
        if char == curses.KEY_ENTER:
            __marker = __maxrow
        else:
            break

curses.endwin()