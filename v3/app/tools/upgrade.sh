#!/bin/bash

UPGRADE_HOST=$1

PFM_GET_SIGNAL_VALUE="/home/pfm/get_signal_value.py"

echo
echo "Performing $0 check on $1"

OS_APP_VERSION=`ssh pfm@${UPGRADE_HOST} "${PFM_GET_SIGNAL_VALUE} SIG_UPDATE_PFM_SOFTWARE_VERSION"`
if [ "X${OS_APP_VERSION}" == "X" ] ;then
	echo "Error in PFM version discovery"
	exit 1
fi
echo " Current running PFM Version (reported by SIG_UPDATE_PFM_SOFTWARE_VERSION): ${OS_APP_VERSION}"

CMD_LINE=`ssh root@${UPGRADE_HOST} "mount /boot; cat /boot/cmdline.txt ;umount /boot"`
ROOT_CMD_LINE=${CMD_LINE/*root=/root=}
ar=($ROOT_CMD_LINE)
ROOT=${ar[0]}
ROOT_PARTITION=${ROOT#root=}
if [ "X${ROOT_PARTITION}" == "X" ] ;then
	echo "Error in root partition discovery"
	exit 1
fi
echo " Root partition according to /boot/cmdline.txt: ${ROOT_PARTITION}"

SPARE_PARTITION=""
if [ "${ROOT_PARTITION}" == "/dev/mmcblk0p3" ] ;then
	SPARE_PARTITION="/dev/mmcblk0p2"
fi
if [ "${ROOT_PARTITION}" == "/dev/mmcblk0p2" ] ;then
	SPARE_PARTITION="/dev/mmcblk0p3"
fi
if [ "X${SPARE_PARTITION}" == "X" ] ;then
	echo "Error in spare partition discovery"
	exit 1
fi
echo " Spare partition set to ${SPARE_PARTITION} from /boot/cmdline.txt current ${ROOT}"

DF_OUTPUT=`ssh root@${UPGRADE_HOST} "df --output='avail' / "`
ar=($DF_OUTPUT)
ROOT_PART_SPACE=${ar[1]}
echo " Available space on root partition: ${ROOT_PART_SPACE}K"


