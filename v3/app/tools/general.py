import sqlite3
import cPickle
import time
import os

"""
Created stolle 2016

Tools and the whole sqlite code is highly experimental. I basically copied them from
other projects and did not get the time to make it nice ...

"""


#DEFAULT_DB = "/home/pfm/pfm_store/stolle_app/LOGS/CURRENT.sqlite"
DEFAULT_DB = "/tmp/pfm_log/CURRENT.sqlite"




#
# the following is used in the app as well.
#
def sqlite_pickle(data):
    return sqlite3.Binary(cPickle.dumps(data, cPickle.HIGHEST_PROTOCOL))


def sqlite_unpickle(data):
    return cPickle.loads(str(data))


def connect_safely(database=None, msg=False, exit_prg=False):
    '''
    Makes sure, there is a database and its ready already. So the tools wont break, if the app
    was restarted and created a new database ...

    :param database:
    :param msg:
    :param exit_prg:
    :return:
    '''
    if database is None:
        database=DEFAULT_DB

    if not os.path.exists(database):
        if msg:
            print database, "not there"
        if exit_prg:
            exit()
        else:
            return None

    if time.time() - os.path.getatime(database) < 2:
        if msg:
            print database, "not ready", time.time() - os.path.getatime(database)
        return None

    return sqlite3.connect(database)


def get_sys_info(key, conn=None):
    close_conn = False

    if conn is None:
        conn = sqlite3.connect(DEFAULT_DB)
        close_conn = True

    c = conn.cursor()
    c.execute("SELECT val from SysInfo where key ='{}'".format(key))

    row = c.fetchone()

    c.close()
    if close_conn:
        conn.close()

    return row[0]