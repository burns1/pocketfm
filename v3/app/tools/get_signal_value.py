#!/usr/bin/python

# import curses.textpad
import sqlite3
import sys
import general
import time

"""
Created stolle 2016

Tools and the whole sqlite code is highly experimental. I basically copied them from
other projects and did not get the time to make it nice ...

"""

__lines = []

def get_signal_value(PFMsignal):
    conn = general.connect_safely()

    if conn is None:
        return

    c = conn.cursor()

    sstr = "SELECT sig_string FROM SignalState where sig_name = ?"
    c.execute(sstr, (PFMsignal,))
    row = c.fetchone()
    if row is None:
        print "'{}' is no valid signal".format(PFMsignal)
        sys.exit(1)

    sigvalstr = "SELECT sig_name, updates, reads, sender, val_pickle FROM SignalState WHERE sig_name = ?"

    y = 1
    for row in c.execute(sigvalstr, (PFMsignal,)):
        sig_name = row[0]
        updates = row[1]
        reads = row[2]
        sender = row[3]

        data = general.sqlite_unpickle(row[4])

        # line = "{:<45} {:>5}   {:<14.14}   {:<20} {:42.42}".format(sig_name, updates, sender, type(data), str(data))
        line = str(data)

        print(line)

        y += 1

    c.execute("SELECT MAX(rowid) FROM SignalLog")
    maxs = c.fetchone()[0]

    conn.close()


__select = 'SELECT sig_name, updates, reads, sender, val_pickle FROM SignalState'

if len(sys.argv) == 2:
    __select += ' WHERE sig_name like "%{}%"'.format(sys.argv[1])
else:
    print('A signal name must be given')
    sys.exit(2)


get_signal_value(sys.argv[1])

