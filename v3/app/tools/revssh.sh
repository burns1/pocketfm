#!/bin/bash
#
#-M monitor port on the server to check if the ssh connection is alive
#it´s superseeded with inbuilt SSH2 functions: ServerAliveInterval and
#ServerAliveCountMax
#-p is the port to connect to on the server
#-q quiet mode
#-f backgrounding the ssh client
#-N Do not execute remote command
#-g allows remote hosts to connect to local forwarded ports
#port on server to connect to the connection starts with 50000
#corresponding to the serial number
#for dev machines it´s 10000
#for hardware test it was: 2525,2626,2727 and 2530-2547
#StrictHostKeyChecking=no
#for avoiding to verify host first

# for debugging ...
# export AUTOSSH_LOGFILE=/home/pfm/pfm_store/var/log/autossh.log
# export AUTOSSH_LOGLEVEL=7

PORTFILE="/home/pfm/revsshport.txt"

if [ -f $PORTFILE ]; then
    SSHPORT=`cat $PORTFILE`
else
    #better to create a random nr here?
    SSHPORT="10001"
fi
   
#option without the autossh monitoring functionality
autossh -M0 -p2323 -q -f -N -o 'StrictHostKeyChecking=no' -o 'ExitOnForwardFailure=yes' -o 'ServerAliveInterval 30' -o 'ServerAliveCountMax 8' -g -R ${SSHPORT}:localhost:22 mict@dam.mict-international.org

exit 0
