#!/usr/bin/python

import curses.textpad
import sqlite3
import sys
import general
import time

"""
Created stolle 2016

Tools and the whole sqlite code is highly experimental. I basically copied them from
other projects and did not get the time to make it nice ...

"""
stdscr = curses.initscr()
curses.noecho()
curses.halfdelay(1)           # How many tenths of a second are waited, from 1 to 255
curses.noecho()               # Wont print the input
stdscr.addstr(0, 0, "Signal  Updates  Sender  Value ... ", curses.A_BOLD)


__lines = []

def print_status(select):
    conn = general.connect_safely()

    if conn is None:
        return

    uptime = general.get_sys_info("pfm app active") - general.get_sys_info("pfm app start", conn)

    uptime_minutes = uptime / 60.0

    uptime = time.strftime("%H:%M:%S", time. gmtime(uptime)) # day!

    stdscr.addstr (0,37, "uptime: " + str(uptime), curses.A_BOLD)



    c = conn.cursor()


#    c.execute("SELECT val_string from SysInfo where info ='pfm start up'")

    # stdscr.addstr(10, 10, c.fetchone())



    y = 1
    for row in c.execute(select):
        sig_name = row[0]
        updates = row[1]
        reads = row[2]
        sender = row[3]

        data = general.sqlite_unpickle(row[4])

        line = "{:<45} {:>5}   {:<14.14}   {:<20} {:42.42}".format(sig_name, updates, sender, type(data), str(data))

        #    __lines.append(line)
        stdscr.addstr(y, 1,line)

        #stdscr.addstr(y, 1, "{:<40}  {:<10}    {:<20}".format(sig_name, updates, row[2]))
        y += 1
        if y > 42:
            stdscr.addstr (y,1, "There are more, use better filter or wait for better version ;) ")
            break

    c.execute("SELECT MAX(rowid) FROM SignalLog")
    maxs = c.fetchone()[0]


    stdscr.addstr(0, 62, "signals: {} -> {:4.0f} per minute".format(maxs, maxs / uptime_minutes), curses.A_BOLD)

    conn.close()
    stdscr.refresh()



__select = 'SELECT sig_name, updates, reads, sender, val_pickle FROM SignalState'

if len(sys.argv) > 1:
    __select += ' WHERE sig_name like "%{}%"'.format(sys.argv[1])

    for arg in sys.argv[2:]:
        __select += ' OR sig_name like "%{}%"'.format(arg)




# t = periodic.PeriodicThread(0.2, print_status, __select)
# t.start()
#
# raw_input()
# stdscr.echo()
# t.close()

while True:
    print_status(__select)
    char = stdscr.getch()
    if char != curses.ERR:    # This is true if the user pressed something
        break

curses.endwin()