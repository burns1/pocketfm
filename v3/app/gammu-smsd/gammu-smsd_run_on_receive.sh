#!/bin/bash
# Should be installed at /usr/sbin/gammu-smsd_run_on_receive.sh
#
from=$SMS_1_NUMBER
message=$SMS_1_TEXT
reply=""

source /usr/sbin/sms_command_parse_functions.sh

PIN=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_PFM_LOCK_PIN`

# echo "PIN= ${PIN}"

if test "$message" = "Ping"; then
    reply="Pong!"
else
#    reply="Y U NO PLAY PING PONG?"
     reply="$(parseSms "${PIN}" "${message}")"
fi

logger "run-on-receive from=$from message=$message reply=$reply"
logger "$reply | gammu-smsd-inject  TEXT $from"
echo "${reply}" | /usr/bin/gammu-smsd-inject TEXT "${from}" >/tmp/gammu-smsd-output.log
logger "SMS queued"
