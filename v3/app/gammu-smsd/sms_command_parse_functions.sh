#!/bin/bash
# should be installed at /usr/sbin/sms_command_parse_functions.sh

PFM_TOOLS_DIR="/home/pfm/app/tools"
PFM_TOOLS_GET_SIGVAL="${PFM_TOOLS_DIR}/get_signal_value.py"
PFM_TOOLS_SET_SIGNAL="${PFM_TOOLS_DIR}/send_signal.py"

calc() { awk "BEGIN{print $*}"; }

verifySmsPin()
{
    local pfm_pin="$1"
    local sms_message="$2"
    local pfm_pin_length=${#pfm_pin}
    local regex="\([0-9]\{${pfm_pin_length}\}\)"
    local sms_pin=`expr "$sms_message" : "$regex"`
    if [ "${sms_pin}" == "${pfm_pin}" ]; then
        echo $sms_pin
    fi
}
getAuthorisedSmsCommand()
{
    local pfm_pin="$1"
    local sms_message="$2"
    if [ ! -z $(verifySmsPin "$pfm_pin" "$sms_message") ]; then
        echo "${sms_message#$pfm_pin}"
    fi
}
parseSms()
{
    local pfm_pin="$1"
    local sms_message="$2"
    local sms_commandline=""
    local sms_command=""
    local sms_parameters=""
    local sms_valid_command="false"
    local sms_valid_parameters="false"
    local valid_command=""

    # cleanse sms_message to alphanumeric removing leading & trailing whitespace
    sms_message="$(echo -e "${sms_message}" | sed -e 's/[^[:alnum:]=\.\ ]//g' -e 's/=/\ /g' -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

    # Incorrect PIN
    sms_pin_verify=$(verifySmsPin "$pfm_pin" "$sms_message")
    if [ -z "${sms_pin_verify}" ]; then
             echo "Failed: Invalid PIN: ${sms_message}"
             return
    fi
    # validate pin & return sms command line
    sms_commandline="$(getAuthorisedSmsCommand "$pfm_pin" "$sms_message")"


    # if authorisation correct with non-empty command given
    if [ -n "${sms_commandline}" ] && [ ${#sms_message} -gt ${#pfm_pin} ]; then

        sms_commandline="$(echo -e "${sms_commandline}" | sed -e 's/^[[:space:]]*//')"
        sms_commandline="${sms_commandline^^}" # capitalise
        sms_command="${sms_commandline%%[[:space:]]*}"
        sms_parameters="$(echo -e "${sms_commandline}" | sed -e "s/^${sms_command}*//" -e 's/^[[:space:]]*//')"

        # DEFINED COMMANDS

        valid_command="STATUS"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"
             sms_valid_parameters="true"
             dark_mode=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_PFM_DARK_MODE`
             station_id=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_MAXPRO_STATIONID`
             frequency=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_MAXPRO_FREQUENCY`
             power=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_MAXPRO_POWER_PERCENT`
             input_source=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_AUDIOMANAGER_INPUT`
             wifi=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_WIFI_STATUS`
             location=`${PFM_TOOLS_GET_SIGVAL} SIG_UPDATE_GPS_POSITION`
             echo "DARK ${dark_mode}"
             echo "SID ${station_id}"
             echo "FRQ ${frequency}MHz"
             echo "PWR ${power}%"
             echo "SRC ${input_source}"
             echo "WIFI ${wifi}"
             echo "GPS ${location}"
             # echo PFM_TOOLS_GET_SIGVAL
             # echo ${station_id}
        fi

        valid_command="FRQ"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             sms_valid_command="true"

             if [ ! -z "${sms_parameters}" ]; then
                 # bash does not handle floats
                 freq_dec=`python -c "print int(${sms_parameters} * 10)"`
                 freq_dec=`expr ${freq_dec}`
                 if [ ${freq_dec} -ge 870 ] && [ ${freq_dec} -le 1080 ]; then
                     sms_valid_parameters="true"
                     freq_float=`python -c "print ${freq_dec} / 10.0"`
                     sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_MAXPRO_FREQUENCY -f -v ${freq_float} >/dev/null
                     sms_parameters=${freq_float}
                 fi
             fi
        fi

        valid_command="PWR"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             sms_valid_command="true"

             # 0-25 integer
             if [ ! -z "${sms_parameters}" ]; then
                 sms_parameters=`expr ${sms_parameters}`
                 if [ ${sms_parameters} -ge 0 ] && [ ${sms_parameters} -le 100 ]; then
                     sms_valid_parameters="true"
                     sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_MAXPRO_POWER_PERCENT -i -v ${sms_parameters} >/dev/null
                 fi
             fi
        fi

        valid_command="SRC"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             # 
             valid_parameters="USB"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
                 #echo "${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v USB"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v USB >/dev/null

                 
                 fi
             valid_parameters="ANA"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v ANALOG >/dev/null
             fi
             valid_parameters="SAT"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v SATELLITE >/dev/null
             fi
             valid_parameters="INT"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v INTERNAL >/dev/null
             fi
             valid_parameters="STREAM"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_AUDIOMANAGER_INPUT -v STREAM >/dev/null
             fi
        fi

        valid_command="WIFI"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             #
             valid_parameters="CLIENT"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_WIFI_MODE -v CLIENT >/dev/null
                 # set wifi
                 # echo "Setting Wifi AP mode to $valid_parameters"
             fi
             valid_parameters="AP"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_WIFI_MODE -v 'ACCESS POINT' >/dev/null
                 # set wifi
                 # echo "Setting Wifi AP mode to $valid_parameters"
             fi
             valid_parameters="OFF"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_WIFI_MODE -v OFF >/dev/null
                 # set wifi
                 # echo "Setting Wifi mode to $valid_parameters"
             fi
        fi

        valid_command="GSM"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             #
             valid_parameters="ON"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_3G_MODE -v 'ON' >/dev/null
                 echo "GSM mode $valid_parameters"
             fi
             valid_parameters="OFF"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_3G_MODE -v 'OFF' >/dev/null
                 echo "GSM mode $valid_parameters"
             fi
        fi

        valid_command="DARK"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             #
             valid_parameters="ON"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_PFM_DARK_MODE -i -v 1 >/dev/null
                 echo "Dark mode $valid_parameters"
             fi
             valid_parameters="OFF"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 sudo -u pfm ${PFM_TOOLS_SET_SIGNAL} -s SIG_SET_PFM_DARK_MODE -i -v 0 >/dev/null
                 echo "Dark mode $valid_parameters"
             fi
        fi
#  as per issue #48 this has been disabled until specified in design
#       valid_command="SHUTDOWN"
#       if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
#            if [ ${#sms_command} != ${#valid_command} ]; then
#                sms_parameters="${sms_command:${#valid_command}}"
#            fi
#            # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
#            sms_valid_command="true"
#            sms_valid_parameters="true"
#            # echo "Shutting down"
#       fi
    fi

    # CATCH ALL INVALID COMMAND
    if [ "${sms_valid_command}" != "true" ]; then
         echo "Failed: Invalid Command \"${sms_command} ${sms_parameters}\""
    else
        # CATCH ALL INVALID PARAMETERS
        if [ "${sms_valid_parameters}" != "true" ]; then
           echo "Failed: Invalid Parameters for Command \"${sms_command} ${sms_parameters}\""
        fi
    fi

    if [ "${sms_valid_command}" == "true" ] && [ "${sms_valid_parameters}" == "true" ]; then
        echo "Success: ${sms_command} ${sms_parameters}"
    fi

}    

if [ "$1" == "test" ]; then 
    pin="123456"; sms="123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="213456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="12345 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="123456HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms=" 123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms=""; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="123456"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="1234566"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="123456 "; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    pin="123456"; sms="12345"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
    echo " ---- "
    pin="123456"; sms="123456 STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="666666 STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="666666STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR 0"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR0"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR  1 "; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR 21"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 FRQ=101.9"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 FRQ 100"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 FRQ 26"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="1234567"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="12345"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="1234"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 IN SAT"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 IN USB"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 IN HD"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 IN AN"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 IN HORSE"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 WIFI ON"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 WIFI=OFF"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 WIFI=OFF extraparam"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 SHUTDOWN"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 SHUTDOWNextraending"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 INVALIDCOMMAND INVALID PARAMETER"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
#   pin="123456"; sms="123456 "; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
fi
