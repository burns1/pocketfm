#!/usr/bin/python
# -*- coding: utf-8-unix;

from threading import Thread
from pydispatch import dispatcher
from multiprocessing import Process, Queue
try:
    from queue import Empty
except ImportError:
    from Queue import Empty
import serbus
import sys
import socket
import re
import ast
import time
import Queue
import logging

import os

import pfm_signals

class MaxProBoard(Thread):
    '''MaxProBoard defines attributes and methods to handle audio input, FM transmission and RDS functions provided by the MaxPro hardware connected via i2c'''
    def __init__(self):
        super(MaxProBoard, self).__init__()

        # turn myself on
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, True)

        # The MaxProBoard over i2c exhibits read/write errors.
        self.i2cWriteSuccesses = 0
        self.i2cWriteErrors = 0
        self.i2cReadSuccesses = 0
        self.i2cReadErrors = 0

        # I2C specific
        self.i2cBus = 1
        self.i2cAddress = 0x40
        self.i2cReadLength = 25
        self.i2cWriteLength = 111
        self.i2cReadBlock =  [32] * self.i2cReadLength
        self.i2cWriteBlock = [32] * self.i2cWriteLength
        self.i2cForceWrite = False
        try:
            self.i2cHandle = serbus.I2CDev(self.i2cBus)
            self.i2cHandle.open()
        except IOError:
            logging.info("I2C connection failed.")
            sys.exit(2)

        self.closing = False # used for closing io thread

        # Writable Components
        self.TxPower = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, 0)

        self.Frequency = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, 87.0)

        self.ExtAmpCurrAlarm = 25  # 
        self.AlarmSWR = 8
        self.AlarmTemperature = 7
        self.ExtAmpVoltAlarm = 20
        self.AudioMode = 1 # 0 = mono / 1=stereo
        self.ExtAmpPowerLimit = 28
        self.RDSEnable = 1
        self.RDSECCFlags = 0
        self.RDSPTY = 0 # RDS Program Type
        self.RDSTP = 0  # RDS Traffic Programme Identification
        self.RDSTA = 0  # RDS Traffic Announcement Identification
        self.RDSMS = 1  # RDS Music/Speech 0=Music 1=Speech

        self.RDS = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'PocketFM')  # 8 byte RDS Programme Service Name

        self.SiLabsAudioSetting = 1
        self.SiLabsCompressorLimiter = 3
        self.SiLabsCompressionRate = 8
        self.SiLabsThreshold = 10
        self.SiLabsCompressionAttack = 0
        self.SiLabsCompressionDecay = 5
        self.SiLabsPreemphasis = 1
        self.SiLabsInputGain = 1
        self.SiLabsInputMute = 0
        self.RDSRtCtEON = 0
        self.RDSECC = 224
        self.RDSPIlo = 0
        self.RDSPIhi = 0
        self.RDSAF = 0

        self.SiLabsAudioInput = 0

        self.SiLabsStereoPilotLevel = 4
        # self.RDSRadioText = 'Radio Isla Negra ... live from Chile\n' # 64 bytes ASCII terminated by 0x0D, padded by 0x20 (spaces)
        self.RDSRadioText = '\n{}'.format([32] * 63) # 64 bytes ASCII default 0x0D, padded by 63 0x20 (spaces)
        self.SiLabsMPXDeviation = 10
        self.RDSPilotLevel = 6
        self.Hour = 0
        self.Minute = 0
        self.Second = 0
        self.Year = 0
        self.Month = 0
        self.Day = 0

        # Readable Values
        self.OutputPower = -1.0
        self.ReflectedPower = -1.0
        self.Temperature = 0.0
        self.Voltage = 0.0
        self.ExtAmpVoltage = 0.0
        self.UptimeDay = 0
        self.UptimeHour = 0
        self.UptimeMinute = 0
        self.ErrorStatus = 0
        self.ExtAmpTemp = 0.0
        self.ExtAmpStatus = 0
        self.ExtAmpSwitch = 0
        self.ExtAmpValue = 0.0
        self.ReadFrequency = 0.0
        self.VULeft = ''
        self.VURight = ''

        # Route set events
        dispatcher.connect(self.handle_set_frequency, signal=pfm_signals.SIG_SET_MAXPRO_FREQUENCY, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_txpower, signal=pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_input_source, signal=pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_stationid, signal=pfm_signals.SIG_SET_MAXPRO_STATIONID, sender=dispatcher.Any)

        # internal signal handler to force write on power up of maxproboard
        dispatcher.connect(self.handle_set_forcewrite, signal=pfm_signals.SIG_SET_MAXPRO_FORCEWRITE, sender=dispatcher.Any)

        self.PreReadSleeptime = 0.5
        self.PreWriteSleeptime = 1.0

        self.RDS = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_STATIONID, socket.gethostname())
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, self, self.RDS)
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, self, self.RDS)

        self.Frequency = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, 88.0)
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, self, self.Frequency)
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, self, self.Frequency)

        self.TxPower = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, 0)
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, pfm_signals.SENDER_MAXPRO, int (self.TxPower))
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, pfm_signals.SENDER_MAXPRO, int (self.TxPower))

        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_DAY, pfm_signals.SENDER_MAXPRO, self.UptimeDay)
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_HOUR, pfm_signals.SENDER_MAXPRO, self.UptimeHour)
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_ERROR_STATUS, pfm_signals.SENDER_MAXPRO, self.ErrorStatus)
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_MINUTE, pfm_signals.SENDER_MAXPRO, self.UptimeMinute)



    def ReadMaxProBlock(self):
        try:
            MaxProBlock = self.i2cHandle.read(self.i2cAddress, self.i2cReadLength)
        except IOError as e:
            logging.info('I2C read failed!'.format(e))
            self.i2cReadErrors += 1
            dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_I2C_READ_ERRORS, pfm_signals.SENDER_MAXPRO, self.i2cReadErrors)
            return None
        else:
            self.i2cReadSuccesses += 1
            dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_I2C_READ_SUCCESSES, pfm_signals.SENDER_MAXPRO, self.i2cReadSuccesses)
            return MaxProBlock

    def WriteMaxProBlock(self, MaxProBlock):
        if MaxProBlock != self.i2cWriteBlock or self.i2cWriteSuccesses == 0 or self.i2cForceWrite == True:
            try:
                self.i2cHandle.write(self.i2cAddress, MaxProBlock)
            except IOError as e:
                logging.info('I2C write error! {}'.format(e))
                self.i2cWriteErrors += 1
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_I2C_WRITE_ERRORS, pfm_signals.SENDER_MAXPRO, self.i2cWriteErrors)
            else:
                if MaxProBlock[0] != self.i2cWriteBlock[0]:
                    dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, pfm_signals.SENDER_MAXPRO, int (self.TxPower))
                if MaxProBlock[1] != self.i2cWriteBlock[1] or MaxProBlock[2] != self.i2cWriteBlock[2]:
                    dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, pfm_signals.SENDER_MAXPRO, self.Frequency)
                if MaxProBlock[15:23] != self.i2cWriteBlock[15:23]:
                    dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, pfm_signals.SENDER_MAXPRO, self.RDS)
                if MaxProBlock[37] != self.i2cWriteBlock[37]:
                    dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT, pfm_signals.SENDER_MAXPRO, self.SiLabsAudioInput)
                self.i2cWriteBlock = MaxProBlock
                self.i2cWriteSuccesses += 1
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_I2C_WRITE_SUCCESSES, pfm_signals.SENDER_MAXPRO, self.i2cWriteSuccesses)
                # if True, self.i2cForceWrite is a one shot write, that must be cleared on a successful write
                if self.i2cForceWrite == True:
                    self.i2cForceWrite = False
                    dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FORCEWRITE, self, False)

    def run(self):
        try:
            while not self.closing:
                # Read MaxProBoard values
                if self.PreReadSleeptime:
                    time.sleep(self.PreReadSleeptime)
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD) == True:
                    inBlock = self.ReadMaxProBlock()
                    if inBlock != None:
                        if inBlock != self.i2cReadBlock:
                            self.ParseReadMaxProBlock(inBlock)
                            self.i2cReadBlock = inBlock
                            # logging.debug('DEBUG I2C: read block change detected')
                        # else:
                        #     logging.debug('DEBUG I2C: read block identical')
                    # else:
                    #     logging.debug('ReadMaxProBlock returned None')

                if self.PreWriteSleeptime:
                    time.sleep(self.PreWriteSleeptime)
                outBlock = self.ParseWriteMaxProBlock()
                if outBlock != self.i2cWriteBlock or self.i2cWriteSuccesses == 0 or self.i2cForceWrite == True:
                    logging.debug('DEBUG I2C: write block')
                    if pfm_signals.get_value(pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD) == True:
                        self.WriteMaxProBlock(outBlock)
                        self.i2cWriteBlock = outBlock
                # else:
                #     logging.debug('DEBUG I2C: write no change')

        except Queue.Empty:
            pass
        except (KeyboardInterrupt, SystemExit):
            pass
        self.close()

    def ParseReadMaxProBlock(self, newBlock):
        try: 
            # read transmission output power in Watts
            if self.OutputPower != self.decode_word(newBlock[0], newBlock[1]):
                if self.decode_word(newBlock[0], newBlock[1]) < 100.0: # Discard insane input
                    self.OutputPower = self.decode_word(newBlock[0], newBlock[1])
                    dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_OUTPUT_POWER, pfm_signals.SENDER_MAXPRO, self.OutputPower)
            # read reflected power in Watts
            if self.ReflectedPower != self.decode_word(newBlock[2], newBlock[3]):
                self.ReflectedPower = self.decode_word(newBlock[2], newBlock[3])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_REFLECTED_POWER, pfm_signals.SENDER_MAXPRO, self.ReflectedPower)
            # read exciter temperature in Celcius
            if self.Temperature != self.decode_word(newBlock[4], newBlock[5]):
                self.Temperature = self.decode_word(newBlock[4], newBlock[5])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_TEMPERATURE, pfm_signals.SENDER_MAXPRO, self.Temperature)
            # read exciter voltage
            if self.Voltage != self.decode_word(newBlock[6], newBlock[7]):
                self.Voltage = self.decode_word(newBlock[6], newBlock[7])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_VOLTAGE, pfm_signals.SENDER_MAXPRO, self.Voltage)
            # read uptime day
            if self.UptimeDay != self.decode_word(newBlock[10], newBlock[11]):
                self.UptimeDay = self.decode_word(newBlock[10], newBlock[11])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_DAY, pfm_signals.SENDER_MAXPRO, self.UptimeDay)
            # read uptime hour
            if self.UptimeHour != self.decode_word(newBlock[12], 0x0b):
                self.UptimeHour = self.decode_word(newBlock[12], 0x0b)
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_HOUR, pfm_signals.SENDER_MAXPRO, self.UptimeHour)
            # read uptime minutes
            if self.UptimeMinute != self.decode_word(newBlock[13], 0x0b):
                self.UptimeMinute = self.decode_word(newBlock[13], 0x0b) 
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_MINUTE, pfm_signals.SENDER_MAXPRO, self.UptimeMinute)
            # read VU Left
            if self.VULeft != str(newBlock[23]):
                self.VULeft = str(newBlock[23])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_VU_LEFT, pfm_signals.SENDER_MAXPRO, self.VULeft)
            # read VU Right
            if self.VURight != str(newBlock[24]):
                self.VURight = str(newBlock[24])
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_VU_RIGHT, pfm_signals.SENDER_MAXPRO, self.VURight)
            # read Frequency in MHz
            if self.ReadFrequency != (newBlock[21] + newBlock[22]*256) * (5.0/1000):
                self.ReadFrequency = (newBlock[21] + newBlock[22]*256) * (5.0/1000)
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_READ_FREQUENCY, pfm_signals.SENDER_MAXPRO, self.ReadFrequency)
            # read error status bits
            if self.ErrorStatus != newBlock[14]:
                self.ErrorStatus = newBlock[14]
                dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_ERROR_STATUS, pfm_signals.SENDER_MAXPRO, self.ErrorStatus)

        except (SyntaxError, ValueError) as e:
            self.i2cReadErrors += 1
            dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_I2C_READ_ERRORS, pfm_signals.SENDER_MAXPRO, self.i2cReadErrors)
            logging.info('MaxProBoard I2C Soft read error {} with {} successes'.format(self.i2cReadErrors, self.i2cReadSuccesses))

    def ParseWriteMaxProBlock(self):
        WriteMaxProBlock = [32] * self.i2cWriteLength 

        WriteMaxProBlock[0]  = int (43 * self.TxPower / 100)
        WriteMaxProBlock[1]  = int(self.Frequency * 1000 / 5) % 256
        WriteMaxProBlock[2]  = int((self.Frequency * 1000 / 5) / 256)
        WriteMaxProBlock[3]  = 25
        WriteMaxProBlock[4]  = 7 # SWR Alarm 
        WriteMaxProBlock[5]  = self.AlarmTemperature # Temperature alarm on MP representative in steps of 5 & 10 degrees
        WriteMaxProBlock[6]  = 20 # External Amp Alarm 
        WriteMaxProBlock[7]  = 1 # Audio mode 0 = mono, 1 = stereo
        WriteMaxProBlock[8]  = 28 # External Amp Power Limit
        WriteMaxProBlock[9]  = self.RDSEnable # 0=off, 1=on
        WriteMaxProBlock[10] = self.RDSECCFlags # RDS ECC Flags default = 1
        WriteMaxProBlock[11] = self.RDSPTY      # RDS Program Type
        WriteMaxProBlock[12] = self.RDSTP       # RDS Traffic Program
        WriteMaxProBlock[13] = self.RDSTA       # RDS Traffic Announcement
        WriteMaxProBlock[14] = self.RDSMS       # RDS Music/Speech 0/1

        for i in xrange(8):
            try:
                WriteMaxProBlock[15 + i] = ord(self.RDS[i])      # RDS Service/Station Name
            except IndexError:
                WriteMaxProBlock[15 + i] = ord(' ')

        WriteMaxProBlock[23] = self.SiLabsAudioSetting
        WriteMaxProBlock[24] = self.SiLabsCompressorLimiter
        WriteMaxProBlock[25] = self.SiLabsCompressionRate
        WriteMaxProBlock[26] = self.SiLabsThreshold
        WriteMaxProBlock[27] = self.SiLabsCompressionAttack
        WriteMaxProBlock[28] = self.SiLabsCompressionDecay
        WriteMaxProBlock[29] = self.SiLabsPreemphasis
        WriteMaxProBlock[30] = self.SiLabsInputGain
        WriteMaxProBlock[31] = self.SiLabsInputMute
        WriteMaxProBlock[32] = self.RDSECC
        WriteMaxProBlock[33] = self.RDSPIlo
        WriteMaxProBlock[34] = self.RDSPIhi

        WriteMaxProBlock[35] = self.RDSRtCtEON
        WriteMaxProBlock[36] = self.RDSAF
        WriteMaxProBlock[37] = self.SiLabsAudioInput # 0=analog/1=analog ext/2=dig 32KHz/3=dig 40KHz/4=dig 44.1KHz/5=dig 48KHz
        WriteMaxProBlock[38] = self.SiLabsStereoPilotLevel
        for i in xrange(64):
            try:
                WriteMaxProBlock[39 + i] = ord(self.RDSRadioText[i])
            except:
                WriteMaxProBlock[39 + i] = ord(' ')
        WriteMaxProBlock[103] = self.SiLabsMPXDeviation
        WriteMaxProBlock[104] = self.RDSPilotLevel
        WriteMaxProBlock[105] = self.Hour
        WriteMaxProBlock[106] = self.Minute
        WriteMaxProBlock[107] = self.Second
        WriteMaxProBlock[108] = self.Year
        WriteMaxProBlock[109] = self.Month
        WriteMaxProBlock[110] = self.Day
        return WriteMaxProBlock

    #conversions
    def decode_word(self, first_byte, second_byte):
        """
        Applies string substitution on hex representation of
        - temperature
        - power
        - voltage
        values returned by MaxPro.

        See MaxPro API.

        Example: 26*256 + 80 = 6736 = 0x1A50 means 1.5W power
        Mainly some string substitution here. means .
        b and c are just skipped. Only relevant for the special
        maxpro display.
        """
        retval = 0
        word = re.sub('^0x', '', hex(first_byte)) + re.sub('^0x', '', hex(second_byte))
        word = re.sub('a', '.', word)
        word = re.sub('b', '', word)
        word = re.sub('c', '', word)
        # retval = ast.literal_eval(word)
        try:
            retval = ast.literal_eval(word)
        except (SyntaxError, ValueError) as e:
            # logging.debug('DEBUG: maxproboard.py: Caught Syntax/Value Error in ({}) decode_word(): {}'.format(word, e))
            self.i2cReadErrors = self.i2cReadErrors + 1
            pass
        return retval

    def close(self):
        self.closing = True

    # an event handler to listen to write requests for setting FM transmission frquency
    def handle_set_frequency(self, frequency, signal, sender):
        """Dispatcher handler to type and range check of frequency.
        Frequency should be a number between 87.0 and 108.0 as this is
        frequency range in Mhz.
        """
        # consider sanity checking for signal type & sender
        validp = isinstance(frequency, (int, long, float)) and frequency >= 87.0 and frequency <= 108.0
        if not validp:
            print('DEBUG: maxproboard.py: Frequency value {} not valid!'.format(frequency))
            return
        else:
            self.Frequency = frequency

    # an event handler to listen to write requests for setting FM transmission power
    def handle_set_txpower(self, power, signal, sender):
        """Accepts power as a number from 0 to 100 as ar range from 0% to 100%.
        Scaled to the range 0-43 as documented in the MaxPro API.
        """
        # consider sanity checking for signal type & sender
        validp = isinstance(power, (int, long, float)) and power >= 0 and power <= 100
        if not validp:
            print('DEBUG: maxproboard.py: Power value {} is not valid!'.format(power))
            return
        else:
           self.TxPower = power 

    # an event handler to listen to write requests for setting audio input type
    def handle_set_input_source(self, input_source, signal, sender):
        """Accepts 'analog' and 'digital' as string and returns the
        accoding SiLabs Audio byte value. See MaxPro API.
        """
        # consider sanity checking for signal type & sender
        if input_source == 'analog':
            self.SiLabsAudioInput = 0
            self.AudioInputType = 'analog'
            # print('DEBUG: SILABS Analog')
        else:
            self.SiLabsAudioInput = 4
            self.AudioInputType = 'digital'
            # print('DEBUG: SILABS Digital')
        dispatcher.send(pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT, self, self.AudioInputType)

    # an event handler to listen to write requests for setting Station ID (aka RDS PN)
    def handle_set_stationid(self, station_id, signal, sender):
        """Accepts a string, strips whitespace & uses first 8 chars for station ID
        """
        # consider sanity checking for signal type & sender

        if station_id == "" or station_id == None:
            self.RDSEnable = 0
        else:
            self.RDSEnable = 1
            station = station_id.strip()
            self.RDS = station[:8]

    def handle_set_forcewrite(self, state, signal, sender):
        """This signal forces attempt to write to MAXPROBOARD until successful write occurs
        """
        if state == True:
            self.i2cForceWrite = True
        else:
            self.i2cForceWrite = False

# example event handler
def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender

def main():
    from pfm import PocketFM

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout
    # m = MaxProBoard()
    # m.start()

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_POWER_MAXPROBOARD,          sender=dispatcher.Any)

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,        sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_STATIONID,        sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT,    sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT,      sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_REFLECTED_POWER,  sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_TEMPERATURE,      sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_VOLTAGE,          sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_DAY,       sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_HOUR,      sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_UPTIME_MINUTE,    sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_ERROR_STATUS,     sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_READ_FREQUENCY,   sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_VU_LEFT,          sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_VU_RIGHT,         sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_ERROR_STATUS,     sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_READ_FREQUENCY,   sender=dispatcher.Any)

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_MAXPRO_FREQUENCY,           sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_MAXPRO_STATIONID,           sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_MAXPRO_OUTPUT_POWER,        sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT,         sender=dispatcher.Any)

    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'maxpro main()', True)    
    cnt = 0
    try:
        while True:
            cnt += 1
            time.sleep(1)
            if cnt == 1: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'maxproboard Main()', 'analog')
            if cnt == 4: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'maxproboard Main()', 'digital')
            try:
                if cnt ==  5:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, 'maxproboard main()', 80)
            except ValueError:
                print 'Tested invalid frequency value'
            if cnt ==  6:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, 'maxproboard main()', 100.1)
            if cnt ==  4:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'HAPPY')
            if cnt ==  8:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'SAD')
            if cnt ==  12: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'Average')
            if cnt ==  16: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'PocketFMPicketFMPacketFM')
            if cnt % 11 == 0: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'HAPPY')
            if cnt % 13 == 7: dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, 'maxproboard main()', 'PacketFM')
            m.RDSPTY = cnt % 31
            # print 'm.RDSPTY = ', m.RDSPTY
            if cnt ==  6:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, 'maxproboard main()', 72)
            if cnt ==  9:  dispatcher.send(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, 'maxproboard main()', 80)
    except (KeyboardInterrupt, SystemExit):
        m.close()
        print "\nInterrupt: m.is_live: {} m.closing: {} i2c write errors = {}".format(m.is_alive(), m.closing, m.i2cWriteErrors)
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'maxpro main()', False)
    finally:
        m.close()
    m.join()

if __name__ == "__main__":
    main()
