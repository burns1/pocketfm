#!/usr/bin/python

from pydispatch import dispatcher
import pfm_signals

class Encoder(object):
    def __init__(self):

        self.BBQuery = "\x1b?E"

        self.Rotator = 0
        self.RotatorMaximum = 255
        # self.Rotator_changed = pfm_signals.SIG_ENCODER_ROTATOR_ANALOG

        self.RotatorLeftRight = 0
        # self.RotatorTurnedLeft  = pfm_signals.SIG_ENCODER_ROTATOR_LEFT
        # self.RotatorTurnedRight = pfm_signals.SIG_ENCODER_ROTATOR_RIGHT

        self.Button = 0
        # self.ButtonPressed  = pfm_signals.SIG_ENCODER_BUTTON_PRESSED
        # self.ButtonReleased = pfm_signals.SIG_ENCODER_BUTTON_RELEASED

    def updateAnalogRotator(self, rotation):
        if rotation != self.Rotator:
            if self.Rotator == self.RotatorMaximum and rotation == 0: # right overflow
                self.Rotator = rotation
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_RIGHT, self, self.Rotator)
            elif self.Rotator == 0 and rotation == self.RotatorMaximum: # left overflow
                self.Rotator = rotation
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_LEFT, self, self.Rotator)
            if rotation > self.Rotator:
                self.Rotator = rotation
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_RIGHT, self, self.Rotator)
            elif rotation < self.Rotator:
                self.Rotator = rotation
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_LEFT, self, self.Rotator)
            self.Rotator = rotation
            dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_ANALOG, self, self.Rotator)

    def updateButton(self, state):
        if state != 0 and self.Button == 0:
            self.Button = state & 0xC0
            dispatcher.send(pfm_signals.SIG_ENCODER_BUTTON_PRESSED, self, self.Button)
        if state == 0 and self.Button != 0:
            self.Button = state & 0xC0
            dispatcher.send(pfm_signals.SIG_ENCODER_BUTTON_RELEASED, self, self.Button)

    def updateDigitalRotatorLeftRight(self, state):
        self.RotatorLeftRight = state & 0x30
        if self.RotatorLeftRight & 0x30 != 0x30:
            if state & 0x10:
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_RIGHT, self, self.RotatorLeftRight)
            if state & 0x20:
                dispatcher.send(pfm_signals.SIG_ENCODER_ROTATOR_LEFT, self, self.RotatorLeftRight)

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main():
    import sys
    import time
    from pfm import PocketFM

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_ROTATOR_RIGHT, sender=dispatcher.Any)
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_ROTATOR_LEFT, sender=dispatcher.Any)
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_BUTTON_PRESSED, sender=dispatcher.Any)
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_BUTTON_RELEASED, sender=dispatcher.Any)
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_ROTATOR_ANALOG, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_RIGHT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_LEFT, sender=dispatcher.Any)

    time.sleep(200)


if __name__ == "__main__":
    main()
