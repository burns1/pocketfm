#!/usr/bin/python

import serial
import time
import sys
import subprocess
import gsm
import wifi
from pydispatch import dispatcher
import pfm_signals
import usb_storage
import power
import logging
import socket
import fcntl
import struct

cnt = 0
#sys.stdout = open('/dev/console', 'w')
hwlogfile_path = "/home/pfm/pfm_store/hw_test.log"

#clear the log
with open(hwlogfile_path, 'w'):
    pass

#configure log
logging.basicConfig(filename=hwlogfile_path,level=logging.DEBUG)

def parse_angle(str_retval):
    enc_seq = str_retval.split('\\x1b')
    enc_angles = []
    i = 0
    rl_set = 0
    if enc_seq[0] == "\'":
        del enc_seq[0]
    if len(enc_seq) > 0:
        for i,enc_pos in enumerate(enc_seq):
            if enc_pos != "\'":
                #print("Encoder Positions: "+str(enc_seq))
                enc_angle = enc_pos.split(",")
                print("Encoder angle: "+str(enc_angle[0])+" "+str(i))
                logging.info("Encoder angle: "+str(enc_angle[0])+" "+str(i))
                enc_angles.append(enc_angle[0])
        if len(enc_angles) > 1:
            #flip back on 0
            if int(enc_angles[0]) == 0 and int(enc_angles[1]) == 255:
                rl_set = -1
            elif int(enc_angles[1]) < int(enc_angles[0]):
                #print("Encoder turn left success!")
                rl_set = -1
            elif int(enc_angles[1]) > int(enc_angles[0]):
                #print("Encoder turned right?")
                rl_set = 1
            else:
                if len(enc_angles) > 1:
                    if int(enc_angles[2]) > int(enc_angles[1]):
                        rl_set = 1
                    elif int(enc_angles[2]) < int(enc_angles[1]):
                        rl_set = -1

    return rl_set

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]

def getIPNr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])    


#running the test
while cnt == 0:
    cnt =+ 1

    port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)
    #check BOB, get version
    print("Info: Check breakout board")
    port.write("\x1b?G\x1b253P")
    time.sleep(3)
    port.flushInput()

    port.write("\x1b?V")
    time.sleep(3)
    rcv = port.read(19)
    #old version: 6,2,10,44217
    #new one: 7,2,10,36988
    #if rcv == "\x1b7,2,10,36988V":
    #    print("Success: Software Version reported as "+repr(rcv))
    #    logging.info("Success: Software Version reported as "+repr(rcv))
    #    port.write("\r\nSW version: "+repr(rcv))
    #elif rcv == "":
    #    print("Probably no BoB\n")
    #    logging.info("Probably no BoB\n")
    #    port.write("\r\nProbably no BoB")
    #    sys.exit(-1)   
    #else:
    #    print("Failure: Software Version: "+repr(rcv)+"\n")
    #    print("Wrong software version or no BoB\n")
    #    logging.info("Failure: Software Version: "+repr(rcv)+"\n")
    #    port.write("\r\nErr: SW version: "+repr(rcv))
    time.sleep(3)
        
    #switch everything on
    print("\nInfo: Power everything on")
    logging.info("\nInfo: Power everything on")
    port.write("\r\nPower everything on")
    port.write("\x1b255,0P")
    time.sleep(3)
    port.write("\r\nSW version: "+repr(rcv))

    u = usb_storage.USBStorage()
    time.sleep(3)
    port.write("\x1b207P")
    time.sleep(3)
    rcv = port.read(19)
    print repr(rcv)
    port.write("\x1b255P")
    rcv = port.read(19)
    print repr(rcv)
    time.sleep(3)
    print('USB Storage device detection state {}'.format(u.Active))
    logging.info('USB Storage device detection state {}'.format(u.Active))
    port.write("\r\nUSB Storage: {}".format(u.Active))
    
    #read the power mode back
    #port.write("\x1b?P")
    #rcv = port.read(35)
    #print("\nPower mode bit: "+repr(rcv))
    
    #set leds
    print('\nInfo: LED Test - visual check required')
    port.write("\r\nLED Test")
    led_color = 2
    led_colors = {0,1,2,3,4,5,6,7}

    for lcolor in led_colors:
        #led_value2 = ((2 | (2 << 3) | (2 << 6) | (2 << 9) | (2 << 12)))
        led_value2 = ((lcolor | (lcolor << 3) | (lcolor << 6) | (lcolor << 9) | (lcolor << 12)))
        print("Info: LED value "+str(led_value2))
        #port.write("\x1bL")
        port.write("\x1b"+str(led_value2)+",200L")
        time.sleep(0.5)

    #activate and check GPS
    print("\nInfo: Activate and check GPS\n")
    port.write("\x1bG")
    rcv = port.read(40)
    if rcv != "":
        print("Success: GPS active: "+repr(rcv))
        logging.info("Success: GPS active: "+repr(rcv))
        port.write("\r\nGPS OK")
    else:
        print('Failure: GPS Output not detected')
        logging.info('Failure: GPS Output not detected')
        port.write("\r\nGPS ERR")
        #sys.exit(-1)
    port.write("\x1b?G")
    port.write("\x1b253P")
    port.flushInput()

    #read ADC
    print("\nInfo: Read ADC")
    port.write("\x1b\x3fI")
    rcv = port.read(128)
    print("Success: ADC values: "+repr(rcv))
    logging.info("Success: ADC values: "+repr(rcv))
    port.write("\r\nADC OK")
    
    #Encoder-Test
    #enc_cnt = 0
    test_score = 0
    
    #read Encoder
    last_enc = 0
    print("\nInfo: Read Encoder")
    port.write("\x1b\x3fE")
    rcv = port.read(128)
    last_enc = repr(rcv)
    print("Success: Got encoder values: "+repr(rcv))
    logging.info("Success: Encoder values: "+repr(rcv))
    port.write("\r\nEnc got some data")
    timeout_wait = 5

    time.sleep(1)

    #Turn Encoder left
    port.write("\r\nTurn the encoder left")
    print("Turn the encoder left")
    for waitfor in range(0,timeout_wait+1):
        port.write("\x1b\x3fE")
        rcv = port.read(128)
        print("Encoder Value: "+repr(rcv))
        if repr(rcv) != last_enc and repr(rcv) != "":
            break
        time.sleep(1)

    if repr(rcv) == last_enc or repr(rcv) == "" or waitfor >= timeout_wait:
        port.write("\r\nTime out Enc left: "+repr(rcv))
        print("Turn left failed. TimeOut counter: "+str(waitfor))
        logging.info("Time out or prob with Enc left: "+repr(rcv))
    else:
        #port.write("\r\nGot some data")
        logging.info("L:Got some data on enc")
        print("Got data on encoder: "+repr(rcv))
        #print("Turn left data. TimeOut counter: "+str(waitfor))
        enc_retvals = str(repr(rcv))
        angle_check = parse_angle(enc_retvals)

        if angle_check < 0:
            print("Encoder turned left success!")
            port.write("\r\nL:Enc turn left OK")
            test_score += 1
        elif angle_check > 0:
            print("Encoder turned right?")
            port.write("\r\nL:Enc turn right?")
        else:
            print("No changes angle")
            port.write("\r\nL:No changes angle")

    #get encoder val again
    time.sleep(1)
    port.write("\x1b\x3fE")
    rcv = port.read(128)
    last_enc = repr(rcv)
    print("Encoder Value: "+repr(rcv))

    #Turn Encoder right
    port.write("\r\nTurn encoder right")

    for waitfor in range(0,timeout_wait+1):
        port.write("\x1b\x3fE")
        rcv = port.read(128)
        print("Encoder Value: "+repr(rcv)+"TimeOut count: "+str(waitfor))
        if repr(rcv) != last_enc and repr(rcv) != "":
            break
        time.sleep(1)

    if repr(rcv) == last_enc or repr(rcv) == "" or waitfor >= timeout_wait:
        port.write("\r\nR:Time out")
        logging.info("Time out or prob with Enc right: "+repr(rcv))
        print("Turn right failed. TimeOut counter: "+str(waitfor))
    else:
        #port.write("\r\nR:Got some data")
        logging.info("Section right: Got data on encoder: "+repr(rcv))
        last_enc = repr(rcv)
        print("Section right: Got data on encoder:"+repr(rcv))
        print("Turn right data. TimeOut counter: "+str(waitfor))

        enc_retvals = str(repr(rcv))
        angle_check = parse_angle(enc_retvals)

        if angle_check < 0:
            print("Encoder turned left?")
            port.write("\r\nR:Encoder turned left?")
        elif angle_check > 0:
            print("Encoder turned right OK")
            port.write("\r\nR:Enc turn right OK")
            test_score += 1
        else:
            print("No changes regarding the angle detected")
            port.write("\r\nR:No changes angle")

    #get encoder val again
    time.sleep(1)
    port.write("\x1b\x3fE")
    rcv = port.read(128)
    last_enc = repr(rcv)
    print("Encoder Value: "+repr(rcv))

    #Encoder Push
    print("Push encoder button!")
    port.write("\r\nPush encoder button!")

    for waitfor in range(0,timeout_wait+1):
        port.write("\x1b\x3fE")
        rcv = port.read(128)
        print("Encoder Value: "+repr(rcv)+"TimeOut count: "+str(waitfor))
        #port.write("Idling: "+str(waitfor))
        if repr(rcv) != last_enc and repr(rcv) != "":
            break
        time.sleep(1)

    if repr(rcv) == last_enc or repr(rcv) == "" or waitfor >= timeout_wait:
        port.write("\r\nTime out Enc push")
        logging.info("Time out or prob with Enc push: "+repr(rcv))
        print("Push failed. TimeOut counter: "+str(waitfor))
    else:
        port.write("\r\nSuccess push!")
        logging.info("Success push: "+repr(rcv))
        last_enc = repr(rcv)
        print("Success push: "+repr(rcv))
        print("Push success. TimeOut counter: "+str(waitfor))
        test_score += 1

    if test_score == 3:
        port.write("\r\nEnc test succeeded")
    else:
        port.write("\r\nEnc test fail:"+str(test_score))

        
    #check maxpro
    #check for presence of bus
    print('\nInfo: Maxpro Board i2c detection')
    rcv = subprocess.check_output(['/usr/sbin/i2cdetect', '-l'])
    if rcv != "":
        # print("Some I2C bus present: "+repr(rcv))
        i2cdect = subprocess.Popen(('/usr/sbin/i2cdetect', '-y', '1'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', '40'), stdin=i2cdect.stdout)
        i2cdect.wait()
        # print("Result of i2cdetect for maxpro: "+repr(rcv))
        i2cadd = repr(rcv).split()
        if i2cadd[1] == "40":
            print('Success: Maxpro i2c address detected ({}) '.format(i2cadd[1]))
            logging.info('Success: Maxpro i2c address detected ({}) '.format(i2cadd[1]))
            port.write("\r\nMaxPro i2c ({})".format(i2cadd[1]))
        else:
            print('Failure: Maxpro i2c address not found by Raspberry Pi')
            logging.info('Failure: Maxpro i2c address not found by Raspberry Pi')
            port.write("\r\nMaxPro ERR")
            #sys.exit(-1)
    else:
        print('Failure: Maxpro i2c detection failed')
        logging.info('Failure: Maxpro i2c detection failed')
        port.write("\r\nMaxPro ERR")
        #sys.exit(-1)

    #port.close()
    time.sleep(3)
    print('\nInfo: USB Satellite device detection')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        try:
            usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
            rcv = subprocess.check_output(('grep', 'TechnoTrend'), stdin=usbdect.stdout)
            usbdect.wait()
            usbadd = repr(rcv).split()
            if usbadd[6] == "TechnoTrend":
                print('Success: USB Satellite detected ({}) '.format(usbadd[6]))
                logging.info('Success: USB Satellite detected ({}) '.format(usbadd[6]))
                port.write("\r\nSAT OK: {}".format(usbadd[6]))
            else:
                print('Failure: USB Satellite not detected ({}) '.format(usbadd[6]))
                logging.info('Failure: USB Satellite not detected ({}) '.format(usbadd[6]))
                port.write("\r\nSAT ERR")
        except subprocess.CalledProcessError:
            print('Failure: USB Satellite not detected')
            logging.info('Failure: USB Satellite not detected')
            port.write("\r\nSAT ERR")
            #sys.exit(-1)

    
    print('\nInfo: USB GSM device detection')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', 'Huawei'), stdin=usbdect.stdout)
        usbdect.wait()
        usbadd = repr(rcv).split()
        if usbadd[6] == "Huawei":
            print('Success: USB GSM detected ({}) '.format(usbadd[6]))
            logging.info('Success: USB GSM detected ({}) '.format(usbadd[6]))
            port.write("\r\nGSM OK")
        else:
            print('Failure: USB GSM not detected ({}) '.format(usbadd[6]))
            logging.info('Failure: USB GSM not detected ({}) '.format(usbadd[6]))
            port.write("\r\nGSM ERR")
            sys.exit(-1)
    print('Info: GSM SIM card Network Detection ...')
    #gsm.main()
    g = gsm.GSM()
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, sender=dispatcher.Any)
    time.sleep(25)
    test = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, 'Failure: no GSM SIM Network Operator found')
    print test
    logging.info(test)
    port.write("\r\nGSM Operator: "+test)
    
    print('\nInfo: Detecting USB WiFi device')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', 'Atheros'), stdin=usbdect.stdout)
        usbdect.wait()
        usbadd = repr(rcv).split()
        if usbadd[6] == "Atheros":
            print('Success: USB WiFi detected ({}) '.format(usbadd[6]))
            logging.info('Success: USB WiFi detected ({}) '.format(usbadd[6]))
            port.write("\r\nWifi OK ({})".format(usbadd[6]))
        else:
            print('Failure: USB WiFi not detected ({}) '.format(usbadd[6]))
            logging.info('Failure: USB WiFi not detected ({}) '.format(usbadd[6]))
            port.write("\r\nWifi ERR")
            sys.exit(-1)
            
    #print('\nInfo: WiFi scanning for networks ...')
    #w = wifi.WiFi()
    #time.sleep(10)
    #try: 
        #dispatcher.send(pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, 'hardware_test main()', [])
        #time.sleep(20)
    #except:
        #raise
        #pass
    
    #test = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, ['Failure: no WiFi networks found'])
    #print('Success: WiFi networks found: {}'.format(test))
    #logging.info('Success: WiFi networks found: {}'.format(test))

    #Get the MAC addresses
    #macaddr_wlan = getHwAddr("wlan0")
    #print("MAC address wlan0: "+str(macaddr_wlan))
    #port.write("\r\nMAC wlan0:")
    #port.write("\r\n"+str(macaddr_wlan))
    #macaddr_eth = getHwAddr("eth0")
    #print("MAC address eth0: "+str(macaddr_eth))
    #port.write("\r\nMAC eth0:")
    #port.write("\r\n"+str(macaddr_eth))

    #Get IP Adresses
    ipaddr_wlan = getIPNr("wlan0")
    print("IP address wlan0: "+str(ipaddr_wlan))
    port.write("\r\nIP wlan0:")
    port.write("\r\n"+str(ipaddr_wlan))

    ipaddr_eth = getIPNr("eth0")
    print("IP address eth0: "+str(ipaddr_eth))
    port.write("\r\nIP eth0:")
    port.write("\r\n"+str(ipaddr_eth))
    
#closing serial connection
port.close()


