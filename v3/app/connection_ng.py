#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-
"""
Display detailed information about currently active connections.
"""
from threading import Thread
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
import NetworkManager
import socket
from pydispatch import dispatcher
import json

import logging
import pfm_signals
# import pfm_config

import wifi

class PFMConnections():
    def __init__(self):
        # Read in known, saved, NetworkManager connections, settings and secrets
        self.read_list_connections()
        # self.print_known_connections(self.KnownNMConnections)

        # Read in all currently active network connections
        self.read_active_connections()
        # self.print_active_connections(self.ActiveNMConnections)

        _loop = GObject.MainLoop()
        t = Thread(target=_loop.run)
        t.start()

    def read_list_connections(self):
        self.KnownNMConnections = {}
        for conn in NetworkManager.Settings.ListConnections():
            settings = conn.GetSettings()
            logging.debug('Retrieving known connection {}'.format(settings['connection']['id']))
            secrets = {}
            try:
                secrets = conn.GetSecrets()
            except Exception, e:
                pass
            for key in secrets:
                settings[key].update(secrets[key])
            self.KnownNMConnections[settings['connection']['id']] = settings

    def read_active_connections(self):
        self.ActiveNMConnections = {}
        for conn in NetworkManager.NetworkManager.ActiveConnections:
            settings = conn.Connection.GetSettings()
            logging.debug('Retrieving active connection {}'.format(settings['connection']['id']))
            print('Retrieving active connection {}'.format(settings['connection']['id']))
            for s in list(settings.keys()):
                if 'data' in settings[s]:
                    settings[s + '-data'] = settings[s].pop('data')
            secrets = {}
            try:
                secrets = conn.Connection.GetSecrets()
                for key in secrets:
                    settings[key].update(secrets[key])
            except Exception, e:
                pass
            self.ActiveNMConnections[settings['connection']['id']] = settings


    def handle_active_connection_change(self, *args, **kwargs):
        # msg  = kwargs['d_member']
        # path = kwargs['d_path']
        # device   = NetworkManager.Device(path)
        # newState = NetworkManager.const('device_state', args[0])
        # oldState = NetworkManager.const('device_state', args[1])
        # print("Received signal: %s:%s" % (kwargs['d_interface'], kwargs['d_member']))
        # print("Sender:          (%s)%s" % (kwargs['d_sender'], kwargs['d_path']))
        # print("Network Device: {}".format(device.Interface))
        # print("Arguments:       (%s)" % ", ".join([str(x) for x in args]))
        print('Debug: ******************** ENTERED ACTIVE CONNECTION CHANGE SIGNAL')
        for count, thing in enumerate(args):
            print '{0}. {1}'.format(count, thing)
        for name, value in kwargs.items():
            print '{0} = {1}'.format(name, value)
        pass


    def print_known_connections(self,saved_connections):
        print ("# CURRENT SAVED NETWORK MANAGER PROFILES")
        print ("  Number of saved NM connections {}".format(len(saved_connections)))
        for conn in saved_connections:
            print("------------- ID    {}".format(saved_connections[conn]['connection'].get('id')))
            print("------------- TYPE  {}".format(saved_connections[conn]['connection'].get('type')))
            print("------------- IFACE {}".format(saved_connections[conn]['connection'].get('interface-name')))
            # for wifi connections
            if saved_connections[conn]['connection'].get('type') == '802-11-wireless':
                print("------------- SSID  {}".format(saved_connections[conn]['802-11-wireless'].get('ssid')))
                if saved_connections[conn]['802-11-wireless'].get('security') == '802-11-wireless-security':
                    print("------------- WSEC  {}".format(saved_connections[conn]['802-11-wireless'].get('security')))
                    # for wifi wpa-psk
                    if saved_connections[conn]['802-11-wireless-security'].get('key-mgmt') == 'wpa-psk':
                        print("------------- PSK   {}".format(saved_connections[conn]['802-11-wireless-security'].get('psk')))
            # for ethernet connections
            elif saved_connections[conn]['connection'].get('type') == '802-3-ethernet':
                    print("------------- MAC   {}".format(saved_connections[conn]['802-3-ethernet'].get('mac-address')))
            # for GSM/3G connections
            elif saved_connections[conn]['connection'].get('type') == 'gsm':
                    print("------------- APN   {}".format(saved_connections[conn]['gsm'].get('apn')))
                    print("------------- USER  {}".format(saved_connections[conn]['gsm'].get('user')))
                    print("------------- PASS  {}".format(saved_connections[conn]['gsm'].get('password')))
            else:
                print(json.dumps(saved_connections[conn], indent = 4))
            print

    def print_active_connections(self, active_connections):
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        print ('@ CURRENT ACTIVE NETWORK MANAGER PROFILES')
        print ('@ Number of ACTIVE NM connections {}'.format(len(active_connections)))
        for conn in active_connections:
            # print("------------- STRUCT    {}".format(active_connections[conn]))
            print("@ {:20} {:20} {:20} {:20}".format(active_connections[conn]['connection'].get('id'), 
                active_connections[conn]['connection'].get('type'), 
                active_connections[conn]['connection'].get('interface-name'),
                active_connections[conn]['connection'].get('uuid')))
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')


def test_print_active_profiles():
    print ("@@CURRENT ACTIVE NETWORK MANAGER PROFILES")
    print ("  Number of ACTIVE NM connections {}".format(len(NetworkManager.NetworkManager.ActiveConnections)))
    active_connections = {}
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        settings = conn.Connection.GetSettings()
        for s in list(settings.keys()):
            if 'data' in settings[s]:
                settings[s + '-data'] = settings[s].pop('data')
        secrets = {}
        try:
            secrets = conn.Connection.GetSecrets()
            for key in secrets:
                settings[key].update(secrets[key])
        except Exception, e:
            pass
        devices = ""
        if conn.Devices:
            devices = " (on %s)" % ", ".join([x.Interface for x in conn.Devices])
        print('Active connection: {}{}'.format(settings['connection']['id'], devices))
        size = max([max([len(y) for y in x.keys() + ['']]) for x in settings.values()])
        format = "      %%-%ds %%s" % (size + 5)
        for key, val in sorted(settings.items()):
            print("   %s" % key)
            for name, value in val.items():
                print(format % (name, value))
        for dev in conn.Devices:
            print("Device: %s" % dev.Interface)
            try:
                print('   Type             {}'.format(NetworkManager.const('device_type', dev.DeviceType)))
                devicedetail = dev.SpecificDevice()
                if not callable(devicedetail.HwAddress):
                    print('   MAC address      {}'.format(devicedetail.HwAddress))
                print('   IPv4 config')
                print('      Addresses')
                try:
                    for addr in dev.Ip4Config.Addresses:
                        print('         {}'.format(addr[0]))
                    print('      Routes')
                    for route in dev.Ip4Config.Routes:
                        print('         {}'.format(route))
                    print('      Nameservers')
                    for ns in dev.Ip4Config.Nameservers:
                        print('         {}'.format(ns))
                except AttributeError:
                    print('No IP Address details available for {}'.format(dev.Interface))
            except ValueError:
                print('   Type             {}'.format(dev.DeviceType))


def n_m_activate_connection(names):
    connections = NetworkManager.Settings.ListConnections()
    connections = dict([(x.GetSettings()['connection']['id'], x) for x in connections])

    if not NetworkManager.NetworkManager.NetworkingEnabled:
        NetworkManager.NetworkManager.Enable(True)
    for n in names:
        if n not in connections:
            print('DEBUG: connectionmanager.py: n_m_activate_connection: No such connection: {}'.format(n))
        else:
            conn = connections[n]
            ctype = conn.GetSettings()['connection']['type']
            if ctype == 'vpn':
                for dev in NetworkManager.NetworkManager.GetDevices():
                    if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED and dev.Managed:
                        break
                else:
                    print('DEBUG: connectionmanager.py: n_m_activate_connection: No active, managed device found')
            else:
                dtype = {
                    '802-11-wireless': 'wlan',
                    'gsm': 'wwan',
                }
                # if dtype in connection_types:
                #     enable(dtype)
                dtype = {
                    '802-11-wireless': NetworkManager.NM_DEVICE_TYPE_WIFI,
                    '802-3-ethernet': NetworkManager.NM_DEVICE_TYPE_ETHERNET,
                    # 'gsm': NetworkManager.NM_DEVICE_TYPE_MODEM,
                }.get(ctype,ctype)
                devices = NetworkManager.NetworkManager.GetDevices()
    
                for dev in devices:
                    # print('DEBUG: n_m_activate_connection: {} {} {}'.format(NetworkManager.Device(dev).Interface, dev.DeviceType, dev.State))
                    if dev.DeviceType == dtype and dev.State == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                        break
                else:
                    logging.debug('No suitable and available {} device found'.format(ctype))

            NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")


def n_m_deactivate_connection(names):
    active = NetworkManager.NetworkManager.ActiveConnections
    active = dict([(x.Connection.GetSettings()['connection']['id'], x) for x in active])
    for n in names:
        if n in active:
            logging.debug('deactivating connection: ({})'.format(n))
            NetworkManager.NetworkManager.DeactivateConnection(active[n])
        else:
            logging.debug('No such connection: ({})'.format(n))

def n_m_delete_connection(names):
    active = [x.Connection.GetSettings()['connection']['id']
              for x in NetworkManager.NetworkManager.ActiveConnections]
    connections = [(x.GetSettings()['connection']['id'], x)
                   for x in NetworkManager.Settings.ListConnections()]
    for conn in sorted(connections):
        if conn[0] in names:
            logging.debug('Deleting connection {} {}'.format(conn[0], conn[1]))
            try:
                conn[1].Delete()
            except:
                pass

def get_active_connections_by_interface(network_interface):
    # print('DEBUG: connectionmanager.py get_active_connections_by_interface({})'.format(network_interface))
    active_interface_connections = []
    sleep(2) #  TODO: IMPORTANT: Yes, this is needed. NetworkManager will throw an exception without it. No idea why yet!!!
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        try:
            settings = conn.Connection.GetSettings()['connection']
            if network_interface in [x.Interface for x in conn.Devices]:
                logging.debug(' active connection {} {} {} {}'.format(settings['id'], settings['type'], conn.Default, ", ".join([x.Interface for x in conn.Devices])))
                active_interface_connections.append(settings['id'])
        except:
            print('DEBUG: connectionmanager.py get_active_connections_by_interface({}) reached exception!!!'.format(network_interface))
    return(active_interface_connections)





# testCon = PFMConnections()
# testCon.print_active_connections(testCon.ActiveNMConnections)
# testCon.print_known_connections(testCon.KnownNMConnections)

# testWiFi = wifi.PFMWiFiDevice('wlan0')
# testWiFi2 = wifi.PFMWiFiDevice('wlan1')

# _loop = GObject.MainLoop()
# t = Thread(target=_loop.run)
# t.start()


