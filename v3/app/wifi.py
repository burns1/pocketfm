#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-
"""
Display detailed information about currently active connections.
"""
from threading import Thread
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
import NetworkManager
import socket
from pydispatch import dispatcher
import struct
import uuid
import json
from time import sleep

import logging
import pfm_signals
#import pfm_config

PFM_WIFI_DEVICE = 'wlan0'

PFM_WIFI_MODE_OFF = 'OFF'
PFM_WIFI_MODE_CLIENT = 'CLIENT MODE'
PFM_WIFI_MODE_AP = 'ACCESS POINT'
PFM_CONNECTIONMANAGER_WIFI_MODE_LIST = [PFM_WIFI_MODE_AP, PFM_WIFI_MODE_CLIENT, PFM_WIFI_MODE_OFF]

# default passwords mandated ... *sigh*
PFM_WIFI_AP_DEFAULT_PASSWORD = '12345678'
PFM_WIFI_CLIENT_DEFAULT_PASSWORD = '12345678'

PFM_WIFI_STATUS_ERROR = '(error)'
PFM_WIFI_STATUS_WAIT = '(wait...)'
PFM_WIFI_STATUS_OFF = '(off)'

# ui state messages for WIFI client mode
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'] =                       'No network selected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'] =                         'Network not found'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi client connected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Connecting ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED] = 'WiFi disconnected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_PREPARE]      = 'Preparing ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'WiFi disconnecting ..'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_SECONDARIES]  = 'Checking ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH]    = 'Checking password ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED]       = 'Password not accepted'

# ui state messages for AP mode
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi AP active'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED] = 'WiFi AP disconnected'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_PREPARE]      = 'Preparing ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Configuring ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CHECK]     = 'IP check ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_SECONDARIES]  = 'Checking ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED]       = 'AP Failed'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'AP Deactivating'


# for dbus connect_to_signal()
d_args = ('sender', 'destination', 'interface', 'member', 'path')
d_args = dict([(x + '_keyword', 'd_' + x) for x in d_args])

class PFMWiFiDevice(Thread):
    def __init__(self, interface):

        super(PFMWiFiDevice, self).__init__()

        # dBus NetworkManager
        self.SystemDBus = dbus.SystemBus()
        self.NMProxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager")
        self.NMInterface = dbus.Interface(self.NMProxy, "org.freedesktop.NetworkManager")

        self.WiFiDevice = None
        self.WiFiDeviceState = None
        self.WiFiClientSSID = None
        self.WiFiAvailableNetworks = []
        # Intialize hooks into DBus NM interface
        for dev in NetworkManager.NetworkManager.GetDevices():
            if dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_WIFI:
                logging.debug('Found WiFi Device {}'.format(NetworkManager.Device(dev).Interface))
                if NetworkManager.Device(dev).Interface == interface:
                    self.WiFiDevice = interface
                    logging.debug('Found my WiFi device {}'.format(self.WiFiDevice))
                    devicedetail = dev.SpecificDevice()
                    res = dev.SpecificDevice().connect_to_signal('StateChanged', self.handle_device_state_change, **d_args)
                    self.WiFiDeviceState = NetworkManager.Device(dev).State
                    logging.debug('Registered device {} for dbus NM state change. Current NM device state: {} ({})'.format(self.WiFiDevice, NetworkManager.const('device_state',self.WiFiDeviceState), self.WiFiDeviceState))
                    if NetworkManager.const('device_state', self.WiFiDeviceState) == 'activated':
                        logging.debug('device {} is activated at init!'.format(self.WiFiDevice))
                        for addr in dev.Ip4Config.Addresses:
                            logging.debug('device {} has IP4 address: {}'.format(self.WiFiDevice, addr[0]))
                        for route in dev.Ip4Config.Routes:
                            logging.debug('device {} has IP4 route: {}'.format(self.WiFiDevice, route))
                        for ns in dev.Ip4Config.Nameservers:
                            logging.debug('device {} has Nameserver: {}'.format(self.WiFiDevice, ns))
                    res = dev.SpecificDevice().connect_to_signal('AccessPointAdded', self.handle_available_ap_list_change, **d_args)
                    logging.debug('Registered {} for dbus NM available AP added to list'.format(self.WiFiDevice))
                    res = dev.SpecificDevice().connect_to_signal('AccessPointRemoved', self.handle_available_ap_list_change, **d_args)
                    logging.debug('Registered {} for dbus NM available AP removed from list'.format(self.WiFiDevice))
        if self.WiFiDevice != None:
            if self.WiFiDevice == PFM_WIFI_DEVICE:
                # set PFM Signal Handlers
                # WiFi Mode (AP, Client or Off)
                dispatcher.connect(self.handle_set_wifi_mode, signal=pfm_signals.SIG_SET_WIFI_MODE, sender=dispatcher.Any)
                # AP Mode
                dispatcher.connect(self.handle_set_wifi_ap_active, signal=pfm_signals.SIG_SET_WIFI_AP_ACTIVE, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_wifi_ap_ssid, signal=pfm_signals.SIG_SET_WIFI_AP_NETWORK, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_wifi_ap_password, signal=pfm_signals.SIG_SET_WIFI_AP_PASSWORD, sender=dispatcher.Any)
                # Client Mode
                dispatcher.connect(self.handle_set_wifi_client_active, signal=pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_wifi_client_ssid, signal=pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_wifi_client_password, signal=pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, sender=dispatcher.Any)
                # WiFi Interface Mode: (currently Client Mode, AP Mode, or Off)
                self.WiFiCurrentMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_MODE, PFM_WIFI_MODE_OFF)
                dispatcher.connect(self.handle_set_wifi_mode, signal=pfm_signals.SIG_SET_WIFI_MODE, sender=dispatcher.Any)
                #  Access Point Mode
                self.WiFiAPActive = False
                self.WiFiAPDevice = self.WiFiDevice
                self.WiFiAPSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_NETWORK, socket.gethostname())
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK, self, self.WiFiAPSSID)
                self.WiFiAPPassword = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, PFM_WIFI_AP_DEFAULT_PASSWORD)
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)
                dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, self.WiFiAPActive)
                #  Client Mode
                self.WiFiClientActive = False
                self.WiFiClientDevice = self.WiFiDevice
                self.WiFiClientSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, None)
                self.PFM_update_available_aps()
                if self.WiFiClientSSID == None:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'])
                self.WiFiClientPassword = PFM_WIFI_CLIENT_DEFAULT_PASSWORD
                dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, self, self.WiFiClientSSID)
                # Set initial WIFI Mode and UI mode list
                self.WiFiModeList = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST, PFM_CONNECTIONMANAGER_WIFI_MODE_LIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST, self, self.WiFiModeList)
                dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, self, self.WiFiCurrentMode)
            else:
                #  Non native (ie external) PFM Wifi Device
                self.WiFiAPActive = False
                self.WiFiAPDevice = interface
                self.WiFiAPSSID = socket.gethostname()
                self.WiFiClientActive = False
                self.WiFiClientDevice = interface
                self.WiFiClientSSID = None
                self.WiFiClientSSID = 'AndroidAP' #  TEST!!
                # if self.WiFiClientSSID == None:
                #     self.WiFiClientPassword = PFM_WIFI_CLIENT_DEFAULT_PASSWORD
        else:
            print('Debug: ****** Error!! PFMWiFiDevice {} not found!!'.format(interface))

        # print('PFMWiFiDevice.__init__({}) properties:'.format(interface))
        # for property, value in vars(self).iteritems():
        #     if property[0] != '_':
        #         print property, ": ", value
        # print('__init end__')

    def PFM_update_available_aps(self):
         new_ap_list = []
         for dev in NetworkManager.NetworkManager.GetDevices():
                 if dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_WIFI:
                     #print('Debug: Found WiFi Device {}'.format(NetworkManager.Device(dev).Interface))
                     if NetworkManager.Device(dev).Interface == self.WiFiDevice:
                         #print('Debug: Found my WiFi device {}'.format(self.WiFiDevice))
                         ap_report = ''
                         for visible_ap in dev.SpecificDevice().GetAccessPoints():
                             try:
                                 visible_ssid = visible_ap.Ssid
                                 # selective addition of APs based on WPA or other criteria can occur here ...
                                 new_ap_list.append(visible_ssid)
                                 # new_ap_list.append(str(visible_ssid))
                                 ap_report += u'{:>35}: {}MHz {}bits/sec Flags: {} WPA flags: {} RSN flags: {} Signal Strength {}% \n'.format(visible_ssid,
                                         visible_ap.Frequency, visible_ap.MaxBitrate, visible_ap.Flags, visible_ap.WpaFlags, visible_ap.RsnFlags, ord(visible_ap.Strength))
                             except Exception as ex:
                                 logging.debug('WARNING: visible_ap.Ssid {} exception with args {} thrown for AP {} dumping properties ...'.format(type(ex).__name__, ex.args, visible_ap))
                                 for prop, value in vars(visible_ap).iteritems():
                                     logging.debug('{}: {}'.format(prop, value))
                                 pass
                         new_ap_list = list(set(new_ap_list))
                         if self.WiFiAvailableNetworks != new_ap_list:
                             self.WiFiAvailableNetworks = new_ap_list
                             if self.WiFiDevice == PFM_WIFI_DEVICE:
                                 if self.WiFiClientSSID not in self.WiFiAvailableNetworks:
                                     #print('Debug: Device {} SIG_UPDATE_WIFI_AVAILABLE_NETWORKS exclusive of Client SSID {} ADDING! ...'.format(self.WiFiClientDevice, self.WiFiClientSSID))
                                     self.WiFiAvailableNetworks.append(self.WiFiClientSSID)
                                 else:
                                     #print('Debug: Device {} SIG_UPDATE_WIFI_AVAILABLE_NETWORKS inclusive of Client SSID {} FOUND ME!!!!!!'.format(self.WiFiClientDevice, self.WiFiClientSSID))
                                     pass
                                 dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, self, self.WiFiAvailableNetworks)
                             else:
                                 #print('Debug: Device {} deduped new ap list self.WiFiAvailableNetworks {}'.format(self.WiFiClientDevice, self.WiFiAvailableNetworks))
                                 pass

    def handle_device_state_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        newState = NetworkManager.const('device_state', args[0])
        oldState = NetworkManager.const('device_state', args[1])
        #print('Debug: ******************** ENTERED DEVICE STATE CHANGE SIGNAL')
        #print("Received signal: %s:%s" % (kwargs['d_interface'], kwargs['d_member']))
        #print("Sender:          (%s)%s" % (kwargs['d_sender'], kwargs['d_path']))
        #print("Network Device: {}".format(device.Interface))
        #print("Arguments:       (%s)" % ", ".join([str(x) for x in args]))

        #print('*****************************Debug: interface {} state changed from {} to {}'.format(self.WiFiDevice, oldState, newState))
        self.WiFiDeviceState = args[0]
        if NetworkManager.Device(path).Interface == self.WiFiClientDevice and pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_CLIENT:
            if args[0] == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED])
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK))
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                #print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, ipaddr_dotted)
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == False:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                # dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED])
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, None)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_DEACTIVATING:
                if args[1] == NetworkManager.NM_DEVICE_STATE_CONFIG:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'])
                else:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, None)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CHECK:
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, ipaddr_dotted)
                #print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_NEED_AUTH:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH])
            elif args[0] == NetworkManager.NM_DEVICE_STATE_FAILED:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED])
                if args[1] == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'])
                else:
                    #print('DEBUG: networkmanager_device_state_change(): failed state reached. Deleting profiles for "{}"'.format(self.WiFiClientSSID))
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, PFM_WIFI_STATUS_ERROR)
                    n_m_delete_connection([self.WiFiClientSSID])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            logging.debug('WiFi Client Interface {} is in state {}'.format(self.WiFiClientDevice, newState))
            #print('DEBUG: WiFi Client Status: {} : {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[args[0]])))

        if NetworkManager.Device(path).Interface == self.WiFiAPDevice and pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_AP:
            logging.debug('WiFi AP Interface {} is in state {}'.format(self.WiFiAPDevice, newState))
            #print('WiFi AP Interface {} is in state {}'.format(self.WiFiAPDevice, newState))
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[args[0]])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK))

        if NetworkManager.Device(path).Interface == self.WiFiAPDevice and pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_OFF:
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, None)

        # TODO this should be in connectionmanager_ng
        # If Wifi Client mode is active, then set that the PFM unit is network connected
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == True:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == False:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        #  TODO connectivity check should include 3G and not be done here
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == False and pfm_signals.get_value(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE) == False:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == True:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, False)


    def handle_available_ap_list_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        #  DEBUG for internal PFM device to reattach to currently configured Access Point
        #logging.debug('--- begin len(args) = {}'.format(len(args)))
        #for count, thing in enumerate(args):
        #    logging.debug('{0}. {1}'.format(count, thing))
        #    # print('{0}. {1}'.format(count, thing))
        #for name, value in kwargs.items():
        #    logging.debug('{0} = {1}'.format(name, value))
        #    # print('{0} = {1}'.format(name, value))

        self.PFM_update_available_aps()
        if msg == 'AccessPointAdded': # AccessPointRemoved does not pass args[0] ap object
            try:
                ssid = args[0].Ssid
                if self.WiFiClientSSID == ssid:
                    logging.debug('WiFi client mode set & SSID {} has appeared as available'.format(self.WiFiClientSSID))
                    if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == False:
                        logging.debug('SIG_UPDATE_WIFI_CLIENT_ACTIVE is FALSE: attempting reconnect on interface {}'.format(self.WiFiClientDevice))
                        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, self, PFM_WIFI_MODE_CLIENT)
                    else:
                        logging.debug('SIG_UPDATE_WIFI_CLIENT_ACTIVE is TRUE - not attempting reconnect')
                logging.debug("signal: %s (%s)" % (kwargs['d_member'], ssid))
            except:
                logging.debug('WARNING: args[0].Ssid exception occured for {}'.format(args[0]))

        if msg == 'AccessPointRemoved': # AccessPointRemoved does not seem to pass args[0] ap object?
            logging.debug("signal: %s" % (kwargs['d_member']))

    def handle_set_wifi_mode(self, mode, signal, sender):
        """
        Sets WiFi mode, AP mode for emulating an access point, Client mode for attaching directly to an external access point, or off
        Currently CLIENT and ACCESS POINT modes are set to be mutually exclusive
        """
        if mode.upper() == PFM_WIFI_MODE_OFF:
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, False)
            self.WiFiCurrentMode = PFM_WIFI_MODE_OFF
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, PFM_WIFI_STATUS_OFF)
        elif mode.upper() == "AP" or mode.upper() == PFM_WIFI_MODE_AP or mode.upper == "ACCESS POINT MODE":
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_AP_STATUS['PFM_PLEASE_WAIT'])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, True)
            self.WiFiCurrentMode = PFM_WIFI_MODE_AP
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)
        elif mode.upper() == "CLIENT" or mode.upper() == PFM_WIFI_MODE_CLIENT:
            # HACK because AP MODE breaks visibility of NetworkManager's GetAP()!!!
            # if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_AP:
            #     print('HACK {}'.format(self.WiFiClientSSID))
            #     self.WiFiAvailableNetworks.append(self.WiFiClientSSID)
            #     print(self.WiFiAvailableNetworks)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_PLEASE_WAIT'])
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_MODE) == "ON":
                dispatcher.send(pfm_signals.SIG_SET_3G_MODE, self, "OFF")
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, True)
            self.WiFiCurrentMode = PFM_WIFI_MODE_CLIENT
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)


    def handle_set_wifi_ap_ssid(self, ssid, signal, sender):
        """
        Sets HOSTAPD wifi network SSID
        """
        if ssid != self.WiFiAPSSID:
            self.WiFiAPSSID = ssid
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK, self, self.WiFiAPSSID)

    def handle_set_wifi_ap_password(self, password, signal, sender):
        """
        Set handler for WiFi AP password - simply updates internal property for AP password
        """
        if password != pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD):
            self.WiFiAPPassword = password
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)


    def handle_set_wifi_client_ssid(self, ssid, signal, sender):
        """
        Set handler for WiFi Client Network SSID
        """
        found_existing_connection = False
        found_existing_password = False
        for conn in NetworkManager.Settings.ListConnections():
            settings = conn.GetSettings()
            if settings['connection'].get('type') == '802-11-wireless':
                logging.debug('scanned saved wifi connection {}'.format(settings['connection']))
                if settings['connection']['id'] == ssid:
                    logging.debug('found connection {}'.format(settings['connection']['id']))
                    found_existing_connection = True
                    secrets = {}
                    try:
                        secrets = conn.GetSecrets()
                    except Exception, e:
                        pass
                    for key in secrets:
                        settings[key].update(secrets[key])
                    if settings['802-11-wireless'].get('security') == '802-11-wireless-security':
                        if settings['802-11-wireless-security'].get('key-mgmt') == 'wpa-psk':
                            if settings['802-11-wireless-security'].get('psk'):
                                self.WiFiClientPassword = settings['802-11-wireless-security']['psk']
                                logging.debug('found saved WPA PSK password {} for connection {}'.format(self.WiFiClientPassword, ssid))
                                dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, settings['802-11-wireless-security']['psk'])
                                found_existing_password = True
        if found_existing_connection == False:
            logging.debug('did not find saved connection {} for SSID {}'.format(self.WiFiClientSSID, ssid))
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, PFM_WIFI_CLIENT_DEFAULT_PASSWORD)
        if found_existing_password == False:
            logging.debug('did not find password for saved connection {} - setting default'.format(self.WiFiClientSSID))
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, PFM_WIFI_CLIENT_DEFAULT_PASSWORD)
        self.WiFiClientSSID = ssid
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, self, self.WiFiClientSSID)


    def handle_set_wifi_client_password(self, password, signal, sender):
        """
        Set handler for WiFi Client password
        """
        # just set signal & property for now, actually connect when SIG_WIFI_CLIENT_ACTIVE (True) is requested through handle_set_wifi_client_active()
        self.WiFiClientPassword = password
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, self, self.WiFiClientPassword)


    def handle_set_wifi_client_active(self, state, signal, sender):
        """
        Set handler for WiFi Client mode (de)activation with current network name and password settings
        """
        dpath = None
        if state == True or state == 'True':
            #print('DEBUG: wifi.py: handle_set_wifi_client_active() state == True')
            connections = {}
            for conn in NetworkManager.Settings.ListConnections():
                settings = conn.GetSettings()
                try:
                    secrets = conn.GetSecrets()
                    for key in secrets:
                        settings[key].update(secrets[key])
                    connections[settings['connection']['id']] = settings
                except Exception, e:
                    pass
            if self.WiFiClientSSID not in connections:
                #print('handle_set_wifi_client_active: NO NM PROFILE FOUND')
                logging.debug('requested NM connection NOT found, creating connection for {}'.format(self.WiFiClientSSID))
                #print('requested NM connection NOT found, creating connection for {}'.format(self.WiFiClientSSID))
                # HACK! make sure password is NOT null or less than 8 characters in length for WPA network types
                if self.WiFiClientPassword == None:
                    self.WiFiClientPassword = ''
                if len(self.WiFiClientPassword) < 8:
                    logging.debug('null password passed for WPA network {} filling with 8 spaces ...'.format(self.WiFiClientSSID))
                    self.WiFiClientPassword = '        '
                    dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, self.WiFiClientPassword)
                s_con = dbus.Dictionary({
                    'type': '802-11-wireless',
                    'uuid': str(uuid.uuid4()),
                    'permissions': ['user:pfm:'],
                    'autoconnect': 'false',
                    'interface-name': self.WiFiClientDevice,
                    'id': self.WiFiClientSSID })
                s_wifi = dbus.Dictionary({
                    'ssid': dbus.ByteArray(self.WiFiClientSSID),
                    'mode': 'infrastructure', })
                s_wsec = dbus.Dictionary({
                    'key-mgmt': 'wpa-psk',
                    'auth-alg': 'open',
                    'psk': self.WiFiClientPassword, })
                s_ip4 = dbus.Dictionary({'method': 'auto'})
                s_ip6 = dbus.Dictionary({'method': 'ignore'})
                con = dbus.Dictionary({
                    'connection': s_con,
                    '802-11-wireless': s_wifi,
                    '802-11-wireless-security': s_wsec,
                    'ipv4': s_ip4,
                    'ipv6': s_ip6})
                proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings")
                settings = dbus.Interface(proxy, "org.freedesktop.NetworkManager.Settings")
                try:
                    settings.AddConnection(con)
                    self.WiFiClientActive = True
                except:
                    logging.debug('DBUS Exception trapped')
                    print('WiFi add client connection DBUS Exception trapped')
                    self.WiFiClientActive = False
            else:
                #print('handle_set_wifi_client_active: SAVED NM PROFILE FOUND')
                pass
            n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiClientDevice)) #  deactivate all connections on this device
            n_m_activate_connection([self.WiFiClientSSID])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, self.WiFiClientActive)
        else:
            #print('DEBUG: wifi.py: handle_set_wifi_client_active() state == False')
            n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiClientDevice))
            self.WiFiClientActive = False
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)


    def handle_set_wifi_ap_active(self, state, signal, sender):
        """
        Set handler for WiFi AP client mode activation
        """
        if state == True or state == 'True':
            #print('DEBUG: wifi.py: handle_set_wifi_ap_active() state == True disconnecting previous ...')
            n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiAPDevice)) #  deactivate all connections on this device
            n_m_delete_connection([self.WiFiAPSSID])
            # dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
            # HACK! make sure password is NOT null or less than 8 characters in length for WPA network types
            if self.WiFiAPPassword == None:
                self.WiFiAPPassword = ''
            if self.WiFiAPPassword == PFM_WIFI_AP_DEFAULT_PASSWORD:
                logging.debug('default password passed for network {} filling with none ...'.format(self.WiFiAPSSID))
                self.WiFiAPPassword = 'none'
                dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)
            s_con = dbus.Dictionary({
                'type': '802-11-wireless',
                'uuid': str(uuid.uuid4()),
                'autoconnect': 'false',
                'permissions': ['user:pfm:'],
                'interface-name': self.WiFiAPDevice,
                'id': self.WiFiAPSSID })
            s_wifi = dbus.Dictionary({
                'ssid': dbus.ByteArray(self.WiFiAPSSID),
                'mode': 'ap',
                'band': 'bg',
                'channel': '1' })
            s_wsec = dbus.Dictionary({
                'key-mgmt': 'wpa-psk',
                'auth-alg': 'open',
                'psk': self.WiFiAPPassword, })
            s_ip4 = dbus.Dictionary({'method': 'shared'})
            s_ip6 = dbus.Dictionary({'method': 'ignore'})
            if len(self.WiFiAPPassword) < 8:
                self.WiFiAPPassword = 'none'
                dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)
                con = dbus.Dictionary({
                    'connection': s_con,
                    '802-11-wireless': s_wifi,
                    # '802-11-wireless-security': s_wsec,
                    'ipv4': s_ip4,
                    'ipv6': s_ip6})
            else:
                con = dbus.Dictionary({
                    'connection': s_con,
                    '802-11-wireless': s_wifi,
                    '802-11-wireless-security': s_wsec,
                    'ipv4': s_ip4,
                    'ipv6': s_ip6})

            proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings")
            settings = dbus.Interface(proxy, "org.freedesktop.NetworkManager.Settings")
            try:
                #print('DEBUG: wifi.py: handle_set_wifi_ap_active(): attempting settings.AddConnection ...')
                settings.AddConnection(con)
                self.WiFiAPActive = True
            except:
                logging.debug('DBUS Exception attempting to add connection')
                print('DBUS Exception attempting to add connection')
                self.WiFiAPActive = False
            n_m_activate_connection([self.WiFiAPSSID])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE, self, self.WiFiAPActive)
        else:
            #print('DEBUG: wifi.py: handle_set_wifi_ap_active() state == False deactivating & deleting')
            n_m_deactivate_connection([self.WiFiAPSSID]) #  deactivate client NM connection
            n_m_delete_connection([self.WiFiAPSSID])
            # if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE) == True:
            #     print('AP_active false HACK {}'.format(self.WiFiClientSSID))
            #     self.WiFiAvailableNetworks.append(self.WiFiClientSSID)
            #     print(self.WiFiAvailableNetworks)

def n_m_activate_connection(names):
    connections = NetworkManager.Settings.ListConnections()
    connections = dict([(x.GetSettings()['connection']['id'], x) for x in connections])

    if not NetworkManager.NetworkManager.NetworkingEnabled:
        NetworkManager.NetworkManager.Enable(True)
    for n in names:
        if n not in connections:
            logging.debug('DEBUG: wifi.py: n_m_activate_connection: No such connection: {}'.format(n))
        else:
            conn = connections[n]
            ctype = conn.GetSettings()['connection']['type']
            if ctype == 'vpn':
                for dev in NetworkManager.NetworkManager.GetDevices():
                    if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED and dev.Managed:
                        break
                else:
                    logging.debug('DEBUG: wifi.py: n_m_activate_connection: No active, managed device found')
            else:
                dtype = {
                    '802-11-wireless': 'wlan',
                    'gsm': 'wwan',
                }
                # if dtype in connection_types:
                #     enable(dtype)
                dtype = {
                    '802-11-wireless': NetworkManager.NM_DEVICE_TYPE_WIFI,
                    '802-3-ethernet': NetworkManager.NM_DEVICE_TYPE_ETHERNET,
                    # 'gsm': NetworkManager.NM_DEVICE_TYPE_MODEM,
                }.get(ctype,ctype)
                devices = NetworkManager.NetworkManager.GetDevices()

                for dev in devices:
                    # print('DEBUG: n_m_activate_connection: {} {} {}'.format(NetworkManager.Device(dev).Interface, dev.DeviceType, dev.State))
                    if dev.DeviceType == dtype and dev.State == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                        break
                else:
                    logging.debug('No suitable and available {} device found'.format(ctype))

            logging.debug('activating connection: ({})'.format(n))
            NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
            

def n_m_deactivate_connection(names):
    active = NetworkManager.NetworkManager.ActiveConnections
    active = dict([(x.Connection.GetSettings()['connection']['id'], x) for x in active])
    for n in names:
        if n in active:
            logging.debug('deactivating connection: ({})'.format(n))
            #print('deactivating connection: ({})'.format(n))
            NetworkManager.NetworkManager.DeactivateConnection(active[n])
        else:
            logging.debug('Cannot deactivate. No such connection: ({})'.format(n))
            #print('Cannot deactivate. No such connection: ({})'.format(n))

def n_m_delete_connection(names):
    # active = [x.Connection.GetSettings()['connection']['id']
    #           for x in NetworkManager.NetworkManager.ActiveConnections]
    connections = [(x.GetSettings()['connection']['id'], x)
                   for x in NetworkManager.Settings.ListConnections()]
    for conn in sorted(connections):
        if conn[0] in names:
            logging.debug('Deleting connection {} {}'.format(conn[0], conn[1]))
            #print('Deleting connection {} {}'.format(conn[0], conn[1]))
            try:
                conn[1].Delete()
            except:
                logging.debug('Deletion Attempt failed for {} {}'.format(conn[0], conn[1]))
                print('Deletion Attempt failed for {} {}'.format(conn[0], conn[1]))
                pass

def get_active_connections_by_interface(network_interface):
    # print('DEBUG: wifi.py get_active_connections_by_interface({})'.format(network_interface))
    active_interface_connections = []
    sleep(2) #  TODO: IMPORTANT: Yes, this is needed. NetworkManager will throw an exception without it. No idea why yet!!!
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        try:
            settings = conn.Connection.GetSettings()['connection']
            if network_interface in [x.Interface for x in conn.Devices]:
                logging.debug(' active connection {} {} {} {}'.format(settings['id'], settings['type'], conn.Default, ", ".join([x.Interface for x in conn.Devices])))
                active_interface_connections.append(settings['id'])
        except:
            logging.debug('DEBUG: wifi.py get_active_connections_by_interface({}) reached exception!!!'.format(network_interface))
    return(active_interface_connections)

# testWiFi = PFMWiFiDevice('wlan0')

# _loop = GObject.MainLoop()
# t = Thread(target=_loop.run)
# t.start()


