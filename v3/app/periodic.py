#!/usr/bin/python

import time
import threading

class PeriodicThread(threading.Thread):
    '''Similar to a Timer(), but uses only one thread, stops cleanly and exits when the main thread exits'''

    def __init__ (self, period, callable, *args, **kwargs):
        super(PeriodicThread, self).__init__()
        self.period   = period
        self.callable = callable
        self.args     = args
        self.kwargs   = kwargs
        self.daemon   = True
        self.closing  = False

    def run(self):
        while not self.closing:
            time.sleep(self.period)
            self.callable(*self.args, **self.kwargs)

    def close(self):
        self.closing = True
