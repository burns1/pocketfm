#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import threading
from pydispatch import dispatcher
from periodic import PeriodicThread
import pfm_signals
import pfm_config
from breakoutboard import BreakoutBoard
from maxproboard import MaxProBoard
from audiomanager import AudioManager
from connectionmanager import ConnectionManager
from connection_ng import PFMConnections
from wifi import PFMWiFiDevice
from wifi import PFM_WIFI_DEVICE
from mobiledata import PFM3GDevice
from mobiledata import PFM_3G_DEVICE
from satellite import Satellite
from gsm import GSM
from usb_storage import USBStorage,PFM_USBSTORAGE_PLAYLIST
from internal_storage import InternalStorage,PFM_INTERNALSTORAGE_PLAYLIST
import display

from time import sleep
import os
import socket
import logging

PFM_LOCK_LEN = 6

PFM_MAIN_THREAD_ID = 'pfm.py main()'

class PocketFM():
    def __init__(self):
        self.PFMIdentifier = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_HOSTNAME, socket.gethostname())
        self.PFMSoftwareVersion = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_PFM_SOFTWARE_VERSION, 'unknown')
        self.RaspberryPiSerial = getserial()
        self.PIN = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_PFM_LOCK_PIN, '000000')
        self.DarkMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_PFM_DARK_MODE, False)
        self.Breakout = BreakoutBoard()
        self.Breakout.start()
        self.MaxPro = MaxProBoard()
        self.MaxPro.start()
        self.AudioMan = AudioManager()
        # self.ConnectionMan = ConnectionManager()
        self.Connections = PFMConnections()
        self.PFMWiFi = PFMWiFiDevice(PFM_WIFI_DEVICE)
        self.PFM3G = PFM3GDevice(PFM_3G_DEVICE)
        self.GSM = GSM()
        self.Satellite = Satellite()
        self.USBDrive = USBStorage()
        self.InternalDrive = InternalStorage()

        dispatcher.send(pfm_signals.SIG_UPDATE_PFM_HOSTNAME, self, self.PFMIdentifier)
        dispatcher.send(pfm_signals.SIG_UPDATE_PFM_SOFTWARE_VERSION, self, self.PFMSoftwareVersion)

        dispatcher.connect(self.handle_set_dark_mode, signal=pfm_signals.SIG_SET_PFM_DARK_MODE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_pin, signal=pfm_signals.SIG_SET_PFM_LOCK_PIN, sender=dispatcher.Any)

        # clear dark mode update value to False for testing
        dispatcher.send(pfm_signals.SIG_SET_PFM_DARK_MODE, self, self.DarkMode)
        dispatcher.send(pfm_signals.SIG_UPDATE_PFM_DARK_MODE, self, self.DarkMode)

        # set support pin from master config
        dispatcher.send(pfm_signals.SIG_UPDATE_PFM_SUPPORT_PIN, self, pfm_signals.get_value(pfm_signals.SIG_SET_PFM_SUPPORT_PIN))


    def handle_set_pin(self, pin, sender):
        #  todo - input error checking
        dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, self, pin)


    def handle_set_dark_mode(self, state, sender):

        if (state != self.DarkMode):
            if (state == True):
                # set dark mode on
                dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, False) # power off maxpro entirely to stop FM transmission
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, True) # stop mplayer
                self.DarkMode = True
                dispatcher.send(pfm_signals.SIG_UPDATE_PFM_DARK_MODE, self, True)
            else:
                # set dark mode off
                dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, True) # power on maxpro entirely to start FM transmission
                dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT)) # get mplayer to resume last played input
                self.DarkMode = False
                dispatcher.send(pfm_signals.SIG_UPDATE_PFM_DARK_MODE, self, False) # signal to all that dark mode is off


def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo','r')
        for line in f:
            if line[0:6]=='Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"
    return cpuserial

# example event handler
def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main():

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    # pfm_signals.set_logging(True)

    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, PFM_MAIN_THREAD_ID, b.RTC.BBQuery) # get RTC time
    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, PFM_MAIN_THREAD_ID, b.LED.BBQuery) # get led status

    dispatcher.send(pfm_signals.SIG_UPDATE_RASPBERRYPI_SERIAL_NUMBER, PFM_MAIN_THREAD_ID, p.RaspberryPiSerial)
    dispatcher.send(pfm_signals.SIG_UPDATE_PFM_SOFTWARE_VERSION, PFM_MAIN_THREAD_ID, p.PFMSoftwareVersion)
    dispatcher.send(pfm_signals.SIG_SET_POWER_DISPLAY, PFM_MAIN_THREAD_ID, False)
    dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, PFM_MAIN_THREAD_ID, False)
    # test turn USB2 power off for USB stick
    dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, PFM_MAIN_THREAD_ID, False)

    #  Seed Periodic Timers for minimal polling because BB hardware does not provide interrupts
    rtc_poll = PeriodicThread(1, b.writeBB, b.RTC.BBQuery) # yes, we poll for RTC settings *sigh* (note: BB battery & time status flag is given by this poll!)
    rtc_poll.start()

    #  Seed Periodic Timers for minimal polling because BB hardware does not provide interrupts
    adc_poll = PeriodicThread(0.15, b.writeBB, b.ADC.BBQuery) # this is now used for level meter with analog audiomanager input for VDU1 & VDU2 values by led.py
    adc_poll.start()

    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, PFM_MAIN_THREAD_ID, b.BBQuery) # get breakoutboard versions
    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, PFM_MAIN_THREAD_ID, b.GPS.BBActivate) # activate gps

    dispatcher.send(pfm_signals.SIG_SET_POWER_DISPLAY, PFM_MAIN_THREAD_ID, True)
    dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, PFM_MAIN_THREAD_ID, True)
    dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, PFM_MAIN_THREAD_ID, True)
    dispatcher.send(pfm_signals.SIG_SET_POWER_ATMEL, PFM_MAIN_THREAD_ID, True)

    # display.start(b)
    threading.Thread(target=display.start(b)).start()

    cnt = 0
    Good = True
    try:
        while Good:
            cnt += 1
            #print "PFM  heartbeat (PID: {}) {}".format(os.getpid(),b.is_alive())
            sleep(5)

    except (KeyboardInterrupt, SystemExit):
        Good = False
        print "\nInterrupt dispatch: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        b.close()
        m.close()
        t.close()
        print "\nAfter close() dispatch2: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        t.join()
        print "After t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        b.join()
        m.join()
        print "After b.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())

    print "\n dispatch: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
    t.close()
    t.join()
    sleep(1)
    print "\n dispatch after t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
    sleep(1)
    b.close()
    b.join()
    print "\n dispatch after t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())


if __name__ == "__main__":
    main()
