#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

from pulsectl import *
import time
import requests

import pfm_signals
from pydispatch import dispatcher
from mplayer import MPlayer, PFM_SILENCE_PLAYLIST
import usb_storage
import internal_storage
import satellite
import logging

PFM_PULSE_CLIENT_NAME = 'pfm-pulse-client'
PFM_INTERNET_STREAM_PLAYLIST = '/tmp/pfm_internetstream_playlist'
PFM_PULSE_SINK_NAME = 'alsa_output.hw_1_0' # used by led peak level meter in digital mode

class AudioManager():
    def __init__(self):
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, False)
        self.PulseClient = None  # handle to be used for audio level monitor?
        self.MPlayerSlave = None # single mplayer slave instance to be controlled
        self.MPlayerSlaveSatellite = None # second slave instance to get around mplayer seizing when satellite channel is not properly tuned
        self.loadfile = None # single shot url ie dvb:/ http://, etc.
        self.loadlist = None # pre-constructed playlists required for usb or internal
        self.MPlayerOutput = '' # any output given from command to mplayer
        self.InputList = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT_LIST, ['ANALOG', 'USB', 'STREAM', 'INTERNAL', 'SATELLITE'])

        try:
            self.PulseClient = pulsectl.Pulse(PFM_PULSE_CLIENT_NAME)
        except:
            pass
        try:
            self.MPlayerSlave = MPlayer('-af', 'volnorm', '-idle','-loop','0','-playlist', PFM_SILENCE_PLAYLIST)
            self.MPlayerSlaveSatellite = MPlayer('-af', 'volnorm', '-idle','-loop','0')
        except:
            pass
        try:
            self.MPlayerSlave.populate()
            self.MPlayerSlaveSatellite.populate()
        except:
            pass

        # AudioManager Select Input handler
        dispatcher.connect(self.handle_set_audio_input_device, signal=pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, sender=dispatcher.Any)

        # AudioManager Internal Storage handlers
        # dispatcher.connect(self.handle_internalstorage_audiofiles, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES_EXIST, sender=dispatcher.Any)
        dispatcher.connect(self.handle_internalstorage_active, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, sender=dispatcher.Any)

        # AudioManager USB Storage handlers
        dispatcher.connect(self.handle_usbstorage_attach, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED, sender=dispatcher.Any)
        dispatcher.connect(self.handle_usbstorage_audiofiles, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, sender=dispatcher.Any)
        dispatcher.connect(self.handle_usbstorage_active, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, sender=dispatcher.Any)

        # AudioManager Satellite handlers
        dispatcher.connect(self.handle_satellite_active, signal=pfm_signals.SIG_UPDATE_SATELLITE_ACTIVE, sender=dispatcher.Any)

        # AudioManager Back End Mplayer handlers
        dispatcher.connect(self.handle_set_mplayer_loadfile, signal=pfm_signals.SIG_SET_MPLAYER_LOADFILE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_mplayer_loadlist, signal=pfm_signals.SIG_SET_MPLAYER_LOADLIST, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_mplayer_pause, signal=pfm_signals.SIG_SET_MPLAYER_PAUSE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_mplayer_stop, signal=pfm_signals.SIG_SET_MPLAYER_STOP, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_mplayer_loop, signal=pfm_signals.SIG_SET_MPLAYER_LOOP, sender=dispatcher.Any)

        # AudioManager Internet Stream handlers
        dispatcher.connect(self.handle_set_audio_input_stream, signal=pfm_signals.SIG_SET_INTERNETSTREAM_URL, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_audio_input_stream_domain, signal=pfm_signals.SIG_SET_INTERNETSTREAM_DOMAIN, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_audio_input_stream_port, signal=pfm_signals.SIG_SET_INTERNETSTREAM_PORT, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_audio_input_stream_path, signal=pfm_signals.SIG_SET_INTERNETSTREAM_PATH, sender=dispatcher.Any)

        # AudioManager Internet Stream handlers
        dispatcher.connect(self.handle_connectionmanager_connected, signal=pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, sender=dispatcher.Any)

        # AudioManager Internet Stream initialisation
        self.InternetStreamDomain = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_INTERNETSTREAM_DOMAIN, '66.228.60.216')
        self.InternetStreamPort = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_INTERNETSTREAM_PORT, '8002')
        self.InternetStreamPath = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_INTERNETSTREAM_PATH, 'stream')
        # self.InternetStreamURL = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_INTERNETSTREAM_URL, 'http://66.228.60.216:8002/stream')
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_DOMAIN, self, self.InternetStreamDomain)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_PORT, self, self.InternetStreamPort)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_PATH, self, self.InternetStreamPath)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_URL, self, '')
        # dispatcher.send(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL, self, self.InternetStreamURL)

        self.CurrentInput = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'ANALOG') # current audio input mode, eg satellite,USB,aux/analog/internal storage
        dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, self, self.CurrentInput)

        dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST, self, self.InputList)

        # Ensure MaxproBoard is turned on, ready to play
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, True)

    def handle_set_audio_input_stream(self, url, sender):
        # TODO: parse for URL input sanity
        # if url == '' or url == None: # construct stream URL from domain & path
        #     if self.InternetStreamDomain != '' and self.InternetStreamDomain != None and self.InternetStreamPort != None and self.InternetStreamPath != '' and self.InternetStreamPath != None:
        #         self.InternetStreamURL = 'http://{}:{}/{}'.format(self.InternetStreamDomain, self.InternetStreamPort, self.InternetStreamPath)
        # else: # take url parameter as url
        #     self.InternetStreamURL = url
        self.InternetStreamURL = 'http://{}:{}/{}'.format(self.InternetStreamDomain, self.InternetStreamPort, self.InternetStreamPath)

        # write playlist file
        with open(PFM_INTERNET_STREAM_PLAYLIST, 'w+') as f:
           f.write('{}\n'.format(self.InternetStreamURL))
        # if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'STREAM':
        #     # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, PFM_INTERNET_STREAM_PLAYLIST)
        #     dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, self, 'STREAM')

        dispatcher.send(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL, self, self.InternetStreamURL)

    def handle_set_audio_input_stream_domain(self, domain, sender):
        # TODO: parse for DNS Domain + optional :port input sanity
        if True: # self.InternetStreamURL != domain:
            self.InternetStreamDomain = domain
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNETSTREAM_DOMAIN, self, self.InternetStreamDomain)

    def handle_set_audio_input_stream_port(self, port, sender):
        # TODO: parse for DNS Domain + optional :port input sanity
        if True: # self.InternetStreamURL != domain:
            if port == None:
                port = 80
            self.InternetStreamPort = port
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PORT, self, self.InternetStreamPort)

    def handle_set_audio_input_stream_path(self, path, sender):
        # TODO: parse for post domain/port URL path input sanity
        if True: # self.InternetStreamURL != url:
            self.InternetStreamPath = path.rstrip()
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PATH, self, self.InternetStreamPath)

    def handle_set_audio_input_device(self, source, sender):
        if   source.upper() == 'ANA' or source.upper() == 'ANALOG' or source.upper() == 'AUX' or source.upper() == 'ANALOGUE':
            # stop mplayer as ANALOG doesn't require it
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
            # instruct maxpro to switch to analog input because analog aux is plugged directly into maxpro board
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, self, 'analog')
            self.CurrentInput = 'ANALOG'
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, self, 'ANALOG')
            logging.debug('AudioManager: ANALOG: ACTIVE')
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")

        elif source.upper() == 'USB':
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Stopping {}".format(self.CurrentInput))
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, self, 'digital') # set maxpro input selection to digital 44.1KHz
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE) == True:
                # play USB playlist
                logging.debug('AudioManager: USB: ACTIVE')
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, usb_storage.PFM_USBSTORAGE_PLAYLIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")
            else:
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED) == False:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No USB Device")
                    logging.debug('AudioManager: No USB Device')
                else:
                    if pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, False) == False:
                        dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No Playable Files")
                        logging.debug('AudioManager: No Playable Files')
                    else:
                        logging.debug('DEBUG: AudioManager - SIG_UPDATE_USBSTORAGE_ACTIVE is False, but USB stick inserted AND audio files exist!')
                        logging.debug('DEBUG: AudioManager - USB ACTIVE:{}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE)))
                        logging.debug('DEBUG: AudioManager - USB ATTACHED:{}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED)))
                        logging.debug('DEBUG: AudioManager - AUDIO FILES: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST)))
            self.CurrentInput = 'USB'
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, self, 'USB')

        elif source.upper() == 'INT' or source.upper() == 'HD' or source.upper() == 'INTERNAL':
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Stopping {}".format(self.CurrentInput))
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, self, 'digital') # set maxpro input selection to digital 44.1KHz
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE) == True:
                # play Internal playlist
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, internal_storage.PFM_INTERNALSTORAGE_PLAYLIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")
            else:
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No Audio Files")
            self.CurrentInput = 'INTERNAL'
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, self, 'INTERNAL')

        elif source.upper() == 'SAT' or source.upper() == 'SATELLITE':
            # if not already, instruct breakout board to turn power on for Satellite receiver
            # satellite module handles its own power by attaching to SIG_SET_AUDIOMANAGER_INPUT and detecting for value SAT or SATELLITE
            dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, self, True)
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Stopping {}".format(self.CurrentInput))
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, self, 'digital') # set maxpro input selection to digital 44.1KHz
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, satellite.PFM_SATELLITE_PLAYLIST)
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")
            self.CurrentInput = 'SATELLITE'
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, self, 'SATELLITE')

        elif source.upper() == 'STREAM':
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Stopping {}".format(self.CurrentInput))
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, self, 'digital') # set maxpro input selection to digital 44.1KHz
            self.CurrentInput = 'STREAM'
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, self, 'STREAM')
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == True:
                try:
                    request = requests.get(self.InternetStreamURL, stream = True, timeout = 1.0)
                    # print('DEBUG: audiomanager.py stream: URL {}'.format(self.InternetStreamURL))
                    # print('DEBUG: audiomanager.py stream: return code {}'.format(request.status_code))
                    # print('DEBUG: audiomanager.py stream: content type {}'.format(request.headers['content-type']))
                    self.InternetStreamStatusCode = request.status_code
                    self.InternetStreamContentType = request.headers['content-type']
                    if self.InternetStreamStatusCode == 200:
                        if self.InternetStreamContentType.startswith('audio/'): 
                            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, PFM_INTERNET_STREAM_PLAYLIST)
                            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")
                        else:
                            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No Audio Stream")
                    else:
                        dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No URL response")

                except requests.ConnectionError:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Connection Error")

                except requests.Timeout:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Connection Timeout")

                except requests.exceptions.InvalidURL:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Invalid URL")

                except requests.URLRequired:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "URL Required")

                except requests.HTTPError:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "HTTP Error")

            else:
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No net connection")

        else:
            logging.debug('DEBUG: audiomanager - ValueError for INPUT')

    def handle_set_mplayer_loadfile(self, loadfile, signal, sender):
        """
        Sets MPlayer loadfile - see mplayer slave mode desc for details
            loadfile <file|url> <append>
               Load the given file/URL, stopping playback of the current file/URL.
               If <append> is nonzero playback continues and the file/URL is
               appended to the current playlist instead.
        """
        if loadfile != self.loadfile:
            try:
                self.MPlayerSlave.loadfile(loadfile)
                self.loadfile = loadfile
                dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_LOADFILE, self, self.loadfile)
            except:
                pass

    def handle_set_mplayer_loadlist(self, loadlist, signal, sender):
        """
        Sets MPlayer loadlist - see mplayer slave mode desc for details
        loadlist <file> <append>
            Load the given playlist file, stopping playback of the current file.
            If <append> is nonzero playback continues and the playlist file is
            appended to the current playlist instead.
        """

        # this conditional is to get around horrible bug where satellite crashes app
        # with workaround to get mplayer loop working, ie silence playlist params, etc.
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'SATELLITE':
            try:
                self.MPlayerSlave.stop()
                self.MPlayerOutput = self.MPlayerSlaveSatellite.loadlist(loadlist)
                self.loadlist = loadlist
                dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_LOADLIST, self, self.loadlist)
                dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_OUTPUT, self, self.MPlayerOutput)
            except:
                pass

        else:
            try:
                self.MPlayerSlaveSatellite.stop()
                self.MPlayerOutput = self.MPlayerSlave.loadlist(loadlist)
                self.loadlist = loadlist
                dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_LOADLIST, self, self.loadlist)
                dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_OUTPUT, self, self.MPlayerOutput)
            except:
                pass

    def handle_set_mplayer_stop(self, dummy, signal, sender):
        """
        Stops MPlayer playing - see mplayer slave mode desc for details
        stop 
        """
        try:
            self.MPlayerSlaveSatellite.stop()
            self.MPlayerSlave.stop()
            dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_STOP, self, self.loadlist)
        except:
            print('DEBUG: audiomanager.py handle_set_mplayer_stop exception')
            pass

    def handle_set_mplayer_pause(self, state, signal, sender):
        """
        Pauses MPlayer playing - see mplayer slave mode desc for details
        pause 
        """
        try:
            self.MPlayerSlave.pause()
            dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_PAUSE, self, self.loadlist)
        except:
            pass

    def handle_set_mplayer_loop(self, state, signal, sender):
        """
        Loops MPlayer playing - see mplayer slave mode desc for details
        loop 
        """
        try:
            self.MPlayerSlave.command('loop {}'.format(state))
            dispatcher.send(pfm_signals.SIG_UPDATE_MPLAYER_LOOP, self, state)
        except:
            pass

    def handle_usbstorage_attach(self, data, signal, sender):
        """
        Handle User Event of pulling USB storage out while playing USB audio
        """
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'USB':
            if signal == pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED:
                if data == False:
                    # stop Mplayer USB stream immediately
                    dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "USB device removed")
                else:
                    dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "USB device inserted")

    def handle_usbstorage_audiofiles(self, data, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'USB':
            if signal == pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST and data == False:
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No playable files")

    def handle_usbstorage_active(self, data, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'USB' and data == True:
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, usb_storage.PFM_USBSTORAGE_PLAYLIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")

    def handle_internalstorage_active(self, data, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'INTERNAL' and data == True:
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, internal_storage.PFM_INTERNALSTORAGE_PLAYLIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")

    def handle_satellite_active(self, data, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'SATELLITE' and data == True:
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, satellite.PFM_SATELLITE_PLAYLIST)
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")

    def handle_connectionmanager_connected(self, data, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'STREAM':
            if data == True:
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOOP, self, '0')
                # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, PFM_INTERNET_STREAM_PLAYLIST)
                # dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "Now Playing")
                dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, self, 'STREAM')
            else:
                dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, self, 'STOP')
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, "No net connection")

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender

def main():

    import sys
    from pfm_thread import PocketFM

    # dependent on these systems
    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB1, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB2, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_SATELLITE_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADFILE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADLIST, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_PAUSE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_STOP, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOOP, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNETSTREAM_DOMAIN, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNETSTREAM_PORT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNETSTREAM_PATH, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL, sender=dispatcher.Any)

    # print('Maxpro switch power off')
    # dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'pulse.py main()', False)
    # time.sleep(5)
    # print('Maxpro switch power on')
    # dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'pulse.py main()', True)
    # time.sleep(5)

    print('Testing ... sleeping for 5')
    time.sleep(5)

    print('INTERNET STREAM DOMAIN: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNETSTREAM_DOMAIN)))
    print('INTERNET STREAM PORT: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PORT)))
    print('INTERNET STREAM PATH: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PATH)))
    print('INTERNET STREAM URL: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL)))
    print('Play STREAM: {}'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL)))
    dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'STREAM')
    time.sleep(5)

    # print('If audio exists in PFM_INTERNALSTORAGE_ROOT={}, play ...'.format(internal_storage.PFM_INTERNALSTORAGE_ROOT))
    # dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'internal')
    # time.sleep(20)
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))

    # print('Play USB if present')
    # dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'USB')
    # time.sleep(5)
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))
    # time.sleep(5)
    # print('Testing MPlayer Pause')
    # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_PAUSE, 'audiomanager.py main()', 'PAUSE') 
    # time.sleep(5)
    # print('Testing MPlayer Pause twice (unpause)')
    # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_PAUSE, 'audiomanager.py main()', 'PAUSE') 
    # time.sleep(5)
    # print('Testing MPlayer Stop')
    # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_STOP, 'audiomanager.py main()', 'STOP') 
    # time.sleep(5)

    time.sleep(20)


    # print('If switched on, playing satellite')
    # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADFILE, 'audiomanager.py main()', 'dvb://DKULTUR')
    # time.sleep(20)
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))

    # print('If audio exists in PFM_INTERNALSTORAGE_ROOT={}, play ...'.format(internal_storage.PFM_INTERNALSTORAGE_ROOT))
    # dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'internal')
    # time.sleep(20)
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))

    # print('Play ANALOG')
    # dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'ANALOG')
    # time.sleep(5)

    # print('Play USB if present')
    # dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'audiomanager.py main()', 'USB')
    # time.sleep(20)
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))

    time.sleep(1200)
    # print('If online, playing RADIO ISLA NEGRA internet stream')
    # dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADFILE, 'audiomanager.py main()', 'http://66.228.60.216:8002/stream')

    # print('Pulseaudio sinks  : {0}'.format(p.AudioMan.PulseClient.sink_list()))
    # print('Pulseaudio sources: {0}'.format(p.AudioMan.PulseClient.source_list()))
    # print('Pulseaudio inputs : {0}'.format(p.AudioMan.PulseClient.sink_input_list()))

if __name__ == "__main__":
    main()

