 
sqlite> .tables
articles    comments    groups      settings  
categories  extras      modules     users 

sqlite> select * from articles where category is 4;

 .schema articles
CREATE TABLE articles (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title varchar(255) DEFAULT NULL,
        alias varchar(255) DEFAULT NULL,
        author varchar(255) DEFAULT NULL,
        description varchar(255) DEFAULT NULL,
        keywords varchar(255) DEFAULT NULL,
        robots int(1) DEFAULT NULL,
        text longtext DEFAULT NULL,
        language char(3) DEFAULT NULL,
        template varchar(255) DEFAULT NULL,
        sibling int(10) DEFAULT NULL,
        category int(10) DEFAULT NULL,
        headline int(1) DEFAULT 1,
        byline int(1) DEFAULT 1,
        comments int(1) DEFAULT 0,
        status int(1) DEFAULT 1,
        rank int(10) DEFAULT NULL,
        access varchar(255) DEFAULT NULL,
        date timestamp DEFAULT CURRENT_TIMESTAMP
        
see http://stackoverflow.com/questions/19968847/merging-two-sqlite-databases-which-both-have-junction-tables

---
cp 58457004b6ba6.sqlite test.sqlite
sqlite3 test.sqlite

attach '58457004b6ba6.sqlite' as ncp;

# delete from articles where category is 4;
delete from articles where category is (select id from categories where title is "content_proxy");

insert into articles (title, alias, author, description, keywords, robots, text, language, template, sibling, category, headline, byline, comments, status, rank, access, date)
select title, alias, author, description, keywords, robots, text, language, template, sibling, category, headline, byline, comments, status, rank, access, date from ncp.articles where category is 4;






http://stackoverflow.com/questions/17773832/mysql-insert-into-table-with-auto-increment-while-selecting-from-another-table#17773872

---


ATTACH content_proxy.sqlite AS cp;

INSERT INTO Fruit(name)
SELECT name
FROM cp.Fruit
WHERE name NOT IN (SELECT name
                   FROM Fruit);
INSERT INTO Juice(name)
SELECT name
FROM cp.Juice
WHERE name NOT IN (SELECT name
                   FROM Juice);

                   
INSERT INTO Recipe(juice_id, fruit_id)
SELECT (SELECT id
        FROM Juice
        WHERE name = (SELECT name
                      FROM db2.Juice
                      WHERE id = Recipe2.juice_id)),
       (SELECT id
        FROM Fruit
        WHERE name = (SELECT name
                      FROM db2.Fruit
                      WHERE id = Recipe2.fruit_id))
FROM db2.Recipe AS Recipe2;
