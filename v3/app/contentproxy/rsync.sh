#!/bin/bash

SSH_PORT="2323"
SSH_USER="adam"
SSH_HOST="syrn.mict-international.org"

RSYNC_CMD="/usr/bin/rsync"
RSYNC_OPT="-c"
RSYNC_SSH="ssh -p ${SSH_PORT}"
RSYNC_SRC_PATH="contentproxy"
RSYNC_SRC_GLOB="*.sqlite"
RSYNC_DST_PATH="/home/pfm/pfm_store"

echo ${RSYNC_CMD} ${RSYNC_OPT} "-e" \"${RSYNC_SSH}\" ${SSH_USER}@${SSH_HOST}:${RSYNC_SRC_PATH}/${RSYNC_SRC_GLOB} ${RSYNC_DST_PATH}

${RSYNC_CMD} ${RSYNC_OPT} -e "${RSYNC_SSH}" ${SSH_USER}@${SSH_HOST}:${RSYNC_SRC_PATH}/${RSYNC_SRC_GLOB} ${RSYNC_DST_PATH}

# rsync -vc -e "ssh -p2323" adam@syrn.mict-international.org:contentproxy/*.sqlite .


