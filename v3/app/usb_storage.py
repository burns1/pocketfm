#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import os
import pyudev
import magic

import pfm_signals
from pydispatch import dispatcher

PFM_USB_ROOT = '/media/usb0' # USB storage device mounting handled by Debian package 'usbmount'

PFM_USBSTORAGE_PLAYLIST = '/tmp/pfm_usb_playlist'

class USBStorage():
    def __init__(self):
        self.Active = False
        self.DevicePresent = False
        self.AudioFilesPresent = False
        self.DeviceName = ''
        self.DeviceFormat = ''
        self.DeviceSysDev = ''
        self.AudioFileList = []
        self.AudioFullPathFileList = []
        self.ScanRecursive = False # recurse search for audio files down file system

        try:
            self.udev_context = pyudev.Context()
            self.udev_monitor = pyudev.Monitor.from_netlink(self.udev_context)
            self.udev_monitor.filter_by('block') # we're only interested in block device events
            self.udev_observer = pyudev.MonitorObserver(self.udev_monitor, callback=self.watch_usbstorage_event, name='monitor-observer')
        except:
            pass
        self.udev_observer.start()
        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, self, False)
        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED, self, False)
        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, self, False)
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, self, True)

    def watch_usbstorage_event(self, device):
        AudioFiles = []
        AudioFullPathFiles = []

        if 'ID_FS_TYPE' in device:
            if device.action == 'add':
                self.Active = True
                self.DevicePresent = True
                self.DeviceName = device.get('ID_FS_LABEL')
                self.DeviceFormat = device.get('ID_FS_TYPE')
                self.DeviceSysDev = device.sys_name
                dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED, self, True)
                # print('USB Insertion: Device Name: {0} | Device Format: {1} | Device System dev: /dev/{2}'.format(self.DeviceName, self.DeviceFormat, self.DeviceSysDev))
                for root, dirs, files in os.walk(PFM_USB_ROOT):
                    for file_ in files:
                        mime_type = magic.from_file(os.path.join(root, file_), mime=True)
                        if mime_type.startswith('audio/') or mime_type == 'application/octet-stream':
                            if self.ScanRecursive == True or root == PFM_USB_ROOT:
                                AudioFiles.append(file_)
                                AudioFullPathFiles.append(os.path.join(root, file_))
                                # print('{0} : {1}'.format(os.path.join(root, file_), magic.from_file(os.path.join(root, file_), mime=True)))
                if AudioFullPathFiles != self.AudioFullPathFileList:
                    self.AudioFullPathFileList = AudioFullPathFiles
                    dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES, self, self.AudioFullPathFileList)
                    # regenerate playlist file for mplayer
                    with open(PFM_USBSTORAGE_PLAYLIST, 'w+') as f: 
                        i = ''
                        for i in self.AudioFullPathFileList:
                            f.write('{}\n'.format(i))
                    if len(self.AudioFullPathFileList) > 0:                                                              # Audio content found on USB storage partition
                        self.AudioFilesPresent = True
                        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, self, True)
                        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, self, True)                            # USB is in a state to play audio
                        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_PLAYLIST, self, PFM_USBSTORAGE_PLAYLIST) # send a signal that playlist has updated
                    else:                                                                                                # No audio content found on USB storage device partition
                        self.AudioFilesPresent = False
                        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, self, False)
                        dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, self, False)
                if AudioFiles != self.AudioFileList:
                    self.AudioFileList = AudioFiles
            elif device.action == 'remove':
                # print('USB Removal  : Device Name: {0} | Device Format: {1} | Device System dev: /dev/{2}'.format(self.DeviceName, self.DeviceFormat, self.DeviceSysDev))
                dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST, self, False)
                dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ATTACHED, self, False)
                dispatcher.send(pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, self, False)
                self.Active = False
                self.DevicePresent = False
                self.DeviceName = None
                self.DeviceFormat = None
                self.DeviceSysDev = None
                self.AudioFileList = []
                self.AudioFullPathFileList = []

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender

def handle_play_usb_list(data, signal, sender):
    pass

def main():
    import sys
    import time
    from pfm import PocketFM

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB1, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB2, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADFILE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADLIST, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_ACTIVE, sender=dispatcher.Any)
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_USBSTORAGE_AUDIO_FILES, sender=dispatcher.Any)

    # print('Maxpro switch power on')
    # dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'usb_storage.py main()', True)
    # time.sleep(2)
    # print('Maxpro switch power off')
    # dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'usb_storage.py main()', False)
    # time.sleep(2)
    print('Maxpro switch power on')
    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'usb_storage.py main()', True)
    time.sleep(2)
    # print('Maxpro switch to digital input')
    # dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'usb_storage.py main()', 'digital')
    # time.sleep(2)
    # print('Maxpro switch to analog input')
    # dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'usb_storage.py main()', 'analog')
    # time.sleep(2)
    # print('Maxpro switch to digital input')
    # dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'usb_storage.py main()', 'digital')
    # time.sleep(2)

    # turn USB2 on
    dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'usb_storage.py main()', True)
    time.sleep(10)
    dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'usb_storage.py main()', 'usb')
    time.sleep(1000)

if __name__ == "__main__":
    main()
