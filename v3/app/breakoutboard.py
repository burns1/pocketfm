#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import getopt
import sys
import re
import ast
import logging


from threading import Thread,currentThread
from pydispatch import dispatcher
from time import sleep

import serialmanager
#from display import Display
from led import LED
from encoder import Encoder
from power import Power
from adc import ADC
from rtc import RTC
from gps import GPS

import pfm_signals

class BreakoutBoard(Thread):

    def __init__(self):
        self.BBQuery = "\x1b?V"
        self.SoftwareVersion = 0
        self.BootloaderVersion = 0
        self.HardwareVersion = 0
        self.SoftwareCRC = 0

        self.closing = False
        self.writeBreakoutBoard = "sendStrToBreakoutBoard"

        # Initialise SerialManager object
        # self.serialDevice = "/dev/pts/8"
        self.ser = serialmanager.SerialManager("/dev/ttyAMA0")

        # Initialise Breakout Board Components
        #self.Display = Display()
        self.LED = LED()
        self.Encoder = Encoder()
        self.Power = Power()
        self.ADC = ADC()
        self.RTC = RTC()
        self.GPS = GPS()
        Thread.__init__(self, name='BreakoutBoardLoop', target=self.InputLoop)
        dispatcher.connect(self.writeBB, signal=self.writeBreakoutBoard, sender=dispatcher.Any)
        dispatcher.connect(self.writeBB, signal=pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, sender=dispatcher.Any)

    def writeBB(self, BBString):
        self.ser.out_queue.put(BBString)

    def InputLoop(self):
        try:
            while not self.closing:
                data = self.ser.in_queue.get()
                gotEsc = False
                gotCmd = False
                gotParams = False
                parameters = []
                paramCnt = 0
                gpsStr = ""
                gpsParams = []
                paramStr = ""
                postData = bytearray([1]*64)
                cmdCnt = 0
                command = ""
                first_sender = object()
                for i, c in enumerate(data):

                    if gotEsc == True and gotCmd == False and gotParams == False:
                        if  str.isdigit(c): # numeric parameter
                            paramStr = paramStr + c
                        elif c ==" " or c == "," or c == "/" or c == ":": # parameter seperators
                            try:
                                parameters.append(int(paramStr))
                                paramCnt += 1
                            except ValueError:
                                logging.debug('parameter ValueError "{}"'.format(paramStr))
                            paramStr = ""
                        elif str.isalpha(c): # command
                            try:
                                parameters.append(int(paramStr))
                                paramCnt += 1
                            except ValueError:
                                logging.debug('parameter ValueError "{}"'.format(paramStr))
                            gotParams = True
                            command = c
                            cmdCnt += 1
                            # logging.debug 'Got command ({0} in buffer): {1}  with {2} parameters: {3}'.format(cmdCnt, command, paramCnt, parameters)
                            if command == "B":
                                postData = data[i:i+64]
                                i += 64
                            if command == "I":
                                # logging.debug(parameters)
                                if paramCnt == 9:
                                    self.ADC.updateInputVoltage( parameters[0] / 1000.0 )
                                    self.ADC.updateInputCurrent( parameters[1] / 1000.0 )
                                    self.ADC.updateAtmelVoltage( parameters[2] / 1000.0 )
                                    self.ADC.updateSatelliteVoltage( parameters[3] / 1000.0 )
                                    # check spec, but these 2 appear to be reversed
                                    self.ADC.updateTransmitterVoltage( parameters[5] / 1000.0 )
                                    self.ADC.updateSecondInputVoltage( parameters[4] / 1000.0 )

                                    self.ADC.updateRaspberryPiVoltage( parameters[6] / 1000.0 )
                                    self.ADC.updateVU1Voltage( parameters[7] / 1000.0 )
                                    self.ADC.updateVU2Voltage( parameters[8] / 1000.0 )
                                else:
                                    logging.debug('InputLoop(): Got Command "I" with wrong number of {} parameters'.format(paramCnt))

                            if command == "E":
                                if paramCnt == 2:
                                    self.Encoder.updateAnalogRotator( parameters[0] )
                                    self.Encoder.updateButton( parameters[1] & 0xC0 )
                                    self.Encoder.updateDigitalRotatorLeftRight( parameters[1] & 0x30 )
                                else:
                                    logging.debug('InputLoop(): Got Command "E" with wrong number of {} parameters'.format(paramCnt))

                            if command == "L":
                                if paramCnt == 2:
                                    self.LED.updateLightMap( parameters[0] ) # should delete this in preference for array for lights
                                    self.LED.updateBrightness( parameters[1] )
                                    self.LED.updateColours( [parameters[0] & 7, (parameters[0] >> 3) & 7, (parameters[0] >> 6) & 7, (parameters[0] >> 9) & 7, (parameters[0] >> 12) & 7] )
                                else:
                                    logging.debug('InputLoop(): Got Command "L" with wrong number of {} parameters'.format(paramCnt))

                            if command == "P":
                                if paramCnt == 2:
                                    self.Power.updateMode( parameters[0] )
                                else:
                                    logging.debug('InputLoop(): Got Command "P" with wrong number of {} parameters'.format(paramCnt))

                            if command == "T":
                                if paramCnt == 8:
                                    self.RTC.updateRTC( parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], parameters[6], parameters[7] )
                                else:
                                    logging.debug('InputLoop(): Got Command "T" with wrong number of {} parameters'.format(paramCnt))

                            if command == "V":
                                if paramCnt == 4:
                                    self.SoftwareVersion = parameters[0]
                                    self.BootloaderVersion = parameters[1]
                                    self.HardwareVersion = parameters[2]
                                    self.SoftwareCRC = parameters[3]
                                    logging.debug(' Breakout Board Hardware version:   {}'.format(self.HardwareVersion))
                                    logging.debug(' Breakout Board Bootloader version: {}'.format(self.BootloaderVersion))
                                    logging.debug(' Breakout Board Software version:   {}'.format(self.SoftwareVersion))
                                    logging.debug(' Breakout Board Software CRC:       {}'.format(self.SoftwareCRC))
                                    dispatcher.send(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_SOFTWARE_VERSION, self, self.SoftwareVersion)
                                    dispatcher.send(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_HARDWARE_VERSION, self, self.HardwareVersion)
                                    dispatcher.send(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_BOOTLOADER_VERSION, self, self.BootloaderVersion)
                                else:
                                    logging.debug('DEBUG: breakoutboard.py: InputLoop(): Got Command "V" with wrong number of {} parameters'.format(paramCnt))

                            if command == "B":
                                #logging.debug('DEBUG: breakoutboard.py InputLoop(): GotCommand Screen dump DEBUG', parameters)
                                pass
                            if command == "G":
                                #  logging.debug('DEBUG: breakoutboard.py InputLoop(): GotCommand GPS DEBUG', parameters)
                                pass
                            gotEsc = False
                            gotParams = False
                            paramCnt = 0
                            paramStr = ""
                            parameters = []
                    else:
                        #  when we are outside ESC mode, we pick up GPS data if GPS is sending it to us
                        if c == "\n" or c== "\r":
                            gpsParams = gpsStr.split(",")
                            # logging.debug "**** GPS String:", gpsParams
                            # print('DEBUG: BB gpsParams len {}'.format(len(gpsParams)))
                            if len(gpsParams) > 6:
                                if gpsParams[0].startswith( '$GPGLL'):
                                    if gpsParams[6] == 'A':
                                        self.GPS.updateLatitude(gpsParams[1], gpsParams[2])
                                        self.GPS.updateLongitude(gpsParams[3], gpsParams[4])
                                        self.GPS.updateGotFix(True)
                                    elif gpsParams[6] == 'V':
                                        self.GPS.updateGotFix(False)
                                    # logging.debug('BB DEBUG {}'.format(gpsParams[5]))
                                    self.GPS.updateTime(gpsParams[5]) #  TEST: this should give an event for 1 second increments whether we have fix or not?
                                gpsStr = ""
                        elif c != "\x1b":
                            gpsStr = gpsStr + c
            
                    if c == "\x1b":
                        gotEsc = True

        except (KeyboardInterrupt, SystemExit):
            self.close()
            print "DEBUG: Breakout Board Interrupt"
            sys.exit(2)

        finally:
            self.close()

    def close(self):
        self.closing = True


def main(argv):
    """
    Main entry function in case the program is called from the command line.
    """

    try:
        options, arguments = getopt.getopt(argv, 'p:s:');
    except getopt.GetoptError:
        print "example usage: " 
        sys.exit(2)

    dictionary = {}
    for option, argument in options:
        if(option == '-p'):
            dictionary['power'] = ast.literal_eval(argument)
            print 'power = ', dictionary['power']
        if(option == '-i'):
            dictionary['screen'] = argument

    try: 
        b = BreakoutBoard()
        b.start()

        print "Running State: b.is_live: {} b.closing: {} ".format(b.is_alive(), b.closing)

        b.close()
        b.join()

    except (KeyboardInterrupt, SystemExit):
       print "\nInterrupt State: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
       pass

if __name__ == "__main__":
    """If invoked from the command line, call
    the main function including all command line parameters.
    """
    main(sys.argv[1:])

