from pydispatch import dispatcher
import logging
import time
import pfm_logging

SIG_SET_PFM_SOFTWARE_VERSION = 'Pocket FM Set Software Version'
SIG_SET_PFM_DARK_MODE = 'PFM Set Dark Mode State'

SIG_UPDATE_PFM_HOSTNAME = 'Pocket FM Host Identifier'
SIG_UPDATE_PFM_SOFTWARE_VERSION = 'Pocket FM Software Version'
SIG_UPDATE_PFM_DARK_MODE = 'PFM Dark Mode State'



SIG_UPDATE_RASPBERRYPI_SERIAL_NUMBER = 'Raspberry Pi SoC Serial Number'

SIG_BREAKOUTBOARD_SEND_COMMAND = 'Send Escape command to Breakout Board'
SIG_UPDATE_BREAKOUTBOARD_SOFTWARE_VERSION = 'Breakout Board Sofware Version'
SIG_UPDATE_BREAKOUTBOARD_HARDWARE_VERSION = 'Breakout Board Hardware Version'
SIG_UPDATE_BREAKOUTBOARD_BOOTLOADER_VERSION = 'Breakout Board Bootloader Version'

SIG_UPDATE_ADC_INPUT_VOLTAGE = 'ADC Input Voltage'
SIG_UPDATE_ADC_INPUT_CURRENT = 'ADC Input Current'
SIG_UPDATE_ADC_ATMEL_VOLTAGE = 'ADC Atmel Voltage'
SIG_UPDATE_ADC_SATELLITE_VOLTAGE = 'ADC Satellite Voltage'
SIG_UPDATE_ADC_TRANSMITTER_VOLTAGE = 'ADC Transmitter Voltage'
SIG_UPDATE_ADC_SECONDINPUT_VOLTAGE = 'ADC SecondInput Voltage'
SIG_UPDATE_ADC_RASPBERRYPI_VOLTAGE = 'ADC RaspberryPi Voltage'
SIG_UPDATE_ADC_VU1_VOLTAGE = 'ADC VU1 Voltage'
SIG_UPDATE_ADC_VU2_VOLTAGE = 'ADC VU2 Voltage'

SIG_ENCODER_ROTATOR_ANALOG = 'Encoder Rotator Analog'
SIG_ENCODER_ROTATOR_ANALOG_RIGHT = 'Encoder Rotator Analog Right'
SIG_ENCODER_ROTATOR_ANALOG_LEFT = 'Encoder Rotator Analog Left'
SIG_ENCODER_ROTATOR_RIGHT = 'Encoder Rotator Right'
SIG_ENCODER_ROTATOR_LEFT = 'Encoder Rotator Left'
SIG_ENCODER_BUTTON_PRESSED = 'Encoder Button Pressed'
SIG_ENCODER_BUTTON_RELEASED = 'Encoder Button Released'

SIG_UPDATE_GPS_ACTIVE = 'GPS Active' # True or False: If True, GPS Power == True, GPS Enabled via Breakout Board
SIG_UPDATE_GPS_GOT_FIX = 'GPS Satellite Position Fix'
SIG_UPDATE_GPS_LATITUDE = 'GPS Position Latitude'
SIG_UPDATE_GPS_LATITUDE_DIRECTION = 'GPS Position Latitude Direction'
SIG_UPDATE_GPS_LONGITUDE = 'GPS Position Longitude'
SIG_UPDATE_GPS_LONGITUDE_DIRECTION = 'GPS Position Longitude Direction'
SIG_UPDATE_GPS_ALTITUDE = 'GPS Position Altitude'
SIG_UPDATE_GPS_TIME = 'GPS Time'
SIG_UPDATE_GPS_POSITION = 'GPS Composite Position'

SIG_UPDATE_GSM_NETWORK_NAME = 'GSM Network Name'

SIG_UPDATE_LED_LIGHTMAP = 'LED Light Map'
SIG_UPDATE_LED_COLOUR = 'LED Colour'
SIG_UPDATE_LED_BRIGHTNESS = 'LED Brightness'
SIG_UPDATE_LED_LEVELMETER_COLOUR = 'LED Level Meter Colour'
SIG_UPDATE_LED_POWERSTATE_COLOUR_ON = 'LED Power OFF State Colour'
SIG_UPDATE_LED_POWERSTATE_COLOUR_OFF = 'LED Power ON State Colour'

SIG_UPDATE_POWER_MODE = 'Power State bitmap'
SIG_UPDATE_POWER_DISPLAY = 'Power State of Display'
SIG_UPDATE_POWER_GPS = 'Power State of GPS Module'
SIG_UPDATE_POWER_MAXPROBOARD = 'Power State of MaxProBoard'
SIG_UPDATE_POWER_SATELLITE = 'Power State of Satellite Receiver'
SIG_UPDATE_POWER_USB1 = 'Power State of Breakout Board USB1'
SIG_UPDATE_POWER_USB2 = 'Power State of Breakout Board USB2'
SIG_UPDATE_POWER_RASPBERRYPI = 'Power State of RaspberryPi ;-)'
SIG_UPDATE_POWER_ATMEL = 'Power State of Breakout Board Atmel'

SIG_SET_POWER_DISPLAY = 'Power Set Display'
SIG_SET_POWER_GPS = 'Power Set GPS Module'
SIG_SET_POWER_MAXPROBOARD = 'Power Set MaxProBoard'
SIG_SET_POWER_SATELLITE = 'Power Set Satellite Receiver'
SIG_SET_POWER_USB1 = 'Power Set Breakout Board USB1'
SIG_SET_POWER_USB2 = 'Power Set Breakout Board USB2'
SIG_SET_POWER_RASPBERRYPI = 'Power Set RaspberryPi ;-)'
SIG_SET_POWER_ATMEL = 'Power Set Breakout Board Atmel'

SIG_SET_RTC = 'RTC Set Real Time Clock'
SIG_SET_RTC_SYSTEMTIME = 'PFM Set System Time to RTC'
SIG_UPDATE_RTC_WEEKDAY = 'RTC Weekday'
SIG_UPDATE_RTC_DAY = 'RTC Day'
SIG_UPDATE_RTC_MONTH = 'RTC Month'
SIG_UPDATE_RTC_YEAR = 'RTC Year'
SIG_UPDATE_RTC_HOUR = 'RTC Hour'
SIG_UPDATE_RTC_MINUTE = 'RTC Minute'
SIG_UPDATE_RTC_SECOND = 'RTC Second'
SIG_UPDATE_RTC_FLAGS = 'RTC Flags'
SIG_UPDATE_RTC = 'RTC Update'

# Read only Maxpro I2C values
SIG_UPDATE_MAXPRO_FREQUENCY = 'MaxProBoard FM Frequency'
SIG_UPDATE_MAXPRO_STATIONID = 'MaxProBoard RDS Station ID'
SIG_UPDATE_MAXPRO_OUTPUT_POWER = 'MaxProBoard Transmission Power'
SIG_UPDATE_MAXPRO_POWER_PERCENT = 'MaxProBoard Power Percent'
SIG_UPDATE_MAXPRO_AUDIO_INPUT = 'MaxProBoard Audio Input'
SIG_UPDATE_MAXPRO_REFLECTED_POWER = 'MaxProBoard Reflected Power'
SIG_UPDATE_MAXPRO_TEMPERATURE = 'MaxProBoard Temperature'
SIG_UPDATE_MAXPRO_VOLTAGE = 'MaxProBoard Voltage'
SIG_UPDATE_MAXPRO_UPTIME_DAY = 'MaxProBoard Uptime Day'
SIG_UPDATE_MAXPRO_UPTIME_HOUR = 'MaxProBoard Uptime Hour'
SIG_UPDATE_MAXPRO_UPTIME_MINUTE = 'MaxProBoard Uptime Minute'
SIG_UPDATE_MAXPRO_VU_LEFT = 'MaxProBoard VU Left'
SIG_UPDATE_MAXPRO_VU_RIGHT = 'MaxProBoard VU Right'
SIG_UPDATE_MAXPRO_ERROR_STATUS = 'MaxProBoard Error Status'
SIG_UPDATE_MAXPRO_READ_FREQUENCY = 'MaxProBoard Read FM Frequency'  # Note this is dynamic read from MAXPRO, not(!) last set!!!!

# Write only Maxpro I2C values
SIG_SET_MAXPRO_FREQUENCY = 'MaxProBoard Set FM Frequency'
SIG_SET_MAXPRO_STATIONID = 'MaxProBoard Set RDS Station ID'
"""
Purpose : demands a new station id given by its value
Sent    : By panel and other possible system parts
Received: maxpro.py,
Outcome : if a usable station id was given, it set, which is indicated by `SIG_UPDATE_MAXPRO_STATIONID`
Notes   : Accepts invalid content, inclusive None-Value
"""
SIG_SET_MAXPRO_POWER_PERCENT = 'MaxProBoard Set Power Percent'
SIG_SET_MAXPRO_OUTPUT_POWER = 'MaxProBoard Set Transmission Power'
SIG_SET_MAXPRO_AUDIO_INPUT = 'MaxProBoard Set Audio Input'

# special signal for a one shot forced write of write only MAXPRO values - used when powering MAXPRO up again
SIG_SET_MAXPRO_FORCEWRITE = 'MaxProBoard Set Force I2C Write'

# Maxpro Stats
SIG_UPDATE_MAXPRO_I2C_WRITE_SUCCESSES = 'MaxProBoard I2C write successes'
SIG_UPDATE_MAXPRO_I2C_WRITE_ERRORS = 'MaxProBoard I2C write errors'
SIG_UPDATE_MAXPRO_I2C_READ_SUCCESSES = 'MaxProBoard I2C read successes'
SIG_UPDATE_MAXPRO_I2C_READ_ERRORS = 'MaxProBoard I2C read errors'

SIG_UPDATE_WIFI_ACTIVE = 'WiFi Device Active'
SIG_UPDATE_WIFI_STATUS = 'WiFi Device Status'
SIG_UPDATE_WIFI_IP_ADDRESS = 'WiFi IP Address'
SIG_UPDATE_WIFI_AP_ACTIVE = 'WiFi Access Point Device Active'
SIG_UPDATE_WIFI_CLIENT_ACTIVE = 'WiFi Client Device Active'
SIG_UPDATE_WIFI_MODE = 'WiFi Device Current Mode'
SIG_UPDATE_WIFI_MODE_LIST = 'WiFi Device Mode List' # AP or Client Mode or OFF
SIG_UPDATE_WIFI_AP_NETWORK = 'WiFi AP Mode Network Name'
SIG_UPDATE_WIFI_AP_PASSWORD = 'WiFi AP Mode Network Password'
SIG_UPDATE_WIFI_AP_STATUS = 'WiFi AP Mode Status'
SIG_UPDATE_WIFI_AVAILABLE_NETWORKS = 'WiFi Network Scan'
SIG_UPDATE_WIFI_CLIENT_NETWORK ='WiFi Client Mode Network SSID'
SIG_UPDATE_WIFI_CLIENT_PASSWORD ='WiFi Client Mode Password'
SIG_UPDATE_WIFI_CLIENT_STATUS ='WiFi Client Mode Status'
SIG_SET_WIFI_AVAILABLE_NETWORKS = 'WiFi Set Network Scan'
SIG_SET_WIFI_AP_ACTIVE = 'WiFi Set Access Point Device Active'
SIG_SET_WIFI_CLIENT_ACTIVE = 'WiFi Set Client Device Active'
SIG_SET_WIFI_MODE = 'WiFi Set Device Current Mode'
SIG_SET_WIFI_AP_NETWORK = 'WiFi Set AP Mode Network Name'
SIG_SET_WIFI_AP_PASSWORD = 'WiFi Set AP Mode Network Password'
SIG_SET_WIFI_CLIENT_NETWORK ='WiFi Set Client Mode Network SSID'
SIG_SET_WIFI_CLIENT_PASSWORD ='WiFi Set Client Mode Password'

SIG_UPDATE_3G_ACTIVE = '3G Device Active'
SIG_UPDATE_3G_STATUS = '3G Device Status'
SIG_UPDATE_3G_IP_ADDRESS = '3G IP Address'
SIG_UPDATE_3G_MODE = '3G Networking Mode' # ON or OFF
SIG_UPDATE_3G_DIALNUMBER = '3G APN Dial Number'
SIG_UPDATE_3G_APN = '3G APN'
SIG_UPDATE_3G_USERNAME = '3G APN username'
SIG_UPDATE_3G_PASSWORD = '3G APN password'
SIG_SET_3G_ACTIVE = '3G Set 3G Connection Activation'
SIG_SET_3G_MODE = '3G Set Networking Mode' # ON or OFF
SIG_SET_3G_DIALNUMBER = '3G Set APN Dial Number'
SIG_SET_3G_APN = '3G Set APN'
SIG_SET_3G_USERNAME = '3G Set APN Username'
SIG_SET_3G_PASSWORD = '3G Set APN Password'
SIG_UPDATE_3G_SIM_INSERTED = 'SIM Card Inserted'
SIG_UPDATE_3G_SIM_REMOVED = 'SIM Card Removed'
SIG_UPDATE_3G_SIM_GPIO_INITIAL_STATE = 'SIM Card GPIO Initial State'


SIG_UPDATE_ETHERNET_ACTIVE = 'ConnectionManager Ethernet connection state'

SIG_UPDATE_AUDIOMANAGER_INPUT = 'AudioManager Input'
SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS = 'AudioManager Input Status'
SIG_UPDATE_AUDIOMANAGER_INPUT_LIST = 'AudioManager Input List'
SIG_SET_AUDIOMANAGER_INPUT = 'AudioManager Set Input'
SIG_SET_AUDIOMANAGER_INPUT_LIST = 'AudioManager Set Input List'

SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED = 'ConnectionManager PFM network connection state'

SIG_UPDATE_MPLAYER_LOADFILE = 'MPlayer Loadfile'
SIG_UPDATE_MPLAYER_LOADLIST = 'MPlayer Loadlist'
SIG_UPDATE_MPLAYER_STOP = 'MPlayer Stop'
SIG_UPDATE_MPLAYER_PAUSE = 'MPlayer Pause'
SIG_UPDATE_MPLAYER_LOOP = 'MPlayer Loop'
SIG_UPDATE_MPLAYER_OUTPUT = 'MPlayer Playlist Output'
SIG_SET_MPLAYER_LOADFILE = 'MPlayer Set Loadfile'
SIG_SET_MPLAYER_LOADLIST = 'MPlayer Set Loadlist'
SIG_SET_MPLAYER_STOP = 'MPlayer Set Stop'
SIG_SET_MPLAYER_PAUSE = 'MPlayer Set Pause'
SIG_SET_MPLAYER_LOOP = 'MPlayer Set Loop'

SIG_UPDATE_SATELLITE_ACTIVE = 'Satellite Active ' # True or False: If True, Satellite power == True, transponders scanned, channel list populated, signal strength & SNR active
SIG_UPDATE_SATELLITE_STATUS = 'Satellite Status' 
SIG_UPDATE_SATELLITE_SIGNAL_LEVEL = 'Satellite Signal Level'
SIG_UPDATE_SATELLITE_SIGNAL_QUALITY = 'Satellite Signal Quality'
SIG_UPDATE_SATELLITE_FEMON_OUTPUT = 'Satellite FEMON Output'
SIG_UPDATE_SATELLITE_CHANNEL_NAME = 'Satellite Channel Service Name'
SIG_UPDATE_SATELLITE_CHANNEL_SERVICE_ID = 'Satellite Channel Service ID'
SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY = 'Satellite Channel Frequency'
SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION = 'Satellite Channel Polarisation'
SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE = 'Satellite Channel Symbol Rate'
SIG_UPDATE_SATELLITE_CHANNEL_FEC = 'Satellite Channel FEC'
SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID = 'Satellite Channel Audio PID'
SIG_UPDATE_SATELLITE_CHANNEL_VIDEO_PID = 'Satellite Channel Video PID'
SIG_UPDATE_SATELLITE_CHANNEL_CUSTOM_SETTINGS = 'Satellite PFM Custom Channel Settings'

SIG_SET_SATELLITE_CHANNEL_NAME = 'Satellite Set Channel Service Name'
SIG_SET_SATELLITE_CHANNEL_SERVICE_ID = 'Satellite Set Channel Service ID'
SIG_SET_SATELLITE_CHANNEL_FREQUENCY = 'Satellite Set Channel Frequency'
SIG_SET_SATELLITE_CHANNEL_POLARISATION = 'Satellite Set Channel Polarisation'
SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE = 'Satellite Set Channel Symbol Rate'
SIG_SET_SATELLITE_CHANNEL_FEC = 'Satellite Set Channel FEC'
SIG_SET_SATELLITE_CHANNEL_AUDIO_PID = 'Satellite Set Channel Audio PID'
SIG_SET_SATELLITE_CHANNEL_VIDEO_PID = 'Satellite Set Channel Video PID'
SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS = 'Satellite Set PFM Custom Channel Settings'

SIG_UPDATE_USBSTORAGE_ACTIVE = 'USB Storage Device Active' # True or False: If True, USB2 power == True, USB stick inserted, File System mounted & playable audio files found
SIG_UPDATE_USBSTORAGE_ATTACHED = 'USB Storage Device Attached'
SIG_UPDATE_USBSTORAGE_AUDIO_FILES_EXIST = 'USB Storage Device Contains Audio Files'
SIG_UPDATE_USBSTORAGE_AUDIO_FILES = 'USB Storage Audio File List'
SIG_UPDATE_USBSTORAGE_AUDIO_PLAYLIST = 'USB Storage Audio Playlist'


SIG_UPDATE_INTERNALSTORAGE_ACTIVE = 'Internal Storage Device Active' # True or False: If True, SD storage is accessible & contains audio files
SIG_UPDATE_INTERNALSTORAGE_AUDIO_PLAYLIST = 'Internal Storage Audio Playlist'
SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES = 'Internal Storage Audio File List'
SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES_EXIST = 'Internal Storage Device Contains Audio Files'

SIG_SET_INTERNETSTREAM_DOMAIN = "Internet Stream Set Audio Domain"
SIG_SET_INTERNETSTREAM_PORT = "Internet Stream Set Audio Port"
SIG_SET_INTERNETSTREAM_PATH = "Internet Stream Set Audio Path"
SIG_SET_INTERNETSTREAM_URL = "Internet Stream Set Audio URL"
SIG_UPDATE_INTERNETSTREAM_DOMAIN = "Internet Stream Audio Domain"
SIG_UPDATE_INTERNETSTREAM_PORT = "Internet Stream Audio Port"
SIG_UPDATE_INTERNETSTREAM_PATH = "Internet Stream Audio Path"
SIG_UPDATE_INTERNETSTREAM_URL = "Internet Stream Audio URL"

SIG_UPDATE_PFM_LOCK_PIN = "Panel PIN"
SIG_SET_PFM_LOCK_PIN = "Panel Set PIN"

SIG_SET_PFM_SUPPORT_PIN = "PFM Set Support PIN"
SIG_UPDATE_PFM_SUPPORT_PIN = "PFM Support PIN"

SIG_UPDATE_PFM_LOCK_TIMEOUT = "Panel Lock Timeout"
SIG_SET_PFM_LOCK_TIMEOUT = "Panel Set Lock Timeout"

__values = {}
__updates = {}
__reads = {}
__senders = {}
__started = time.time()

__logging_signal = []
__logging_sender = []
logging_status = False

NOSIGYET = "nosigyet"
SENDER_PANEL = "Panel"
SENDER_CONFIG_MASTER = "Config MASTER"
SENDER_CONFIG_WORKING = "Config WORKING"
SENDER_MAXPRO = "MAXPRO"
SENDER_AUDIOMNG = "AUDIOMNG"
SENDER_ADC = "ADC"
SENDER_GPS = "GPS"
SENDER_WIFI = "WIFI"
SENDER_ENCODER = "ENCODER"


__nosig_value_calls = 0
__nosig_values = {}


def set_logging(status):
    global logging_status
    logging_status = status
    if status:
        logging.info("START LOGGING SIGNALS!")
        log_logging_status()
    else:
        logging.info("STOP LOGGING SIGNALS!")

def log_logging_status():
    if not logging_status:
        logging.info("NO SIGNAL LOGGING AT THE MOMENT!")
        return

    if __logging_signal.__len__() == 0:
        logging.info("Log ALL signals")
    else:
        logging.info("Log signals containing: {}".format(__logging_signal))

    if __logging_sender.__len__() == 0:
        logging.info("Log signals send by ANY Sender".format(__logging_sender))
    else:
        logging.info("Log signals where sender name contains: {}".format(__logging_sender))


def add_logging_by_sender(*args):
    for fragment in args:
        __logging_sender.append(fragment)

def add_logging_by_signal(*args):
    for fragment in args:
        __logging_signal.append(fragment)


def __logging_signal_applies(signal):
    if __logging_signal.__len__() == 0:
        return True
    for fragment in __logging_signal:
        # print "wanna find", fragment, " in ", signal
        if signal.find(fragment) != -1:
            return True
    return False


def __logging_sender_applies(sender):
    if __logging_sender.__len__() == 0:
        return True
    for fragment in __logging_sender:
        # print "wanna find", fragment, " in ", signal
        if str(sender).find(fragment) != -1:
            return True

    return False

#def __logging_filer_applies(signal):
#    return


def __pfm_signals_handle(data, signal, sender):
    __values.update({signal: data})
    current_updates = __updates.get(signal)
    #if current_updates is None:  # this can not happen, as long this handle connects only to the signals defined here
    #    raise Exception("current_updates is None?? Signal:", signal)
    __updates.update({signal: current_updates + 1})
    __senders.update({signal: sender})

    pfm_logging.signal_log.update_and_log(data, signal, sender, current_updates+1)

    if logging_status and __logging_signal_applies(signal) and __logging_sender_applies(sender):
        logging.info("{:>15.15} -> {:30.30} -> {}".format(sender, signal, data))

def get_value(signal):
    """ returns the last value for this signal or NOSIGYET if there was no
        signal yet.

        todo: maybe it should be more strict with not registered signals?
    """
    tmp = __values.get(signal)
    if tmp == NOSIGYET:
        global __nosig_value_calls
        __nosig_value_calls += 1
        __nosig_values.update({signal: NOSIGYET})
    else:
        __nosig_values.pop(signal, "remove from the list if in ...")

    return tmp


def get_value_with_default(signal, default_value):
    """ returns the last value for this signal default_value, if there was no
        signal yet.
        maybe it should be more strict with not registered signals?
    """
    current_reads = __reads.get(signal)
    if current_reads is None:
        current_reads = 0
    __reads.update({signal: current_reads + 1})
    pfm_logging.signal_log.update_reads(signal, current_reads + 1)

    tmp = __values.get(signal)

    if logging_status and __logging_signal_applies(signal):
        if tmp == NOSIGYET:
            logging.info("({},{}) USES DEFAULT, current reads: {}".format(signal, default_value, current_reads + 1))
        else:
            logging.info("({}) = {}    current reads: {}".format(signal, tmp, current_reads + 1))

    if tmp == NOSIGYET:
        global __nosig_value_calls
        __nosig_value_calls += 1
        __nosig_values.update({signal: default_value})
        return default_value

    __nosig_values.pop(signal, "remove from the list if in ...")
    return tmp


def is_value_set(signal, sender=None):
    """returns true, if the value is set, meaning the signal was sent at least once.
        if there is a sender given, it needs to match as well, eg:

        id_value_set (SIG1) will be True, if SIG1 was sent at least one time

        is_value_set (SIG2, SENDER1) will be only true, if SIG2 was send at least one time and the last
                                     signal came from SENDER1

    """
    tmp = __values.get(signal)
    if tmp == NOSIGYET:
        return False

    if sender is None:
        return True
    else:
        if sender == value_set_by(signal):
            return True

    return False


def signal_count(signal):
    return __updates.get(signal)


def value_set_by(signal):
    return __senders.get(signal)


def print_stats(use_logging_filter=True):
    sum = 0
    for signal in __updates:
        sum += __updates.get(signal)

    uptime = (time.time() - __started) / 60.0
    print "--------------------------------------------------------------------------------------------------------------"
    print "   pfm_signals stats. Uptime: {:.2f} minutes. {} signals processed, makes {:.2f} signals per minute ".format(uptime, sum, sum/uptime)
    print "--------------------------------------------------------------------------------------------------------------"
    print "          SIGNAL                             UPDATES upd/min READS   CURRENT TYPE AND VALUE"
    print "--------------------------------------------------------------------------------------------------------------"
    for signal in sorted(__updates.iterkeys()):
        if use_logging_filter and not __logging_signal_applies(signal):
            continue

        updates = __updates.get(signal)
        updates_per_minute = updates / uptime
        if updates_per_minute > 1:
            updates_per_minute = "{:.1f}".format(updates_per_minute)
        else:
            updates_per_minute = "     "

        # if signal no in __values
        value = __values.get(signal)
        print "{:<45}|{:>5} |{:>6} |{:>4} |  {:<20} {:42.42}".format(signal, updates, updates_per_minute,
                                                                     __reads.get(signal), type(value), str(value))
    print "--------------------------------------------------------------------------------------------------------------"

    count = 0
    for con1 in dispatcher.connections.iterkeys():
        for con2 in dispatcher.connections.get(con1):
            if con2 not in __values.keys():
                if count == 0:
                    print "The following signals are connected but not in pfm_signals.py:"

                print " ", con2, " received by:"
                for rec in dispatcher.getAllReceivers(signal=con2):
                    print "  > ", rec

                count += 1

    if count > 0:
        print "--------------------------------------------------------------------------------------------------------------"

    if __nosig_value_calls > 0:
        print __nosig_value_calls, "calls of get_value() with nosigyet values"

        for signal in __nosig_values.iterkeys():
            print "  {:<35}{}".format(signal, __nosig_values.get(signal))

        print "--------------------------------------------------------------------------------------------------------------"

        # check, if the handle still there
        # for rec in dispatcher.getAllReceivers(signal=SIG_ENCODER_ROTATOR_LEFT):
        #    print rec

    print_senders()


def print_senders (use_logging_filter=True):
    map = {}
    for signal in __senders.iterkeys():
        # print signal
        sender = __senders.get(signal)
        if sender not in map:
            # print " -------------- NEW Sender", sender
            map[sender] = []

        map[sender].append(signal)

    print "SENDERS ACTIVE SO FAR:", map.__len__() - 1
    print "SIGNALS NEVER SENT:",   map[NOSIGYET].__len__()

    for sender in map.iterkeys():
        if sender == NOSIGYET:
            continue
        else:
            print sender, " has set:"
            for signal in map.get(sender):
                value = __values.get(signal)
                print "      {:<45} to {} {:42.42}".format(signal, type(value), str(value))

def print_stats2(self):
    for tuple in sorted(self.sends.items(), key=lambda x: x[1]):
        print tuple

if logging_status:
    logging.info ("register signals:")

for signal in dir():
    if signal[:3] == "SIG":
        sstring = eval(signal)
        if logging_status:
            logging.info("   {:<40s} -> {} ".format(signal, sstring))
        #if __values.get(sstring):
        #    raise Exception("DOUBLE DEFINITION OF SIGNAL: ", signal, sstring)

        # pfm_logging.__buffer.put (("INSERT INTO SignalState VALUES (?,?,?,?,?,?,?)",
        #                            signal, sstring, 0, 0, str(NOSIGYET), str(NOSIGYET), tools.general.sqlite_pickle(NOSIGYET)))

        pfm_logging.signal_log.register(signal, sstring, NOSIGYET)

        dispatcher.connect(__pfm_signals_handle, sstring, weak=False)
        __values.update({sstring: NOSIGYET})
        __senders.update({sstring: NOSIGYET})
        __updates.update({sstring: 0})
        __reads.update({sstring: 0})

if logging_status:
    logging.info("done registering signals")
