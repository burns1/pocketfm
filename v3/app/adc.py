#!/usr/bin/python

from pydispatch import dispatcher
import pfm_signals

class ADC(object):
    def __init__(self):

        self.BBQuery = "\x1b?I"

        # Initialise internal representation
        self.InputVoltage = 0.0
        self.InputCurrent = 0.0
        self.AtmelVoltage = 0.0
        self.SatelliteVoltage = 0.0
        self.TransmitterVoltage = 0.0
        self.SecondInputVoltage = 0.0
        self.RaspberryPiVoltage = 0.0
        self.VU1Voltage = 0.0
        self.VU2Voltage = 0.0

        # Initialise pfm_signals
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE, self, self.InputVoltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_INPUT_CURRENT, self, self.InputCurrent)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_ATMEL_VOLTAGE, self, self.AtmelVoltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_SATELLITE_VOLTAGE, self, self.SatelliteVoltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_TRANSMITTER_VOLTAGE, self, self.TransmitterVoltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_SECONDINPUT_VOLTAGE, self, self.SecondInputVoltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_VU1_VOLTAGE, self, self.VU1Voltage)
        dispatcher.send(pfm_signals.SIG_UPDATE_ADC_VU2_VOLTAGE, self, self.VU2Voltage)

    def updateInputVoltage(self, voltage):
        if voltage != self.InputVoltage:
            self.InputVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE, self, self.InputVoltage)

    def updateInputCurrent(self, current):
        if current != self.InputCurrent:
            self.InputCurrent = current
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_INPUT_CURRENT, self, self.InputCurrent)

    def updateAtmelVoltage(self, voltage):
        if voltage != self.AtmelVoltage:
            self.AtmelVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_ATMEL_VOLTAGE, self, self.AtmelVoltage)

    def updateSatelliteVoltage(self, voltage):
        if voltage != self.SatelliteVoltage:
            self.SatelliteVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_SATELLITE_VOLTAGE, self, self.SatelliteVoltage)

    def updateTransmitterVoltage(self, voltage):
        if voltage != self.TransmitterVoltage:
            self.TransmitterVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_SATELLITE_VOLTAGE, self, self.TransmitterVoltage)

    def updateSecondInputVoltage(self, voltage):
        if voltage != self.SecondInputVoltage:
            self.SecondInputVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_SECONDINPUT_VOLTAGE, self, self.SecondInputVoltage)

    def updateRaspberryPiVoltage(self, voltage):
        if voltage != self.RaspberryPiVoltage:
            self.RaspberryPiVoltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_RASPBERRYPI_VOLTAGE, self, self.RaspberryPiVoltage)

    def updateVU1Voltage(self, voltage):
        if voltage != self.VU1Voltage:
            self.VU1Voltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_VU1_VOLTAGE, self, self.VU1Voltage)

    def updateVU2Voltage(self, voltage):
        if voltage != self.VU2Voltage:
            self.VU2Voltage = voltage
            dispatcher.send(pfm_signals.SIG_UPDATE_ADC_VU2_VOLTAGE, self, self.VU2Voltage)
