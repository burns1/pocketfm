from pydispatch import dispatcher
import ConfigParser
import time
import os

import pfm_signals
import logging

__master_file = "/home/pfm/pfm_master.ini"
__working_file = "/home/pfm/pfm_conf/pfm_working.ini"

__master_config = ConfigParser.RawConfigParser()
__master_config.optionxform = str


if os.path.isfile(__master_file):
    logging.info ("Use master config file: {}".format(__working_file))
    __master_config.read(__master_file)
else:
    logging.info ("NO MASTER CONFIG FILE: {}".format(__working_file))


__working_config = ConfigParser.RawConfigParser()
__working_config.optionxform = str

if os.path.isfile(__working_file):
    logging.info ("Use working config file: {}".format(__working_file))
    __working_config.read(__working_file)
else:

    if os.path.isfile(__master_file):
        logging.info ("NO working config file. Create it with the master options from {}".format(__master_file))
        with open(__working_file, 'wb') as tmp:
            __master_config.write(tmp)
            tmp.close()
        __working_config.read(__working_file)
    else:
        logging.info("NO working and no master config file. Create empty working file {}".format(__working_file))
        __working_config.add_section("SIGNALS")


def __configuration_update_handler(data, signal, sender):
    logging.info("UPDATE CONFIG: set '{}' to '{}' (signal from '{}')".format(signal, data, sender))  # , signal, data
    __working_config.set("SIGNALS", signal, str(data))
    updater = time.strftime("%Y-%m-%d-%H:%M:%S") + " by " + str(sender)
    __working_config.set("SIGNALS", signal + " LAST UPDATE", updater)
    #Disable buffering explicitly. Added ,0 while openening the
    #file handler for the working configuration file
    #added flush and sync too
    #FIXME probably the logging module has a buffer too?
    #zsolt, 19.8.2016
    with open(__working_file, 'wb', 0) as tmp:
        __working_config.write(tmp)
        tmp.flush()
        os.fsync(tmp)
        #probably we need to add a delay here for giving it
        #time to flush/sync
        tmp.close()

def __connect(signal):
    # print "Connect to config:", signal
    dispatcher.connect(__configuration_update_handler, signal, weak=False)


def read_and_send_int (sig_update, sig_set, connect_handler=False, master_only=False):
    if connect_handler:
        __connect(sig_update)

    if not master_only and __working_config.has_option ('SIGNALS', sig_update):
        val = __working_config.getint('SIGNALS', sig_update)
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_WORKING, val)
        logging.info("CONFIG FROM WORKING: {} -> {}".format(sig_update,val))
        return

    if __master_config.has_option('SIGNALS', sig_update):
        val = __master_config.getint('SIGNALS', sig_update)
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_MASTER, val)
        logging.info("CONFIG FROM MASTER: {} -> {}".format(sig_update,val))
        return

    logging.info("NO CONFIG ENTRY FOR: {}".format(sig_update))

def read_and_send_float (sig_update, sig_set, connect_handler=False, master_only=False):
    if connect_handler:
        __connect(sig_update)

    if not master_only and __working_config.has_option ('SIGNALS', sig_update):
        val = __working_config.getfloat('SIGNALS', sig_update)
        #if frq is not None:  # todo: finalize the policy!!!
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_WORKING, val)
        logging.info("CONFIG FROM WORKING: {} -> {}".format(sig_update,val))
        return

    if __master_config.has_option('SIGNALS', sig_update):
        val = __master_config.getfloat('SIGNALS', sig_update)
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_MASTER, val)
        logging.info("CONFIG FROM MASTER: {} -> {}".format(sig_update,val))
        return

    logging.info("NO CONFIG ENTRY FOR: {}".format(sig_update))

def read_and_send_str (sig_update, sig_set, connect_handler=False, master_only=False):
    if connect_handler:
        __connect(sig_update)

    if not master_only and __working_config.has_option ('SIGNALS', sig_update):
        val = __working_config.get('SIGNALS', sig_update)
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_WORKING, val)
        logging.info("CONFIG FROM WORKING: {} -> {}".format(sig_update,val))
        return

    if __master_config.has_option('SIGNALS', sig_update):
        val = __master_config.get('SIGNALS', sig_update)
        dispatcher.send(sig_set, pfm_signals.SENDER_CONFIG_MASTER, val)
        logging.info("CONFIG FROM MASTER: {} -> {}".format(sig_update,val))
        return

    logging.info("NO CONFIG ENTRY FOR: {}".format(sig_update))



def reset_to_master():
    logging.info("SET VALUES BACK TO DEFAULT!!")
    #__master_config.read(__master_file)
    set_values(False, True)

def set_values(connect_handler=False, master_only=False):

    #  PFM hardware name / ID
    # read_and_send_str(pfm_signals.SIG_UPDATE_PFM_HOSTNAME, pfm_signals.SIG_UPDATE_PFM_HOSTNAME,
    #                      connect_handler, master_only)
    #  PFM software version
    read_and_send_str(pfm_signals.SIG_UPDATE_PFM_SOFTWARE_VERSION, pfm_signals.SIG_SET_PFM_SOFTWARE_VERSION,
                         connect_handler, master_only)

    #  MAXPROBOARD signal configuration
    read_and_send_float(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, pfm_signals.SIG_SET_MAXPRO_FREQUENCY,
                         connect_handler, master_only)
    read_and_send_int(pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT,
                         connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, pfm_signals.SIG_SET_MAXPRO_STATIONID,
                      connect_handler, master_only)

    #  AUDIOMANAGER signal configuration
    read_and_send_str(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, pfm_signals.SIG_SET_AUDIOMANAGER_INPUT,
                      connect_handler, master_only)

    #  AUDIOMANAGER SATELLITE signal configuration
    read_and_send_str(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, pfm_signals.SIG_SET_SATELLITE_CHANNEL_FREQUENCY,
                      connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, pfm_signals.SIG_SET_SATELLITE_CHANNEL_POLARISATION,
                      connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE, pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE,
                      connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, pfm_signals.SIG_SET_SATELLITE_CHANNEL_AUDIO_PID,
                      connect_handler, master_only)

    #  AUDIOMANAGER INTERNETSTREAM signal configuration
    read_and_send_str(pfm_signals.SIG_UPDATE_INTERNETSTREAM_DOMAIN, pfm_signals.SIG_SET_INTERNETSTREAM_DOMAIN,
                      connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PORT, pfm_signals.SIG_SET_INTERNETSTREAM_PORT,
                      connect_handler, master_only)
    read_and_send_str(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PATH, pfm_signals.SIG_SET_INTERNETSTREAM_PATH,
                      connect_handler, master_only)
    # read_and_send_str(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL, pfm_signals.SIG_SET_INTERNETSTREAM_URL,
    #                   connect_handler, master_only)

    #  CONNECTIONMANAGER signal configuration
    read_and_send_str(pfm_signals.SIG_UPDATE_WIFI_MODE, pfm_signals.SIG_SET_WIFI_MODE,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, pfm_signals.SIG_SET_WIFI_AP_PASSWORD,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_3G_MODE, pfm_signals.SIG_SET_3G_MODE,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_3G_DIALNUMBER, pfm_signals.SIG_SET_3G_DIALNUMBER,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_3G_APN, pfm_signals.SIG_SET_3G_APN,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_3G_USERNAME, pfm_signals.SIG_SET_3G_USERNAME,
                      connect_handler, master_only)

    read_and_send_str(pfm_signals.SIG_UPDATE_3G_PASSWORD, pfm_signals.SIG_SET_3G_PASSWORD,
                      connect_handler, master_only)

    #  UI PIN LOCK AND TIMEOUT
    read_and_send_str(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, pfm_signals.SIG_SET_PFM_LOCK_PIN,
                      connect_handler, master_only)
    read_and_send_int(pfm_signals.SIG_UPDATE_PFM_LOCK_TIMEOUT, pfm_signals.SIG_SET_PFM_LOCK_TIMEOUT,
                      connect_handler, master_only)

    #  DARK MODE configuration
    read_and_send_str(pfm_signals.SIG_UPDATE_PFM_DARK_MODE, pfm_signals.SIG_SET_PFM_DARK_MODE,
                      connect_handler, master_only)

    #  SUPPORT PIN (for remote support to change pin)
    read_and_send_str(pfm_signals.SIG_UPDATE_PFM_SUPPORT_PIN, pfm_signals.SIG_SET_PFM_SUPPORT_PIN,
                      connect_handler, False)

# set values from working AND/OR master and connect signals
set_values(True, False)

