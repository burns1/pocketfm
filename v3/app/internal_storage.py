#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import os
import socket
import magic
import subprocess
from threading import Thread
from pydispatch import dispatcher
import logging

import pfm_signals

PFM_INTERNALSTORAGE_ROOT = '/home/pfm/pfm_store/Audio' 

PFM_INTERNALSTORAGE_PLAYLIST = '/tmp/pfm_internal_playlist'

PFM_FTP_AUTH_DIR = '/home/pfm/pfm_conf/vsftpd/'     #  This is defined in /etc/pam.d/vsftpd.virtual
PFM_FTP_AUTH_TXT_FILE = PFM_FTP_AUTH_DIR + 'vusers.txt' # This text file is made to generate FTP passwd credentials for vsftp 
PFM_FTP_AUTH_DB_FILE = PFM_FTP_AUTH_DIR + 'vsftpd-virtual-user.db' #  This is defined in /etc/pam.d/vsftpd.virtual


PFM_INTERNALSTORAGE_STATUS_SCANNING = ' Scanning for Files ...'
PFM_INTERNALSTORAGE_STATUS_PLAYING = 'Now Playing'
PFM_INTERNALSTORAGE_STATUS_NO_AUDIO_FILES = 'No Audio Files Found'

class InternalStorage():
    def __init__(self):
        self.Active = False
        self.DeviceName = ''
        self.DeviceFormat = ''
        self.DeviceSysDev = ''
        self.AudioFileList = []
        self.AudioFullPathFileList = []
        self.Scanning = False # is audio file scanning taking place

        dispatcher.connect(self.handle_set_ftp_credentials, signal=pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, sender=dispatcher.Any)

        # publish intial internal storage state before first file scan
        dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, self, False)
        dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES_EXIST, self, False)

        # perform initial scan to populate audio file playlist
        t = Thread(target=self.handle_scan_internal_storage)
        t.start()

        # dispatcher.send(pfm_signals.SIG_UPDATE_PFM_HOSTNAME, self, socket.gethostname())
        # dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN))

        self.handle_set_ftp_credentials('dummy', self)

        dispatcher.connect(self.watch_audiomanager_input, signal=pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, sender=dispatcher.Any)



    def handle_scan_internal_storage(self):
        self.Scanning = True
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'INTERNAL':
            dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, PFM_INTERNALSTORAGE_STATUS_SCANNING)
        AudioFiles = []
        AudioFullPathFiles = []
        for root, dirs, files in os.walk(PFM_INTERNALSTORAGE_ROOT):
            for file_ in files:
                if os.path.islink(os.path.join(root, file_)) != True:
                    mime_type = magic.from_file(os.path.join(root, file_), mime=True)
                    if mime_type.startswith('audio/') or mime_type == 'application/octet-stream':
                        AudioFiles.append(file_)
                        AudioFullPathFiles.append(os.path.join(root, file_))
                    else:
                        # print('DEBUG: Internal Non Audio File {} {}'.format(file_,magic.from_file(os.path.join(root, file_), mime=True)))
                        pass
        self.AudioFullPathFileList = AudioFullPathFiles
        dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES, self, self.AudioFullPathFileList)
        # regenerate playlist file for mplayer
        with open(PFM_INTERNALSTORAGE_PLAYLIST, 'w+') as f: 
            i = ''
            for i in self.AudioFullPathFileList:
                f.write('{}\n'.format(i))
        if len(self.AudioFullPathFileList) > 0:                                                              # Audio content found on Internal storage partition
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES_EXIST, self, True)
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_PLAYLIST, self, PFM_INTERNALSTORAGE_PLAYLIST) # send a signal that playlist has update
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, self, True)
            self.Active = True
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'INTERNAL':
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, PFM_INTERNALSTORAGE_STATUS_PLAYING)
        else:                                                                                                # No audio content found on Internal storage device partition
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES_EXIST, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, self, False)
            self.Active = False
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'INTERNAL':
                dispatcher.send(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS, self, PFM_INTERNALSTORAGE_STATUS_NO_AUDIO_FILES)
        if AudioFiles != self.AudioFileList:
            self.AudioFileList = AudioFiles
        self.Scanning = False


    def handle_set_ftp_credentials(self, dummy, sender):
        # initialise FTP credentials for vsftpd FTP server
        # note self.FTPuser must be set with default as pfm_signals.SIG_SET_PFM_HOSTNAME
        self.FTPuser = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_HOSTNAME, socket.gethostname())
        self.FTPpassword = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, None) 
        # Write text based credential file
        with open(PFM_FTP_AUTH_TXT_FILE, 'w+') as f:
            f.write('{}\n'.format(self.FTPuser))
            f.write('{}\n'.format(self.FTPpassword))
        # Remove any trace of old credentials
        rm_output = subprocess.check_output(['/bin/rm', PFM_FTP_AUTH_DB_FILE])
        # Generate PAM authentication database
        db_output = subprocess.check_output(['/usr/bin/db_load', '-T', '-t', 'hash', '-f', PFM_FTP_AUTH_TXT_FILE, PFM_FTP_AUTH_DB_FILE])
        logging.info('FTP User {} PIN reset to {}'.format(self.FTPuser, self.FTPpassword))

    def watch_audiomanager_input(self, state, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == "INTERNAL" and self.Scanning == False:
            scan_thread = Thread(target=self.handle_scan_internal_storage)
            scan_thread.start()


def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main():
    import sys
    import time
    from pfm import PocketFM

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB1, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB2, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MAXPRO_AUDIO_INPUT, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADFILE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_MPLAYER_LOADLIST, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_FILES, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_INTERNALSTORAGE_AUDIO_PLAYLIST, sender=dispatcher.Any)

    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'internal_storage.py main()', False)
    time.sleep(5)
    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'internal_storage.py main()', True)
    time.sleep(5)
    dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'internal_storage.py main()', 'analog')
    time.sleep(5)
    time.sleep(1000)

    #dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, 'internal_storage.py main()', PFM_INTERNALSTORAGE_PLAYLIST)
    dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'internal_storage.py main()', 'INTERNAL')

    #pyudev.list_devices()
    time.sleep(1000)

if __name__ == "__main__":
    main()
