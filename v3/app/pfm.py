#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import threading
from pydispatch import dispatcher
from periodic import PeriodicThread
import pfm_signals
import pfm_config
from breakoutboard import BreakoutBoard
from maxproboard import MaxProBoard
from audiomanager import AudioManager
from wifi import WiFi
from gsm import GSM
from usb_storage import USBStorage,PFM_USBSTORAGE_PLAYLIST
from internal_storage import InternalStorage,PFM_INTERNALSTORAGE_PLAYLIST
import display

from time import sleep
import os

PFM_LOCK_LEN = 6

class PocketFM():
    def __init__(self):
        self.LockPIN ='000000' # must be 6 numeric digits
        self.Breakout = BreakoutBoard()
        self.Breakout.start()
        self.MaxPro = MaxProBoard()
        self.MaxPro.start()
        self.AudioMan = AudioManager()
        self.WiFi = WiFi()
        self.GSM = GSM()
        self.USBDrive = USBStorage()
        self.InternalDrive = InternalStorage()

# example event handler
def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender

def main():

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout


    pfm_signals.set_logging(True)


    b.writeBB(b.BBQuery)         # initialise BB versions into object
    b.writeBB(b.ADC.BBQuery)     #
    b.writeBB(b.Encoder.BBQuery)
    b.writeBB(b.LED.BBQuery)
    b.writeBB(b.Power.BBQuery)
    b.writeBB(b.RTC.BBQuery)
    b.writeBB(b.GPS.BBActivate)

    # hack led colours on
    #print "*** Debug ser out", bin(1 | 3 << 3 | 5 << 6 | 6 << 9 | 7 << 12)
    b.writeBB(b.LED.BBPlot((1 | (3 << 3) | (5 << 6) | (6 << 9) | (7 << 12)) , 128))

    dispatcher.send(pfm_signals.SIG_SET_POWER_DISPLAY, 'pfm.py main()', True)
    display.start(b)

    cnt = 0

    # running test for AudioManager & USBStorage integration

    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'pfm.py main()', False)
    sleep(5)

    dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'pfm.py main()', True)
    sleep(5)

    print('Maxpro switch to digital input')
    dispatcher.send(pfm_signals.SIG_SET_MAXPRO_AUDIO_INPUT, 'pfm.py main()', 'digital')

    # test turn USB2 power off for USB stick
    dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'pfm.py main()', False)
    sleep(3)
    # test turn USB2 power on for USB stick
    dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'pfm.py main()', True)
    sleep(10)

    #dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'pfm.py main()', 'USB')
    #dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, 'pfm.py main()', 'USB')

    # Seed Periodic Timers for minimal polling because hardware does not provide interrupts
    t = PeriodicThread(0.1, b.writeBB, b.ADC.BBQuery)
    t.start()

    Good = True
    try:
        while Good:
            cnt += 1
            #print "PFM  heartbeat (PID: {}) {}".format(os.getpid(),b.is_alive())
            sleep(4)

    except (KeyboardInterrupt, SystemExit):
        Good = False
        print "\nInterrupt dispatch: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        b.close()
        m.close()
        t.close()
        print "\nAfter close() dispatch2: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        t.join()
        print "After t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        b.join()
        m.join()
        print "After b.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())

    print "\n dispatch: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
    t.close()
    t.join()
    sleep(1)
    print "\n dispatch after t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
    sleep(1)
    b.close()
    b.join()
    print "\n dispatch after t.join(): b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())


if __name__ == "__main__":
    main()

