import sqlite3
import logging
import time
import Queue
import threading
from pydispatch import dispatcher
import tools.general
import os
import sys
import shutil

class SQLiteHandler(logging.Handler):
    """
    Logging handler for SQLite.
    Based on Vinay Sajip's DBHandler class (http://www.red-dove.com/python_logging.html)

    This version sacrifices performance for thread-safety:
    Instead of using a persistent cursor, we open/close connections for each entry.

    AFAIK this is necessary in multi-threaded applications,
    because SQLite doesn't allow access to objects across threads.
    """

    initial_sql = """CREATE TABLE log(
                        tstamp FLOAT NOT NULL,
                        Name text,
                        LogLevel int,
                        Message text,
                        Args text,
                        Module text,
                        FuncName text,
                        LineNo int,
                        Exception text,
                        Process int,
                        Thread text,
                        ThreadName text)"""

    insertion_sql = """INSERT INTO log(tstamp, Name, LogLevel,Message,Args,Module,
                        FuncName, LineNo, Exception,
                        Process,Thread,
                        ThreadName) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"""


    def __init__(self, buffer, limit = 0):
        logging.Handler.__init__(self)
        self.stmt_queue = buffer
        self.stmt_queue.put((SQLiteHandler.initial_sql,))

        self.limit = limit
        if limit > 0:
            self.log_ids = Queue.Queue()
            self.log_id_current = 0


    def emit(self, rec):
        # Use default formatting:
        if self.limit == 0:
            return

        self.format(rec)

        if rec.exc_info:
            rec.exc_text = logging._defaultFormatter.formatException(rec.exc_info)
        else:
            rec.exc_text = ""

        self.stmt_queue.put((self.insertion_sql, rec.created, rec.name, rec.levelno, rec.msg, str(rec.args),
                             rec.module, rec.funcName, rec.lineno, rec.exc_text, rec.process, rec.thread, rec.threadName))

        if self.limit > 0:
            self.log_id_current += 1
            self.log_ids.put(self.log_id_current)

            if self.log_id_current > self.limit:
                self.stmt_queue.put(("DELETE FROM log where rowid = ?", self.log_ids.get()))


class SQLiteSignalLogger(object):

    _update_state = "UPDATE SignalState SET last_update = ?, updates=?, sender=?, " \
                    "val_string=?, val_pickle=? WHERE sig_name = ?"

    _insert_log = "INSERT INTO SignalLog (tstamp, sig_name, sender, val_pickle) VALUES (?,?,?,?)"

    def __init__(self, stmt_buffer, limit=0):
        print "set up signal log with limit", limit
        self.stmt_buffer = stmt_buffer
        self.limit = limit
        self.signal_names = {}   # cause mostly its easier to stick with the names ...
        if limit > 0:
            self.log_ids = Queue.Queue()
            self.log_id_current = 0

        self.stmt_buffer.put(('''CREATE TABLE SignalState(
                  sig_name text PRIMARY KEY NOT NULL,
                  sig_string TEXT NOT NULL,
                  last_update FLOAT NOT NULL,
                  updates INT NOT NULL,
                  reads INT NOT NULL,
                  sender TEXT NOT NULL,
                  val_string TEXT,
                  val_pickle BLOB)''',))

        #  if self.limit > 0: possible - but then to tools fail all the time ...
        self.stmt_buffer.put(('''CREATE TABLE SignalLog(
                  tstamp FLOAT NOT NULL,
                  sig_name TEXT NOT NULL,
                  sender TEXT NOT NULL,
                  val_pickle BLOB)''',))

    def register(self, sig_name, sig_string, nosiyet):
        self.signal_names.update ({sig_string: sig_name})

        self.stmt_buffer.put(("INSERT INTO SignalState VALUES (?,?,?,?,?,?,?,?)",
                              sig_name, sig_string, time.time(), 0, 0, nosiyet, nosiyet,
                              tools.general.sqlite_pickle(nosiyet)))

    def update_and_log(self, data, sig_string, sender, updates):
        pickle = tools.general.sqlite_pickle(data)
        sig_name = self.signal_names.get(sig_string)
        tstamp = time.time()

        self.stmt_buffer.put((SQLiteSignalLogger._update_state,
                              tstamp, updates, str(sender), str(data)[:20], pickle, sig_name))

        if self.limit == 0:
            return

        self.stmt_buffer.put((SQLiteSignalLogger._insert_log,
                              tstamp, sig_name, str(sender), pickle))

        if self.limit > 0:
            self.log_id_current += 1
            self.log_ids.put(self.log_id_current)

            if self.log_id_current > self.limit:
                self.stmt_buffer.put(("DELETE FROM SignalLog where rowid = ?", self.log_ids.get()))

    def update_reads(self, sig_string, reads):
        self.stmt_buffer.put(("UPDATE SignalState SET reads = ? WHERE sig_name = ?",
                              reads, self.signal_names.get(sig_string)))


def formatDBTime(the_time):
    '''
    The format used in sqlite, but I use float anyway - less memory ...
    Args:
        self:
        the_time:

    Returns:

    '''
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(the_time))


def format_filename_tstamp():
    return time.strftime("%Y-%m-%d_%H%M%S")



__buffer = Queue.Queue()
__log_dir = '/tmp/pfm_log/'

shutil.rmtree (__log_dir, ignore_errors=True)

#__log_dir = '/home/pfm/pfm_store/stolle_app/LOGS/'  # must end with / !!!!!

LOG_FILE = __log_dir + "pfm_" + format_filename_tstamp() + ".log"
LOG_DB = __log_dir + "pfm_" + format_filename_tstamp() + ".sqlite"
__current_log = __log_dir + "CURRENT.log"
__current_db = __log_dir + "CURRENT.sqlite"



if not os.path.exists(__log_dir):
    #logging.info ("create log dir {}".format(__log_dir)) # this would prevent logging into file ...
    os.makedirs(__log_dir)
# else:
    #logging.info ("log dir {} already exists".format(__log_dir))

logging.basicConfig(filename=LOG_FILE,
                    level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(module)s.%(funcName)s: %(message)s',
                    datefmt='%H:%M:%S')


if os.path.lexists(__current_log):
    if os.path.islink(__current_log):
        os.remove(__current_log)
    #else:
    #    raise Exception(__current_log + " is no link!?")

if os.path.lexists(__current_db):
    if os.path.islink(__current_db):
        os.remove(__current_db)
        print "delete:", __current_db
    #else:
    #    raise Exception(__current_db + " is no link!?")

os.symlink(LOG_FILE, __current_log)
os.symlink(LOG_DB, __current_db)

# more: %(threadName)s  %(processName)s ...
# see: https://docs.python.org/2/library/logging.html



logging.getLogger().addHandler(SQLiteHandler(__buffer, 10000))
signal_log = SQLiteSignalLogger(__buffer, 10000)

__buffer.put(('''CREATE TABLE SysInfo(
                  key TEXT PRIMARY KEY NOT NULL,
                  val,
                  val_pickle BLOB DEFAULT NULL)''',))

def store_sys_info(key, val):
    __buffer.put(("INSERT INTO SysInfo (key, val) VALUES (?,?)", key, val))

#
__buffer.put(('''CREATE TABLE SignalQueue(
                  sig_string TEXT NOT NULL,
                  sender TEXT NOT NULL,
                  val_pickle BLOB NOT NULL)''',))

store_sys_info("pfm app start", time.time())
store_sys_info("pfm app active", time.time())


__closing = False

def loop_write():
    logging.info ("starting database input loop")
    skip = 0
    while not __closing:
        start = time.time()
        if __buffer.qsize() > 3 or skip > 2:
            sql_all = None
            try:
                skip = 0
                commit_count = 0
                conn = sqlite3.connect(LOG_DB)
                curs = conn.cursor()

                while not __buffer.empty():
                    if commit_count > 500:  # todo: define somewehre
                        logging.warning("commit_count > 500 and still {} statements in queue".format(__buffer.qsize()))
                        break

                    commit_count += 1

                    sql_all = __buffer.get_nowait()

                    #            if type(tmp) == "<type 'str'>":
                    sql_stmt = sql_all[0]
                    sql_par = sql_all[1:]

                    if len(sql_par) > 0:
                        # print "sql:", sql
                        # print "par:", par
                        curs.execute(sql_stmt, sql_par)
                    else:
                        curs.execute(sql_stmt)
                        #print "just sql:", sql


                curs.execute("UPDATE SysInfo SET val=? WHERE key = 'pfm app active'", (time.time(), ))

                conn.commit()

                sent = 0
                ids_delete = []   # todo handle invalid signals!! Handle None-Values
                for row in curs.execute('SELECT rowid, sig_string, sender, val_pickle FROM SignalQueue'):
                    dispatcher.send(row[1], row[2], tools.general.sqlite_unpickle(row[3]))
                    ids_delete.append(row[0])
                    sent += 1
                    print "SEND: ", row[1]

                if sent > 0:
                    for id in ids_delete:
                        curs.execute('DELETE FROM SignalQueue WHERE rowid = ?', (id,))
                    # curs.executemany('DELETE FROM SignalQueue WHERE id = ?', iter(ids_delete)) ???
                    conn.commit()

                conn.close()
                #print "comitted", commit_count, "rows in", time.time() - start, "sent: ", sent
            except:
                print "------------------------------------------------------------------------"
                print "ERROR PROCESSING SQL:", sys.exc_info()[0]
                print sys.exc_type
                print sys.exc_value
                print sql_all
                print "------------------------------------------------------------------------"
                exit()

        else:
            skip += 1
            #print "NOT ENOUGH TO COMMIT", time.time() - start, skip, __buffer.qsize()


        time.sleep(0.3)


        #conn.commit()
        #conn.close()

threading.Thread(target=loop_write, name="sqlite_conn").start()

