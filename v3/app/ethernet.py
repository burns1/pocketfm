#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-
"""
PFM module
"""
from threading import Thread
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
import NetworkManager
import socket
from pydispatch import dispatcher
import struct
import uuid
from time import sleep

import logging
import pfm_signals
#import pfm_config

PFM_ETHERNET_DEVICE = 'eth0'

PFM_ETHERNET_DEFAULT_MODE = 'ON'
PFM_ETHERNET_DEFAULT_NM_PROFILE = 'pfm_eth0'


PFM_ETHERNET_STATUS_ERROR = '(error)'
PFM_ETHERNET_STATUS_WAIT = '(wait...)'
PFM_ETHERNET_STATUS_OFF = '(off)'

# ui state messages for WIFI client mode
PFM_CONNECTIONMANAGER_ETHERNET_STATUS  = {}
PFM_CONNECTIONMANAGER_ETHERNET_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS['PFM_NO_NET_SELECTED'] =                       'No network selected'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS['PFM_NET_NOT_FOUND'] =                         'Network not found'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_UNAVAILABLE]  = 'Net Unavailable'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED] = 'Mobile disconnected'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_PREPARE]      = 'Preparing connection'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Connecting ...'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH]    = 'Password not accepted'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CHECK]     = 'Checking IP config ..'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_SECONDARIES]  = 'Checking secondaries.'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'Mobile data connected'
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'Mobile disconnecting '
PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED]       = 'Connection Failed'


# for dbus connect_to_signal()
d_args = ('sender', 'destination', 'interface', 'member', 'path')
d_args = dict([(x + '_keyword', 'd_' + x) for x in d_args])

class PFMEthernetDevice(Thread):
    def __init__(self, interface):

        super(PFMEthernetDevice, self).__init__()

        # dBus NetworkManager
        self.SystemDBus = dbus.SystemBus()
        self.NMProxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager")
        self.NMInterface = dbus.Interface(self.NMProxy, "org.freedesktop.NetworkManager")

        self.Active = False

        self.Profile = None # NM connection profile to generate - maybe use device name? ...

        self.Device = None
        self.DeviceState = None
        self.Dialnumber = None
        self.APN = None
        self.Username = None
        self.Password = None
        self.IPAdrresses = None
        self.DNSServers = None
        # Intialize hooks into DBus NM interface
        # sleep(30)
        for dev in NetworkManager.NetworkManager.GetDevices():
            if dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_MODEM:
                logging.debug('Found 3G mobile modem Device {}'.format(NetworkManager.Device(dev).Interface))
                print('Debug: Found 3G mobile modem Device {}'.format(NetworkManager.Device(dev).Interface))                
                if NetworkManager.Device(dev).Interface == PFM_3G_MODEM_DEVICE:
                    self.Device = PFM_3G_MODEM_DEVICE
                    self.Profile = PFM_3G_DEFAULT_NM_PROFILE
                    logging.debug('Found my 3G mobile modem device {}'.format(self.Device))
                    print('Debug: Found my 3G mobile modem device {}'.format(self.Device))
                    devicedetail = dev.SpecificDevice()
                    res = dev.SpecificDevice().connect_to_signal('StateChanged', self.handle_device_state_change, **d_args)
                    self.DeviceState = NetworkManager.Device(dev).State
                    logging.debug('Debug: Registered device {} for dbus NM state change. Current NM device state: {} ({})'.format(self.Device, NetworkManager.const('device_state',self.DeviceState), self.DeviceState))
                    print('Debug: Registered device {} for dbus NM state change. Current NM device state: {} ({})'.format(self.Device, NetworkManager.const('device_state',self.DeviceState), self.DeviceState))
                    if NetworkManager.const('device_state', self.DeviceState) == 'activated':
                        logging.debug('device {} is activated at init'.format(self.Device))
                        print('Debug: device {} is activated at init'.format(self.Device))
                        self.Active = True
                        for addr in dev.Ip4Config.Addresses:
                            logging.debug('device {} has IP4 address: {}'.format(self.Device, addr[0]))
                            print('Debug: device {} has IP4 address: {}'.format(self.Device, addr[0]))
                            self.IPAddresses = addr[0]
                        for route in dev.Ip4Config.Routes:
                            logging.debug('device {} has IP4 route: {}'.format(self.Device, route))
                            print('Debug: device {} has IP4 route: {}'.format(self.Device, route))
                        for ns in dev.Ip4Config.Nameservers:
                            logging.debug('device {} has Nameserver: {}'.format(self.Device, ns))
                            print('Debug: device {} has Nameserver: {}'.format(self.Device, ns))
                    else:
                        self.Active = False
            # TODO: RESEARCH: Network Manager appears to only see/manage the modem device (ttyUSB2), however once active, ifconfig lists 'Ethernet' device (wwan0)
            if NetworkManager.Device(dev).Interface == PFM_3G_ETHERNET_DEVICE:
                print('Debug: Found 3G mobile ethernet Device {}'.format(NetworkManager.Device(dev).Interface))                
                if NetworkManager.Device(dev).Interface == PFM_3G_ETHERNET_DEVICE:
                    print('Debug: Found my 3G ethernet device {}'.format(self.Device))
                    self.EthernetDevice = PFM_3G_ETHERNET_DEVICE
                    devicedetail = dev.SpecificDevice()
                    res = dev.SpecificDevice().connect_to_signal('StateChanged', self.handle_device_state_change, **d_args)
                    self.EthernetDeviceState = NetworkManager.Device(dev).State
                    print('Debug: Registered device {} for dbus NM state change. Current NM device state: {} ({})'.format(self.EthernetDevice, NetworkManager.const('device_state',self.EthernetDeviceState), self.EthernetDeviceState))
                    if NetworkManager.const('device_state', self.EthernetDeviceState) == 'activated':
                        print('Debug: device {} is activated'.format(self.EthernetDevice))
                        for addr in dev.Ip4Config.Addresses:
                            print('Debug: device {} has IP4 address: {}'.format(self.EthernetDevice, addr[0]))
                        for route in dev.Ip4Config.Routes:
                            print('Debug: device {} has IP4 route: {}'.format(self.EthernetDevice, route))
                        for ns in dev.Ip4Config.Nameservers:
                            print('Debug: device {} has Nameserver: {}'.format(self.EthernetDevice, ns))
        if self.Device != None:
            if self.Device == PFM_3G_MODEM_DEVICE:
                # set PFM Signal Handler Functions
                dispatcher.connect(self.handle_set_3g_mode, signal=pfm_signals.SIG_SET_3G_MODE, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_3g_active, signal=pfm_signals.SIG_SET_3G_ACTIVE, sender=dispatcher.Any)
                # dispatcher.connect(self.handle_set_3g_apn, signal=pfm_signals.SIG_SET_3G_DIAL_NUMBER, sender=dispatcher.Any) # might need signal handler for special dial number too!
                dispatcher.connect(self.handle_set_3g_apn, signal=pfm_signals.SIG_SET_3G_APN, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_3g_username, signal=pfm_signals.SIG_SET_3G_USERNAME, sender=dispatcher.Any)
                dispatcher.connect(self.handle_set_3g_password, signal=pfm_signals.SIG_SET_3G_PASSWORD, sender=dispatcher.Any)

                self.Dialnumber = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_3G_DIALNUMBER, PFM_3G_DEFAULT_DIALNUMBER)
                self.APN = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_3G_APN, PFM_3G_DEFAULT_APN)
                self.Username = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_3G_USERNAME, PFM_3G_DEFAULT_USERNAME)
                self.Password = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_3G_PASSWORD, PFM_3G_DEFAULT_PASSWORD)
                self.Mode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_3G_MODE, PFM_3G_DEFAULT_MODE)


                dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, self.Active)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_DIALNUMBER, self, self.Dialnumber)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_APN, self, self.APN)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_USERNAME, self, self.Username)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_PASSWORD, self, self.Password)
                
                dispatcher.send(pfm_signals.SIG_SET_3G_MODE, self, self.Mode)

                # dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[self.DeviceState])
            else:
                #  Non native (ie external) PFM mobile modem Device
                pass
        else:
            print('Debug: ****** Error!! PFMEthernetDevice {} not found!!'.format(interface))


        # print('PFMEthernetDevice.__init__({}) properties:'.format(interface))
        # for property, value in vars(self).iteritems():
        #     if property[0] != '_':
        #         print property, ": ", value
        # print('__init end__')

    def handle_device_state_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        newState = NetworkManager.const('device_state', args[0])
        oldState = NetworkManager.const('device_state', args[1])
        # print('Debug: ******************** ENTERED DEVICE STATE CHANGE SIGNAL')
        # print("Received signal: %s:%s" % (kwargs['d_interface'], kwargs['d_member']))
        # print("Sender:          (%s)%s" % (kwargs['d_sender'], kwargs['d_path']))
        # print("Network Device: {}".format(device.Interface))
        # print("Arguments:       (%s)" % ", ".join([str(x) for x in args]))
        print('*****************************Debug: interface {} state changed from {} to {}'.format(self.Device, oldState, newState))
        self.DeviceState = args[0]
        if NetworkManager.Device(path).Interface == self.Device:
            if   args[0] == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED])
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                # print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_IP_ADDRESS, self, ipaddr_dotted)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, False)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, False)
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_IP_ADDRESS, self, None)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_DEACTIVATING:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, False)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CHECK:
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_IP_ADDRESS, self, ipaddr_dotted)
                # print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_NEED_AUTH:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, False)
                # print('DEBUG: networkmanager_device_state_change(): need auth state reached. Deleting profiles for "{}"'.format(self.WiFiClientSSID))
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_STATUS, self, PFM_ETHERNET_STATUS)
                n_m_delete_connection([self.WiFiClientSSID])
            logging.debug('3G Interface {} is in state {}'.format(self.Device, newState))

        # If Wifi Client mode is active, then set that the PFM unit is network connected
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == True:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == False:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        #  TODO connectivity check should include 3G and not be done here
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == False and pfm_signals.get_value(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE) == False:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == True:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, False)


    def handle_set_3g_mode(self, mode, signal, sender):
        """
        Sets 3G network dial number
        """
        if mode.upper() == "ON":
            dispatcher.send(pfm_signals.SIG_SET_3G_ACTIVE, self, True)
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_MODE, self, "ON")
        elif mode.upper() == "OFF":
            dispatcher.send(pfm_signals.SIG_SET_3G_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_MODE, self, "OFF")

    def handle_set_3g_dialnumber(self, dialnumber, signal, sender):
        """
        Sets 3G network dial number
        """
        if password != pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_DIALNUMBER):
            self.Dialnumber = dialnumber
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_DIALNUMBER, self, self.Dialnumber)

    def handle_set_3g_apn(self, apn, signal, sender):
        """
        Sets 3G network APN
        """
        if apn != pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_APN):
            self.APN = apn
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_APN, self, self.APN)

    def handle_set_3g_username(self, username, signal, sender):
        """
        Set handler for 3G password - simply updates internal property Username
        """
        if username != pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_USERNAME):
            self.Username = username
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_USERNAME, self, self.Username)

    def handle_set_3g_password(self, password, signal, sender):
        """
        Set handler for 3G password - simply updates internal property Password
        """
        if password != pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_PASSWORD):
            self.Password = password
            dispatcher.send(pfm_signals.SIG_UPDATE_3G_PASSWORD, self, self.Password)

    def handle_set_3g_active(self, state, signal, sender):
        """
        Set handler for 3G networking (de)activation with current APN, username and password settings
        """
        dpath = None
        if state == True or state == 'True':
            print('DEBUG: connectionmanager.py: handle_set_3g_active() state == True')
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_ETHERNET_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
            found_existing_connection = False
            for conn in NetworkManager.Settings.ListConnections():
                settings = conn.GetSettings()
                if settings['connection'].get('type') == 'gsm':
                    print('D: found gsm connection')
                    if settings['connection'].get('id') == self.Profile:
                        print('D: found MY gsm connection {}'.format(self.Profile))
                        found_existing_connection = True
                        secrets = {}
                        try:
                            secrets = conn.GetSecrets()
                        except Exception, e:
                            pass
                        for key in secrets:
                            settings[key].update(secrets[key])
                        print('Connection Settings'.format(settings))

            if found_existing_connection == False:
                logging.debug('requested NM connection NOT found, creating connection for {}'.format(self.Profile))
                print('requested NM connection NOT found, creating connection for {}'.format(self.Profile))
                s_con = dbus.Dictionary({
                    'type': 'gsm',
                    'uuid': str(uuid.uuid4()),
                    'permissions': ['user:pfm:'],
                    'autoconnect': 'false',
                    'interface-name': self.Device,
                    'id': self.Profile })
                s_gsm = dbus.Dictionary({
                    'number': dbus.ByteArray(self.Dialnumber),
                    'apn':    dbus.ByteArray(self.APN), })
                s_ip4 = dbus.Dictionary({'method': 'auto'})
                s_ip6 = dbus.Dictionary({'method': 'ignore'})
                con = dbus.Dictionary({
                    'connection': s_con,
                    'gsm': s_gsm})
                proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings")
                settings = dbus.Interface(proxy, "org.freedesktop.NetworkManager.Settings")
                try:
                    n_m_deactivate_connection(get_active_connections_by_interface(self.Device)) #  deactivate all connections on this device
                    settings.AddConnection(con)
                    self.Active = True
                except:
                    logging.debug('DBUS Exception trapped')
                    self.Active = False
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, self.Active)
                n_m_activate_connection([self.Profile])
            else:
                self.Active = True
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, self.Active)
                n_m_activate_connection([self.Profile])

        else:
            # print('DEBUG: connectionmanager.py: handle_set_wifi_client_active() state == False')
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_3G_ACTIVE) == True:
                # devices = self.NMInterface.GetDevices()
                # for d in devices:
                #     dev_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", d)
                #     prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                #     iface = prop_iface.Get("org.freedesktop.NetworkManager.Device", "Interface")
                #     if iface == self.WiFiClientDevice:
                #         dpath = d
                #         break
                #if not dpath or not len(dpath):
                #    raise Exception('NetworkManager knows nothing about WiFi Client Device %s'.format(self.WiFiClientDevice))
                # dev_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", dpath)
                # dev_iface = dbus.Interface(dev_proxy, "org.freedesktop.NetworkManager.Device")
                # prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                # Make sure the device is connected before we try to disconnect it
                # state = prop_iface.Get("org.freedesktop.NetworkManager.Device", "State")
                # if state == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                #     logging.debug('WiFi Client Device {} is already not connected, cannot disconnect'.format(self.WiFiClientDevice))
                # else:
                # n_m_deactivate_connection([self.Device])
                # experimental just-close-them-all alternative
                n_m_deactivate_connection(get_active_connections_by_interface(self.Device))
                self.Active = False
                dispatcher.send(pfm_signals.SIG_UPDATE_3G_ACTIVE, self, False)

def n_m_activate_connection(names):
    connections = NetworkManager.Settings.ListConnections()
    connections = dict([(x.GetSettings()['connection']['id'], x) for x in connections])

    if not NetworkManager.NetworkManager.NetworkingEnabled:
        NetworkManager.NetworkManager.Enable(True)
    for n in names:
        if n not in connections:
            print('DEBUG: connectionmanager.py: n_m_activate_connection: No such connection: {}'.format(n))
        else:
            conn = connections[n]
            ctype = conn.GetSettings()['connection']['type']
            if ctype == 'vpn':
                for dev in NetworkManager.NetworkManager.GetDevices():
                    if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED and dev.Managed:
                        break
                else:
                    print('DEBUG: connectionmanager.py: n_m_activate_connection: No active, managed device found')
            else:
                dtype = {
                    '802-11-wireless': 'wlan',
                    'gsm': 'wwan',
                }
                # if dtype in connection_types:
                #     enable(dtype)
                dtype = {
                    '802-11-wireless': NetworkManager.NM_DEVICE_TYPE_WIFI,
                    '802-3-ethernet': NetworkManager.NM_DEVICE_TYPE_ETHERNET,
                    'gsm': NetworkManager.NM_DEVICE_TYPE_MODEM,
                }.get(ctype,ctype)
                devices = NetworkManager.NetworkManager.GetDevices()

                for dev in devices:
                    print('DEBUG: n_m_activate_connection: {} {} {}'.format(NetworkManager.Device(dev).Interface, dev.DeviceType, dev.State))
                    if dev.DeviceType == dtype and dev.State == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                        break
                else:
                    logging.debug('No suitable and available {} device found'.format(ctype))

            logging.debug('activating connection: ({})'.format(n))
            NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
            


def n_m_deactivate_connection(names):
    active = NetworkManager.NetworkManager.ActiveConnections
    active = dict([(x.Connection.GetSettings()['connection']['id'], x) for x in active])
    for n in names:
        if n in active:
            logging.debug('deactivating connection: ({})'.format(n))
            NetworkManager.NetworkManager.DeactivateConnection(active[n])
        else:
            logging.debug('No such connection: ({})'.format(n))

def n_m_delete_connection(names):
    active = [x.Connection.GetSettings()['connection']['id']
              for x in NetworkManager.NetworkManager.ActiveConnections]
    connections = [(x.GetSettings()['connection']['id'], x)
                   for x in NetworkManager.Settings.ListConnections()]
    for conn in sorted(connections):
        if conn[0] in names:
            logging.debug('Deleting connection {} {}'.format(conn[0], conn[1]))
            try:
                conn[1].Delete()
            except:
                pass

def get_active_connections_by_interface(network_interface):
    # print('DEBUG: connectionmanager.py get_active_connections_by_interface({})'.format(network_interface))
    active_interface_connections = []
    sleep(2) #  TODO: IMPORTANT: Yes, this is needed. NetworkManager will throw an exception without it. No idea why yet!!!
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        try:
            settings = conn.Connection.GetSettings()['connection']
            if network_interface in [x.Interface for x in conn.Devices]:
                logging.debug(' active connection {} {} {} {}'.format(settings['id'], settings['type'], conn.Default, ", ".join([x.Interface for x in conn.Devices])))
                active_interface_connections.append(settings['id'])
        except:
            print('DEBUG: connectionmanager.py get_active_connections_by_interface({}) reached exception!!!'.format(network_interface))
    return(active_interface_connections)

#testWiFi = PFMEthernetDevice('ttyUSB0')

#_loop = GObject.MainLoop()
#t = Thread(target=_loop.run)
#t.start()


