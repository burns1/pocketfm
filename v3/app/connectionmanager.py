#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

from pydispatch import dispatcher
from threading import Thread
import os
import sys
import socket
import subprocess
import dbus
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
import NetworkManager
import struct
import uuid
import re
from time import sleep
import logging

import pfm_signals

# PFM_WIFI_HOSTAP_CONF_FILE = '/home/pfm/pfm_conf/hostapd.conf'
PFM_SYS_HOSTAP_CONF_FILE  = '/etc/hostapd/hostapd.conf'

PFM_WIFI_MODE_OFF ='OFF'
PFM_WIFI_MODE_CLIENT = 'CLIENT MODE'
PFM_WIFI_MODE_AP = 'ACCESS POINT'
PFM_CONNECTIONMANAGER_WIFI_MODE_LIST = [PFM_WIFI_MODE_AP, PFM_WIFI_MODE_CLIENT, PFM_WIFI_MODE_OFF]

PFM_WIFI_STATUS_ERROR = '(error)'
PFM_WIFI_STATUS_WAIT = '(wait...)'
PFM_WIFI_STATUS_OFF = '(off)'

PFM_WIFI_CLIENT_DEFAULT_PASSWORD = '12345678'
PFM_WIFI_AP_DEFAULT_PASSWORD = '12345678'

# ui state messages for WIFI client mode
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'] =                       'No network selected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'] =                         'Network not found'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi client connected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Connecting ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'WiFi disconnected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_SECONDARIES]  = 'Checking ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH]    = 'Password not accepted'

# ui state messages for AP mode
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi AP active'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED] = 'WiFi AP disconnected'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Configuring ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CHECK]     = 'IP check ...'

PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED]       = 'AP Failed'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'AP Deactivating'

d_args = ('sender', 'destination', 'interface', 'member', 'path')
d_args = dict([(x + '_keyword', 'd_' + x) for x in d_args])
Devices = {}

# connection_types = ['wireless','wwan','wimax']


class ConnectionManager(Thread):
    """
    Connection interface class.
    Holds attributes & methods required for PFM WiFi/GSM network control
    """
    def __init__(self):

        super(ConnectionManager, self).__init__()

        # DBUS NetworkManager specific properties
        self.SystemDBus = dbus.SystemBus()
        self.NMProxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager")
        self.NMInterface = dbus.Interface(self.NMProxy, "org.freedesktop.NetworkManager")

        # ethernet specific properties
        self.EthernetDevice = 'eth0' # for testing or people who open the box up and connect

        # wifi specific properties
        self.WiFiCurrentMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_MODE, PFM_WIFI_MODE_OFF)

        self.WiFiAPActive = False
        self.WiFiAPDevice = 'wlan0'
        self.WiFiAPSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_NETWORK, socket.gethostname())
        self.WiFiAPPassword = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, PFM_WIFI_AP_DEFAULT_PASSWORD)

        self.WiFiClientActive = False
        self.WiFiClientDevice = 'wlan0'
        self.WiFiClientSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, None)
        if self.WiFiClientSSID == None:
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'])
        self.WiFiClientPassword = PFM_WIFI_CLIENT_DEFAULT_PASSWORD

        self.WiFiAvailableNetworks = [] # scan results from available wifi networks
        self.WiFiInitialScanCompleted = False

        # Trigger scan for available networks for Client Mode
        #dispatcher.send(pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, self,'')
        logging.debug('Triggering initial WiFi network scan')
        self.scan_available_networks()
        logging.debug('Completed initial WiFi network scan')

        # delete all PFM AP connections - keep NM profiles clean
        n_m_delete_connection([self.WiFiAPSSID])

        # set connectionmanager signal handlers
        # wifi mode
        dispatcher.connect(self.handle_set_wifi_mode, signal=pfm_signals.SIG_SET_WIFI_MODE, sender=dispatcher.Any)
        # wifi ap mode
        dispatcher.connect(self.handle_set_wifi_ap_active, signal=pfm_signals.SIG_SET_WIFI_AP_ACTIVE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_wifi_ap_ssid, signal=pfm_signals.SIG_SET_WIFI_AP_NETWORK, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_wifi_ap_password, signal=pfm_signals.SIG_SET_WIFI_AP_PASSWORD, sender=dispatcher.Any)
        # wifi client mode
        dispatcher.connect(self.handle_set_wifi_client_active, signal=pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_wifi_client_ssid, signal=pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_wifi_client_password, signal=pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, sender=dispatcher.Any)
        dispatcher.connect(self.handle_scan_available_networks, signal=pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, sender=dispatcher.Any)

        # Publish WiFi AP signals
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK, self, self.WiFiAPSSID) # AP network name is fixed to hostname, so update not set
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED])
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, self.WiFiAPActive) # mode set handles this


        # Publish WiFi Client signal values
        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, self, self.WiFiClientSSID)
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, self.WiFiClientPassword) # client passwords handled by NetworkManager
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, self.WiFiClientActive) # SIG_SET_WIFI_MODE handles this

        # Set initial WIFI Mode
        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, self, self.WiFiCurrentMode)

        # NetworkManager Event Handlers
        NetworkManager.NetworkManager.connect_to_signal('DeviceAdded', self.networkmanager_device_add_remove, **d_args)
        NetworkManager.NetworkManager.connect_to_signal('DeviceRemoved', self.networkmanager_device_add_remove, **d_args)

        for dev in NetworkManager.NetworkManager.GetDevices():
            dev.connect_to_signal('StateChanged', self.networkmanager_device_state_change, **d_args)
            # connectionType = type(dev.SpecificDevice()).__name__
            # print('Device {} {} {}'.format(NetworkManager.Device(dev).Interface, dev, NetworkManager.const('device_state',dev.State)))
            # Devices[dev.object_path] = {}
            # Devices[dev.object_path]["type"] = connectionType
            # if NetworkManager.const('device_state',dev.State) == "activated":
            #     Devices[dev.object_path]["active"] = True

        self._loop = GObject.MainLoop()
        t = Thread(target=self._loop.run)
        t.start()

        # determine initial connectivity state
        dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, False)
        if get_active_connections_by_interface(self.EthernetDevice) == []:
            # print('DEBUG: connectionmanager.py __init__ intial ethernet state DOWN')
            dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE, self, False)
        else:
            # print('DEBUG: connectionmanager.py __init__ intial ethernet state UP')
            dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE, self, True)
            # DEBUG: COMMENTED OUT FOR TESTING ISSUE #73 BUG | Audiomanager: Stream will not automatically reconnect when switching Wifi from OFF to CLIENT 
            # dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        if get_active_connections_by_interface(self.WiFiClientDevice) == []:
            # print('DEBUG: connectionmanager.py __init__ intial wifi client state DOWN')
            pass
        else:
            # print('DEBUG: connectionmanager.py __init__ intial wifi client state UP: {}'.format(get_active_connections_by_interface(self.WiFiClientDevice)))
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
            dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        # for ui
        self.WiFiModeList = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST, PFM_CONNECTIONMANAGER_WIFI_MODE_LIST)
        self.WiFiCurrentMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_MODE, PFM_WIFI_MODE_OFF) # current wifi mode, eg AP, client or off
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST, self, self.WiFiModeList)
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)

    def scan_available_networks(self):
        """ 
        scan for available WiFi APs
        """
        logging.debug('Initiating WiFi network scan with WiFiInitialScanCompleted as {}'.format(self.WiFiInitialScanCompleted))
        devices = self.NMInterface.GetDevices()

        for d in devices:
            dev_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", d)
            prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")

            # Make sure the device is enabled before we try to use it
            state = prop_iface.Get("org.freedesktop.NetworkManager.Device", "State")
            # NM_DEVICE_STATE_UNKNOWN = 0      the device's state is unknown
            # NM_DEVICE_STATE_UNMANAGED = 10   the device is recognized, but not managed by NetworkManager
            # NM_DEVICE_STATE_UNAVAILABLE = 20 the device is managed by NetworkManager, but is not available for use
            if state == NetworkManager.NM_DEVICE_STATE_UNKNOWN or state == NetworkManager.NM_DEVICE_STATE_UNMANAGED or state == NetworkManager.NM_DEVICE_STATE_UNAVAILABLE:
                continue

            # Get device's type; we only want wifi devices
            iface = prop_iface.Get("org.freedesktop.NetworkManager.Device", "Interface")
            dtype = prop_iface.Get("org.freedesktop.NetworkManager.Device", "DeviceType")

            # NM_DEVICE_TYPE_WIFI = 2 an 802.11 WiFi device
            if dtype == NetworkManager.NM_DEVICE_TYPE_WIFI:
                logging.debug('WiFi Interface {} in state {}'.format(NetworkManager.Device(d).Interface, state))
                # Get a proxy for the wifi interface
                wifi_iface = dbus.Interface(dev_proxy, "org.freedesktop.NetworkManager.Device.Wireless")
                wifi_prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                # experimental RequestScan
                # aps = wifi_iface.RequestScan("")
                WiFiAvailableNetworks = []
                # EXPERIMENTAL: Place current WiFiClientSSID in list, just to make sure it is in selectable list
                # WiFiAvailableNetworks.append(pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK))
                # Get all APs the card can see
                # aps = wifi_iface.GetAllAccessPoints()
                aps = wifi_iface.GetAccessPoints()
                for path in aps:
                    ap_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", path)
                    ap_prop_iface = dbus.Interface(ap_proxy, "org.freedesktop.DBus.Properties")

                    ssid = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "Ssid")
                    netname = ''.join([chr(byte) for byte in ssid])
                    flags = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "Flags")
                    wpaflags = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "WpaFlags")
                    rsnflags = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "RsnFlags")
                    # bssid = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "HwAddress")
                    # strength = ap_prop_iface.Get("org.freedesktop.NetworkManager.AccessPoint", "Strength")
                    status_flags = ''
                    status_wpaflags = ''
                    status_rsnflags = ''
                    if flags == NetworkManager.NM_802_11_AP_FLAGS_NONE:
                        status_flags = 'no security capabilities'
                    elif flags == NetworkManager.NM_802_11_AP_FLAGS_PRIVACY:
                        status_flags = 'authentication & encryption required'
                    if wpaflags == NetworkManager.NM_802_11_AP_SEC_NONE:
                        status_wpaflags = 'no security requirements:'
                    if wpaflags & NetworkManager.NM_802_11_AP_SEC_KEY_MGMT_PSK:
                        status_wpaflags += ':WPA/RSN PSK supported'
                    if wpaflags & NetworkManager.NM_802_11_AP_SEC_PAIR_CCMP:
                        status_wpaflags += ':AES/CCMP pair supported'
                    if wpaflags & NetworkManager.NM_802_11_AP_SEC_GROUP_CCMP:
                        status_wpaflags += ':AES/CCMP group supported'
                    logging.debug('scanned network name: {} flags: {} wpaflags: {} rsnflags: {}'.format(netname, flags, hex(wpaflags), hex(rsnflags)))
                    WiFiAvailableNetworks.append(netname)
                # experimental dup & null removal (probably should not do this & let ui deal!)
                WiFiAvailableNetworks = list(set(WiFiAvailableNetworks)) # remove duplicates
                if WiFiAvailableNetworks != self.WiFiAvailableNetworks:
                    self.WiFiAvailableNetworks = WiFiAvailableNetworks
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, self, self.WiFiAvailableNetworks)
                self.WiFiInitialScanCompleted = True

    def handle_set_wifi_mode(self, mode, signal, sender):
        """
        Sets WiFi mode, AP mode for emulating an access point, Client mode for attaching directly to an external access point, or off
        Currently CLIENT and ACCESS POINT modes are set to be mutually exclusive
        """
        logging.debug('WIFI MODE SET == {} self.WiFiInitialScanCompleted is {}'.format(mode,self.WiFiInitialScanCompleted))
        if mode.upper() == PFM_WIFI_MODE_OFF:
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, False)
            self.WiFiCurrentMode = PFM_WIFI_MODE_OFF
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, PFM_WIFI_STATUS_OFF)
        elif mode.upper() == "AP" or mode.upper() == PFM_WIFI_MODE_AP or mode.upper == "ACCESS POINT MODE":
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_AP_STATUS['PFM_PLEASE_WAIT'])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, True)
            self.WiFiCurrentMode = PFM_WIFI_MODE_AP
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)
        elif mode.upper() == "CLIENT" or mode.upper() == PFM_WIFI_MODE_CLIENT:
            # rescan networks
            # TEST: comment scan out until wifi mode is changed 
            # dispatcher.send(pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, self,'')
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_PLEASE_WAIT'])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_WIFI_STATUS_OFF)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, self, False)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, self, True)
            self.WiFiCurrentMode = PFM_WIFI_MODE_CLIENT
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_MODE, self, self.WiFiCurrentMode)
        # TEST: rescan at end of wifi mode change regardless of state
        dispatcher.send(pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, self,'')

    # set wifi ap ssid (not used at the moment)
    def handle_set_wifi_ap_ssid(self, ssid, signal, sender):
        """
        Sets HOSTAPD wifi network SSID
        """
        if ssid != self.WiFiAPSSID:
            self.WiFiAPSSID = ssid
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK, self, self.WiFiAPSSID)

    def handle_set_wifi_ap_password(self, password, signal, sender):
        """
        Set handler for WiFi AP password
        """
        if password != pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD):
            self.WiFiAPPassword = password
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)

    def handle_set_wifi_client_ssid(self, ssid, signal, sender):
        # if networkmanager knows the password for the client ssid, then we need to set & send ...

        # if ssid in pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS):
        # print('\n* Network {} found in available WiFi networks\n'.format(ssid))
        connections = {}

        found_existing_connection = False
        for conn in NetworkManager.Settings.ListConnections():
            settings = conn.GetSettings()
            try:
                secrets = conn.GetSecrets()
                for key in secrets:
                    settings[key].update(secrets[key])
                connections[settings['connection']['id']] = settings
            except Exception, e:
                pass
        logging.debug('checking for {} in self.WiFiAvailableNetworks = {}'.format(ssid, self.WiFiAvailableNetworks))
        logging.debug('checking for {} in NM connections {}'.format(ssid, connections))
        for n in self.WiFiAvailableNetworks:
            if n in connections:
                if n == ssid:
                     logging.debug('requested NM connection {} found'.format(n))
                     self.WiFiClientSSID = ssid
                     dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, self, ssid)
                     self.WiFiClientPassword = connections[n]['802-11-wireless-security']['psk']
                     dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, self, connections[n]['802-11-wireless-security']['psk'])
                     found_existing_connection = True
                     break
        if found_existing_connection == False:
            logging.debug('requested NM connection {} NOT found with self.WiFiInitialScanCompleted set to {}'.format(ssid,self.WiFiInitialScanCompleted))
            self.WiFiClientSSID = ssid
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, self, self.WiFiClientSSID)
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, self, PFM_WIFI_CLIENT_DEFAULT_PASSWORD)

    def handle_set_wifi_client_password(self, password, signal, sender):
        # just set signal & property for now, actually connect when SIG_WIFI_CLIENT_ACTIVE (True) is requested through handle_set_wifi_client_active()
        self.WiFiClientPassword = password
        dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, self, self.WiFiClientPassword)

    # an event handler to listen to write requests for available networks
    def handle_scan_available_networks(self, dummy, signal, sender):
        """
        In seperate thread, scans for updated available WiFi networks for potential internet connection
        """
        t = Thread(target=self.scan_available_networks)
        t.start()

    def handle_set_wifi_client_active(self, state, signal, sender):
        """
        Set handler for WiFi Client mode (de)activation with current network name and password settings
        """
        dpath = None
        if state == True or state == 'True':
            # print('DEBUG: connectionmanager.py: handle_set_wifi_client_active() state == True')
            if self.WiFiClientSSID in self.WiFiAvailableNetworks:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
                connections = {}
                found_existing_connection = False
                for conn in NetworkManager.Settings.ListConnections():
                    settings = conn.GetSettings()
                    try:
                        secrets = conn.GetSecrets()
                        for key in secrets:
                            settings[key].update(secrets[key])
                        connections[settings['connection']['id']] = settings
                    except Exception, e:
                        pass
                for n in self.WiFiAvailableNetworks:
                    if n in connections:
                        if n == self.WiFiClientSSID:
                             logging.debug('requested NM connection {} found'.format(self.WiFiClientSSID))
                             found_existing_connection = True
                             n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiClientDevice))
                             n_m_activate_connection([self.WiFiClientSSID])
                             self.WiFiClientActive = True
                             dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
                             break
                if found_existing_connection == False:
                    logging.debug('requested NM connection NOT found, creating connection for {}'.format(self.WiFiClientSSID))
                    # HACK! make sure password is NOT null or less than 8 characters in length for WPA network types
                    if self.WiFiClientPassword == None:
                        self.WiFiClientPassword = ''
                    if len(self.WiFiClientPassword) < 8:
                        logging.debug('null password passed for WPA network {} filling with 8 spaces ...'.format(self.WiFiClientSSID))
                        self.WiFiClientPassword = '        '
                        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, self, self.WiFiClientPassword)
                    s_con = dbus.Dictionary({
                        'type': '802-11-wireless',
                        'uuid': str(uuid.uuid4()),
                        'permissions': ['user:pfm:'],
                        'autoconnect': 'false',
                        'interface-name': self.WiFiClientDevice,
                        'id': self.WiFiClientSSID })
                    s_wifi = dbus.Dictionary({
                        'ssid': dbus.ByteArray(self.WiFiClientSSID),
                        'mode': 'infrastructure', })
                    s_wsec = dbus.Dictionary({
                        'key-mgmt': 'wpa-psk',
                        'auth-alg': 'open',
                        'psk': self.WiFiClientPassword, })
                    s_ip4 = dbus.Dictionary({'method': 'auto'})
                    s_ip6 = dbus.Dictionary({'method': 'ignore'})
                    con = dbus.Dictionary({
                        'connection': s_con,
                        '802-11-wireless': s_wifi,
                        '802-11-wireless-security': s_wsec,
                        'ipv4': s_ip4,
                        'ipv6': s_ip6})
                    proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings")
                    settings = dbus.Interface(proxy, "org.freedesktop.NetworkManager.Settings")
                    try:
                        n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiClientDevice)) #  deactivate all connections on this device
                        settings.AddConnection(con)
                        self.WiFiClientActive = True
                    except:
                        logging.debug('DBUS Exception trapped')
                        self.WiFiClientActive = False
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, self.WiFiClientActive)
                    n_m_activate_connection([self.WiFiClientSSID])
            else:
                logging.info('requested Network {} not found in Available Networks'.format(self.WiFiClientSSID))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'])
        else:
            # print('DEBUG: connectionmanager.py: handle_set_wifi_client_active() state == False')
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == True:
                devices = self.NMInterface.GetDevices()
                for d in devices:
                    dev_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", d)
                    prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                    iface = prop_iface.Get("org.freedesktop.NetworkManager.Device", "Interface")
                    if iface == self.WiFiClientDevice:
                        dpath = d
                        break
                #if not dpath or not len(dpath):
                #    raise Exception('NetworkManager knows nothing about WiFi Client Device %s'.format(self.WiFiClientDevice))
                dev_proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", dpath)
                dev_iface = dbus.Interface(dev_proxy, "org.freedesktop.NetworkManager.Device")
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                # Make sure the device is connected before we try to disconnect it
                state = prop_iface.Get("org.freedesktop.NetworkManager.Device", "State")
                if state == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                    logging.debug('WiFi Client Device {} is already not connected, cannot disconnect'.format(self.WiFiClientDevice))
                else:
                    n_m_deactivate_connection([self.WiFiClientSSID])
                    # dev_iface.Disconnect()
                    self.WiFiClientActive = False
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
                # experimental just-close-them-all alternative
                n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiClientDevice))


    def handle_set_wifi_ap_active(self, state, signal, sender):
        """
        Set handler for WiFi AP client mode activation
        """
        # TEST: dpath not used in this method
        # dpath = None
        if state == True or state == 'True':
            #  print('DEBUG: coonectionmanager.py: handle_set_wifi_ap_active() state == True')
            n_m_deactivate_connection(get_active_connections_by_interface(self.WiFiAPDevice)) #  deactivate all connections on this device
            # n_m_delete_connection([self.WiFiAPDevice])
            # dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
            # HACK! make sure password is NOT null or less than 8 characters in length for WPA network types
            if self.WiFiAPPassword == None:
                self.WiFiAPPassword = ''
            if len(self.WiFiAPPassword) < 8:
                logging.debug('null password passed for network {} filling with 12345678 ...'.format(self.WiFiAPSSID))
                self.WiFiAPPassword = PFM_WIFI_AP_DEFAULT_PASSWORD
                dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, self, self.WiFiAPPassword)
            s_con = dbus.Dictionary({
                'type': '802-11-wireless',
                'uuid': str(uuid.uuid4()),
                'permissions': ['user:pfm:'],
                'id': self.WiFiAPSSID })
            s_wifi = dbus.Dictionary({
                'ssid': dbus.ByteArray(self.WiFiAPSSID),
                'mode': 'ap', 
                'band': 'bg',
                'channel': '1' })
            s_wsec = dbus.Dictionary({
                'key-mgmt': 'wpa-psk',
                'auth-alg': 'open',
                'psk': self.WiFiAPPassword, })
            s_ip4 = dbus.Dictionary({'method': 'shared'})
            s_ip6 = dbus.Dictionary({'method': 'ignore'})
            con = dbus.Dictionary({
                'connection': s_con,
                '802-11-wireless': s_wifi,
                '802-11-wireless-security': s_wsec,
                'ipv4': s_ip4,
                'ipv6': s_ip6})
            proxy = self.SystemDBus.get_object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings")
            settings = dbus.Interface(proxy, "org.freedesktop.NetworkManager.Settings")
            try:
                #  print('DEBUG: connectionmanager.py: handle_set_wifi_ap_active(): attempting settings.AddConnection ...')
                settings.AddConnection(con)
                self.WiFiAPActive = True
            except:
                logging.debug('DBUS Exception attempting to add connection')
                self.WiFiAPActive = False
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE, self, self.WiFiAPActive)
            n_m_activate_connection([self.WiFiAPSSID])
        else:
            #  print('DEBUG: connectionmanager.py: handle_set_wifi_ap_active() state == False')
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE) == True:
                 n_m_deactivate_connection([self.WiFiAPSSID]) #  deactivate ap NM connection

    def networkmanager_device_add_remove(self, *args, **kwargs):
        msg = kwargs['d_member']
        if msg == "DeviceAdded":
            # Argument will be the device, which we want to monitor now
            print "networkmanager_device_add_remove: Device added:  %s" % (args[0].object_path)
            args[0].connect_to_signal('StateChanged', self.networkmanager_device_state_change, **d_args)
            return
        if msg == "DeviceRemoved":
            print "networkmanager_device_add_remove: Device removed:  %s" % (args[0].object_path)
            if args[0].object_path in Devices:
                del args[0].object_path

    def networkmanager_device_state_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        newState = NetworkManager.const('device_state', args[0])

        #  print('DEBUG: connectionmanager.py: networkmanager_device_state_change: {} {} ({}) ({}) {}'.format(NetworkManager.Device(path).Interface, self.WiFiClientDevice, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE),PFM_WIFI_MODE_CLIENT, path))

        if NetworkManager.Device(path).Interface == self.EthernetDevice:
            if   args[0] == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE, self, True)
            else:
                dispatcher.send(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE, self, False)
            logging.debug('Internal Ethernet Interface {} is in state {}'.format(self.EthernetDevice, newState))
            # print('DEBUG: connectionmanager.py: network_manager_device_state_change(): Ethernet Interface {} is in state {}'.format(self.EthernetDevice, newState))

        if NetworkManager.Device(path).Interface == self.WiFiClientDevice and pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_CLIENT:
            if   args[0] == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED])
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK))
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                # print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, ipaddr_dotted)
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == False:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_DEACTIVATING:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CHECK:
                dev_proxy = self.SystemDBus.get_object('org.freedesktop.NetworkManager',path)
                prop_iface = dbus.Interface(dev_proxy, "org.freedesktop.DBus.Properties")
                ipaddr = prop_iface.Get('org.freedesktop.NetworkManager.Device','Ip4Address')
                ipaddr_dotted = socket.inet_ntoa(struct.pack('<L', ipaddr))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, self, ipaddr_dotted)
                # print('****************** IP ADDRESS {}'.format(ipaddr_dotted))
            elif args[0] == NetworkManager.NM_DEVICE_STATE_IP_CONFIG:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, True)
            elif args[0] == NetworkManager.NM_DEVICE_STATE_NEED_AUTH:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH])
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_ACTIVE) == True:
                    dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, self, False)
                # print('DEBUG: networkmanager_device_state_change(): need auth state reached. Deleting profiles for "{}"'.format(self.WiFiClientSSID))
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, PFM_WIFI_STATUS_ERROR)
                n_m_delete_connection([self.WiFiClientSSID])
            logging.debug('WiFi Client Interface {} is in state {}'.format(self.WiFiClientDevice, newState))

        if NetworkManager.Device(path).Interface == self.WiFiAPDevice and pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_MODE) == PFM_WIFI_MODE_AP:
            logging.debug('WiFi AP Interface {} is in state {}'.format(self.WiFiAPDevice, newState))
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[args[0]])
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_STATUS, self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK))

        # If Wifi Client mode is active, then set that the PFM unit is network connected 
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == True:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == False:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        # If Ethernet is active, then set that the PFM unit is network connected 
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE) == True:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == False:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, True)

        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == False and pfm_signals.get_value(pfm_signals.SIG_UPDATE_ETHERNET_ACTIVE) == False:
        # if pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE) == False:
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED) == True:
                dispatcher.send(pfm_signals.SIG_UPDATE_CONNECTIONMANAGER_PFM_CONNECTED, self, False)

def n_m_activate_connection(names):
    connections = NetworkManager.Settings.ListConnections()
    connections = dict([(x.GetSettings()['connection']['id'], x) for x in connections])

    if not NetworkManager.NetworkManager.NetworkingEnabled:
        NetworkManager.NetworkManager.Enable(True)
    for n in names:
        if n not in connections:
            print('DEBUG: connectionmanager.py: n_m_activate_connection: No such connection: {}'.format(n))
        else:
            conn = connections[n]
            ctype = conn.GetSettings()['connection']['type']
            if ctype == 'vpn':
                for dev in NetworkManager.NetworkManager.GetDevices():
                    if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED and dev.Managed:
                        break
                else:
                    print('DEBUG: connectionmanager.py: n_m_activate_connection: No active, managed device found')
            else:
                dtype = {
                    '802-11-wireless': 'wlan',
                    'gsm': 'wwan',
                }
                # if dtype in connection_types:
                #     enable(dtype)
                dtype = {
                    '802-11-wireless': NetworkManager.NM_DEVICE_TYPE_WIFI,
                    '802-3-ethernet': NetworkManager.NM_DEVICE_TYPE_ETHERNET,
                    # 'gsm': NetworkManager.NM_DEVICE_TYPE_MODEM,
                }.get(ctype,ctype)
                devices = NetworkManager.NetworkManager.GetDevices()
    
                for dev in devices:
                    # print('DEBUG: n_m_activate_connection: {} {} {}'.format(NetworkManager.Device(dev).Interface, dev.DeviceType, dev.State))
                    if dev.DeviceType == dtype and dev.State == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
                        break
                else:
                    logging.debug('No suitable and available {} device found'.format(ctype))

            NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")


def n_m_deactivate_connection(names):
    active = NetworkManager.NetworkManager.ActiveConnections
    active = dict([(x.Connection.GetSettings()['connection']['id'], x) for x in active])
    for n in names:
        if n in active:
            logging.debug('deactivating connection: ({})'.format(n))
            NetworkManager.NetworkManager.DeactivateConnection(active[n])
        else:
            logging.debug('No such connection: ({})'.format(n))

def n_m_delete_connection(names):
    active = [x.Connection.GetSettings()['connection']['id']
              for x in NetworkManager.NetworkManager.ActiveConnections]
    connections = [(x.GetSettings()['connection']['id'], x)
                   for x in NetworkManager.Settings.ListConnections()]
    for conn in sorted(connections):
        if conn[0] in names:
            logging.debug('Deleting connection {} {}'.format(conn[0], conn[1]))
            try:
                conn[1].Delete()
            except:
                pass

def networkmanager_connection_list():
    # print('NetworkManager.NetworkManager.ActiveConnections = {}'.format(NetworkManager.NetworkManager.ActiveConnections))
    # print('len(NetworkManager.NetworkManager.ActiveConnections) = {}'.format(len(NetworkManager.NetworkManager.ActiveConnections)))
    active = [x.Connection.GetSettings()['connection']['id']
              for x in NetworkManager.NetworkManager.ActiveConnections]
    connections = [(x.GetSettings()['connection']['id'], x.GetSettings()['connection']['type'])
                   for x in NetworkManager.Settings.ListConnections()]
    fmt = "DEBUG: connectionmanager.py network_connection_list() %%s %%-%ds    %%s" % max([len(x[0]) for x in connections])
    for conn in sorted(connections):
        prefix = '* ' if conn[0] in active else '  '
        print(fmt % (prefix, conn[0], conn[1]))

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender

def print_active_connections():
    print('\nActive connections')
    print('{} {} {} {}'.format('Name', 'Type', 'Default', 'Devices'))
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        settings = conn.Connection.GetSettings()['connection']
    try:
        print('{} {} {} {}'.format(settings['id'], settings['type'], conn.Default, ", ".join([x.Interface for x in conn.Devices])))
    except NameError:
        print("Probably no active connections")

def get_active_connections_by_interface(network_interface):
    # print('DEBUG: connectionmanager.py get_active_connections_by_interface({})'.format(network_interface))
    active_interface_connections = []
    sleep(2) #  TODO: IMPORTANT: Yes, this is needed. NetworkManager will throw an exception without it. No idea why yet!!!
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        try:
            settings = conn.Connection.GetSettings()['connection']
            if network_interface in [x.Interface for x in conn.Devices]:
                logging.debug(' active connection {} {} {} {}'.format(settings['id'], settings['type'], conn.Default, ", ".join([x.Interface for x in conn.Devices])))
                active_interface_connections.append(settings['id'])
        except:
            print('DEBUG: connectionmanager.py get_active_connections_by_interface({}) reached exception!!!'.format(network_interface))
    return(active_interface_connections)


def main():
    w = ConnectionManager()
    dummy = 0

    print('\nBegin WiFi tests ...\n')

    dispatcher.connect(handle_event, signal=pfm_signals.SIG_SET_WIFI_MODE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_AP_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_AP_STATUS, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, sender=dispatcher.Any)

    act_conns = get_active_connections_by_interface(w.WiFiClientDevice)
    if act_conns != None and act_conns != []:
        print(' Deactivating existing active connections on {}: {}'.format(act_conns, w.WiFiClientDevice))
        n_m_deactivate_connection(act_conns)


    # dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanagerwifi.py main()', PFM_WIFI_MODE_OFF)
    # dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', PFM_WIFI_MODE_AP)
    # dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', PFM_WIFI_MODE_CLIENT)
    # dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', 'GARBAGE')

    # return for now just testing scan()
    # return


    # print('\nBegin WiFi AP tests ...\n')

    # dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, 'wifi.py main()', 'unittest')
    # dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', PFM_WIFI_MODE_AP)


    # print('-------------AP-Mode------------------------------------')

    # sleep(6000)

    # for testcon in [['AndroidAP','qhyv4701'],['pfmtest','mictmedia']]:

    print('\nBegin WiFi Client tests ...\n')
    
    # for testcon in [['efeendy2','wrongpasswd'],['pfmtest','mictmedia'],['AndroidAP','qhyv4701'],['efeendy2', 'EmvA3D14Yn']]:
    for testcon in [['MiCT_Lab','internationalmict'],['pfmtest','mictmedia']]:
        # let's delete all existing connections ...
        # n_m_delete_connection([testcon[0]])
        print('\n* Testing connection to {}'.format(testcon[0]))
        if testcon[0] in pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS):
            print('\n* Network {} found in available WiFi networks\n'.format(testcon[0]))
        else:
            print('\n* Network {} *not* found in available WiFi networks\n'.format(testcon[0]))
        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, 'wifi.py main()', testcon[0])
        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, 'wifi.py main()', testcon[1])
        print('  Connecting to {} ...\n'.format(pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK)))
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, 'wifi.py main()', True)
        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', PFM_WIFI_MODE_CLIENT)

        print('\nDEBUG: Status Connection: {} Active: {} Status: {}'.format(testcon, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE), pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS)))
        sleep(15)
        print('\nDEBUG: Status Connection: {} Active: {} Status: {}'.format(testcon, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_ACTIVE), pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS)))
        # print_active_connections()
        sleep(10)
        print('\n Disconnecting from {} ...\n'.format(testcon[0]))
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, 'wifi.py main()', False)
        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, 'connectionmanager.py main()', PFM_WIFI_MODE_OFF)
        sleep(5)
        # print_active_connections()
    print('\nEnd WiFi client tests ...\n')


    # quit GObject main loop
    w._loop.quit()

if __name__ == "__main__":
    """If invoked from the command line, call
    the main function including all command line parameters.
    """
    main()


