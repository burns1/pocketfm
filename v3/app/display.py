
import pfm_signals

import ui.display.demo.HelloWorld
import ui.display.demo.print_force

import ui.display.demo.HelloEncoder
import ui.display.demo.HelloEncoder2
import ui.display.demo.SimpleStatus
import ui.display.demo.HelloClock
import ui.display.demo.OutputPowerSolutions
import ui.display.demo.InputAndStatus
import ui.display.demo.TheDelayProblem
import ui.display.demo.PanelDemo_MainMenu
import ui.display.demo.FileBrowser
import ui.display.demo.ItemUpdatableNotAttached
import ui.display.demo.TextInput

import ui.display.v1.main

def start (bboard):

    # ui.display.demo.print_force.start(bboard)
    # ui.display.demo.HelloWorld.start(bboard)

    #ui.display.demo.HelloEncoder.start(bboard)
    #ui.display.demo.HelloEncoder2.start(bboard)
    # ui.display.demo.HelloClock.start(bboard)
    # ui.display.demo.SimpleStatus.start(bboard)
    # ui.display.demo.OutputPowerSolutions.start(bboard)
    # ui.display.demo.InputAndStatus.start(bboard)
    #ui.display.demo.TheDelayProblem.start(bboard)
    #ui.display.demo.PanelDemo_MainMenu.start(bboard)
    #ui.display.demo.FileBrowser.start(bboard)
    # ui.display.demo.ItemUpdatableNotAttached.start(bboard)


    # activate this to see the new ItemTextInput class in action:
    # ui.display.demo.TextInput.start(bboard)

    ui.display.v1.main.start(bboard)

