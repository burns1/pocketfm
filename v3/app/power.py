#!/usr/bin/python

import sys
import threading
from pydispatch import dispatcher
from time import sleep

import pfm_signals

PowerModeBit = {'Display': 0x1, 'GPS': 0x2, 'MaxProBoard': 0x4, 'Satellite': 0x8, 'USB1': 0x10, 'USB2': 0x20, 'RaspberryPi': 0x40, 'Atmel': 0x80}

PowerModeSignals = {'Display': pfm_signals.SIG_UPDATE_POWER_DISPLAY, 'GPS': pfm_signals.SIG_UPDATE_POWER_GPS, 'MaxProBoard': pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, 'Satellite': pfm_signals.SIG_UPDATE_POWER_SATELLITE, 'USB1': pfm_signals.SIG_UPDATE_POWER_USB1, 'USB2': pfm_signals.SIG_UPDATE_POWER_USB2, 'RaspberryPi': pfm_signals.SIG_UPDATE_POWER_RASPBERRYPI, 'Atmel': pfm_signals.SIG_UPDATE_POWER_ATMEL}

class Power(object):

    def __init__(self):

        self.BBQuery = "\x1b?P"

        self.Mode = PowerModeBit['RaspberryPi'] | PowerModeBit['Atmel'] # sane initial value, else may accidentally turn power off to RPi or Atemel & RPi (bad!)

        for component in PowerModeBit:
            setattr(self, component, False)

        dispatcher.connect(self.handle_set_power_display, signal=pfm_signals.SIG_SET_POWER_DISPLAY, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_gps, signal=pfm_signals.SIG_SET_POWER_GPS, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_maxproboard, signal=pfm_signals.SIG_SET_POWER_MAXPROBOARD, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_satellite, signal=pfm_signals.SIG_SET_POWER_SATELLITE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_usb1, signal=pfm_signals.SIG_SET_POWER_USB1, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_usb2, signal=pfm_signals.SIG_SET_POWER_USB2, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_raspberrypi, signal=pfm_signals.SIG_SET_POWER_RASPBERRYPI, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_power_atmel, signal=pfm_signals.SIG_SET_POWER_ATMEL, sender=dispatcher.Any)

        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, self.BBQuery)
        # setting power levels here does not seem to work
        #sleep(2)
        # set default power state of devices
        #dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, self, True)
        #dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, self, True)
        #dispatcher.send(pfm_signals.SIG_SET_POWER_USB1, self, True)
        #dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, self, True)
        dispatcher.send(pfm_signals.SIG_SET_POWER_RASPBERRYPI, self, True) # set initial power state for RPi :)

    def updateMode(self, mode):
        if mode != self.Mode:
            self.Mode = mode
            for key in PowerModeBit:
                if self.Mode & PowerModeBit[key] == 0:
                    if getattr(self,key) != False:
                        setattr(self, key, False)
                        dispatcher.send(PowerModeSignals[key], self, False)
                else:
                    if getattr(self,key) != True:
                        setattr(self, key, True)
                        dispatcher.send(PowerModeSignals[key], self, True)
            dispatcher.send(pfm_signals.SIG_UPDATE_POWER_MODE, self, self.Mode)

    def switch(self, component, state):
        pcompstate = getattr(self,component)
        switch_signal = PowerModeSignals[component]
        if state != pcompstate:
            setattr(self, component, state)
            if state:
                newMode = self.Mode | PowerModeBit[component]  
            else:
                newMode = self.Mode & (PowerModeBit[component] ^ 0xff)
            #print('DEBUG: power.py: switch(): SIG_BREAKOUTBOARD_SEND_COMMAND ESC{},{}P'.format(bin(newMode), bin(newMode ^ 0xff)))
            dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, '\x1b{},{}P'.format(newMode, newMode ^ 0xff))
            dispatcher.send(switch_signal, self, state)
            self.updateMode(newMode)
            dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, self.BBQuery) # sanity check
            return True
        return False


    def handle_set_power_display(self, power_state, signal, sender):
        self.switch('Display', power_state)

    def handle_set_power_gps(self, power_state, signal, sender):
        self.switch('GPS', power_state)

    def handle_set_power_maxproboard(self, power_state, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_SET_PFM_DARK_MODE) != True:
            self.switch('MaxProBoard', power_state)
        else:
            self.switch('MaxProBoard', False)
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FORCEWRITE, self, True)

    def handle_set_power_satellite(self, power_state, signal, sender):
        self.switch('Satellite', power_state)

    def handle_set_power_usb1(self, power_state, signal, sender):
        self.switch('USB1', power_state)

    def handle_set_power_usb2(self, power_state, signal, sender):
        self.switch('USB2', power_state)

    def handle_set_power_raspberrypi(self, power_state, signal, sender):
        self.switch('RaspberryPi', power_state)

    def handle_set_power_atmel(self, power_state, signal, sender):
        self.switch('Atmel', power_state)

# example event handler
def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", bin(data), "signal:", signal, "sender:", sender

def main(argv):
    from breakoutboard import BreakoutBoard
    try: 
        b = BreakoutBoard()
        b.start()

        print 'Connect diagnostic event handlers ...'
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_MODE,        sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_DISPLAY,     sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_GPS,         sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_SATELLITE,   sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB1,        sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_USB2,        sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_RASPBERRYPI, sender=dispatcher.Any)   
        dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_POWER_ATMEL,       sender=dispatcher.Any)   

        print 'Send initial power query {} ...'.format(b.Power.BBQuery)
        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, 'power main()', b.Power.BBQuery) 

        sleep(5)

        print 'Begin tests'
        print 'USB1 off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB1, 'power main()', False)
        sleep(5)
        print 'USB2 off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'power main()', False)
        sleep(5)
        print 'GPS off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'power main()', False)
        sleep(5)
        print 'MaxProBoard off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'power main()', False)
        sleep(5)
        print 'Satellite off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'power main()', False)
        sleep(5)

        print 'USB1 on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB1, 'power main()', True)
        sleep(5)
        print 'USB2 on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'power main()', True)
        sleep(5)
        print 'GPS on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'power main()', True)
        sleep(5)
        print 'MaxProBoard on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'power main()', True)
        print 'Wait 5 seconds ...'
        sleep(5)
        print 'Satellite on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'power main()', True)
        sleep(5)

        print 'USB1 off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB1, 'power main()', False)
        sleep(5)
        print 'USB2 off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'power main()', False)
        sleep(5)
        print 'GPS off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'power main()', False)
        sleep(5)
        print 'MaxProBoard off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'power main()', False)
        sleep(5)
        print 'Satellite off ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'power main()', False)
        sleep(5)

        print 'USB1 on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB1, 'power main()', True)
        sleep(5)
        print 'USB2 on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_USB2, 'power main()', True)
        sleep(5)
        print 'GPS on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'power main()', True)
        sleep(5)
        print 'MaxProBoard on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_MAXPROBOARD, 'power main()', True)
        print 'Wait 5 seconds ...'
        sleep(5)
        print 'Satellite on ...'
        dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'power main()', True)
        sleep(5)

        print 'End tests'

        b.close()
        b.join()

        print "\nPower State: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())
        # print("Power State: b.is_live: {} b.closing: {} ".format(b.is_alive(), b.closing))

    except (KeyboardInterrupt, SystemExit):
        print "\nInterrupt State: b.is_live: {} b.closing: {} Active Threads: {}".format(b.is_alive(), b.closing, threading.active_count())

if __name__ == "__main__":
    """If invoked from the command line, call
    the main function including all command line parameters.
    """
    main(sys.argv[1:])
