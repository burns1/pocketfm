#!/usr/bin/python

import serial
import threading
import time
import Queue


class SerialManager():
    """ This class is a modified version of the work of Philipp Klaus to be found on
        https://gist.github.com/4039175 .

        This one separates read and write into separate threads

    """
    def __init__(self, device, **kwargs):
        settings = dict()
        settings['baudrate'] = 115200
        settings['bytesize'] = serial.EIGHTBITS
        settings['parity'] = serial.PARITY_NONE
        settings['stopbits'] = serial.STOPBITS_ONE
        settings['timeout'] = 0.05
        settings['xonxoff'] = True

        settings.update(kwargs)
        self.ser = serial.Serial(device, **settings)

        self.read_num_bytes = 256
        self.sleeptime = 0.01 # no sleep time chews cpu load

        self.in_queue = Queue.Queue()
        self.out_queue = Queue.Queue()
        self.closing = False  # A flag to indicate thread shutdown

        threading.Thread(target=self.loop_read).start()
        threading.Thread(target=self.loop_write).start()

    def loop_read(self):
        try:
            while not self.closing:
                if self.sleeptime:
                    time.sleep(self.sleeptime)
                in_data = self.ser.read(self.read_num_bytes)
                #in_data = None
                if in_data:
                    self.in_queue.put(in_data)
        except (KeyboardInterrupt, SystemExit):
            pass
        self.ser.close()

    def loop_write(self):
        while not self.closing:
            try:
                out_buffer = self.out_queue.get_nowait()
                #start = time.time()
                bytes = self.ser.write(out_buffer)
                #print "WROTE ", bytes, "bytes in", time.time() - start
            except Queue.Empty:
                pass
                if self.sleeptime:
                    time.sleep(self.sleeptime)
        self.ser.close()

    def close(self):
        self.closing = True


