#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

from pydispatch import dispatcher
from threading import Thread
import sys
import subprocess
import shlex
from time import sleep
import pfm_signals

class GSM():
    """ GSM interface class.
    Holds attributes & methods required for GSM network & SMS control
    """
    def __init__(self):
        self.Network =''
        # self.Network_changed = pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME
        self.gammu_output = ''
        # t = Thread(target=self.scan_SIM)
        # t.start()

    def scan_SIM(self):
        tsplit = []
        try:
             smsd_off_output = subprocess.check_output(['sudo','/bin/systemctl', 'stop', 'gammu-smsd'])
        except:
             pass
        sleep(6) # time seems to be needed to free up serial port from gammu-smsd
        try:
             self.gammu_output = subprocess.check_output(['gammu', 'networkinfo'])
        except:
             pass
        try:
             smsd_on_output = subprocess.check_output(['sudo','/bin/systemctl', 'start', 'gammu-smsd'])
        except:
             pass
        for line in self.gammu_output.split('\n'):
            if 'Name in phone' in line:
                ninp = line
                tsplit = shlex.split(line)
                if tsplit[4] != self.Network:
                    self.Network = tsplit[4]
                    dispatcher.send(pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, self, self.Network)

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main():

    g = GSM()
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, sender=dispatcher.Any)
    sleep(20)
    test = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, 'Failure: no GSM SIM Network Operator found')
    print test
if __name__ == "__main__":
    main()
