#!/usr/bin/python

from pydispatch import dispatcher
import subprocess
import re
import pfm_signals


# Note this acts as a static ui object at the moment.
# Also note there is a periodic timer function

class RTC(object):

    def __init__(self):


        self.BBQuery = "\x1b?T"

        self.Weekday = 0
        self.Day = 0
        self.Month = 0
        self.Year = 0
        self.Hour = 0
        self.Minute = 0
        self.Second = 0
        self.Flags = 0
        self.DateString = ''
        dispatcher.send(pfm_signals.SIG_UPDATE_RTC_FLAGS, self, self.Flags)
        dispatcher.connect(self.handler_set_rtc, signal=pfm_signals.SIG_SET_RTC, sender=dispatcher.Any)
        dispatcher.connect(self.handler_set_systemtime, signal=pfm_signals.SIG_SET_RTC_SYSTEMTIME, sender=dispatcher.Any)


    def updateRTC(self, weekday, day, month, year, hour, minute, second, flags):
        if weekday != self.Weekday:
            self.Weekday = weekday
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_WEEKDAY, self, self.Weekday)
        if day != self.Day:
            self.Day = day
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_DAY, self, self.Day)
        if month != self.Month:
            self.Month = month
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_MONTH, self, self.Month)
        if year != self.Year:
            self.Year = year
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_YEAR, self, self.Year)
        if hour != self.Hour:
            self.Hour = hour
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_HOUR, self, self.Hour)
        if minute != self.Minute:
            self.Minute = minute
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_MINUTE, self, self.Minute)
        if second != self.Second:
            self.Second = second
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_SECOND, self, self.Second)
        if flags != self.Flags:
            self.Flags = flags
            dispatcher.send(pfm_signals.SIG_UPDATE_RTC_FLAGS, self, self.Flags)
        DateString = "{0:02d}/{1:02d}/{2:02d} {3:02d}:{4:02d}:{5:02d}".format(self.Day, self.Month, self.Year, self.Hour, self.Minute, self.Second)
        if DateString != self.DateString:
             self.DateString = DateString
             dispatcher.send(pfm_signals.SIG_UPDATE_RTC, self, self.DateString)

    def handler_set_rtc(self, datetime, signal, sender):
        # parse requested datetime string
        fields = datetime.split(' ')
        if len(fields) == 3:
            weekday = fields[0]
            day,month,year = fields[1].split('/')
            hour,minute,second = fields[2].split(':')
        # todo check validity of all date and time values
        rtc_fmtstr = '{} {}/{}/{} {}:{}:{}'.format(weekday, day, month, year, hour, minute, second)
        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, '\x1b'+rtc_fmtstr+'T' )

        # todo split alarm reset function out to complete alarm module
        # but as this is not a requirement for v1, we clear the BB alarm function when
        # setting the rtc clock

        alarm_fmtstr = '99,99,99,99'
        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, '\x1b'+alarm_fmtstr+'A' )

        dispatcher.send(pfm_signals.SIG_UPDATE_RTC, self, rtc_fmtstr)

    def handler_set_systemtime(self, dummy, signal, sender):
        #stime_fmtstr = '{%02d}-{%02d}-{%02d} {%02d}:{%02d}:{%02d}'.format(self.Year, self.Month, self.Day, self.Hour, self.Minute, self.Second)
        stime_fmtstr = '{}-{}-{} {}:{}:{}'.format(str(self.Year).zfill(2), str(self.Month).zfill(2), str(self.Day).zfill(2), str(self.Hour).zfill(2), str(self.Minute).zfill(2), str(self.Second).zfill(2))
        print('Setting system time to '.format(stime_fmtstr))
        date_output = subprocess.check_output(['sudo','/bin/date', '-s', stime_fmtstr])

