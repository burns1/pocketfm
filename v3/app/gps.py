#!/usr/bin/python

import sys
import time
import math

from pydispatch import dispatcher
import pfm_signals

class GPS(object):

    BBActivate = "\x1bG"
    BBDeactivate = "\x1b?G"

    def __init__(self):

        self.Active = False; # default state of GPS unit

        self.gotFix = True

        self.Latitude = 0.0
        self.Latitude_suffix = 'N'

        self.Longitude = 0.0
        self.Longitude_suffix = 'E'

        self.Altitude = 0.0

        self.Time = 0.0

        self.Location = '< No GPS Fix  >'

        # ensure power to GPS unit is on
        dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, self, True)

        # route composite location update events
        dispatcher.connect(self.handle_update_location, signal=pfm_signals.SIG_UPDATE_GPS_LATITUDE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_update_location, signal=pfm_signals.SIG_UPDATE_GPS_LATITUDE_DIRECTION, sender=dispatcher.Any)
        dispatcher.connect(self.handle_update_location, signal=pfm_signals.SIG_UPDATE_GPS_LONGITUDE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_update_location, signal=pfm_signals.SIG_UPDATE_GPS_LONGITUDE_DIRECTION, sender=dispatcher.Any)
        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, self.BBActivate)
        self.updateStatus(True)

    def updateStatus(self, state):
        if state != self.Active:
            self.Active = state
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_ACTIVE, self, self.Active)

    def updateLatitude(self, bearing, suffix):
        try:
            validp = float(bearing)
        except:
            print('DEBUG: gps.py: Latitude value {} not valid!'.format(repr(bearing)))
            return

        # convert WGS value from GPS into decimal
        degree_integer = ((validp / 100.0) // 1)
        degree_decimal = ((validp % 100.0) / 60) 
        validp = degree_integer + degree_decimal
        # print('DEBUG: gps.py: Latitude Degree {}'.format(validp))

        if validp != self.Latitude or suffix != self.Latitude_suffix:
            self.Latitude = validp
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_LATITUDE, self, self.Latitude)
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_LATITUDE_DIRECTION, self, self.Latitude_suffix)

    def updateLongitude(self, bearing, suffix):
        try:
            validp = float(bearing)
        except:
            print('DEBUG: gps.py Longitude value {} not valid!'.format(repr(bearing)))
            return

        # convert WGS value from GPS into decimal
        degree_integer = ((validp / 100.0) // 1)
        degree_decimal = ((validp % 100.0) / 60) 
        validp = degree_integer + degree_decimal
        # print('DEBUG: gps.py: Longitude Decimal Degree {}'.format(validp))

        if validp != self.Longitude or suffix != self.Longitude_suffix:
            self.Longitude = validp
            self.Longitude_suffix = suffix
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_LONGITUDE, self, self.Longitude)
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_LONGITUDE_DIRECTION, self, self.Longitude_suffix)

    def updateAltitude(self, height):
        try:
            validp = float(height) / 100.0
        except:
            print('DEBUG: gps.py Height value {} not valid!'.format(repr(height)))
            return
        if validp != self.Altitude:
            self.Altitude = validp
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_ALTITUDE, self, self.Altitude)

    def updateTime(self, time):
        if time != self.Time:
            self.Time = time
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_TIME, self, self.Time)

    def updateGotFix(self, state):
        if state != self.gotFix:
            self.gotFix = state
            dispatcher.send(pfm_signals.SIG_UPDATE_GPS_GOT_FIX, self, self.gotFix)

    def handle_update_location(self, data, signal, sender):
         if self.gotFix == True:
             Location = '{:07.4f}{}{:08.4f}{}'.format(self.Latitude, self.Latitude_suffix, self.Longitude, self.Longitude_suffix)
             # Location = '{:07.4f},{:08.4f}'.format(self.Latitude, self.Longitude)
         else:
             Location = '(no fix)'
         if Location != self.Location:
             self.Location = Location
             dispatcher.send(pfm_signals.SIG_UPDATE_GPS_POSITION, self, self.Location)

def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main(argv):
    from breakoutboard import BreakoutBoard
    b = BreakoutBoard()
    b.start()

    print 'Connect diagnostic event handlers ...'
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_ACTIVE,             sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_LATITUDE,           sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_LATITUDE_DIRECTION, sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_LONGITUDE,          sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_LONGITUDE_DIRECTION,sender=dispatcher.Any)
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_ALTITUDE,           sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_TIME,               sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_GOT_FIX,            sender=dispatcher.Any)   
    dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GPS_POSITION,            sender=dispatcher.Any)   

    print 'Send initial GPS Deactivation ...'.format(b.GPS.BBDeactivate)
    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, 'GPS main()', b.GPS.BBDeactivate)
    time.sleep(5)
    print 'Turn GPS off ...'
    dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'gps main()', False)
    time.sleep(5)
    print 'Turn GPS on ...'
    dispatcher.send(pfm_signals.SIG_SET_POWER_GPS, 'gps main()', True)
    time.sleep(5)
    print 'Send initial GPS Deactivation ...'.format(b.GPS.BBDeactivate)
    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, 'GPS main()', b.GPS.BBDeactivate)
    time.sleep(5)
    print 'Send initial GPS Activation ...'.format(b.GPS.BBActivate)
    dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, 'GPS main()', b.GPS.BBActivate)
    time.sleep(5)
    while True:
        # loc = '{:0>3.2}{}{:0>2.2}{}'.format(b.GPS.Latitude, b.GPS.Latitude_suffix, b.GPS.Longitude, b.GPS.Longitude_suffix)
        # loc = '{:0>5.2}{}{:0>4.2}{}'.format(b.GPS.Latitude, b.GPS.Latitude_suffix, b.GPS.Longitude, b.GPS.Longitude_suffix)
        loc = '{:05.2f}{}{:06.2f}{}'.format(b.GPS.Latitude, b.GPS.Latitude_suffix, b.GPS.Longitude, b.GPS.Longitude_suffix)
        print '#{}#'.format(loc)
        time.sleep(1)


if __name__ == "__main__":
    """If invoked from the command line, call
    the main function including all command line parameters.
    """
    main(sys.argv[1:])
