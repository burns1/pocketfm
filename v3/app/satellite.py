#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import os
import pyudev
import ast
import subprocess
from threading import Thread
from periodic import PeriodicThread
from time import sleep

import pfm_signals
from pydispatch import dispatcher

PFM_SATELLITE_PLAYLIST = '/tmp/pfm_satellite_playlist'
PFM_SATELLITE_WSCAN_RESULTS = '/home/pfm/pfm_conf/mplayer/pfm_wscan_results.conf'
PFM_SATELLITE_CUSTOM_SETTINGS = '/tmp/pfm_satellite_custom.conf'
PFM_SATELLITE_MPLAYER_CHANNELS_CONF = '/home/pfm/pfm_conf/mplayer/channels.conf.sat'
PFM_SATELLITE_CUSTOM_CHANNEL_NAME = 'PFMCUSTOM'

class Satellite(Thread):
    def __init__(self):
        super(Satellite, self).__init__()
        self.Active = False

        # current satellite settings
        self.CurrentSatelliteCode = False
        self.CurrentSatelliteName = False

        # current tuned channel settings
        self.CurrentChannelName = PFM_SATELLITE_CUSTOM_CHANNEL_NAME
        self.CustomChannelFrequency = '11953'
        self.CustomChannelPolarisation = 'H' # H or V (horizontal or vertical)
        self.CustomChannelSourceID = 0
        self.CustomChannelSymbolRate = '27500' # channel symbol rate
        self.CustomChannelVideoPID = 0
        self.CustomChannelAudioPID = '0711'
        self.CustomChannelServiceID = '28012'

        self.CustomChannelConf = '' # line for custom settings in mplayer channels.conf format

        self.FemonCommand = 'femon -H -c1'
        self.FemonOutput = ''

        self.FemonPollHandle = None
        self.FemonPolling = False

        # signal reception parameters
        self.SignalLevel = None
        self.SignalSNR = None

        self.ChannelList = [PFM_SATELLITE_CUSTOM_CHANNEL_NAME] # pfm custom channel setting plus channel list of satellite added from scan 

        try:
            self.udev_context = pyudev.Context()
            
            self.udev_monitor = pyudev.Monitor.from_netlink(self.udev_context)
            self.udev_monitor.filter_by('dvb') # we're only interested in dvb device events
            self.udev_observer = pyudev.MonitorObserver(self.udev_monitor, callback=self.watch_dvb_device, name='monitor-observer')
        except:
            pass
        self.udev_observer.start()

        dispatcher.connect(self.handle_set_satellite_custom_settings, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_satellite_channelname, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_NAME, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_custom_satellite_frequency, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_FREQUENCY, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_custom_channel_polarisation, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_POLARISATION, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_custom_channel_symbol_rate, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_custom_channel_audio_pid, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_AUDIO_PID, sender=dispatcher.Any)
        dispatcher.connect(self.handle_set_custom_channel_video_pid, signal=pfm_signals.SIG_SET_SATELLITE_CHANNEL_VIDEO_PID, sender=dispatcher.Any)
        dispatcher.connect(self.watch_audiomanager_input, signal=pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, sender=dispatcher.Any)

        self.CurrentChannelName = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_NAME, PFM_SATELLITE_CUSTOM_CHANNEL_NAME)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_NAME, self, self.CurrentChannelName)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_NAME, self, self.CurrentChannelName)

        self.CustomChannelFrequency = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_FREQUENCY, self.CustomChannelFrequency)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_FREQUENCY, self, self.CustomChannelFrequency)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, self, self.CustomChannelFrequency)

        self.CustomChannelPolarisation = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_POLARISATION, self.CustomChannelPolarisation)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_POLARISATION, self, self.CustomChannelPolarisation)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, self, self.CustomChannelPolarisation)

        self.CustomChannelSymbolRate = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE, self.CustomChannelSymbolRate)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE, self, self.CustomChannelSymbolRate)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE, self, self.CustomChannelSymbolRate)

        self.CustomChannelAudioPID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_AUDIO_PID, self.CustomChannelAudioPID)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_AUDIO_PID, self, self.CustomChannelAudioPID)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, self, self.CustomChannelAudioPID)

        self.CustomChannelVideoPID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_VIDEO_PID, self.CustomChannelVideoPID)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_VIDEO_PID, self, self.CustomChannelVideoPID)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_VIDEO_PID, self, self.CustomChannelVideoPID)

        self.CustomChannelVideoPID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_VIDEO_PID, self.CustomChannelVideoPID)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_VIDEO_PID, self, self.CustomChannelVideoPID)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_VIDEO_PID, self, self.CustomChannelVideoPID)

        self.CurrentChannelCustomConf = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS, 'PFMCUSTOM:11953:H:0:27500:0:711:28012')
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS, self, self.CustomChannelConf)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_CUSTOM_SETTINGS, self, self.CustomChannelConf)

        # satellite scan results
        if not os.path.isfile(PFM_SATELLITE_WSCAN_RESULTS):
            with open(PFM_SATELLITE_WSCAN_RESULTS, 'w+  ') as f:
                f.write('')

        # write file for mplayer satellite playlist into tmp RAM partition
        with open(PFM_SATELLITE_PLAYLIST, 'w+') as f:
            f.write('DVB://{}\n'.format(self.CurrentChannelName))

        # write file with single custom sat channel.conf
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS, self, '')

        # combine custom and scanned channels into channels.conf
        filenames = [PFM_SATELLITE_CUSTOM_SETTINGS, PFM_SATELLITE_WSCAN_RESULTS]
        with open(PFM_SATELLITE_MPLAYER_CHANNELS_CONF, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    outfile.write(infile.read())


    def watch_audiomanager_input(self, source, sender):
        # print('DEBUG: satellite.py: watch_audio_manager()')
        if source.upper() == 'SAT' or source.upper() == 'SATELLITE':
            dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, self, True)
        else:
            dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, self, False)


    def watch_dvb_device(self, device):
        if device.action == 'add':
            self.Active = True
            dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_ACTIVE, self, True) # satellite is in a state to play audio, ie power on, firmware loaded
            if self.FemonPolling == False:
                self.FemonPollHandle = PeriodicThread(1, self.get_satellite_status)
                self.FemonPollHandle.start()
                self.FemonPolling = True
        elif device.action == 'remove':
            self.Active = False
            dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_ACTIVE, self, False) 
            if self.FemonPolling == True:
                self.FemonPollHandle.closing = True
                self.FemonPolling = False


    def handle_set_satellite_custom_settings(self, dummy):
        custom_channel_conf = '{}:{}:{}:{}:{}:{}:{}:{}'.format(PFM_SATELLITE_CUSTOM_CHANNEL_NAME, self.CustomChannelFrequency, self.CustomChannelPolarisation, self.CustomChannelSourceID, self.CustomChannelSymbolRate, self.CustomChannelVideoPID, self.CustomChannelAudioPID, self.CustomChannelServiceID)
        # write Custom Channel settings
        with open(PFM_SATELLITE_CUSTOM_SETTINGS, 'w+') as f: 
            f.write('{}\n'.format(custom_channel_conf))
        self.CustomChannelConf = custom_channel_conf
        # combine custom and scanned channels into channels.conf
        filenames = [PFM_SATELLITE_CUSTOM_SETTINGS, PFM_SATELLITE_WSCAN_RESULTS]
        with open(PFM_SATELLITE_MPLAYER_CHANNELS_CONF, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    outfile.write(infile.read())
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'SATELLITE' and pfm_signals.get_value(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_NAME) == PFM_SATELLITE_CUSTOM_CHANNEL_NAME:
            dispatcher.send(pfm_signals.SIG_SET_MPLAYER_LOADLIST, self, PFM_SATELLITE_PLAYLIST)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_CUSTOM_SETTINGS, self, self.CustomChannelConf)


    def handle_set_custom_satellite_frequency(self, frequency):
        validp = isinstance(frequency, (int, str))
        if not validp:
            # satellite frequency is not valid
            print('DEBUG: satellite.py: Satellite Frequency value "{}" is not valid!'.format(frequency))
            return
        else:
            if frequency != self.CustomChannelFrequency:
                if isinstance(frequency, int):
                    self.CustomChannelFrequency = str(frequency)
                elif isinstance(frequency, str):
                    self.CustomChannelFrequency = frequency
                dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, self, self.CustomChannelFrequency)


    def handle_set_custom_channel_polarisation(self, polarisation):
        validp = isinstance(polarisation, str)
        if not validp:
            # satellite polarisation is not valid
            print('DEBUG: satellite.py: Satellite Polarisation type "{}" is not valid!'.format(polarisation))
            return
        else:
            validp = polarisation.upper()
            if validp == 'H' or validp == 'V':
                if polarisation != self.CustomChannelPolarisation:
                    self.CustomChannelPolarisation = polarisation.upper()
                    dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, self, self.CustomChannelPolarisation)
            else:
                print('DEBUG: satellite.py: Satellite Polarisation value "{}" is not valid!'.format(polarisation))


    def handle_set_custom_channel_symbol_rate(self, rate):
        validp = isinstance(rate, (int, str))
        if not validp:
            # satellite symbol rate is not valid
            print('DEBUG: satellite.py: Satellite Symbol Rate type "{}" is not valid!'.format(rate))
            return
        else:
            if rate != self.CustomChannelSymbolRate:
                if isinstance(rate, int):
                    self.CustomChannelSymbolRate = str(rate)
                elif isinstance(rate, str):
                    self.CustomChannelSymbolRate = rate
                dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE, self, self.CustomChannelSymbolRate)


    def handle_set_custom_channel_video_pid(self, pid):
        validp = isinstance(pid, (int))
        if not validp:
            # satellite symbol rate is not valid
            print('DEBUG: satellite.py: Satellite Video PID value "{}" is not valid!'.format(pid))
            return
        else:
            if pid != self.CustomChannelVideoPID:
                self.CustomChannelVideoPID = int(pid)
                dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_VIDEO_PID, self, self.CustomChannelVideoPID)


    def handle_set_custom_channel_audio_pid(self, pid):
        validp = isinstance(pid, (int, str)) 
        if not validp:
            # satellite symbol rate is not valid
            print('DEBUG: satellite.py: Satellite Symbol Audio PID value "{}" is not valid!'.format(pid))
            return
        else:
            if pid != self.CustomChannelAudioPID:
                if isinstance(pid, int):
                    self.CustomChannelAudioPID = str(pid)
                elif isinstance(pid, str):
                    self.CustomChannelAudioPID = pid
                self.CustomChannelAudioPID = self.CustomChannelAudioPID.zfill(4) # UI is dumb & needs zero padding
                dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, self, self.CustomChannelAudioPID)


    def handle_set_satellite_service_id(self, sid):
        validp = isinstance(sid, (int, str))
        if not validp:
            # satellite symbol rate is not valid
            print('DEBUG: satellite.py: Satellite Symbol Audio PID value "{}" is not valid!'.format(pid))
            return
        else:
            if pid != self.CustomChannelServiceID:
                if isinstance(sid, int):
                    self.CustomChannelAudioPID = str(pid)
                elif isinstance(sid, str):
                    self.CustomChannelAudioPID = sid
                dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SERVICE_ID, self, self.CustomChannelServiceID)


    def handle_set_satellite_channelname(self, name):
        pass


    def get_satellite_status(self):
        self.FemonOutput = 'None'
        try:
            self.FemonOutput = subprocess.check_output(self.FemonCommand, stderr=subprocess.STDOUT, shell=True)
        except:
            pass
        self.FemonStatusFlags = self.parse_femon_output(self.FemonOutput, 'status', 5)
        self.FemonSignalStrength = self.parse_femon_output(self.FemonOutput, 'signal ', 2)
        self.FemonSNR = self.parse_femon_output(self.FemonOutput, 'snr ', 2)
        dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_FEMON_OUTPUT, self, self.FemonOutput)
        # print('DEBUG: satellite.py: get_satellite_status output {} Flags {} Signal {} SNR {}'.format(self.FemonOutput, self.FemonStatusFlags, self.FemonSignalStrength, self.FemonSNR))
        if 'L' in self.FemonStatusFlags:
            self.FemonStatus = 'Have Lock'
        else:
            self.FemonStatus = 'Have no Lock'
            self.FemonSignalStrength = 0
            self.FemonSNR = 0

        if self.FemonStatus != pfm_signals.get_value(pfm_signals.SIG_UPDATE_SATELLITE_STATUS):
            dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_STATUS, self, self.FemonStatus)
        if self.FemonSignalStrength != pfm_signals.get_value(pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_LEVEL):
            dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_LEVEL, self, int(self.FemonSignalStrength))
        if self.FemonSNR != pfm_signals.get_value(pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_QUALITY):
            dispatcher.send(pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_QUALITY, self, int(self.FemonSNR))


    # Code from PFM v2!!
    def parse_femon_output(self, femon_row, value, expected_chars):
        try:
            start_pos = femon_row.index(value) + len(value) + 1 # +1 because of space between label and value
            end_pos = start_pos + expected_chars
            return femon_row[start_pos:end_pos]
        except:
            return " "*expected_chars


def handle_event(data, signal, sender):
    """Simple event handler"""
    print "handle_event: data:", data, "signal:", signal, "sender:", sender


def main():
    import sys
    import time
    from pfm_thread import PocketFM

    p = PocketFM()
    m = p.MaxPro
    b = p.Breakout

    print('TEST: satellite.py: Power Satellite Off')
    dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'satellite.py main()', False)
    time.sleep(5)
    print('TEST: satellite.py: Power Satellite On')
    dispatcher.send(pfm_signals.SIG_SET_POWER_SATELLITE, 'satellite.py main()', True)
    sleep(100)


if __name__ == "__main__":
    main()
