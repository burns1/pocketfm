from pydispatch import dispatcher
import pfm_signals
from ui.display.items import Item
from ui.display.items import Button2Screen
from ui.display.items import ButtonOK
from ui.display.items import ItemInputText
from ui.display.screens import Screen
import ui.display.CycleList

numbers_zero2nine = ui.display.CycleList.list_str_integer(0, 10)


class ScreenDarkMode(Screen):
    def __init__(self):
        super(ScreenDarkMode, self).__init__("dark mode")
        self.add(Item(3, 1, "DARK MODE ACTIVE"))
        #self.add(Button2Screen(4, 2, "LOCK", "lock screen"))
        #self.add(Item(0, 3, "Enter PIN:"))
        self.pin_entered = self.add(ItemInputText(8, 3, 6, 6, numbers_zero2nine))
        self.add(ButtonOK(1, 7, "DEACTIVATE & UNLOCK"))

        self.wrong_pin = self.add(Item(6, 5, "WRONG PIN!"))
        self.wrong_pin.visible = False

    def activate(self):
        self.pin_entered.reset("000000")
        super(ScreenDarkMode, self).activate()

    def btn_ok(self, new_screen):
        #deactivate dark mode
        #send signal switch dark mode off
        if self.pin_entered.input == pfm_signals.get_value(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN):
            self.wrong_pin.visible = False
            dispatcher.send(pfm_signals.SIG_SET_PFM_DARK_MODE, pfm_signals.SENDER_PANEL, False)
            #redirect not needed here? cause the update handler in pin.py
            #takes care of it
            #self.panel.activate(new_screen)
        else:
            self.wrong_pin.visible = True
            self.wrong_pin.show()
