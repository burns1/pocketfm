from pydispatch import dispatcher
import logging
import time
import pfm_signals
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import Button2Screen
from ui.display.items import Item
from ui.display.items import ItemInputText
from ui.display.screens import Screen
import ui.display.CycleList

numbers_zero2nine = ui.display.CycleList.list_str_integer(0, 10)


def screen_lock_menu():
    screen = Screen("lock screen")
    screen.add(Item(0, 0, "LOCK SCREEN"))
    screen.add(Button2Screen(4, 2, " Change PIN  ", "change pin", True))
    screen.add(Button2Screen(4, 4, " Lock Screen ", "lock"))
    screen.add(ButtonBack())
    return screen


class ScreenLock(Screen):
    def __init__(self):
        super(ScreenLock, self).__init__("lock")

        self.add(Item(4, 1, "DEVICE LOCKED"))
        self.pin_entered = self.add(ItemInputText(8, 3, 6, 6, numbers_zero2nine))
        self.add(ButtonOK(15, 7, "UNLOCK"))

        self.wrong_pin = self.add(Item(6, 5, "WRONG PIN!"))
        self.wrong_pin.visible = False
        dispatcher.connect(self.handle_darkmode, signal=pfm_signals.SIG_UPDATE_PFM_DARK_MODE, sender=dispatcher.Any,
                           weak=False)

    def activate(self):
        self.pin_entered.reset("000000")
        super(ScreenLock, self).activate()

    def btn_ok(self, new_screen):
        if self.pin_entered.input == pfm_signals.get_value(pfm_signals.SIG_UPDATE_PFM_SUPPORT_PIN):
            self.wrong_pin.visible = False
            self.panel.activate("new pin")
        else:
            if self.pin_entered.input == pfm_signals.get_value(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN):
                self.wrong_pin.visible = False
                self.panel.activate(new_screen)
            else:
                self.wrong_pin.visible = True
                self.wrong_pin.show()
                self.panel.wrong_pin += 1
                logging.warn("UNLOCK FAILED {} TIMES, PIN {}".format(self.panel.wrong_pin, self.pin_entered.input))
            self.select_start_item()

    def handle_darkmode(self, data):
        if data == True:
            #self.wrong_pin.visible = False
            self.panel.last_action_time = time.time()
            self.panel.activate("dark mode")
        elif data == False:
            super(ScreenLock, self).deactivate()
            self.panel.activate()


class ScreenChangePin(Screen):
    def __init__(self):
        super(ScreenChangePin, self).__init__("change pin")

        self.add(Item(0, 0, "LOCK SCREEN/NEW PIN"))

        self.add(Item(2, 2, "Old PIN"))
        self.old_pin = self.add(ItemInputText(13, 2, 6, 6, numbers_zero2nine))

        self.add(Item(2, 3, "New PIN"))
        self.new_pin = self.add(ItemInputText(13, 3, 6, 6, numbers_zero2nine))

        self.wrong_pin = self.add(Item(4, 5, "OLD PIN WRONG!"))
        self.wrong_pin.visible = False

        self.add(ButtonOK(new_screen="lock screen"))
        self.add(ButtonBack(new_screen="lock screen"))

    def btn_ok(self, new_screen):
        if pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, None) == self.old_pin.input:
            dispatcher.send(pfm_signals.SIG_SET_PFM_LOCK_PIN, pfm_signals.SENDER_PANEL, self.new_pin.input)
            self.panel.activate(new_screen)
            logging.info("PIN CHANGED from {} to {}".format(self.old_pin.input, self.new_pin.input))
        else:  # old pin wrong!
            self.wrong_pin.visible = True
            self.wrong_pin.show()
            self.panel.wrong_pin += 1
            self.select_start_item(True)

    def activate(self):
        self.new_pin.reset("000000")
        self.old_pin.reset("000000")

        self.wrong_pin.visible = False
        self.select_start_item(False)

        super(ScreenChangePin, self).activate()


class ScreenNewPin(Screen):
    def __init__(self):
        super(ScreenNewPin, self).__init__("new pin")

        self.add(Item(0, 0, "LOCK SCREEN/NEW PIN"))

        self.add(Item(2, 3, "New PIN"))
        self.new_pin = self.add(ItemInputText(13, 3, 6, 6, numbers_zero2nine))

        self.add(ButtonOK(new_screen="lock screen"))

    def btn_ok(self, new_screen):
        dispatcher.send(pfm_signals.SIG_SET_PFM_LOCK_PIN, pfm_signals.SENDER_PANEL, self.new_pin.input)
        self.panel.activate()
        logging.info("PIN CHANGED to {}".format(self.new_pin.input))

    def activate(self):
        self.new_pin.reset("000000")
        self.select_start_item(False)

        super(ScreenNewPin, self).activate()
