import ui.display.CycleList
import ui.display.panel
import ui.display.screens
from ui.display.items import no_value_line


from wifi import *
from pin import *
from audio import *
from power import *
from info import *
from dark_mode import *
from threeg import *

lock_time = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_TIMEOUT, 90)


def screen_main():
    screen = Screen()

    screen.add(ItemUpdatablePFM(0, 0, "STATION ID   {:>8}", pfm_signals.SIG_UPDATE_MAXPRO_STATIONID,
                                str_no_value=no_value_line("STATION ID"),
                                selectable=True, new_screen="edit station id"))

    screen.add(ItemUpdatablePFM(0, 1, "FREQUENCY    {:5.1f}MHz", pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,
                                str_no_value=no_value_line("FREQUENCY"),
                                selectable=True, new_screen="edit frequency"))

    screen.add(ItemUpdatablePFM(0, 2, "TX POWER     {:7.1f}W", pfm_signals.SIG_UPDATE_MAXPRO_OUTPUT_POWER,
                                str_no_value=no_value_line("TX POWER"), selectable=True, new_screen="power"))

    screen.add(ItemUpdatablePFM(0, 3, "SOURCE      {:>9}", pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT,
                                str_no_value=no_value_line("SOURCE"), selectable=True, new_screen="audio dispatcher"))

    screen.add(ItemUpdatablePFM(0, 4, "WIFI   {:>14.14}", pfm_signals.SIG_UPDATE_WIFI_STATUS,
                                str_no_value=no_value_line("WIFI", "(error)"), selectable=True, new_screen="wifi dispatcher"))

    screen.add(ItemUpdatablePFM(0, 5, "GSM     {:>13}", pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME,
                                str_no_value=no_value_line("GSM"), selectable=True, new_screen="3g dispatcher"))

    screen.add(ItemSelectable(17, 7, "INFO",new_screen="info"))

    if lock_time > 0:
        screen.add(Button2Screen(0, 7, "LOCK", "lock screen"))
    return screen


##############################################################################
#
#           Screen subclasses
#
##############################################################################
class ScreenStationID(Screen):
    def __init__(self):
        super(ScreenStationID, self).__init__("edit station id")
        self.station_id = self.add(ItemInputText(6, 4, 3, 8))
        self.add(Item(0, 0, "STATION ID"))
        self.add(Item(1, 2, "Set max. 8 letters"))
        self.add(ButtonOK())
        self.add(ButtonClear())
        self.add(ButtonBack())
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, None)

    def btn_ok(self, new_screen):
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, pfm_signals.SENDER_PANEL, self.station_id.input)

        self.select_start_item(False)
        self.panel.activate(new_screen)

    def btn_clear(self):
        self.station_id.reset(None)
        self.select_start_item(True)

    def activate(self):
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, None)
        dispatcher.connect(self.update_handler, pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, weak=False)
        self.station_id.reset(self.value)
        super(ScreenStationID, self).activate()

    def update_handler(self, value):
        self.value = value
        self.station_id.reset(value)

    def deactivate(self):
        dispatcher.disconnect(self.update_handler, signal=pfm_signals.SIG_UPDATE_MAXPRO_STATIONID,
                              sender=dispatcher.Any)
        super(ScreenStationID, self).deactivate()

class ScreenFrequency(Screen):
    def __init__(self):
        super(ScreenFrequency, self).__init__("edit frequency")

        self.add(Item(0, 0, "FREQUENCY"))

        self.int_item = ItemCycleList(6, 3, ui.display.CycleList.list_str_integer(87, 109, fmt="{:3d}"), selected=True)
        self.add(self.int_item)
        self.add(Item(9, 3, "."))
        self.frc_item = self.add(ItemCycleList(10, 3, ui.display.CycleList.list_char_one_to_zero()))
        self.add(Item(12, 3, "MHz"))

        self.add(ButtonOK())
        self.add(ButtonBack())
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, None)

    def activate(self):
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, None)
        dispatcher.connect(self.update_handler, pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, weak=False)

        int_part = None
        frc_part = None

        if self.value is not None:
            tmp = "{:5.1f}".format(self.value)
            int_part = tmp.split('.')[0]
            frc_part = tmp.split('.')[1]

        self.int_item.elements.activate(int_part)
        self.int_item.value = self.int_item.elements.active(True)

        self.frc_item.elements.activate(frc_part)
        self.frc_item.value = self.frc_item.elements.active(True)

        super(ScreenFrequency, self).activate()

    def btn_ok(self, new_screen):
        entered_value = float(self.int_item.value + '.' + self.frc_item.value)
        # current_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, None)

        # if entered_value != current_value:
        dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, pfm_signals.SENDER_PANEL, entered_value)

        self.select_start_item(False)
        self.panel.activate(new_screen)

    def update_handler(self, value):
        self.value = value
        int_part = None
        frc_part = None

        if self.value is not None:
            tmp = "{:5.1f}".format(self.value)
            int_part = tmp.split('.')[0]
            frc_part = tmp.split('.')[1]

        self.int_item.reset(int_part)
        self.frc_item.reset(frc_part)
        self.int_item.show()
        self.frc_item.show()


    def deactivate(self):
        dispatcher.disconnect(self.update_handler, signal=pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,
                              sender=dispatcher.Any)
        super(ScreenFrequency, self).deactivate()

__screens = dir()


def start(bboard):
    panel = ui.display.panel.Panel(bboard)

    for screen in __screens:
        if screen.__len__() > 6 and (screen[:6] == "screen" or screen[:6] == "Screen"):
            sstring = eval(screen)
            panel.add(sstring())
            logging.debug("screen added: '{}'".format(sstring))

    panel.add(ui.display.screens.ScreenNotImplemented())

    panel.set_lock_screen("lock", lock_time, 1)
    if lock_time > 0:
        panel.activate("lock")
    else:
        panel.activate()
