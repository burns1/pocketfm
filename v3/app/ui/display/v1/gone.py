class ScreenWifiClientSettingsOLD(Screen):
    def __init__(self):
        super(ScreenWifiClientSettingsOLD, self).__init__("wifi client mode settings OLD")

        self.add(Item(0, 0, "WIFI CLIENT MODE"))

        # self.add(Item(10, 1, "/\\"))
        #self.add(Item(0, 2, "SELECT NETWORK:"))
        self.add(Item(0,1,'>'))
        self.add(Item(20,1,'<'))
        self.wifis = ItemDynamicCycleList(1, 1, selected=True, fmt="{:19.19}")
        self.add(self.wifis)
        # self.add(Item(10, 3, "\\/"))

        self.no_network = self.add(Item(1, 1, " NO NETWORK AVAILABLE "))
        self.pw_text = self.add(Item(0, 2, "Password: "))
        self.pw_input = ItemCollectionText(self, 0, 3, chars_and_numbers, "                    ")

        self.add (ItemUpdatablePFM(0,5, "{:^21}", pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, None,
                                   "  No information :("))

        self.add(ButtonOK(new_screen="wifi select client"))
        self.add(ButtonBack())

    def activate(self):
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, None)

        networks = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, None)

        if networks is None:
            self.no_network.visible = True
            self.pw_text.visible = False
            self.pw_input.set_visibility(False)

        else:
            self.no_network.visible = False
            self.pw_text.visible = True
            self.pw_input.set_visibility(True)

            self.wifis.re_init(networks, None)

        super(ScreenWifiClientSettingsOLD, self).activate()

    def btn_ok(self, new_screen):
        entered_value = self.wifis.value

        logging.info ("set new wifi client conntection: '{}'; password: '{}'".format(entered_value, self.pw_input.get_input()))
        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, pfm_signals.SENDER_PANEL, entered_value)
        #dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, pfm_signals.SENDER_PANEL, self.pw_input.get_input())

        self.btn_right()
        #self.panel.activate(new_screen)
