from pydispatch import dispatcher

import pfm_signals
import ui.display.CycleList
import ui.display.screens
from ui.display.items import ButtonBack
from ui.display.items import Item
from ui.display.items import ItemCycleList
from ui.display.items import ItemUpdatablePFM
from ui.display.items import no_value_line
from ui.display.screens import Screen

output_power = ui.display.CycleList.list_str_integer(0, 101, fmt="{:3}")


class ItemPercent(ItemCycleList):
    def __init__(self, x, y, fmt="{}%", selected=True):
        super(ItemPercent, self).__init__(x, y, output_power, fmt=fmt, selected=selected)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_POWER_PERCENT, pfm_signals.SENDER_PANEL, int(self.value))
            self.screen.btn_right()  # go to the back button
            self.panel.inactivate_marked_text()


class ScreenPower(Screen):
    def __init__(self):
        super(ScreenPower, self).__init__("power")

        self.add(Item(0, 0, "TX POWER"))

        self.add(Item(1, 2, "Set 0-100%"))
        self.perc_item = ItemPercent(16, 2)
        self.add(self.perc_item)

        self.add(ItemUpdatablePFM(0, 3, " Output Power {:5.1f}W ", pfm_signals.SIG_UPDATE_MAXPRO_OUTPUT_POWER,
                                  str_no_value=no_value_line(" Output Power", length=19)))
        self.add(ItemUpdatablePFM(0, 4, " Reflection {:7.1f}W ", pfm_signals.SIG_UPDATE_MAXPRO_REFLECTED_POWER,
                                  str_no_value=no_value_line(" Reflection", length=19)))
        self.add(ItemUpdatablePFM(0, 5, " Temperature {:6d}C ", pfm_signals.SIG_UPDATE_MAXPRO_TEMPERATURE,
                                  str_no_value=no_value_line(" Temperature", length=19)))

        self.add(ButtonBack())
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, 0)

    def activate(self):
        self.value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, 0)   # todo: what if no value
        dispatcher.connect(self.update_handler, pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT, weak=False)
        self.perc_item.reset("{:3d}".format(int(self.value)))  #  int() because sometimes float was coming :(
        self.perc_item.show()
        super(ScreenPower, self).activate()

    def update_handler(self, value):
        self.value = value
        self.perc_item.reset("{:3d}".format(int(self.value)))
        self.perc_item.show()

    def deactivate(self):
        dispatcher.disconnect(self.update_handler, signal=pfm_signals.SIG_UPDATE_MAXPRO_POWER_PERCENT,
                              sender=dispatcher.Any)

        #disconnecting signals handled via ItemUpdatablePFM
        #handlers connected via ItemUpdateable... can not be disconnected
        #at this level
        super(ScreenPower, self).deactivate()
