from pydispatch import dispatcher

import pfm_signals
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import ButtonClear
from ui.display.items import Item
from ui.display.items import ItemInputText
from ui.display.items import ItemDynamicCycleList
from ui.display.items import ItemSelectable
from ui.display.items import ItemUpdatablePFM
from ui.display.screens import Screen
import ui.display.CycleList
import string

from main import numbers_zero2nine

class ItemAudioList(ItemDynamicCycleList):
    """
    The list with the audio input sources. T
    """
    def __init__(self):
        super(ItemAudioList, self).__init__(0, 2, selected=True, fmt=">{:^19.19}<")

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()
            dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, pfm_signals.SENDER_PANEL, self.value)
            self.panel.activate("audio dispatcher")


class ScreenAudioDispatcher(Screen):
    """
    Same concept as for the wifi dispatcher ...
    """
    def __init__(self):
        super(ScreenAudioDispatcher, self).__init__("audio dispatcher")
        self.cur_mode = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        dispatcher.connect(self.mode_changed_handler, pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, weak=False)

    def mode_changed_handler(self, value):
        if self.cur_mode != value:
            self.cur_mode = value
            if self.panel.current_screen.name.startswith("audio"):
                self.activate()

    def activate(self):

        if self.cur_mode == "ANALOG":
            self.panel.activate("audio analog status")
            return

        if self.cur_mode == "USB":
            self.panel.activate("audio usb status")
            return

        if self.cur_mode == "STREAM":
            self.panel.activate("audio stream status")
            return

        if self.cur_mode == "SATELLITE":
            self.panel.activate("audio satellite status")
            return

        if self.cur_mode == "INTERNAL":
            self.panel.activate("audio internal status")
            return

        self.panel.activate("UNKNOW MODE: {}".format(self.cur_mode))


class ScreenAudioSatelliteStatus(Screen):
    def __init__(self):
        super(ScreenAudioSatelliteStatus, self).__init__("audio satellite status")
        self.add(Item(0, 0, "AUDIO SOURCE"))
        self.audio = self.add(ItemAudioList())

        self.add(ItemUpdatablePFM(1, 3, "{:^19}", pfm_signals.SIG_UPDATE_SATELLITE_STATUS,
                                  str_no_value="  (No status info)"))
        self.add(ItemUpdatablePFM(0, 4, "    Level  {:>5d}%     ", pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_LEVEL,
                                  str_no_value="    Level    (None)"))
        self.add(ItemUpdatablePFM(0, 5, "    Quality{:>5d}%     ", pfm_signals.SIG_UPDATE_SATELLITE_SIGNAL_QUALITY,
                                  str_no_value="    Quality  (None)"))

        self.add(ItemSelectable(7, 7, "SETTINGS", new_screen="audio satellite settings"))
        self.add(ButtonBack())

    def activate(self):
        self.panel.clrscr()
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        # todo: if not Satellite?? Dispatch again?
        self.audio.re_init(pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST), current)
        super(ScreenAudioSatelliteStatus, self).activate()

    #ItemUpdatablePFMs needs to be disconnected in item level. Can not happen here.


class ScreenAudioSatelliteSettings(Screen):
    def __init__(self):
        super(ScreenAudioSatelliteSettings, self).__init__("audio satellite settings")

        self.add(Item(0, 0, "SATELLITE SETTINGS"))

        self.add(Item(1, 2, "Frequency"))
        self.freq = self.add(ItemInputText(15, 2, 5, 5, numbers_zero2nine))

        self.add(Item(1, 3, "Polarisation"))
        self.pol = self.add(ItemInputText(19, 3, 1, 1, ['H', 'V']))

        self.add(Item(1, 4, "Symbol Rate"))
        self.sym_rate = self.add(ItemInputText(15, 4, 5, 5, numbers_zero2nine))

        self.add(Item(1, 5, "Audio PID"))
        self.audio_pid = self.add(ItemInputText(16, 5, 1,4, numbers_zero2nine))

        self.add(ButtonOK(new_screen="audio dispatcher"))
        self.add(ButtonClear())
        self.add(ButtonBack(new_screen="audio dispatcher"))
        self.freq_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, 0)
        self.pol_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, 0)
        self.symbol_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE, 0)
        self.apid_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, 0)


    def activate(self):
        #frequency
        self.freq_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, 0)
        dispatcher.connect(self.freq_update_handler, pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY, weak=False)
        self.freq.reset(self.freq_value)
        self.freq.show()
        #polarization
        self.pol_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, 0)
        dispatcher.connect(self.pol_update_handler, pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION, weak=False)
        self.pol.reset(self.pol_value)
        self.pol.show()
        #symbolrate
        self.symbol_value = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE, 0)
        dispatcher.connect(self.sym_update_handler, pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE, weak=False)
        self.sym_rate.reset(self.symbol_value)
        self.sym_rate.show()
        #audio pid
        self.apid_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, 0)
        dispatcher.connect(self.apid_update_handler, pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID, weak=False)
        self.audio_pid.reset(self.apid_value)
        self.audio_pid.show()

        super(ScreenAudioSatelliteSettings, self).activate()

    def btn_ok(self, new_screen):
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_FREQUENCY, pfm_signals.SENDER_PANEL, self.freq.input)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_POLARISATION, pfm_signals.SENDER_PANEL, self.pol.input)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_SYMBOL_RATE, pfm_signals.SENDER_PANEL, self.sym_rate.input)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_AUDIO_PID, pfm_signals.SENDER_PANEL, self.audio_pid.input)
        dispatcher.send(pfm_signals.SIG_SET_SATELLITE_CHANNEL_CUSTOM_SETTINGS, pfm_signals.SENDER_PANEL, '')
        # #self.btn_right()

        self.panel.activate("audio dispatcher")

        #kick input again so the changes have any effect
        #like in wifi settings
        dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, pfm_signals.SENDER_PANEL, "SATELLITE")

    def btn_clear(self):
        self.freq.reset(None)
        self.pol.reset(None)
        self.sym_rate.reset(None)
        self.audio_pid.reset(None)
        self.select_start_item(True)

    #update handlers
    def freq_update_handler(self, value):
        self.freq_value = value
        self.freq.reset(self.freq_value)
        self.freq.show()

    def pol_update_handler(self, value):
        self.pol_value = value
        self.pol.reset(self.pol_value)
        self.pol.show()

    def sym_update_handler(self, value):
        self.symbol_value = value
        self.sym_rate.reset(self.symbol_value)
        self.sym_rate.show()

    def apid_update_handler(self, value):
        self.apid_value = value
        self.audio_pid.reset(self.apid_value)
        self.audio_pid.show()

    def deactivate(self):
        dispatcher.disconnect(self.freq_update_handler, signal=pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_FREQUENCY,
                              sender=dispatcher.Any)
        dispatcher.disconnect(self.pol_update_handler, signal=pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_POLARISATION,
                              sender=dispatcher.Any)
        dispatcher.disconnect(self.sym_update_handler, signal=pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_SYMBOL_RATE,
                              sender=dispatcher.Any)
        dispatcher.disconnect(self.apid_update_handler, signal=pfm_signals.SIG_UPDATE_SATELLITE_CHANNEL_AUDIO_PID,
                              sender=dispatcher.Any)
        super(ScreenAudioSatelliteSettings, self).deactivate()

class ScreenAudioAnalogStatus(Screen):
    def __init__(self):
        super(ScreenAudioAnalogStatus, self).__init__("audio analog status")
        self.add(Item(0, 0, "AUDIO SOURCE"))
        self.audio = self.add(ItemAudioList())

        self.add(ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS,
                                                      str_no_value="(Null)"))

        #self.add(ItemSelectable(7, 7, "SETTINGS", new_screen="audio analog settings"))
        self.add(ButtonBack())

    def activate(self):
        self.panel.clrscr()
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        self.audio.re_init(pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST), current)
        super(ScreenAudioAnalogStatus, self).activate()


class ScreenAudioUSBStatus(Screen):
    def __init__(self):
        super(ScreenAudioUSBStatus, self).__init__("audio usb status")
        self.add(Item(0, 0, "AUDIO SOURCE"))
        self.audio = self.add(ItemAudioList())

        self.add (ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS,
                                                      str_no_value="(Null)"))

        #self.add(ItemSelectable(7, 7, "SETTINGS", new_screen="audio usb settings"))
        self.add(ButtonBack())

    def activate(self):
        self.panel.clrscr()
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        self.audio.re_init(pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST), current)
        super(ScreenAudioUSBStatus, self).activate()


class ScreenAudioStreamStatus(Screen):
    def __init__(self):
        super(ScreenAudioStreamStatus, self).__init__("audio stream status")
        self.add(Item(0, 0, "AUDIO SOURCE"))
        self.audio = self.add(ItemAudioList())


        # self.add (ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL ,
        #                                               str_no_value="(Null)"))
        self.add (ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS,
                                   str_no_value="(Null)"))


        self.add(ItemSelectable(7, 7, "SETTINGS", new_screen="audio stream settings"))
        self.add(ButtonBack())

    def activate(self):
        self.panel.clrscr()
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        self.audio.re_init(pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST), current)
        super(ScreenAudioStreamStatus, self).activate()

class ScreenAudioInternalStatus(Screen):
    def __init__(self):
        super(ScreenAudioInternalStatus, self).__init__("audio internal status")
        self.add(Item(0, 0, "AUDIO SOURCE"))
        # self.add(Item(0, 2, ">"))
        self.audio = ItemAudioList()
        self.add(self.audio)
        # self.add(Item(20, 2, "<"))

        self.add (ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_STATUS,
                                                      str_no_value="(Null)"))

        # self.add(ItemSelectable(7, 7, "SETTINGS", new_screen="audio internal settings"))
        self.add(ButtonBack())

    def activate(self):
        self.panel.clrscr()
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, None)
        self.audio.re_init(pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT_LIST), current)
        super(ScreenAudioInternalStatus, self).activate()


class ScreenAudioInternalSettings(Screen):
    def __init__(self):
        super(ScreenAudioInternalSettings, self).__init__("audio internal settings")

        self.add(Item(0, 0, "INTERNAL SETTINGS"))
        self.add (ItemSelectable(1, 2, "Empty Internal"))
        self.add (ItemSelectable(1, 4, "Copy USB > Internal"))
        self.add (ItemSelectable(1, 5, "Copy Internal > USB"))

        self.add(ButtonBack(new_screen="audio dispatcher"))

class ScreenAudioStreamSettings(Screen):
    def __init__(self):
        super(ScreenAudioStreamSettings, self).__init__("audio stream settings")

        self.add(Item(0, 0, "STREAM SETTINGS"))

        letters = []

        letters.extend(list(string.ascii_lowercase))
        letters.append("-")
        letters.extend(ui.display.CycleList.list_char_one_to_zero())
        letters.append(".")

        self.add (Item(0, 1, "Domain/IP"))
        self.domain = self.add (ItemInputText (0,2,10,21, letters))

        self.add(Item(0, 3, "Port"))
        self.port = self.add (ItemInputText (0, 4, 1, 5, ui.display.CycleList.list_char_one_to_zero()))

        self.add(Item(0, 5, "Path"))
        self.path = self.add (ItemInputText (0,6,10,21))

        # self.add(Item(2, 2, "too smelly to work"))
        # self.add(Item(2, 3, "and too late"))

        self.add(ButtonOK(new_screen="audio dispatcher"))
        self.add(ButtonClear())
        self.add(ButtonBack(new_screen="audio dispatcher"))


    def activate(self):
        #current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_INTERNETSTREAM_URL, None)

        self.domain.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_INTERNETSTREAM_DOMAIN, None))
        self.port.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PORT, None))
        self.path.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_INTERNETSTREAM_PATH, None))

        super(ScreenAudioStreamSettings, self).activate()

    def btn_clear (self):
        self.domain.reset(None)
        self.port.reset(None)
        self.path.reset(None)
        self.select_start_item(True)

    def btn_ok(self, new_screen):

        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_DOMAIN, pfm_signals.SENDER_PANEL, self.domain.input)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_PORT, pfm_signals.SENDER_PANEL, self.port.input)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_PATH, pfm_signals.SENDER_PANEL, self.path.input)
        dispatcher.send(pfm_signals.SIG_SET_INTERNETSTREAM_URL, pfm_signals.SENDER_PANEL, None)

        #kick input again so the changes have any effect
        #like in wifi settings
        dispatcher.send(pfm_signals.SIG_SET_AUDIOMANAGER_INPUT, pfm_signals.SENDER_PANEL, "STREAM")


        self.panel.activate("audio dispatcher")



