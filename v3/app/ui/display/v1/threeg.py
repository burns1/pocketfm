from pydispatch import dispatcher
import logging
import pfm_signals
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import ButtonClear
from ui.display.items import Item
from ui.display.items import no_value_line
from ui.display.items import ItemInputText
from ui.display.items import ItemDynamicCycleList
from ui.display.items import ItemUpdatablePFM
from ui.display.items import ItemSelectable
from ui.display.screens import Screen

"""
Screens for mobile data (3g/GSM?) settings
Zsolt 11.1.2017
"""

list_val = None

class ItemModeList(ItemDynamicCycleList):
    """

    """
    def __init__(self, x=0, y=2, fmt=">{:^19.19}<"):
        super(ItemModeList, self).__init__(x, y, selected=True, fmt=fmt)

    def btn_pressed(self):
        global list_val
        list_val = self.value

        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()
            if self.value == "ON":
                dispatcher.send(pfm_signals.SIG_SET_3G_MODE, pfm_signals.SENDER_PANEL, self.value)
            elif self.value == "OFF":
                dispatcher.send(pfm_signals.SIG_SET_3G_MODE, pfm_signals.SENDER_PANEL, self.value)
            self.panel.activate("3g dispatcher")


class Screen3GDispatcher(Screen):
    """
    Zsolt, 11.1.2017
    Dispatches between active and non active 3G mode
    """
    def __init__(self):
        super(Screen3GDispatcher, self).__init__("3g dispatcher")
        self.cur_mode = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_MODE, None)
        dispatcher.connect(self.mode_changed_handler, pfm_signals.SIG_UPDATE_3G_MODE, weak=False)

    def mode_changed_handler(self, value):
        if self.cur_mode != value:
            self.cur_mode = value
            if self.panel.current_screen.name.startswith ("3g"):
                self.activate()

    def activate(self):
        global list_val
        # if (self.cur_mode == False or self.cur_mode == 'OFF') and (list_val != "ON"):
        if (self.cur_mode == False or self.cur_mode == 'OFF'):
            self.panel.activate("3g off status")
            return

        if (self.cur_mode == True or self.cur_mode == 'ON'):
            self.panel.activate("3g on status")
            return

        #introducing a special status when the device is reconfigured
        #and restarted
        if (self.cur_mode == False) and (list_val == "ON"):
            self.panel.activate("3g on status")
            return

        self.panel.activate("UNKNOW MODE: {}".format(self.cur_mode))


class Screen3GOffStatus(Screen):
    def __init__(self):
        super(Screen3GOffStatus, self).__init__("3g off status")
        self.add(Item(0, 0, "GSM DATA"))
        self.list = self.add(ItemModeList())
        self.add(ItemSelectable(6, 7, "SETTINGS", new_screen="3g settings"))
        self.add(ButtonBack())

    def activate(self):
        # self.panel.clrscr()
        #cur_val = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_ACTIVE, None)
        cur_list = ['ON', 'OFF']
        self.list.re_init(cur_list, 'OFF')
        super(Screen3GOffStatus, self).activate()

class Screen3GOnStatus(Screen):
    def __init__(self):
        super(Screen3GOnStatus, self).__init__("3g on status")
        self.add(Item(0, 0, "GSM DATA"))
        self.list = self.add(ItemModeList())
        self.add(ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_3G_STATUS,
                                  str_no_value="       (none)       "))
        # self.add(ItemSelectable(6, 7, "SETTINGS", new_screen="3g settings"))
        self.add(ButtonBack())

    def activate(self):
        # self.panel.clrscr()
        #cur_val = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_ACTIVE, None)
        cur_list = ['ON', 'OFF']
        self.list.re_init(cur_list, 'ON')
        super(Screen3GOnStatus, self).activate()


class Screen3GSettings(Screen):
    def __init__(self):
        super(Screen3GSettings, self).__init__("3g settings")

        self.add(Item(0, 0, "GSM DATA SETTINGS"))

        #self.add(ItemUpdatablePFM(0, 4, "APN: {:<13}", pfm_signals.SIG_UPDATE_3G_APN,
        #                          str_no_value=no_value_line("APN:")))
        self.add(Item(0, 2, "Dial No"))
        self.dialno_input = self.add(ItemInputText(9, 2, 1, 10))
        self.add(Item(0, 3, "APN"))
        self.apn_input = self.add(ItemInputText(9, 3, 1, 10))
        self.add(Item(0, 4, "User"))
        self.usr_input = self.add(ItemInputText(9, 4, 1, 10))
        self.add(Item(0, 5, "Password"))
        self.pw_input = self.add(ItemInputText(9, 5, 8, 10))

        # self.the_btn_ok =   # to name it btn_ok would strangely interfere with the method!
        self.add(ButtonOK(new_screen="3g dispatcher"))
        self.add(ButtonClear(fmt="RESET"))
        self.add(ButtonBack(new_screen="3g dispatcher"))


    def activate(self):
        self.pw_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_PASSWORD, None))
        self.apn_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_APN, None))
        self.usr_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_USERNAME, None))
        self.dialno_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_3G_DIALNUMBER, None))
        super(Screen3GSettings, self).activate()

    def btn_ok(self, new_screen):
        entered_pw = self.pw_input.input
        dispatcher.send(pfm_signals.SIG_SET_3G_PASSWORD, pfm_signals.SENDER_PANEL, entered_pw)
        dispatcher.send(pfm_signals.SIG_SET_3G_USERNAME, pfm_signals.SENDER_PANEL, self.usr_input.input)
        dispatcher.send(pfm_signals.SIG_SET_3G_APN, pfm_signals.SENDER_PANEL, self.apn_input.input)
        dispatcher.send(pfm_signals.SIG_SET_3G_DIALNUMBER, pfm_signals.SENDER_PANEL, self.dialno_input.input)
        dispatcher.send(pfm_signals.SIG_SET_3G_ACTIVE, pfm_signals.SENDER_PANEL, True)

        self.select_start_item(False)
        self.panel.activate(new_screen)

    def btn_clear(self):
        self.pw_input.reset(None)
        self.usr_input.reset(None)
        self.apn_input.reset("internet")
        self.dialno_input.reset("*99#")
        self.select_start_item(True)

