from pydispatch import dispatcher
import logging
import pfm_signals
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import ButtonClear
from ui.display.items import Item
from ui.display.items import no_value_line
from ui.display.items import ItemInputText
from ui.display.items import ItemDynamicCycleList
from ui.display.items import ItemUpdatablePFM
from ui.display.items import ItemSelectable
from ui.display.screens import Screen


"""
These classes are a bit tricky. Cau
"""

class ItemModeList(ItemDynamicCycleList):
    """

    """
    def __init__(self, x=0, y=2, fmt=">{:^19.19}<"):
        super(ItemModeList, self).__init__(x, y, selected=True, fmt=fmt)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            #print " - - BLINK  1 "
            self.edit = False
            self.panel.inactivate_marked_text()
            dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, pfm_signals.SENDER_PANEL, self.value)
            #print " - - BLINK  2 "
            self.panel.activate("wifi dispatcher")
            #print " - - BLINK  3 "


class ItemWifiClientNetworkList(ItemDynamicCycleList):
    """
    Created by stolle, 2016

    The list of networks to select from.

    """
    def __init__(self, x=0, y=2, fmt=">{:^19.19}<"):
        super(ItemWifiClientNetworkList, self).__init__(x, y, selected=True, fmt=fmt)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, pfm_signals.SENDER_PANEL, self.value)
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, pfm_signals.SENDER_PANEL, True)
            self.panel.activate("wifi client mode set network")
            # self.screen.btn_right()


class ScreenWifiDispatcher(Screen):
    """
    Created by stolle, 2016

    Very interesting screen - it will never screen anything. As the name suggests,
    it dispatches to different screens depending on the state of the network.

    It can do so also, when its not active at all, since a handler is reacting to
    each network change.

    """
    def __init__(self):
        super(ScreenWifiDispatcher, self).__init__("wifi dispatcher")
        self.cur_mode = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE, None)
        dispatcher.connect(self.mode_changed_handler, pfm_signals.SIG_UPDATE_WIFI_MODE, weak=False)

    def mode_changed_handler(self, value):
        if self.cur_mode != value:
            self.cur_mode = value
            if self.panel.current_screen.name.startswith ("wifi"):
                self.activate()  # todo: Check with Philipp and Andam if OK this way

    def activate(self):
        if self.cur_mode == "OFF":
            self.panel.activate("wifi off status")
            return

        if self.cur_mode == "ACCESS POINT":
            self.panel.activate("wifi access point status")
            return

        if self.cur_mode == "CLIENT MODE":
            self.panel.activate("wifi client mode status")
            return

        self.panel.activate("UNKNOW MODE: {}".format(self.cur_mode))


class ScreenWifiOffStatus(Screen):
    """

    """
    def __init__(self):
        super(ScreenWifiOffStatus, self).__init__("wifi off status")
        self.add(Item(0, 0, "WIFI"))
        self.list = self.add(ItemModeList())
        self.add(ButtonBack())

    def activate(self):
        # self.panel.clrscr()
        cur_val = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE, None)
        cur_list = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST,
                                                      ['ACCESS POINT', 'CLIENT MODE', 'OFF'])

        self.list.re_init(cur_list, cur_val)
        super(ScreenWifiOffStatus, self).activate()


class ScreenWifiAccessPointStatus(Screen):
    def __init__(self):
        super(ScreenWifiAccessPointStatus, self).__init__("wifi access point status")
        self.add(Item(0, 0, "WIFI"))
        self.list = self.add(ItemModeList())
        self.add(ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_WIFI_AP_STATUS,
                                  str_no_value="       (none)       "))
        self.add(ItemSelectable(6, 7, "SETTINGS", new_screen="wifi access point settings"))
        self.add(ButtonBack())

    def activate(self):
        # self.panel.clrscr()
        cur_val = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE, None)
        cur_list = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST,
                                                      ['ACCESS POINT', 'CLIENT MODE', 'OFF'])

        self.list.re_init(cur_list, cur_val)
        super(ScreenWifiAccessPointStatus, self).activate()


class ScreenWifiClientModeStatus(Screen):
    def __init__(self):
        super(ScreenWifiClientModeStatus, self).__init__("wifi client mode status")
        self.add(Item(0, 0, "WIFI"))
        self.list = self.add(ItemModeList())
        self.add(ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS,
                                  str_no_value="       (none)       "))
        self.add(ItemSelectable(6, 7, "SETTINGS", new_screen="wifi client mode set network"))
        self.add(ButtonBack())

    def activate(self):
        # self.panel.clrscr()
        cur_val = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE, None)
        cur_list = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_MODE_LIST,
                                                      ['ACCESS POINT', 'CLIENT MODE', 'OFF'])

        self.list.re_init(cur_list, cur_val)
        super(ScreenWifiClientModeStatus, self).activate()


class ScreenWifiAccessPointSettings(Screen):
    def __init__(self):
        super(ScreenWifiAccessPointSettings, self).__init__("wifi access point settings")

        self.add(Item(0, 0, "ACCESS POINT PASSWORD"))

        self.add(ItemUpdatablePFM(0, 2, "SSID: {:<13}", pfm_signals.SIG_UPDATE_WIFI_AP_NETWORK,
                                  str_no_value=no_value_line("SSID:")))

        self.add(Item(0, 4, "Password (8+ char)"))
        self.pw_input = self.add(ItemInputText(0, 5, 8, 21))

        # self.the_btn_ok =   # to name it btn_ok would strangely interfere with the method!
        self.add(ButtonOK(new_screen="wifi dispatcher"))
        self.add(ButtonClear(fmt="RESET"))
        self.add(ButtonBack(new_screen="wifi dispatcher"))


    def activate(self):
        self.pw_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AP_PASSWORD, None))
        super(ScreenWifiAccessPointSettings, self).activate()

    def btn_ok(self, new_screen):
        entered_pw = self.pw_input.input
        dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, pfm_signals.SENDER_PANEL, entered_pw)
        # dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, pfm_signals.SENDER_PANEL, True)
        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, pfm_signals.SENDER_PANEL, "ACCESS POINT")

        self.select_start_item(False)
        self.panel.activate(new_screen)

    def btn_clear(self):
        self.pw_input.reset("12345678")
        self.select_start_item(True)


####################################################################### EXPERIMENTAL New Input ...



class ScreenWifiClientModeSetNetwork(Screen):
    def __init__(self):
        super(ScreenWifiClientModeSetNetwork, self).__init__("wifi client mode set network")
        self.add(Item(0, 0, "CLIENT MODE SETTINGS"))
        self.wifis = self.add(ItemWifiClientNetworkList())
        self.no_network = self.add(Item(1, 1, " NO NETWORK AVAILABLE "))

        self.add(ItemUpdatablePFM(0, 4, "{:^21.21}", pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS,
                                  str_no_value="   no signal yet"))

        self.add(ItemSelectable(6, 7, "PASSWORD", new_screen="wifi client mode set password"))
        self.add(ButtonBack(new_screen="wifi dispatcher"))

    def activate(self):
        current = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, None)
        networks = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, None)

        if networks is None or len(networks) == 0:
            self.no_network.visible = True
        else:
            self.no_network.visible = False
            # self.pw_text.visible = True
            # self.pw_input.visible = True
            self.wifis.re_init(networks, current)
            # self.pw_input.reset(None)

        super(ScreenWifiClientModeSetNetwork, self).activate()


class ScreenWifiClientModeSetPassword(Screen):
    def __init__(self):
        super(ScreenWifiClientModeSetPassword, self).__init__("wifi client mode set password")

        self.add(Item(0, 0, "CLIENT MODE PASSWORD"))

        # self.add(Item(0, 2, "Network"))
        self.add(ItemUpdatablePFM(0, 2, "Network: {:11.11}", pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK,
                                  str_no_value=no_value_line("Network:")))

        self.add(Item(0, 4, "Password (8+ char)"))
        self.pw_input = self.add(ItemInputText(0, 5, 8, 21))

        self.add(ButtonOK(new_screen="wifi dispatcher"))
        self.add(ButtonClear())
        self.add(ButtonBack(new_screen="wifi dispatcher"))

    def activate(self):
        self.pw_input.reset(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_CLIENT_PASSWORD, None))
        # if networks is None or len(networks) == 0:
        #     self.no_network.visible = True
        #     self.pw_text.visible = False
        #     self.pw_input.set_visible = False
        #
        # else:
        #     self.no_network.visible = False
        #     self.pw_text.visible = True
        #     self.pw_input.visible = True
        #     self.wifis.re_init(networks, current)
        #     self.pw_input.reset(None)

        super(ScreenWifiClientModeSetPassword, self).activate()

    def btn_ok(self, new_screen):
        entered_value = self.pw_input.input

        # logging.info("set new wifi client conntection: '{}'; password: '{}'".format(entered_value, self.pw_input.input))

        # dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, pfm_signals.SENDER_PANEL, entered_value)
        # if len(self.pw_input.input) > 0:
        print "send PW", self.pw_input.input
        dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_PASSWORD, pfm_signals.SENDER_PANEL, self.pw_input.input)
        #self.btn_right()
        dispatcher.send(pfm_signals.SIG_SET_WIFI_MODE, pfm_signals.SENDER_PANEL, "CLIENT MODE")
        self.panel.activate(new_screen)

    def btn_clear(self):
        self.pw_input.reset(None)
        self.select_start_item(True)


################################
#
#  GONE
#

# class ItemDeactivate(ItemSelectable):
#     def __init__(self, x, y, fmt, selected=False):
#         super(ItemDeactivate, self).__init__(x, y, fmt, value=None, str_no_value=None, selected=selected)
#
#     def btn_pressed(self):
#         dispatcher.send(pfm_signals.SIG_SET_WIFI_AP_ACTIVE, pfm_signals.SENDER_PANEL, False)
#         dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_ACTIVE, pfm_signals.SENDER_PANEL, False)
#         self.panel.activate(SCREEN_DEFAULT)
