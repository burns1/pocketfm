from pydispatch import dispatcher
from periodic import PeriodicThread
import time

import pfm_signals
from ui.display.items import ButtonBack
from ui.display.items import Item
from ui.display.items import ItemUpdatable
from ui.display.items import ItemUpdatablePFM
from ui.display.screens import Screen

def clock_signal():
    dispatcher.send("INTSIG_TIME", "INTERN", time.strftime("%Y/%m/%d  %H:%M UTC"))

t = PeriodicThread(30, clock_signal)
t.start()

def _merge_numbers(data, signal, sender):
    s = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_SOFTWARE_VERSION, -1)
    b = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_BOOTLOADER_VERSION, -1)
    h = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_BREAKOUTBOARD_HARDWARE_VERSION, -1)
    tmp = "{}/{}/{}".format(s, b, h)
    dispatcher.send("INTSIG_HW", "INTERN", tmp)
    return tmp

dispatcher.connect(_merge_numbers, pfm_signals.SIG_UPDATE_BREAKOUTBOARD_SOFTWARE_VERSION, weak=False)
dispatcher.connect(_merge_numbers, pfm_signals.SIG_UPDATE_BREAKOUTBOARD_BOOTLOADER_VERSION, weak=False)
dispatcher.connect(_merge_numbers, pfm_signals.SIG_UPDATE_BREAKOUTBOARD_HARDWARE_VERSION, weak=False)

def screen_info():
    screen = Screen ("info")
    screen.add(Item(0, 0, "SYSTEM INFORMATION"))
    screen.add(ItemUpdatable(0, 1, "{}", "INTSIG_TIME", " (awaiting time) "))
    screen.add(ItemUpdatablePFM(0, 2, "SN  {}", pfm_signals.SIG_UPDATE_PFM_HOSTNAME, "(none)"))
    screen.add(ItemUpdatablePFM(0, 3, "SW  {}", pfm_signals.SIG_UPDATE_PFM_SOFTWARE_VERSION, "(none)"))
    screen.add(ItemUpdatable(0, 4, "HW  {:<17}", "INTSIG_HW", _merge_numbers(None, None, None)))
    screen.add(ItemUpdatablePFM(0, 5, "IP  {}", pfm_signals.SIG_UPDATE_WIFI_IP_ADDRESS, "(none)"))
    screen.add(ItemUpdatablePFM(0, 6, "GPS {:<17.17}", pfm_signals.SIG_UPDATE_GPS_POSITION,
                                str_no_value="GPS (no data)"))
    screen.add(ButtonBack())
    return screen
