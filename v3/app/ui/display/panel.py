from __future__ import print_function
from pydispatch import dispatcher
from periodic import PeriodicThread
import time
import logging

import pfm_signals
import pfm_config

from items import ButtonBack
from items import Item
import screens

# todo: this doesn really belong here. But where else??

def handle_pin (data, signal, sender):
    dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, "handle_pin", data)

def handle_lock (data, signal, sender):
    dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_TIMEOUT, "handle_lock", data)

dispatcher.connect(handle_pin, pfm_signals.SIG_SET_PFM_LOCK_PIN)
dispatcher.connect(handle_lock, pfm_signals.SIG_SET_PFM_LOCK_TIMEOUT)

pin = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_PFM_LOCK_PIN, "000000")
lock = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_PFM_LOCK_TIMEOUT, 0)

#darkmode = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_DARK_MODE, False)

dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, pfm_signals.SENDER_PANEL, pin)
dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_TIMEOUT, pfm_signals.SENDER_PANEL, lock)


reset_screen = screens.Screen ("reset to default screen")
reset_screen.add (Item(2, 2 , "The pfm was reset"))
reset_screen.add (Item(2, 4,  "to default"))
reset_screen.add (ButtonBack())


class ScreenReset(screens.Screen):
    def __init__(self):
        super(ScreenReset, self).__init__("reset to default screen")
        self.add(Item(2, 2, "The pfm was reset"))
        self.add(Item(2, 4, "to default"))

    def activate(self):
        super(ScreenReset, self).activate()
        time.sleep(3)
        self.panel.activate(screens.SCREEN_DEFAULT)


reset_screen2 = ScreenReset()


class Panel(object):
    def __init__(self, bboard, connect_dispatcher=True,
                 btn_pressed=pfm_signals.SIG_ENCODER_BUTTON_PRESSED,
                 btn_released=pfm_signals.SIG_ENCODER_BUTTON_RELEASED,
                 btn_left=pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_LEFT,
                 btn_right=pfm_signals.SIG_ENCODER_ROTATOR_ANALOG_RIGHT):

        self.bboard = bboard         # will be propagated to screens and items!
        self.current_screen = None   # the actual screen object
        self.screens = {}
        self.error_screens = {}
        self.activated = False  # mainly for the encoder signals

        self.lock_screen = None  # should be used heavily to ensure, that nobody can do anything with a locked display
        self.dark_screen = None
        self.lock_time   = 0
        self.wrong_pin = 0

        self.button_pressed_time = 0
        self.button_pressed = False
        self.last_action_time = time.time()
        #self.rotated_fast = False

        self.add(reset_screen2)

        if connect_dispatcher:
            dispatcher.connect(self.btn_pressed_handler, signal=btn_pressed, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_released_handler, signal=btn_released, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_left_handler, signal=btn_left, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_right_handler, signal=btn_right, sender=dispatcher.Any, weak=False)

    def add(self, screen):
        #if screen.name in self.screens:
        #    raise Exception("screen name is already taken")

        # for the case, items were added before the screen was added to the display
        screen.panel = self
        for item in screen.items:  # Yes, I dont like it either
            item.panel = self

        self.screens.update({screen.name: screen})
        return screen

    def activate(self, name=screens.SCREEN_DEFAULT, clrscr=True):
        """Sets current screen to name and activates it
            VERY IMPORTANT: Before that, it deactivates the current screen!
        """

        if clrscr:
            self.clrscr()

        back_screen = screens.SCREEN_DEFAULT  # just in case, we have to go back
        if self.current_screen is not None:
            back_screen = self.current_screen.name
            logging.debug ("DEACTIVATE: {}".format(back_screen))
            self.current_screen.deactivate()

        logging.debug("ACTIVATE: {}".format(name))


        self.current_screen = self.screens.get(name)

        if self.current_screen is None:
            logging.warn ("Don't have screen: '{}' -> build it!".format(name))
            no_screen = self.add(screens.Screen(name))
            no_screen.add(Item(0, 0, "ERROR"))
            no_screen.add(Item(1, 2, "no screen named:"))
            no_screen.add(Item(2, 4, name))
            no_screen.add(ButtonBack(new_screen=back_screen))
            self.current_screen = no_screen
            self.error_screens.update({name: 0})

        errors = self.error_screens.get(name)
        if errors is not None:
            self.error_screens.update({name: errors + 1})
            logging.warn("ERROR SCREEN '{}' ACTIVATED {} TIMES".format(name, errors+1))

        self.current_screen.activate()
        self.activated = True  # from now on, we accept the signals

    def set_lock_screen(self, lock_screen, lock_time, check_time=5):
        #if not self.screens.has_key(lock_screen):
        #    raise Exception("Lock screen does not exist:", lock_screen)

        self.lock_screen = self.screens.get(lock_screen)
        self.dark_screen = self.screens.get("dark mode")

        if lock_time > 0:
            locker = Locker(self, lock_time)
            t = PeriodicThread(check_time, locker.check)
            t.start()

    def btn_left_handler(self, data, signal, sender):
        self.last_action_time = time.time()
        if not self.activated:
            logging.info("receiving '{}' from '{}' before panel activated!".format(signal, sender))
            return


        # if self.button_pressed:
        #     self.rotated_fast = True
        #     self.current_screen.btn_left(3)
        # else:
        self.current_screen.btn_left()

    def btn_right_handler(self, data, signal, sender):
        self.last_action_time = time.time()
        if not self.activated:
            logging.info("receiving '{}' from '{}' before panel activated!".format(signal, sender))
            return

        # if self.button_pressed:
        #     self.rotated_fast = True
        #     self.current_screen.btn_right(3)
        # else:
        self.current_screen.btn_right()


    def btn_pressed_handler(self, data, signal, sender):
        self.last_action_time = time.time()
        #if self.button_pressed:
        #    raise Exception("btn_pressed true while getting btn pressed signal")

        self.button_pressed = True
        self.button_pressed_time = time.time()

        if not self.activated:
            logging.info("receiving '{}' from '{}' before panel activated!".format(signal, sender))
            return

        #self.current_screen.btn_pressed()

    def btn_released_handler(self, data, signal, sender):
        self.last_action_time = time.time()
        #if not self.button_pressed:
        #    raise Exception("btn_pressed false while releasing ...")
        self.button_pressed = False

        # if self.rotated_fast:
        #     self.rotated_fast = False
        #     return

        # if time.time() - self.button_pressed_time > 25:
        #     pfm_config.reset_to_master()  # todo: nicer!!
        #     self.activate("reset to default screen")
        #     return

        # if time.time() - self.button_pressed_time > 2:
        #     pfm_signals.print_stats(False)  # todo: delete
        #     self.print_stats()
        #     return

        if not self.activated:
            logging.info("receiving '{}' from '{}' before panel activated!".format(signal, sender))
            return

        self.current_screen.btn_pressed()
        #self.current_screen.btn_released()

    def print_stats(self):
        for screen in self.screens:
            print ("**", screen, self.screens.get(screen).activation_count)

    # These functions should be moved to Display module -> Adam
    XPixelsPerChar = 6  # This should be moved along with helper functions into Display module
    YPixelsPerChar = 8  # This should be moved along with helper functions into Display module

    # text with the assumption, normal mode is on
    def print_normal(self, x, y, text, force_normal_mode=False):
        if force_normal_mode:
            self.bboard.writeBB("\x1b{}X\x1b{}Y\x04\x0E{}".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))
        else:
            self.bboard.writeBB("\x1b{}X\x1b{}Y{}".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))

            # inversed text, switches to normal after

    def print_inverse(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x0F{}\x0E".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))

        # underlined text, disables underlining after

    def print_underlined(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x12{}\x14".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))

    def print_blink(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x01{}\x04".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))

    def print_blinki(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x01\x0F{}\x04\x0E".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))

    # underlined invers - works but makes no sense, cause barely visible
    def print_ui(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x0F\x12{}\x14\x0E".format(x * self.XPixelsPerChar, y*self.YPixelsPerChar, text))
        #self.bboard.writeBB("\x01{}X\x1b{}Y\x0F\x12{}\x14\x0E".format(x * self.XPixelsPerChar, y, text))

    def inactivate_marked_text(self):
        self.bboard.writeBB("\x01")


        # clears the screen,

    def clrscr(self, force_normal_text=False):
        if force_normal_text:
            self.bboard.writeBB("\x0C\x0E\x14")
        else:
            self.bboard.writeBB("\x0C")


class Locker(object):
    def __init__(self, panel, lock_time):
        self.panel = panel
        self.lock_time = lock_time

    def check(self):
        if time.time() - self.panel.last_action_time > self.lock_time:
            if (self.panel.current_screen != self.panel.lock_screen) and (self.panel.current_screen != self.panel.dark_screen):
                if pfm_signals.get_value(pfm_signals.SIG_UPDATE_PFM_DARK_MODE) == True:
                    #self.panel.last_action_time = time.time()
                    self.panel.activate("dark mode")
                else:
                    self.panel.activate(self.panel.lock_screen.name)
        # else:
            # logging.debug("lock_time: {}  ->  {:.1f} seconds till screen lock".format(self.lock_time, self.lock_time - (time.time() - self.panel.last_action_time)))
