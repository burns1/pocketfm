from __future__ import print_function
from pydispatch import dispatcher
import time

import pfm_signals

from items import ButtonBack
from items import Item
import screens


class Panel_Simple(object):
    def __init__(self, bboard, connect_dispatcher=True,
                 btn_pressed=pfm_signals.SIG_ENCODER_BUTTON_PRESSED,
                 btn_released=pfm_signals.SIG_ENCODER_BUTTON_RELEASED,
                 btn_left=pfm_signals.SIG_ENCODER_ROTATOR_LEFT,
                 btn_right=pfm_signals.SIG_ENCODER_ROTATOR_RIGHT):

        self.bboard = bboard  # will be propagated to screens and items!
        self.current_screen = None
        self.screens = {}
        self.error_screens = {}
        self.activated = False  # mainly for the encoder signals

        self.locked = False  # should be used heavily to ensure, that nobody can do anything with a locked display
        self.pin = "100000"  # todo: move somewehre else
        self.wrong_pin = 0

        self.button_pressed_time = 0

        if connect_dispatcher:
            dispatcher.connect(self.btn_pressed_handler, signal=btn_pressed, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_released_handler, signal=btn_released, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_left_handler, signal=btn_left, sender=dispatcher.Any, weak=False)
            dispatcher.connect(self.btn_right_handler, signal=btn_right, sender=dispatcher.Any, weak=False)

    def add(self, screen):
        if screen.name in self.screens:
            raise Exception("screen name is already taken")

        # for the case, items were added before the screen was added to the display
        screen.panel = self
        for item in screen.items:  # Yes, I dont like it either
            item.panel = self

        self.screens.update({screen.name: screen})
        return screen

    def activate(self, name=screens.SCREEN_DEFAULT, clrscr=True):
        """Sets current screen to name and activates it
            VERY IMPORTANT: Before that, it deactivates the current screen!
        """
        if clrscr:
            self.clrscr()

        back_screen = screens.SCREEN_DEFAULT  # just in case, we have to go back
        if self.current_screen is not None:
            back_screen = self.current_screen.name
            self.current_screen.deactivate()

        self.current_screen = self.screens.get(name)
        if self.current_screen is None:
            print("Dont have: ", name, "build it!")
            no_screen = self.add(screens.Screen(name))
            no_screen.add(Item(0, 0, "ERROR"))
            no_screen.add(Item(1, 2, "no screen named:"))
            no_screen.add(Item(2, 4, name))
            no_screen.add(ButtonBack(new_screen=back_screen))
            self.current_screen = no_screen
            self.error_screens.update({name: 0})

        errors = self.error_screens.get(name)
        if errors is not None:
            self.error_screens.update({name: errors + 1})
            print("ERROR SCREEN >", name, "< OCCURED", errors + 1, "TIMES")

        self.current_screen.activate()
        self.activated = True  # from now on, we accept the signals

    def btn_left_handler(self, data, signal, sender):
        if self.activated:
            self.current_screen.btn_left()
        else:
            print("receiving before activating:", signal, sender)

    def btn_right_handler(self, data, signal, sender):
        if self.activated:
            self.current_screen.btn_right()
        else:
            print("receiving before activating:", signal, sender)

    def btn_pressed_handler(self, data, signal, sender):
        self.button_pressed_time = time.time()
        if self.activated:
            self.current_screen.btn_pressed()
        else:
            print("receiving before activating:", signal, sender)

    def btn_released_handler(self, data, signal, sender):
        if time.time() - self.button_pressed_time > 2:
            pfm_signals.print_stats()  # todo: nicer!!

        if self.activated:
            # self.current_screen.btn_released()
            pass
        else:
            print("receiving before activating:", signal, sender)

    # These functions should be moved to Display module -> Adam

    XPixelsPerChar = 6 # This should be moved along with helper functions into Display module

    # text with the assumption, normal mode is on
    def print_normal(self, x, y, text, force_normal_mode=False):
        if force_normal_mode:
            self.bboard.writeBB("\x1b{}X\x1b{}Y\x0E{}".format(x * self.XPixelsPerChar, y, text))
        else:
            self.bboard.writeBB("\x1b{}X\x1b{}Y{}".format(x * self.XPixelsPerChar, y, text))

            # inversed text, switches to normal after

    def print_inverse(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x0F{}\x0E".format(x * self.XPixelsPerChar, y, text))

        # underlined text, disables underlining after

    def print_underlined(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x12{}\x14".format(x * self.XPixelsPerChar, y, text))

        # underlined invers - works but makes no sense, cause barely visible

    def print_ui(self, x, y, text):
        self.bboard.writeBB("\x1b{}X\x1b{}Y\x0F\x12{}\x14\x0E".format(x * self.XPixelsPerChar, y, text))

        # clears the screen,

    def clrscr(self, force_normal_text=False):
        if force_normal_text:
            self.bboard.writeBB("\x0C\x0E\x14")
        else:
            self.bboard.writeBB("\x0C")
