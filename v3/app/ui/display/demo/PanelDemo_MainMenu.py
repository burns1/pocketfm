import SimpleStatus
from ui.display.items import *
from ui.display.panel import Panel
from ui.display.screens import *


##############################################################################
#
#  simple buttons
#
##############################################################################
class Button2MainMenu(ItemSelectable):
    def __init__(self, selected, x=0, y=7):
        super(Button2MainMenu, self).__init__(x, y, fmt="MAIN MENU", value=None, selected=selected)

    def btn_pressed(self):
        self.panel.activate(screens.SCREEN_DEFAULT)


##############################################################################
#
#  complicated items
#
##############################################################################


class ItemOptions(ItemSelectable):
    def __init__(self, x, y, fmt, selected=False):
        super(ItemOptions, self).__init__(x, y, fmt, value=" apples   *1 ", selected=selected)

        self.elements = CyclicList()
        self.elements.add(" apples   *1 ", True)
        self.elements.add(" pears    *2 ", False)
        self.elements.add(" oranges  *3 ", False)
        self.elements.add(" durian   *4 ", False)
        self.elements.add(" cherries *5 ", False)

    def btn_pressed(self):
        print " ITEM: btn_pressed"
        if self.edit is False:
            self.edit = True
        else:
            self.edit = False
            # todo that the place to return the value somewhere
        self.show()

    def btn_right(self):
        print " ITEM: btn_right"
        if self.edit:
            self.value = self.elements.__nxt()
            self.show()

    def btn_left(self):
        print " ITEM: btn_left"
        if self.edit:
            self.value = self.elements.__prv()
            self.show()


##############################################################################
#
#           SCREENS
#
##############################################################################
def build_main_menu():
    screen = Screen()
    screen.add(Item(1, 0, "**** Main Menu ****"))
    screen.add(Button2Screen(1, 1, "Status Monitor", "simple status", True))
    screen.add(Button2NotImplemented (1, 3, "Set frequenzy      "))
    screen.add(Button2NotImplemented(1, 4, "Get GPS data       "))
    screen.add(Button2Screen(1, 5, "Values ALPHA      ", "frq devel"))
    screen.add(Button2Screen(1, 7, "Power OFF", "sure"))
    screen.add(Button2NotImplemented(16, 7, "LOCK"))
    return screen


def build_not_implemented():
    screen = Screen("not implemented")
    screen.add(Item(1, 2, "Sorry, not done yet"))
    screen.add(Item(2, 4, "Maybe tomorrow ..", ))
    screen.add(Button2MainMenu(True))
    return screen


def build_serious():
    screen = Screen("serious")
    screen.add(Item(1, 2, "YOU CANT BE SERIOUS"))
    screen.add(Item(0, 4, "  I dont power OFF! "))
    screen.add(Button2MainMenu(True))
    return screen

def build_sure_base():
    screen = Screen("sure")
    screen.add(Item(2, 2, " ARE YOU SURE??"))
    screen.add(Button2Screen(3, 6, "No", screens.SCREEN_DEFAULT, True))
    screen.add(Button2Screen(10, 6, "Yes", "serious"))
    return screen


def add_frq_devel(panel):
    screen = panel.add(Screen("frq devel"))
    screen.add(Item(2, 0, " Pls chose values:"))
    screen.add(Item(1, 2, "Frq:"))
    screen.add(ItemFrq(6, 2, " {:3.1f} Mhz   ", False))
    screen.add(Item(1, 4, "Frt:"))
    screen.add(ItemOptions(6, 4, "{}", False))
    screen.add(Button2Screen(3, 6, "OK", "serious", True))
    screen.add(Button2Screen(10, 6, "CANCEL", screens.SCREEN_DEFAULT))


def start(bboard):

    panel = Panel(bboard)

    panel.add(build_main_menu())  #  DEFAULT Screen

    panel.add(ScreenNotImplemented())
    panel.add(SimpleStatus.SimpleStatus("simple status", True))
    panel.add(build_serious())
    panel.add(build_sure_base())
    add_frq_devel(panel)

    panel.activate()
