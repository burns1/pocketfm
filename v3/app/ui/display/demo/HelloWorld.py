from ui.display.screens import Screen
from ui.display.panel import Panel
from ui.display.items import Item

def start(bboard):

    hello = Screen()
    hello.add(Item(3, 3, "Hello World"))

    panel = Panel(bboard)
    panel.add(hello)
    panel.activate("Hello World")

