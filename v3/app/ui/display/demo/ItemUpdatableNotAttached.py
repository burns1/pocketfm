from __future__ import print_function
from periodic import PeriodicThread
from pydispatch import dispatcher
import time


from ui.display.items import ItemUpdatablePFM
from ui.display.items import ButtonBack
from ui.display.screens import Screen
from ui.display.screens import SCREEN_DEFAULT
from ui.display.panel import Panel
import pfm_signals


def set_none():
    dispatcher.send(pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE, "Test", None)


class SimpleStatus(Screen):
    def __init__(self, name=SCREEN_DEFAULT, backbtn=False):
        super(SimpleStatus, self).__init__(name)
        self.add(ItemUpdatablePFM(0, 0, "Station ID   {:>8}", pfm_signals.SIG_UPDATE_MAXPRO_STATIONID,
                                  str_no_value="Station ID  <not set>", selectable=True))

        self.add(ItemUpdatablePFM(0, 1, "Frequency    {:3.1f}MHz", pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,
                                  str_no_value="Frequency   <not set>", selectable=True))


        input_voltage=ItemUpdatablePFM(0, 2, "Input        {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE,
                                       str_no_value="Input       <not set>", selectable=True)

        time.sleep(1)  # without, I cant get the the error, no matter how many signals I send

        self.add(input_voltage)

        self.add(ItemUpdatablePFM(0, 3, "RPi          {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_RASPBERRYPI_VOLTAGE,
                                  str_no_value="RPi         <not set>", selectable=True))
        self.add(ItemUpdatablePFM(0, 4, "Transmitter  {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_TRANSMITTER_VOLTAGE,
                                  str_no_value="Transm.     <not set>", selectable=True))
        self.add(ItemUpdatablePFM(0, 5, "SIM{:>18}", pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME,
                                  str_no_value="SIM         <not set>", selectable=True))
        self.add(ItemUpdatablePFM(0, 6, "GPS time{:>13}", pfm_signals.SIG_UPDATE_GPS_TIME,
                                  str_no_value="GPS time    <not set>", selectable=True))

        if backbtn:
            self.add(ButtonBack())
        else:
            self.add(ItemUpdatablePFM(0, 7, "GPS{:>18}", pfm_signals.SIG_UPDATE_GPS_POSTION,
                                      str_no_value="GPS         <not set>", selectable=True))


def start(bboard):
    timer = PeriodicThread(1, set_none)
    timer.start()

    panel = Panel(bboard)
    panel.add(SimpleStatus())
    panel.activate()

