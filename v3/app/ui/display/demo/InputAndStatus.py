from pydispatch import dispatcher

import pfm_signals
import ui.display.CycleList
import ui.display.panel
import ui.display.screens
from ui.display.items import Button2Screen
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import Item
from ui.display.items import ItemCollectionText
from ui.display.items import ItemCycleList
from ui.display.items import ItemDynamicCycleList
from ui.display.items import ItemSelectable
from ui.display.items import ItemUpdatablePFM
from ui.display.screens import Screen

chars_station_id = ui.display.CycleList.default_letter_list()
numbers_zero2nine = ui.display.CycleList.list_str_integer(0, 10)
numbers_frq_int = ui.display.CycleList.list_str_integer(87, 109, fmt="{:3d}")

##############################################################################
#
#           constructed screens
#
##############################################################################
def consruct_status_screen():
    screen = Screen()
    screen.add(ItemUpdatablePFM(0, 0, "Station ID   {:>8}", pfm_signals.SIG_UPDATE_MAXPRO_STATIONID,
                                str_no_value="Station ID  <not set>",
                                selectable=True, new_screen="edit station id"))

    screen.add(ItemUpdatablePFM(0, 1, "Frequency    {:5.1f}MHz", pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,
                                str_no_value="Frequency   <not set>",
                                selectable=True, new_screen="edit frequency"))

    screen.add(ItemUpdatablePFM(0, 2, "Input        {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE,
                                str_no_value="Input       <not set>", selectable=True))
    screen.add(ItemUpdatablePFM(0, 3, "RPi          {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_RASPBERRYPI_VOLTAGE,
                                str_no_value="RPI         <not set>", selectable=True))

    screen.add(ItemUpdatablePFM(0, 4, "WIFI  {:7.3f}V", pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK,
                                str_no_value="WIFI        <not set>", selectable=True, new_screen="edit wifi"))
    screen.add(ItemUpdatablePFM(0, 5, "SIM{:>18}", pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME,
                                str_no_value="SIM         <not set>", selectable=True))
    screen.add(ItemUpdatablePFM(0, 6, "GPS time{:>13}", pfm_signals.SIG_UPDATE_GPS_TIME,
                                str_no_value="GPS time    <not set>", selectable=False))
    screen.add(Button2Screen(0, 7, "LOCK SCREEN", "lock screen"))
    return screen


def construct_lock_screen():
    screen = Screen("lock screen")
    screen.add(Item(0, 0, "LOCK SCREEN"))
    screen.add(Button2Screen(5, 2, "Lock Screen", "lock", True))
    screen.add(Button2Screen(5, 4, "Change PIN", "change pin"))
    screen.add(ButtonBack())
    return screen


##############################################################################
#
#           Screen subclasses
#
##############################################################################
class ScreenStationID(Screen):
    def __init__(self):
        super(ScreenStationID, self).__init__("edit station id")
        self.station_id_letters = ItemCollectionText(self, 6, 4, chars_station_id, "abcdefgh", True)
        self.add(Item(0, 0, "STATION ID"))
        self.add(Item(1, 2, "set max. 8 letters"))
        self.add(ButtonOK())
        self.add(ButtonBack())

    def btn_ok(self, new_screen):
        entered_value = self.station_id_letters.get_input()
        current_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, None)

        if current_value != entered_value:
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_STATIONID, pfm_signals.SENDER_PANEL, entered_value)

        self.select_start_item(False)
        self.panel.activate(new_screen)

    def activate(self):
        value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_STATIONID, "        ")  # todo: if none: screen not available or so
        while value.__len__() < 8:  # todo: this handling needs to be refined/discussed with adam
            value += ' '
        value = value[:8]
        self.station_id_letters.reset(value)

        super(ScreenStationID, self).activate()


class ScreenFrequency(Screen):
    def __init__(self):
        super(ScreenFrequency, self).__init__("edit frequency")

        self.add(Item(0, 0, "FREQUENCY"))

        self.int_item = ItemCycleList(6, 3, numbers_frq_int, selected=True)
        self.add(self.int_item)
        self.add(Item(9, 3, "."))
        self.frc_item = self.add(ItemCycleList(10, 3, numbers_zero2nine))
        self.add(Item(12, 3, "MHz"))

        self.add(ButtonOK())
        self.add(ButtonBack())

    def activate(self):
        value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, None)

        int_part = None
        frc_part = None

        if value is not None:
            tmp = "{:5.1f}".format(value)
            int_part = tmp.split('.')[0]
            frc_part = tmp.split('.')[1]

        self.int_item.elements.activate(int_part)
        self.int_item.value = self.int_item.elements.active(True)

        self.frc_item.elements.activate(frc_part)
        self.frc_item.value = self.frc_item.elements.active(True)

        super(ScreenFrequency, self).activate()

    def btn_ok(self, new_screen):
        entered_value = float(self.int_item.value + '.' + self.frc_item.value)
        current_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY, None)

        if entered_value != current_value:
            dispatcher.send(pfm_signals.SIG_SET_MAXPRO_FREQUENCY, pfm_signals.SENDER_PANEL, entered_value)

        self.select_start_item(False)
        self.panel.activate(new_screen)

class ScreenWifi(Screen):
    def __init__(self):
        super(ScreenWifi, self).__init__("edit wifi")

        self.add(Item(0, 0, "WIFI"))

        self.wifis = ItemDynamicCycleList(0, 3, selected=True, fmt="{:21.21}")
        self.add(self.wifis)

        self.add(ButtonOK())
        self.add(ButtonBack())

    def activate(self):
        value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, None)

        self.wifis.re_init(pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS), value)
        super(ScreenWifi, self).activate()

    def btn_ok(self, new_screen):
        entered_value = self.wifis.value
        current_value = pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_CLIENT_NETWORK, None)

        if entered_value != current_value:
            print "set new wifi:", entered_value
            dispatcher.send(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, pfm_signals.SENDER_PANEL, entered_value)
        else:
            print "keep old wifi:", entered_value

        self.select_start_item(False)
        self.panel.activate(new_screen)


class ScreenLock(Screen):
    def __init__(self):
        super(ScreenLock, self).__init__("lock")

        self.add(Item(4, 1, "DEVICE LOCKED"))
        self.add(Item(2, 3, "Please enter PIN"))
        self.pin_entered = ItemCollectionText(self, 7, 5, numbers_zero2nine, "000000", True)
        self.add(ButtonOK(15, 7, "UNLOCK"))

        self.wrong_pin = self.add(Item(6, 2, "WRONG PIN"))
        self.wrong_pin.visible = False

    def btn_ok(self, new_screen):

        print "PIN: " , pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, None)
        if self.pin_entered.get_input() == pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, None):
            self.pin_entered.reset()
            self.wrong_pin.visible = False
            self.panel.activate(new_screen)

        else:
            self.wrong_pin.visible = True
            self.wrong_pin.show()
            self.panel.wrong_pin += 1
            print "UNLOCK FAILED: ", self.panel.wrong_pin

        self.select_start_item()


class ScreenChangePin(Screen):
    def __init__(self):
        super(ScreenChangePin, self).__init__("change pin")

        self.add(Item(0, 0, "LOCK SCREEN/NEW PIN"))

        self.add(Item(2, 2, "Old PIN"))
        self.old_pin_items = ItemCollectionText(self, 13, 2, numbers_zero2nine, "000000", True)
        self.add(Item(2, 4, "New PIN"))
        self.new_pin_items = ItemCollectionText(self, 13, 4, numbers_zero2nine, "000000")

        self.wrong_pin = self.add(Item(4, 5, "OLD PIN WRONG!"))
        self.wrong_pin.visible = False

        self.add(ButtonOK(new_screen="lock screen"))
        self.add(ButtonBack(new_screen="lock screen"))

    def btn_ok(self, new_screen):
        if pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, None) == self.old_pin_items.get_input():
            # self.panel.pin = self.new_pin_items.get_input()  # todo: send a signal or whatever ...
            dispatcher.send(pfm_signals.SIG_SET_PFM_LOCK_PIN, pfm_signals.SENDER_PANEL, self.new_pin_items.get_input())
            dispatcher.send(pfm_signals.SIG_UPDATE_PFM_LOCK_PIN, pfm_signals.SENDER_PANEL, self.new_pin_items.get_input()) # todo: NOT LIKE THIS!!
            self.panel.activate(new_screen)

            print "PIN CHANGED, look config file ;)"

        else:  # old pin wrong!
            self.wrong_pin.visible = True
            self.wrong_pin.show()
            self.panel.wrong_pin += 1
            self.select_start_item(True)

    def activate(self):
        self.new_pin_items.reset()
        self.old_pin_items.reset()

        self.wrong_pin.visible = False
        self.select_start_item(False)

        super(ScreenChangePin, self).activate()


def start(bboard):

    panel = ui.display.panel.Panel(bboard)

    panel.add(consruct_status_screen())  # the default screen
    panel.add(construct_lock_screen())

    panel.add(ScreenStationID())
    panel.add(ScreenFrequency())
    panel.add(ScreenWifi())
    panel.add(ScreenChangePin())
    panel.add(ScreenLock())

    panel.add(ui.display.screens.ScreenNotImplemented())
    panel.activate()


