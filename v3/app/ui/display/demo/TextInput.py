from ui.display.items import ItemUpdatable
from ui.display.items import ItemSelectable
from ui.display.items import Item
from ui.display.items import ButtonOK
#from ui.display.items import ItemCollectionText
from ui.display.items import ButtonBack
from ui.display.screens import Screen
from ui.display.screens import SCREEN_DEFAULT
from ui.display.panel import Panel
import pfm_signals
from pydispatch import dispatcher
import ui.display.CycleList
import logging


chars_and_numbers = ui.display.CycleList.default_letter_list()
TEXT38_SIG = "text38"
TEXT38_STR = "init"

CHAR_NO_INPUT = '\t'

class ScreenMain(Screen):
    def __init__(self, name=SCREEN_DEFAULT, backbtn=False):
        super(ScreenMain, self).__init__(name)
        self.add(ItemUpdatable(0, 0, "Text 3-8   {:>8}", TEXT38_SIG, TEXT38_STR,
                                  selectable=True, new_screen="input 3-8"))

        # self.add(ItemUpdatable(0, 1, "Frequency    {:3.1f}MHz", pfm_signals.SIG_UPDATE_MAXPRO_FREQUENCY,
        #                           selectable=True))
        #
        # self.add(ItemUpdatable(0, 2, "Input        {:7.3f}V", pfm_signals.SIG_UPDATE_ADC_INPUT_VOLTAGE,
        #                           selectable=True))


class Screen38(Screen):
    def __init__(self):
        super(Screen38, self).__init__("input 3-8")

        self.add(Item(0, 0, "Please enter text"))

        self.add(Item(2, 4, "Text:  >"))
        self.pw = self.add(ItemInputText(10, 4, 3, 8, chars_and_numbers))
        self.add(Item(18, 4, "<"))

        self.input_save_btn = ButtonOK()  # makes this an input screen
        self.add(self.input_save_btn)
        self.add(ButtonBack())

    def activate(self):
        self.pw.reset(TEXT38_STR)

        super(Screen38, self).activate()

    def btn_ok(self, new_screen):
        dispatcher.send(TEXT38_SIG, pfm_signals.SENDER_PANEL, self.pw.value)
        global TEXT38_STR
        TEXT38_STR = self.pw.input

        self.select_start_item(False)
        self.panel.activate(new_screen)



def start(bboard):
    print "los geht er"
    panel = Panel(bboard)
    panel.add(ScreenMain())
    panel.add(Screen38())

    panel.activate()



class InputChar(object):
    """
    maintains a letter
    """
    def __init__(self, x, y, char_list):    # todo: default char list
        self.x = x
        self.y = y
        self.no_input = True
        # self.panel = None -> do the printing here
        self.options = ui.display.CycleList.CyclicList()
        for char in char_list:
            if type(char) is not str or len(char) != 1:
                raise Exception("Char: {} is not a character!".format(char))
            self.options.add(char)

    def set(self, char):
        if char == CHAR_NO_INPUT:
            self.no_input = True
            self.options.activate(' ')  #
        else:
            self.no_input = False
            self.options.activate(char)

    def get_current (self, jump_if_no_input=False):
        if self.no_input and jump_if_no_input:
            self.options.activate(None) # reset the list
            self.no_input = False # no way back to no input ...

        return self.options.active(activate_first_element=True)


class ItemInputText(ItemSelectable):
    """
    Offers a collection of 'character items' to enter words or numbers
    The characters offered are given in char_list
    The length of the input is given by min and max.
    """
    def __init__(self, x, y, min, max, char_list):
        super(ItemInputText, self).__init__(x, y, "{}")

        self.min = min
        self.max = max
        self.edit_char = False
        self.current_char = None
        self.current_char_idx = None
        self.input = ""

        self.chars = []
        for idx in xrange(max):
            char = InputChar(x, y, char_list)
            self.chars.append(char)
            x += 1

    def reset(self, current=None):
        """ resets values und active elements in the lists
        """
        self.value = ""
        if current is None:
            for char in self.chars:
                char.set(CHAR_NO_INPUT)
                self.value += ' '
        else:
            if len(current) > self.max+1:
                logging.debug("'{}' will be cut to '{}'".format(current, current[:self.max]))
                self.value = current[:self.max]
            else:
                to_fill = self.max - len(current)
                if to_fill > 0:
                    logging.debug("fill {} with {} no_prints".format(current, to_fill))
                    for fill in xrange(to_fill):
                        current += CHAR_NO_INPUT
                else:
                    logging.debug("{} has exact the max right size of {}".format(current, self.max))

                for idx in xrange (len(self.chars)):
                    current_char = current[idx]
                    self.chars[idx].set(current_char)
                    if current_char == CHAR_NO_INPUT:
                        self.value += ' '
                    else:
                        self.value += current_char



    # def set_visibility(self, visible):
    #     for item in self.items:
    #         item.visible = visible


    def btn_pressed(self):
        if self.edit is False:    # switch to edit mode
            self.edit = True
            #self.deselect(True)   # show without highlight, keep selection in screen
            self.panel.print_normal(self.x, self.y, self.value)

            # self.edit_char = True
            # if self.current_char_idx is None:
            self.current_char_idx = 0  # we start with the first char, old positions are puzzling
            char = self.chars[self.current_char_idx]
            self.panel.print_inverse(char.x, char.y, char.get_current())
        else:
            if self.edit_char:
                print "leave edit char"

                num_entered = 0
                for char in self.chars:
                    if char.no_input:
                        break
                    else:
                        num_entered += 1
                if num_entered >= self.min:
                    self.input_valid = True

                self.edit_char = False
                self.panel.inactivate_marked_text()
                char = self.chars[self.current_char_idx]
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == self.max - 1:  # last letter -> goto next item
                    self.leave()
                else:
                    print "YEAH1"
                    self.current_char_idx += 1
                    char = self.chars[self.current_char_idx]
                    self.panel.print_inverse(char.x, char.y, char.get_current())
            else:
                self.edit_char = True
                char = self.chars[self.current_char_idx]
                self.panel.print_blinki(char.x, char.y, char.get_current(True))

    def leave(self):
        self.edit = False
        self.input = ""
        for char in self.chars:
            if not char.no_input:
                print "'{}'".format(char.get_current())
                self.input += char.get_current()
            else:
                print "NOOOOOOO INPUT"
                break
        self.value = self.input
        for i in xrange(self.max - len(self.value)):
            self.value += ' '

        print "'{}'".format(self.value)
        print "'{}'".format(self.input)

        self.screen.btn_right()


    def btn_right(self):

        print "btn_right()", self.edit, self.edit_char

        if self.edit:
            char = self.chars[self.current_char_idx]
            if self.edit_char:
                self.panel.print_blinki(char.x, char.y, char.options.nxt())
            else:
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == self.max-1 or char.no_input:
                    self.leave()
                else:
                    self.current_char_idx += 1
                    char = self.chars[self.current_char_idx]

                    print char.no_input, char.get_current(), char.no_input
                    self.panel.print_inverse(char.x, char.y, char.get_current())

                    # self.items[self.current_char].edit = False
                    # self.current_char += 1
                    # self.items[self.current_char].edit = True
                    # self.items[self.current_char].show()

        else:
            raise Exception("ItemInputText.btn_right() called in non edit mode. Screen:", self.screen.name)


    def btn_left(self):
        print "btn_left()", self.edit, self.edit_char

        if self.edit:
            char = self.chars[self.current_char_idx]
            if self.edit_char:
                self.panel.print_blinki(char.x, char.y, char.options.prv())
            else:
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == 0:
                    self.leave()
                else:
                    self.current_char_idx -= 1
                    char = self.chars[self.current_char_idx]

                    print char.no_input, char.get_current(), char.no_input
                    self.panel.print_inverse(char.x, char.y, char.get_current())

                    # self.items[self.current_char].edit = False
                    # self.current_char += 1
                    # self.items[self.current_char].edit = True
                    # self.items[self.current_char].show()

        else:
            raise Exception("ItemInputText.btn_left() called in non edit mode. Screen:", self.screen.name)


        # else:
        #     self.edit = False
        #     self.current_char = 0
        #     self.panel.inactivate_marked_text()  # makes show() not necessary anymore
        #     self.screen.btn_right()  # go one element further (wished by Adam)





class ItemInputCharacter(ItemSelectable):
    def __init__(self, x, y, characters, value=CHAR_NO_INPUT, selected=False):
        super(ItemInputCharacter, self).__init__(x, y, value=value, selected=selected, fmt="{}")

        self.elements = ui.display.CycleList.CyclicList()
        for char in characters:
            if type(char) is not str or len(char) != 1:
                raise Exception("ItemInputCharacter: {} is not a character!".format(char))
            self.elements.add(char)

        # self.elements.add_all(elements)

        print self.elements.elements
        self.no_input = None
        self.reset(value)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True

            if self.no_input:
                self.value = self.elements.nxt()

            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()  # makes show() not necessary anymore
            self.screen.btn_right()  # go one element further (wished by Adam)

    def btn_right(self):
        if self.edit:
            self.value = self.elements.nxt()
            self.show()
        else:
            raise Exception("item.btn_right() called in non edit mode. Screen:", self.screen.name)

    def btn_left(self):
        if self.edit:
            self.value = self.elements.prv()
            self.show()
        else:
            raise Exception("item.btn_left() called in non edit mode. Screen:", self.screen.name)

    # def show(self):  # todo ???
    #    self.value = self.elements.active()
    #    super(ItemCyclicList, self).show()

    def reset(self, value):
        """ if there is a value, the list is set to it
            if not, the value is set to the active (or first) element in the list
            should be called every time this items gets active
        """
        if value is CHAR_NO_INPUT:
            self.value = self.elements.activate(None)  # activates the first element in the list
            self.value = '_'  # show() has to print something
            self.no_input = True
        else:
            print "############## '{}'".format(value)
            self.elements.activate(value)
            self.value = value
            self.no_input = False
