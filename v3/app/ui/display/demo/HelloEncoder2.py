from ui.display.screens import Screen
from ui.display.screens import SCREEN_DEFAULT
from ui.display.panel import Panel
from ui.display.items import ItemSelectable
from ui.display.items import ItemUpdatable
from pydispatch import dispatcher


SIGNAL_X = "x"
SIGNAL_Y = "y"


class ItemPosition(ItemSelectable):
    def __init__(self, x, y, fmt, selected=False):
        super(ItemPosition, self).__init__(x, y, fmt, selected=selected)
        self.selectable = True

    def select(self, show=True):
        dispatcher.send(SIGNAL_X, "panel", self.x)
        dispatcher.send(SIGNAL_Y, "panel", self.y)
        super(ItemPosition, self).select()

class Encoder2(Screen):
    def __init__(self, name=SCREEN_DEFAULT, backbtn=False):
        super(Encoder2, self).__init__(name)
        self.add(ItemPosition(5, 1, "*", True))
        self.add(ItemPosition(6, 1, "*"))
        self.add(ItemPosition(7, 1, "*"))
        self.add(ItemPosition(8, 1, "*"))

        self.add(ItemPosition(9, 2, "*"))
        self.add(ItemPosition(10, 3, "*"))
        self.add(ItemPosition(10, 4, "*"))
        self.add(ItemPosition(9, 5, "*"))
        self.add(ItemPosition(8, 6, "*"))
        self.add(ItemPosition(7, 6, "*"))
        self.add(ItemPosition(6, 6, "*"))
        self.add(ItemPosition(5, 6, "*"))
        self.add(ItemPosition(4, 5, "*"))
        self.add(ItemPosition(3, 4, "*"))
        self.add(ItemPosition(3, 3, "*"))
        self.add(ItemPosition(4, 2, "*"))

        self.add(ItemUpdatable (13, 1, "x = {:>2d}", SIGNAL_X, -1))
        self.add(ItemUpdatable (13, 2, "y = {:>2d}", SIGNAL_Y, -1))


def start(bboard):
    panel = Panel(bboard)

    hello = Encoder2()
    panel.add(hello)
    panel.activate()
