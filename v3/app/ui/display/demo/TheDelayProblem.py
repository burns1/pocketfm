from __future__ import print_function
from periodic import PeriodicThread
from pydispatch import dispatcher


from ui.display.items import Item
from ui.display.items import ItemSelectable
from ui.display.items import ItemUpdatable
from ui.display.panel import Panel
from ui.display.screens import Screen


class Tester(object):
    def __init__(self):
        self.delay_val = 0

    def test_delay(self):
        self.delay_val += 1
        dispatcher.send("delay_test", dispatcher.Any, self.delay_val)
        if self.delay_val % 50 == 0:
            dispatcher.send("delay_test_100", dispatcher.Any, self.delay_val)
            print("DELAY:", self.delay_val, " ----------------------------------------- ")
        else:
            print("DELAY:", self.delay_val)


def start(bboard):

    panel = Panel(bboard)

    screen = panel.add(Screen())

    screen.add(Item(1, 0, "Find the delay"))

    screen.add(ItemSelectable(2, 2, "a) Is there?", selected=True))
    screen.add(ItemSelectable(3, 3, "b) Not there?"))
    screen.add(ItemSelectable(4, 4, "c) Coffee??"))

    screen.add(ItemUpdatable(1, 6, " Delay Test: {:>4d} ", "delay_test", 0))
    screen.add(ItemUpdatable(1, 7, " Delay  100: {:>4d} ", "delay_test_100", 0))

    panel.activate()

    tester = Tester()
    timer = PeriodicThread(0.01, tester.test_delay)
    timer.start()
