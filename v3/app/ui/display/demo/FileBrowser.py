import os

from pydispatch import dispatcher

import pfm_signals
import ui.display.CycleList
import ui.display.panel
import ui.display.screens
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import Item
from ui.display.items import ItemCycleList
from ui.display.items import ItemSelectable
from ui.display.items import ItemUpdatablePFM
from ui.display.screens import Screen


class ScreenFile(Screen):
    def __init__(self):
        super(ScreenFile, self).__init__("file")

        self.add(Item(4, 1, "SELECT FILE"))

        self.files = ItemCycleList(0, 3, os.listdir("/"), value=None, fmt="{:20}")

       # self.files = ItemCyclicList(0,4, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS))

        self.add(self.files)

        self.add(ButtonOK())
        self.add(ButtonBack())


class ScreenWifi(Screen):
    def __init__(self):
        super(ScreenWifi, self).__init__("wifi")

        self.add(Item(4, 1, "SELECT NETWORK"))

        self.files = None

        # self.files = ItemCyclicList(0,4, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS))

        #self.add(self.files)

        self.add(ButtonOK())
        self.add(ButtonBack())

    def activate(self):
        self.files = ItemCycleList(0, 3, pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, None), value=None, fmt="{:20}")
        self.add(self.files)
        super(ScreenWifi, self).activate()

class ScreenType(Screen):
    def __init__(self):
        super(ScreenType, self).__init__("wifi")

        self.add(Item(4, 1, "SELECT NETWORK"))

        self.files = None

        # self.files = ItemCyclicList(0,4, pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS))

        #self.add(self.files)

        self.add(ButtonOK())
        self.add(ButtonBack())

    def activate(self):
        self.files = ItemCycleList(0, 3, pfm_signals.get_value_with_default(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, None), value=None, fmt="{:20}")
        self.add(self.files)
        super(ScreenType, self).activate()


class ScreenMain(Screen):
    def __init__(self):
        super(ScreenMain, self).__init__()
        self.add(ItemSelectable(4, 3, "SELECT NETWORK", new_screen="wifi"))
        self.add(ItemSelectable(4, 5, "SELECT File",   new_screen="file"))
        self.add(ItemSelectable(4, 5, "Type Filename", new_screen="type"))

        self.files = None



def start(bboard):

    panel = ui.display.panel.Panel(bboard)
    panel.add (ScreenMain())
    panel.add (ScreenFile())
    panel.add (ScreenWifi())

    panel.add(ui.display.screens.ScreenNotImplemented())
    panel.activate()


