from pydispatch import dispatcher

import ui.display.CycleList
import ui.display.screens
from ui.display.items import Button2Screen
from ui.display.items import ButtonBack
from ui.display.items import ButtonOK
from ui.display.items import Item
from ui.display.items import ItemCycleList
from ui.display.items import ItemUpdatable
from ui.display.panel import Panel
from ui.display.screens import Screen


class data(object):
    """ some fake data, shared over all the screens - more a struct than an object """
    PERCENT = 100

    UPDATE_OUTPUT = "SIGNAL_UPDATE_OUTPUT"
    OUTPUT = 25.3
    UPDATE_REFLECTION = "SIGNAL_UPDATE_REFLECTION"
    REFLECTION = 7.2
    UPDATE_TEMPERATURE = "SIGNAL_UPDATE_TEMPERATURE"
    TEMPERATURE = 67.7

    @staticmethod
    def perc_str():
        return "{:3}".format(data.PERCENT)

    @staticmethod
    def set_and_signal(perc):
        data.PERCENT = perc
        dispatcher.send(data.UPDATE_OUTPUT, "Panel", data.OUTPUT * perc / 100.0)
        dispatcher.send(data.UPDATE_REFLECTION, "Panel", data.REFLECTION * perc / 100.0)
        dispatcher.send(data.UPDATE_TEMPERATURE, "Panel", data.TEMPERATURE * perc / 100.0)


output_power = ui.display.CycleList.list_str_integer(0, 101, fmt="{:3}")


#######################################################################################################################
#
# consistent screen
#
#######################################################################################################################
class ScreenConsistent(Screen):
    def __init__(self):
        super(ScreenConsistent, self).__init__("consistent")

        self.add(Item(0, 0, "OUTPUT POWER"))

        self.add(Item(1, 1, "Set 0-100%"))
        self.perc_item = ItemCycleList(16, 1, output_power, selected=True, fmt="{}%")
        self.add(self.perc_item)

        self.add(ItemUpdatable(1, 3, "Output       {:5.1f}W", data.UPDATE_OUTPUT, data.OUTPUT))
        self.add(ItemUpdatable(1, 4, "Reflection   {:5.1f}W", data.UPDATE_REFLECTION, data.REFLECTION))
        self.add(ItemUpdatable(1, 5, "Temperature  {:5.1f}C", data.UPDATE_TEMPERATURE, data.TEMPERATURE))

        self.add(ButtonOK())
        self.add(ButtonBack())

    def btn_ok(self, new_screen):   # it will be invoked with a new_screen parameter
        data.set_and_signal(int(self.perc_item.value))
        self.btn_right()

    def activate(self):
        self.perc_item.reset(data.perc_str())
        super(ScreenConsistent, self).activate()


#######################################################################################################################
#
#  Philipp Item and screen
#
#######################################################################################################################
class ItemPercentPhilipp(ItemCycleList):
    def __init__(self, x, y,  fmt="{}%", selected=True):
        super(ItemPercentPhilipp, self).__init__(x, y, output_power, fmt=fmt, selected=selected)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            data.set_and_signal(int(self.value))
            self.screen.btn_right()  # go to the back button
            self.panel.inactivate_marked_text()


class ScreenPhilipp(Screen):
    def __init__(self):
        super(ScreenPhilipp, self).__init__("philipp")

        self.add(Item(0, 0, "OUTPUT POWER"))

        self.add(ItemUpdatable(1, 1, "Output       {:5.1f}W", data.UPDATE_OUTPUT, data.OUTPUT))
        self.add(ItemUpdatable(1, 2, "Reflection   {:5.1f}W", data.UPDATE_REFLECTION, data.REFLECTION))
        self.add(ItemUpdatable(1, 3, "Temperature  {:5.1f}C", data.UPDATE_TEMPERATURE, data.TEMPERATURE))

        self.add(Item(1, 5, "Set 0-100%"))
        self.perc_item = ItemPercentPhilipp(16, 5)
        self.add(self.perc_item)

        self.add(ButtonBack())

    def activate(self):
        self.perc_item.reset(data.perc_str())
        super(ScreenPhilipp, self).activate()


#######################################################################################################################
#
#  Responsive Item and screen
#
#######################################################################################################################
class ItemResponsive(ItemCycleList):
    def __init__(self, x, y, fmt="{}%", selected=True):
        super(ItemResponsive, self).__init__(x, y, output_power, fmt=fmt, selected=selected)

    def btn_right(self, steps=1):
        if self.edit:
            self.value = self.elements.nxt(steps)
            data.set_and_signal(int(self.value))
            self.show()
        else:
            raise Exception("Item.btn_right() in non edit mode")

    def btn_left(self, steps=1):
        if self.edit:
            self.value = self.elements.prv(steps)
            data.set_and_signal(int(self.value))
            self.show()
        else:
            raise Exception("Item.btn_left() in non edit mode")


class ScreenResponsive(Screen):
    def __init__(self):
        super(ScreenResponsive, self).__init__("responsive")

        self.add(Item(0, 0, "OUTPUT POWER"))

        self.add(ItemUpdatable(1, 2, "Output       {:5.1f}W", data.UPDATE_OUTPUT, data.OUTPUT))
        self.add(ItemUpdatable(1, 3, "Reflection   {:5.1f}W", data.UPDATE_REFLECTION, data.REFLECTION))
        self.add(ItemUpdatable(1, 4, "Temperature  {:5.1f}C", data.UPDATE_TEMPERATURE, data.TEMPERATURE))

        self.add(Item(9, 6, "Set"))

        self.perc_item = ItemResponsive(13, 6)
        self.add(self.perc_item)

        self.add(ButtonBack())

    def activate(self):
        self.perc_item.reset(data.perc_str())
        super(ScreenResponsive, self).activate()

#######################################################################################################################
#
#  simple screens
#
#######################################################################################################################
def build_main_menu():
    screen = Screen()

    screen.add(Item(0, 0, "OUTPUT PWR SOLUTIONS"))
    screen.add(Button2Screen(3, 2, "Consistent", "consistent", True))
    screen.add(Button2Screen(3, 3, "Phlilipp", "philipp"))
    screen.add(Button2Screen(3, 4, "Responsive", "responsive"))
    screen.add(Button2Screen(3, 6, "Rate them", "just tell"))
    return screen


def build_just_tell():
    screen = Screen("just tell")
    screen.add(Item(3, 0, "JUST TELL Rene!"))
    screen.add(Item(1, 2, "But mind the BACK"))
    screen.add(Item(1, 3, "button here. Do you"))
    screen.add(Item(1, 4, "notice a difference"))
    screen.add(Item(1, 5, "turning left/right??"))
    screen.add(ButtonBack(selected=True))
    return screen


def start(bboard):
    panel = Panel(bboard)

    panel.add(build_main_menu())

    panel.add(ScreenConsistent())
    panel.add(ScreenPhilipp())
    panel.add(ScreenResponsive())

    panel.add(build_just_tell())

    panel.activate()
