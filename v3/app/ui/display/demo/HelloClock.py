import time

from pydispatch import dispatcher

from periodic import PeriodicThread
from ui.display.items import ItemUpdatable
from ui.display.panel import Panel
from ui.display.screens import Screen


def hello_clock_signal():
    dispatcher.send("SIG_TIME", "display", time.strftime("%H:%M:%S"))

def start(bboard):
    panel = Panel(bboard)
    clock = panel.add(Screen())
    clock.add(ItemUpdatable(-1, -1, "Time: {}", "SIG_TIME", time.strftime("%H:%M:%S")))
    panel.activate()

    t = PeriodicThread(2, hello_clock_signal)
    t.start()
