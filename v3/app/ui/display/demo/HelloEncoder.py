from ui.display.screens import Screen
from ui.display.panel import Panel
from ui.display.items import ItemSelectable


def start(bboard):

    panel = Panel(bboard)  # connects automatically to the rotator signals

    four_selectables = Screen()
    four_selectables.add(ItemSelectable(10, 1, "* 1 *"))
    four_selectables.add(ItemSelectable(15, 3, "* 2 *"))
    four_selectables.add(ItemSelectable(10, 5, "* 3 *"))
    four_selectables.add(ItemSelectable(1, 3,  "* 4 *"))

    panel.add(four_selectables)

    panel.activate()
