from __future__ import print_function
import logging

from CycleList import CyclicList
from items import Item
from items import Button2Screen

SCREEN_NOT_IMPLEMENTED = "_NOT_IMPLEMENTED_"  # is also defined hiere
SCREEN_DEFAULT = "_DEFAULT_SCREEN_"  # just the handle, the screen needs to be defined
CHAR_NO_INPUT = '\t'

class Screen(object):

    def __init__(self, name=SCREEN_DEFAULT):
        self.name = name
        self.items = []
        self.selectables = CyclicList()  # all the item which are selectable. current is the active item
        self.panel = None  # set in display.add
        self.visible = False
        self.start_item = None
        self.activation_count = 0

        # self.input_screen = False
        self.input_save_btn = None  # assigning makes it an input screen # todo: remove
        self.active = False

    def add(self, item):
        """ Adds a new item. All selectable item will be stored in CycleList selectabls.
            There can be only one item selected. Th
        """
        if self.panel is not None:
            item.panel = self.panel  #  otherwise its done in display.add()

        if item.screen is None:
            item.screen = self
        #else:
        #    raise Exception("An item can be added to one screen only")

        self.items.append(item)

        if item.selectable:
            self.selectables.add(item, item.selected, type_check=False)  #  different type of items ...
            if item.selected:
                self.start_item = item
        #else:
        #    if item.selected:
        #        raise Exception("Non selectable cannot be selected - bad!!")

        return item

    def select_start_item(self, show=True):
        """ selects the start item. If there is none definend,
            the first item will be selected.
        """
        if self.selectables.size() == 0:
            logging.info ("AVOID: select_start_item() called, with no selectables available")
            return

        if self.selectables.element_activated():
            if self.selectables.active() == self.start_item:
                return
            else:
                self.selectables.active().deselect(show)

        if self.start_item is None:
            self.selectables.activate()
            self.selectables.nxt().select(show)  # will activate the first element
        else:
            self.selectables.activate(self.start_item)
            self.start_item.select(show)

    def btn_left(self):
        # print " *** 2) LEFT ROTATE in screen.btn_left() enter "

        if self.selectables.size() == 0:
            print ("No selectable items at all ")
            return

        if self.selectables.element_activated():
            if self.selectables.active().edit:
                self.selectables.active().btn_left()
            else:
                if self.selectables.size() > 1:
                    self.selectables.active().deselect()

                    item = self.selectables.prv()
                    while item.visible is False:    # todo danger of endless loop
                        item = self.selectables.prv()
                    item.select()
        else:
            self.select_start_item()

            # print " *** 3) LEFT ROTATE in screen.btn_left() done "

    def btn_right(self):
        if self.selectables.size() == 0:
            logging.info ("No selectable item at all")
            return

        if self.selectables.element_activated():
            if self.selectables.active().edit:
                self.selectables.active().btn_right()
            else:
                self.selectables.active().deselect()

                item = self.selectables.nxt()
                while item.visible is False:    # todo danger of endless loop
                    item = self.selectables.nxt()
                item.select()
        else:
            self.select_start_item()

    def btn_pressed(self):
        if self.selectables.element_activated():
            self.selectables.active().btn_pressed()

            if self.input_save_btn is not None:
                self.input_save_btn.visible = True
                for item in self.items:
                    if not item.input_valid:
                        self.input_save_btn.visible = False
                        break
                self.input_save_btn.show()
        else:
            logging.info ("btn_pressed: no active selectables?")

    # def btn_released(self):
    #     print ("btn_released: SHALL NEVER REACH SCREEN!")
    #     pass

    def btn_cancel(self, new_screen):
        """ A default behavior vor cancel:
            - selects the start item
            - activates new_screen

            A default for btn_save doesnt make sense
        """
        self.select_start_item(False)
        self.panel.activate(new_screen)

    def activate(self):
        self.active = True
        #print(" +.+.+ activate sreen ", self.name)
        self.visible = True
        for item in self.items:
            item.show()
        self.activation_count += 1

    def deactivate(self):
        self.active = False
        #print(" +.+.+ deactivate sreen ", self.name)
        self.visible = False


##############################################################################
#
#  some concrete implementations
#
##############################################################################
class ScreenNotImplemented(Screen):
    def __init__(self, btn_x=3, btn_y=6, btn_text="DEFAULT SCREEN"):
        super(ScreenNotImplemented, self).__init__(SCREEN_NOT_IMPLEMENTED)
        self.add(Item(1, 2, "Sorry, not done yet"))
        self.add(Item(2, 4, "Please try later!", ))
        self.add(Button2Screen(btn_x, btn_y, btn_text, SCREEN_DEFAULT, False))
