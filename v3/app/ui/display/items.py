import random
import logging
import pfm_signals

import CycleList
import screens
from pydispatch import dispatcher


##############################################################################
#
#  general Items
#
##############################################################################
class Item(object):
    """
    Items are all things visible on the display. This can be simple text or complex lists.

    """
    def __init__(self, x, y, fmt, value=None, str_no_value=None):
        self.panel = None  # will be set by Screen!
        self.screen = None  # dito
        self.x = x  # negative will cause a random value
        self.y = y  # dito

        self.fmt = fmt
        self.value = value
        self.str_no_value = str_no_value

        self.edit = False
        self.selectable = False
        self.selected = False
        self.visible = True  # will be over ruled by screen.visible

        self.input_valid = True   # todo: subclassing? removing entirely

    def show(self):
        """ writes itself to the display.

            this is actually the only place, where stuff is written to the screen
        """

        # VERY RARE, but possible: updatable item might get signals before properly attached
        # this happened 2016-05-17, when I added str_no_value - I don't know why.
        # can be simulated by creating a updatable item followed by a time.sleep(1) or so
        # in this case, the following if will help
        if self.screen is None or self.panel is None:
            logging.info("show() on not attached item! Screen: {}; Panel: {} ".format(self.screen, self.panel))
            return

        if not self.screen.visible or not self.visible:
            return

        tmp = self.fmt
        if self.value is not None and self.value != pfm_signals.NOSIGYET:  # if not self.value doesnt work with 0!!!
            tmp = self.fmt.format(self.value)
        elif self.str_no_value:
            tmp = self.str_no_value

        x = self.x
        y = self.y

        # nice little gadget
        if y < 0 or x < 0:
            self.panel.clrscr()
            if y < 0:
                y = random.randint(0, 7)
            if x < 0:
                x = random.randint(0, 21 - len(tmp))

        if self.selected:
            if self.edit:
                self.panel.print_blinki(x, y, tmp)
            else:
                self.panel.print_inverse(x, y, tmp)
        else:
            self.panel.print_normal(x, y, tmp)


class ItemSelectable(Item):
    def __init__(self, x, y, fmt, value=None, str_no_value=None, selected=False, new_screen=None):
        super(ItemSelectable, self).__init__(x, y, fmt, value, str_no_value)
        self.selectable = True
        self.selected = selected
        self.new_screen = new_screen

    def select(self, show=True):
        #if self.selected:
        #    raise Exception("try to select an already selected item", self.fmt.format(self.value))

        #if self.selectable is False:  # should never happen ...
        #    raise Exception("try to select an non selectable item", self.fmt.format(self.value))

        self.selected = True
        if show:
            self.show()

    def deselect(self, show=True):
        #if self.selected is False:
        #    raise Exception("try to deselect an non selected item", self.fmt.format(self.value))

        self.selected = False
        if show:
            self.show()

    def btn_pressed(self):
        if self.new_screen is None:
            logging.info ("Item.btn_pressed: no new_screen defined")
        else:
            self.panel.activate(self.new_screen)


class ItemUpdatable(ItemSelectable):
    """ An item which is updating itsself through a signal

        Signal is also received and updates the value, when the item is not visible. So it will always be
        up2date.
    """
    def __init__(self, x, y, fmt, update_signal, value=None, selectable=False, selected=False, new_screen=None):
        super(ItemUpdatable, self).__init__(x, y, fmt, selected=selected, new_screen=new_screen)

        #if update_signal is None:
        #    raise Exception("ItemUpdateable needs update_signal")

        self.value = value  # value is just the backup!!
        self.selectable = selectable
        dispatcher.connect(self.update_handler, update_signal, weak=False)

    def update_handler(self, value):
        if self.value != value:
            self.value = value
            self.show()

class ItemUpdatablePFM(ItemSelectable):
    """
    The same as ItemUpdatable but closer to the pfm signals. Uses pfm_signals.get_value()

    """
    def __init__(self, x, y, fmt, update_signal, value=None, str_no_value=None, selectable=False, selected=False,
                 new_screen=None):
        super(ItemUpdatablePFM, self).__init__(x, y, fmt, str_no_value=str_no_value, selected=selected,
                                               new_screen=new_screen)

        #if update_signal is None:
        #    raise Exception("ItemUpdateable needs update_signal")

        self.value = pfm_signals.get_value_with_default(update_signal, value)  # value as a backup
        self.selectable = selectable

        # self.update_signal = update_signal # is never needed again
        dispatcher.connect(self.update_handler, update_signal, weak=False)

    def update_handler(self, value):
        if self.value != value:
            self.value = value
            self.show()


class Button2Screen(ItemSelectable):
    """
    a selectable item which is a button to a screen"""

    def __init__(self, x, y, fmt, new_screen, selected=False):
        super(Button2Screen, self).__init__(x, y, fmt, value=None, str_no_value=None, selected=selected)
        self.new_screen = new_screen

    def btn_pressed(self):
        self.panel.activate(self.new_screen)


# A button which leads to the standard not implemented screen
class Button2NotImplemented(ItemSelectable):
    def __init__(self, x, y, fmt, selected=False):
        super(Button2NotImplemented, self).__init__(x, y, fmt, value=None, selected=selected)

    def btn_pressed(self):
        # self.display.activate("_NOT_IMPLEMENTED_")
        self.panel.activate(screens.SCREEN_NOT_IMPLEMENTED)


class ButtonOK(ItemSelectable):
    """ a standard SAVE button

        todo calls btn_save on the screen

    """

    def __init__(self, x=17, y=7, fmt="SAVE", new_screen=None, selected=False):
        super(ButtonOK, self).__init__(x, y, fmt, None, selected=selected)
        self.test = screens.SCREEN_DEFAULT
        if new_screen is None:  # using screens.SCREEN_DEFAULT as default did not work
            self.new_screen = screens.SCREEN_DEFAULT
        else:
            self.new_screen = new_screen

    def btn_pressed(self):
        self.screen.btn_ok(self.new_screen)

class ButtonClear(ItemSelectable):
    """ a standard Clear button

    """
    def __init__(self, x=8, y=7, fmt="CLEAR", selected=False):
        super(ButtonClear, self).__init__(x, y, fmt, None, selected=selected)

    def btn_pressed(self):
        self.screen.btn_clear()



class ButtonBack(ItemSelectable):
    """a standard back (cancel) button"""

    def __init__(self, x=0, y=7, fmt="BACK", new_screen=None, selected=False):
        super(ButtonBack, self).__init__(x, y, fmt, None, selected=selected)
        if new_screen is None:  # using screens.SCREEN_DEFAULT as default did not work
            self.new_screen = screens.SCREEN_DEFAULT
        else:
            self.new_screen = new_screen

    def btn_pressed(self):
        self.screen.btn_cancel(self.new_screen)


class ItemCollectionText(object):
    """
    Offers a collection of 'character items' to enter words or numbers
    The characters offered are given in char_list
    The maximum length of the input is given by the length of init.
    """
    def __init__(self, screen, x, y, char_list, init, activate_first=False):

        # print "und los gehts:", init

        self.init = init  # defines the size of the string and the start values
        self.items = []
        for letter in init:
            # print letter
            item = ItemCycleList(x, y, char_list, letter)
            if activate_first:
                item.select(False)
                screen.start_item = item
                activate_first = False
            screen.add(item)
            self.add(item)
            x += 1

    def add(self, item):
        self.items.append(item)

    def get_input(self):
        tmp = ""
        for item in self.items:
            tmp += item.value
        return tmp

    def reset(self, values=None):
        """ resets values und active elements in the lists
        """
        if values is None:
            values = self.init

        len_needed = len(self.items)

        if len_needed > len(values):
            logging.debug("'{}' TOO SHORT, fill with blanks".format(values))

            while len(values) < len(self.items):  # thoroughly discussed with Adam, 2016-06-06
                values += ' '

        if len_needed < len(values):
            logging.debug("'{}' will be cut to '{}'".format(values, values[:len_needed]))

        for n in xrange(0, len_needed):
            self.items[n].elements.activate(values[n])
            self.items[n].value = values[n]


    def set_visibility(self, visible):
        for item in self.items:
            item.visible = visible



class ItemCycleList(ItemSelectable):
    def __init__(self, x, y, elements, value=None, selected=False, fmt="{}"):
        super(ItemCycleList, self).__init__(x, y, fmt, value=value, selected=selected)

        self.elements = CycleList.CyclicList()
        self.elements.add_all(elements)
        self.reset(value)

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True

            if self.value == screens.CHAR_NO_INPUT:
                self.value = self.elements.nxt()

            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()  # makes show() not necessary anymore
            self.screen.btn_right()  # go one element further (wished by Adam)

    def btn_right(self):
        if self.edit:
            self.value = self.elements.nxt()
            self.show()
        #else:
        #    raise Exception("item.btn_right() called in non edit mode. Screen:", self.screen.name)

    def btn_left(self):
        if self.edit:
            self.value = self.elements.prv()
            self.show()
        #else:
        #    raise Exception("item.btn_left() called in non edit mode. Screen:", self.screen.name)

    def show(self):  # todo ???
        self.value = self.elements.active()
        super(ItemCycleList, self).show()

    def reset(self, value):
        """ if there is a value, the list is set to it
            if not, the value is set to the active (or first) element in the list
            should be called every time this items gets active
        """
        if value is None:
            self.value = self.elements.active(activate_first_element=True)
        else:
            self.elements.activate(value)
            self.value = value


class ItemDynamicCycleList(ItemSelectable):
    def __init__(self, x, y, selected=False, fmt="{}"):
        super(ItemDynamicCycleList, self).__init__(x, y, fmt, selected=selected)
        self.elements = CycleList.CyclicList()

    def btn_pressed(self):
        if self.edit is False:
            self.edit = True
            self.show()
        else:
            self.edit = False
            self.panel.inactivate_marked_text()  # makes show() not necessary anymore
            self.screen.btn_right()  # go one element further (wished by Adam)

    def btn_right(self):
        if self.edit:
            self.value = self.elements.nxt()
            self.show()
        #else:
        #    raise Exception("item.btn_right() called in non edit mode. Screen:", self.screen.name)

    def btn_left(self):
        if self.edit:
            self.value = self.elements.prv()
            self.show()
        #else:
        #    raise Exception("item.btn_left() called in non edit mode. Screen:", self.screen.name)

    def re_init(self, elements, value):  # todo: if elements is None or empty ..
        """ if there is a value, the list is set to it
            if not, the value is set to the active (or first) element in the list
            should be called every time this items gets active
        """
        self.elements = CycleList.CyclicList()
        self.elements.add_all(elements)

        if value is None or value == pfm_signals.NOSIGYET:
            self.value = self.elements.active(activate_first_element=True)

        else:
            self.elements.activate_save(value)  # maybe its not there anymore
            self.value = value

CHAR_NO_INPUT = '\t'


class InputChar(object):
    """
    maintains a character - which can also be not
    """
    def __init__(self, x, y, char_list):    # todo: default char list
        self.x = x
        self.y = y
        self.no_input = True
        # self.panel = None -> do the printing here
        self.options = CycleList.CyclicList()
        for char in char_list:
            # if type(char) is not str or len(char) != 1:
            if type(char) is str and len(char) == 1:
            #   raise Exception("Char: {} is not a character!".format(char))
                self.options.add(char)

    def set(self, char):
        if char == CHAR_NO_INPUT:
            self.no_input = True
            #self.options.activate(' ')  # todo: what if space is not allowed!?!?!?!
        else:
            self.no_input = False
            self.options.activate_save(char)

    def get_current (self, jump_if_no_input=False):
        if self.no_input and not jump_if_no_input:
            return ' '

        if self.no_input and jump_if_no_input:
            self.options.activate(None) # reset the list
            self.no_input = False # no way back to no input ...

        return self.options.active(activate_first_element=True)


class ItemInputText(ItemSelectable):
    """
    Offers a collection of 'character items' to enter words or numbers
    The characters offered are given in char_list
    The length of the input is given by min and max.
    """
    def __init__(self, x, y, min, max, char_list=None):
        super(ItemInputText, self).__init__(x, y, "{}")

        self.min = min
        self.max = max
        self.edit_char = False
        self.current_char = None
        self.current_char_idx = None
        self.input = ""

        if char_list is None:
            char_list = CycleList.default_letter_list()
        self.chars = []
        for idx in xrange(max):
            char = InputChar(x, y, char_list)
            self.chars.append(char)
            x += 1

    def reset(self, current=None, show=True):
        """ resets values und active elements in the lists
        """
        self.input = current
        self.value = ""
        if current is None:
            for char in self.chars:
                char.set(CHAR_NO_INPUT)
                self.value += ' '
        else:
            if len(current) > self.max+1:
                logging.debug("'{}' will be cut to '{}'".format(current, current[:self.max]))
                self.value = current[:self.max]
            else:
                to_fill = self.max - len(current)
                if to_fill > 0:
                    logging.debug("fill {} with {} no_prints".format(current, to_fill))
                    for fill in xrange(to_fill):
                        current += CHAR_NO_INPUT
                else:
                    logging.debug("{} has exact the max right size of {}".format(current, self.max))

                for idx in xrange (len(self.chars)):
                    current_char = current[idx]
                    self.chars[idx].set(current_char)
                    if current_char == CHAR_NO_INPUT:
                        self.value += ' '
                    else:
                        self.value +=  self.chars[idx].get_current() # current_char

        if show:
            self.show()

    # def set_visibility(self, visible):
    #     for item in self.items:
    #         item.visible = visible


    def btn_pressed(self):
        if self.edit is False:    # switch to edit mode
            self.edit = True
            #self.deselect(True)   # show without highlight, keep selection in screen
            self.panel.print_normal(self.x, self.y, self.value)

            # self.edit_char = True
            # if self.current_char_idx is None:
            self.current_char_idx = 0  # we start with the first char, old positions are puzzling
            char = self.chars[self.current_char_idx]
            #self.panel.print_blinki(char.x, char.y, char.get_current(True))
            self.panel.print_inverse(char.x, char.y, char.get_current(True))
            # self.edit_char = True
        else:
            if self.edit_char:
                logging.debug("leave edit char")

                num_entered = 0
                for char in self.chars:
                    if char.no_input:
                        break
                    else:
                        num_entered += 1
                if num_entered >= self.min:
                    self.input_valid = True

                self.edit_char = False
                self.panel.inactivate_marked_text()
                char = self.chars[self.current_char_idx]
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == self.max - 1:  # last letter -> goto next item
                    self._leave()
                else:
                    logging.debug("YEAH1")
                    self.current_char_idx += 1
                    char = self.chars[self.current_char_idx]
                    self.panel.print_inverse(char.x, char.y, char.get_current())
            else:
                self.edit_char = True
                char = self.chars[self.current_char_idx]
                self.panel.print_blinki(char.x, char.y, char.get_current(True))

    def _leave(self):
        self.edit = False
        self.input = ""
        for char in self.chars:
            if not char.no_input:
                #print "'{}'".format(char.get_current())
                self.input += char.get_current()
            else:
                #print "NOOOOOOO INPUT"
                break
        self.value = self.input
        for i in xrange(self.max - len(self.value)):
            self.value += ' '

        #print "'{}'".format(self.value)
        #print "'{}'".format(self.input)

        self.screen.btn_right()


    def btn_right(self):

        # print "btn_right()", self.edit, self.edit_char

        if self.edit:
            char = self.chars[self.current_char_idx]
            if self.edit_char:
                self.panel.print_blinki(char.x, char.y, char.options.nxt())
            else:
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == self.max-1 or char.no_input:
                    self._leave()
                else:
                    self.current_char_idx += 1
                    char = self.chars[self.current_char_idx]

                    # print char.no_input, char.get_current(), char.no_input
                    self.panel.print_inverse(char.x, char.y, char.get_current())

                    # self.items[self.current_char].edit = False
                    # self.current_char += 1
                    # self.items[self.current_char].edit = True
                    # self.items[self.current_char].show()

        #else:
        #    raise Exception("ItemInputText.btn_right() called in non edit mode. Screen:", self.screen.name)


    def btn_left(self):
        # print "btn_left()", self.edit, self.edit_char

        if self.edit:
            char = self.chars[self.current_char_idx]
            if self.edit_char:
                self.panel.print_blinki(char.x, char.y, char.options.prv())
            else:
                self.panel.print_normal(char.x, char.y, char.get_current())

                if self.current_char_idx == 0:
                    self._leave()
                else:
                    self.current_char_idx -= 1
                    char = self.chars[self.current_char_idx]

                    # print char.no_input, char.get_current(), char.no_input
                    self.panel.print_inverse(char.x, char.y, char.get_current())

                    # self.items[self.current_char].edit = False
                    # self.current_char += 1
                    # self.items[self.current_char].edit = True
                    # self.items[self.current_char].show()

        #else:
        #    raise Exception("ItemInputText.btn_left() called in non edit mode. Screen:", self.screen.name)


def no_value_line(msg, val="(none)", length=21):
    """
    Created by stolle 2016-08-03
    :param msg:
    :param val:
    :param length:
    :return: "msg        val" of length length ...
    """
    all_len = len(msg) + len(val)
    if all_len <= length:
        #raise Exception ("together too long {} {}".format(msg, val))
        for i in xrange(length - all_len):
            msg += ' '
    return msg+val
