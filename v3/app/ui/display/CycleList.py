import string
import logging
import screens


class CyclicList(object):
    """maintaines a list to cycle through by keeping an active element"""

    def __init__(self):
        self.elements = []
        self.active_position = -1  # the index of the active element. If non is defined, next and prev will start
        # with 0

    def add(self, item, active=False, duplicate_check=True, type_check=True):
        """ adds a new element to the list.
            This can be given the actiive status. I there was another element active, it will loose its
            active state. There is no check for duplicate elements
        """
        if duplicate_check:
            if item in self.elements:
                #raise Exception("Item already in the list", item)
                pass

        if type_check:
            if len(self.elements) > 0:
                if type(self.elements[0]) != type(item):
                    #raise Exception("Item of type", type(item), "instead of", type(self.elements[0]))
                    pass

        self.elements.append(item)

        if active:
            # if self.active_position != -1:
            #    raise Exception("There can be only one active item in the cycle list!")
            # what for: just set this one to active and keep the system running?

            self.active_position = len(self.elements) - 1

    def add_all(self, new_elements, duplicate_check=True):
        for element in new_elements:
            self.add(element, duplicate_check=duplicate_check)

    def element_activated(self):
        if self.active_position == -1:
            return False
        else:
            return True

    """ returns the active element

        if there is no active or no elements in the list, None is returned
    """

    def active(self, activate_first_element=False):
        if len(self.elements) == 0:
            return None

        if self.active_position == -1:
            if activate_first_element:
                self.active_position = 0
            else:
                return None

        return self.elements[self.active_position]

    """ returns the next element
        returns None if the list is empty
        returns the first element, if there is no active element defined
    """

    def __nxt(self):
        size = self.size()

        if size == 0:
            return None

        if size == 1 or self.active_position == -1:
            self.active_position = 0
            return self.elements[0]

        if self.active_position == size - 1:
            self.active_position = 0
        else:
            self.active_position += 1

        return self.elements[self.active_position]

    def __prv(self):
        size = self.size()

        if size == 0:
            return None

        if size == 1 or self.active_position == -1:
            self.active_position = 0
            return self.elements[0]

        # more than one element
        if self.active_position == 0:
            self.active_position = size - 1
        else:
            self.active_position -= 1

        return self.elements[self.active_position]

    def nxt(self, steps=1):
        tmp = None
        for i in xrange(steps):
            tmp = self.__nxt()
        return tmp

    def prv(self, steps=1):
        tmp = None
        for i in xrange(steps):
            tmp = self.__prv()
        return tmp

    def size(self):
        return len(self.elements)

    def activate_save(self, element, log_if_fail=False):
        """ Same as activate(element), but does not throw an exception, when the element
            doesnt exist. In this case, the list is reseted as well as when element is None
        """
        self.active_position = -1
        for index, item in enumerate(self.elements):
            if element == item:
                self.active_position = index
                break

        if log_if_fail:
            if self.active_position == -1:
                logging.info ("{} is not in the list".format(element))

    def activate(self, element=None):
        """activates the given element
            throws a ValueException, if element is not in the list, so use it only for lists
            with elements you know for sure.
            If element is None, position will be reseted
        """
        if element is None:
            self.active_position = -1
        else:
            self.active_position = self.elements.index(element)


def default_letter_list():
    letters = []
    letters.extend(list(string.ascii_uppercase))
    letters.extend(list(string.ascii_lowercase))
    letters.append(' ')
    letters.extend(list_char_one_to_zero())
    letters.extend('-_.,;?!/()+=*#')

    return letters


def list_str_integer(start, stop, step=1, fmt="{}"):
    """

    :param start:
    :param stop:
    :param step:
    :param fmt:
    :return:
    """
    numbers = []
    for l in xrange(start, stop, step):
        numbers.append(fmt.format(l))  # stick with strings

    return numbers

def list_char_one_to_zero():
    """
    Created by stolle 2016-08-03

    wanted by Philipp
    :return: list of CHARACTERS: '1' '2' ... '0
    """
    numbers = list_str_integer(1, 10)
    numbers.append('0')

    return numbers



class CycleNumbers(CyclicList):
    def __init__(self, start, stop, steps=1, active=None):
        super(CycleNumbers, self).__init__()

        for num in xrange(start, stop, steps):
            self.add(num)

        if active is not None:
            self.activate(active)
