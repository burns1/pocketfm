#!/usr/bin/python

from pydispatch import dispatcher
import threading
import pfm_signals
from audiomanager import PFM_PULSE_SINK_NAME
import peak_detect
import logging

PFM_PULSE_SAMPLE_RATE = 60 # pulse sink sample rate for digital peak monitor

PFM_LED_POWERSTATE_LED = 4

PFM_LED_LEVELMETER_COLOUR = 3
PFM_LED_POWERSTATE_COLOUR_OFF = 1
PFM_LED_POWERSTATE_COLOUR_ON  = 2


class LED(object):
    def __init__(self):

        self.BBQuery = "\x1b?L"

        self.NumLEDs = 5
        self.NumLevelMeterLEDs = 4
        self.ColourBitsPerLED = 3
        self.SameCountLED = 0 # counts the number of consecutive identical LED states for level meter
        # array of integer values corresponding to the current colour map of the LED display
        self.ColourArray = [ 0 ] * self.NumLEDs

        self.LightMap = 0

        self.Brightness = 250

        self.Test = 1

        self.PulseMonitoring = False
        self.PulseMonitorThread = None

        dispatcher.connect(self.watch_audiomanager_input, signal=pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT, sender=dispatcher.Any)
        dispatcher.connect(self.watch_maxproboard_powerstate, signal=pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD, sender=dispatcher.Any)

        self.DigitalPeakMonitor = peak_detect.PeakMonitor(PFM_PULSE_SINK_NAME, PFM_PULSE_SAMPLE_RATE)

        dispatcher.send(pfm_signals.SIG_UPDATE_LED_LEVELMETER_COLOUR, self, PFM_LED_LEVELMETER_COLOUR)
        dispatcher.send(pfm_signals.SIG_UPDATE_LED_POWERSTATE_COLOUR_OFF, self, PFM_LED_POWERSTATE_COLOUR_OFF)
        dispatcher.send(pfm_signals.SIG_UPDATE_LED_POWERSTATE_COLOUR_ON, self, PFM_LED_POWERSTATE_COLOUR_ON)
        # initialise the PFM power led
        self.watch_maxproboard_powerstate(self, pfm_signals.get_value(pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD), pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD)

    def updateLightMap(self, map):
        if map != self.LightMap:
            self.LightMap = map
            dispatcher.send(pfm_signals.SIG_UPDATE_LED_LIGHTMAP, self, self.LightMap)

    def updateColours(self, colour):
        colourDiff = False
        for i,c in enumerate(self.ColourArray):
            if colour[i] != self.ColourArray[i]:
                 colourDiff = True
                 self.ColourArray[i] = colour[i]
        if colourDiff == True:
            dispatcher.send(pfm_signals.SIG_UPDATE_LED_COLOUR, self, self.ColourArray)

    def updateBrightness(self, brightness):
        if brightness != self.Brightness:
            self.Brightness = brightness
            dispatcher.send(pfm_signals.SIG_UPDATE_LED_BRIGHTNESS, self, self.Brightness)

    #  formats command string for breakout board based on 15 bit colour map (3 bit per LED) & 8 bit overall array brightness 
    def BBPlot(self, map, brightness):
        string = "\x1b"+str(map)+","+str(brightness)+"L"
        return string

    def printColourArray(self):
        dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, self.BBQuery)
        print('DEBUG: led.py: ColourArray {} LightMap {}'.format(self.ColourArray, bin(self.LightMap)))

    def plotColourArray(self, colour_array):
        if colour_array != self.ColourArray:
            # calculate bit array number
            lightmap = 0
            for i,led in enumerate(colour_array):
                lightmap = lightmap + (colour_array[i] << (self.ColourBitsPerLED * i))
            if lightmap != self.LightMap:
                dispatcher.send(pfm_signals.SIG_BREAKOUTBOARD_SEND_COMMAND, self, self.BBPlot(lightmap, self.Brightness))
                self.LightMap = lightmap
                dispatcher.send(pfm_signals.SIG_UPDATE_LED_LIGHTMAP, self, self.LightMap)
                self.ColourArray = colour_array
                dispatcher.send(pfm_signals.SIG_UPDATE_LED_COLOUR, self, self.ColourArray)
                self.SameCountLED = 0
        else:
            # this is a HACK to understand if an audio stream has failed, to attempt to restart the stream
            if pfm_signals.get_value(pfm_signals.SIG_UPDATE_AUDIOMANAGER_INPUT) == 'STREAM':
                # removed logic here to prevent log overflow, but keep counter in for now
                #if self.SameCountLED // PFM_PULSE_SAMPLE_RATE > 0:
                #    if self.SameCountLED % PFM_PULSE_SAMPLE_RATE == 0:
                #        logging.debug('Digital level LEDs are the same for {} sec(s)'.format(self.SameCountLED // PFM_PULSE_SAMPLE_RATE))
                #        #print('Digital level LEDs are the same for {} sec(s)'.format(self.SameCountLED // PFM_PULSE_SAMPLE_RATE))
                #        pass
                self.SameCountLED += 1

    def testplotColourArray(self):
        colmap = [0] * self.NumLEDs
        for i in range(self.Test, self.NumLEDs):
            colmap[i] = 3
        if self.Test == self.NumLEDs - 1:
            self.Test = 0
        else:
            self.Test = self.Test + 1
        self.plotColourArray(colmap)


    def handle_PlotPeaks(self, level, signal, sender):
        #print('DEBUG: led.py: handle_PlotPeaks {}'.format(level))
        colmap = [0] * self.NumLEDs
        scale = 1.2 #  TODO: fudging scale factor - review
        max_level = 5 # max voltage level from ADC (approximate)
        led_level = int(((level * self.NumLevelMeterLEDs)/max_level) * scale)
        if led_level > self.NumLevelMeterLEDs:
            led_level = self.NumLevelMeterLEDs
        for i in range(self.NumLevelMeterLEDs - led_level, self.NumLevelMeterLEDs):
            colmap[i] = pfm_signals.get_value(pfm_signals.SIG_UPDATE_LED_LEVELMETER_COLOUR)
        for j in range(self.NumLevelMeterLEDs, self.NumLEDs):
            colmap[j] = self.ColourArray[j]
        self.plotColourArray(colmap)

    def GetPeakByPulse(self):
        pulsescale = 5.0 / 127.0 # max sample peak of pulseaudio value is 127
        for sample in self.DigitalPeakMonitor:
            if self.PulseMonitoring == True:
                scaled = sample * pulsescale
                self.handle_PlotPeaks(scaled, 'Fake', self)
                # print('sample {} {}'.format(sample, scaled))
            else:
                break

    # source data to plot peak meter is taken from APC voltage level if source is analog in, else samples from pulseaudio callback
    def watch_audiomanager_input(self, audioinput, signal, sender):
        # print('DEBUG: led.py: watch_audiomanager_input {}'.format(audioinput))
        if audioinput == 'ANALOG':
            self.PulseMonitoring = False # flag to stop pulse audio thread
            dispatcher.connect(self.handle_PlotPeaks, signal=pfm_signals.SIG_UPDATE_ADC_VU1_VOLTAGE, sender=dispatcher.Any)
            dispatcher.connect(self.handle_PlotPeaks, signal=pfm_signals.SIG_UPDATE_ADC_VU2_VOLTAGE, sender=dispatcher.Any)
        else:
            dispatcher.disconnect(self.handle_PlotPeaks, signal=pfm_signals.SIG_UPDATE_ADC_VU1_VOLTAGE, sender=dispatcher.Any)
            dispatcher.disconnect(self.handle_PlotPeaks, signal=pfm_signals.SIG_UPDATE_ADC_VU2_VOLTAGE, sender=dispatcher.Any)
            if self.PulseMonitoring == False:
                self.PulseMonitoring = True
                self.PulseMonitorThread = threading.Thread(target=self.GetPeakByPulse)
                self.PulseMonitorThread.start()

    def watch_maxproboard_powerstate(self, powerstate, signal, sender):
        if pfm_signals.get_value(pfm_signals.SIG_UPDATE_POWER_MAXPROBOARD) == False:
            # print('DEBUG: led.py turning Maxpro light OFF')
            self.ColourArray[PFM_LED_POWERSTATE_LED] = pfm_signals.get_value(pfm_signals.SIG_UPDATE_LED_POWERSTATE_COLOUR_OFF)
        else:
            # print('DEBUG: led.py turning Maxpro light ON')
            self.ColourArray[PFM_LED_POWERSTATE_LED] = pfm_signals.get_value(pfm_signals.SIG_UPDATE_LED_POWERSTATE_COLOUR_ON)

