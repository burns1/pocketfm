#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-

import dbus
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject

def msg_cb(bus, msg):
    args = msg.get_args_list()
    print "Notification from '%s'" % args[0]
    print "Summary: %s" % args[3]
    print "Body: %s", args[4]

if __name__ == '__main__':
    # DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()

    string = "interface='org.freedesktop.Notifications',member='Notify',eavesdrop='true'"
    bus.add_match_string(string)
    bus.add_message_filter(msg_cb)

    mainloop = GObject.MainLoop()
    mainloop.run ()
