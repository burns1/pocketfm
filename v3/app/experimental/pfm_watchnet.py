#!/usr/bin/python
#
# Copyright (c) 2015 iAchieved.it LLC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
##

import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
from threading import Thread
import NetworkManager
import os

d_args = ('sender', 'destination', 'interface', 'member', 'path')
d_args = dict([(x + '_keyword', 'd_' + x) for x in d_args])

Devices = {}

def main():
    NetworkManager.NetworkManager.connect_to_signal('DeviceAdded', device_add_remove, **d_args)
    NetworkManager.NetworkManager.connect_to_signal('DeviceRemoved', device_add_remove, **d_args)

    for dev in NetworkManager.NetworkManager.GetDevices():
        dev.connect_to_signal('StateChanged', device_state_change, **d_args)
        connectionType = type(dev.SpecificDevice()).__name__
        Devices[dev.object_path] = {}
        Devices[dev.object_path]["type"] = connectionType
        if NetworkManager.const('device_state',dev.State) == "activated":
          Devices[dev.object_path]["active"] = True

    _loop = GObject.MainLoop()
    t = Thread(target=_loop.run)
    t.start()

def device_add_remove(*args, **kwargs):
    msg = kwargs['d_member']
    if msg == "DeviceAdded":
        # Argument will be the device, which we want to monitor now
        args[0].connect_to_signal('StateChanged', device_state_change, **d_args)
        return
    if msg == "DeviceRemoved":
        print "Device removed:  %s" % (args[0].object_path)
        if args[0].object_path in Devices:
            del args[0].object_path

def device_state_change(*args, **kwargs):
    msg  = kwargs['d_member']
    path = kwargs['d_path']

    device   = NetworkManager.Device(path)
    newState = NetworkManager.const('device_state', args[0])

    try:
        connectionType = type(device.SpecificDevice()).__name__
    except:
        # D-Bus likely doesn't know about the device any longer,
        # this is typically a removable Wifi stick
        path = kwargs['d_path']
        if path in Devices:
            connectionType = Devices[path]["type"]

    if newState == "activated":
        path = kwargs['d_path']
        Devices[path] = {"type":  connectionType, "active": True}
    print('Device {}: {}'.format(newState, NetworkManager.Device(path).Interface))

if __name__ == '__main__':
    main()

