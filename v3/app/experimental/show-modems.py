#!/usr/bin/env python
import dbus

bus = dbus.SystemBus()

modem_proxy = bus.get_object("org.freedesktop.ModemManager1", "/org/freedesktop/ModemManager1/Modem/0")


modem_prop_iface = dbus.Interface(modem_proxy, "org.freedesktop.DBus.Properties") 

sim = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Sim")
bearers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Bearers")
supported_capabilities = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "SupportedCapabilities")
current_capabilities = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "CurrentCapabilities")
max_bearers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "MaxBearers")
max_active_bearers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "MaxActiveBearers")
manufacturer = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Manufacturer")
model = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Model")
revision = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Revision")
device_identifier = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "DeviceIdentifier")
device = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Device")
drivers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Drivers")
plugin = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Plugin")
primary_port = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "PrimaryPort")
ports = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Ports")
bearers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "Bearers")
equipment_identifier = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "EquipmentIdentifier") # IMEI
unlock_required = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "UnlockRequired") # SIM PIN code needed
unlock_retries = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "UnlockRetries") # Wrong SIM PIN codes
state = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "State")
state_failed_reason = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "StateFailedReason")
access_technologies = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "AccessTechnologies")
signal_quality = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "SignalQuality")
own_numbers = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "OwnNumbers")
power_state = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem", "PowerState")

print('---- modem.0')
print('Manufacturer {}, Model {}, Revision {} IMEI {}'.format(manufacturer, model, revision, equipment_identifier))
print('Connected via {}, primary port {}'.format(device, primary_port))
# print('Connected via {}, primary port {} exposing {}'.format(device, primary_port, ports))
print('SIM path ({})'.format(sim)) # No SIM: '/'
print('Device numeric state ({})'.format(state)) # No SIM: -1, Inactive with SIM: 8, Active with SIM: 11

print('Bearers ({})'.format(bearers)) # No SIM: dbusArray = [], Inactive with SIM: dbusArray = []
# print('Bearers ({})'.format(bearers[0])) # No SIM: Index Error exception as dbusArray = []
# print('Signal Quality ({})'.format(signal_quality)) 
# with no SIM card, Modem3gpp is not exposed!

if sim != '/':
    print('---- m3gpp')
    m3gpp_imei = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem.Modem3gpp", "Imei")
    m3gpp_registration_state = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem.Modem3gpp", "RegistrationState")
    m3gpp_operator = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem.Modem3gpp", "OperatorName")
    m3gpp_operator_code = modem_prop_iface.Get("org.freedesktop.ModemManager1.Modem.Modem3gpp", "OperatorCode")
    
    print('Operator {} ({}), Registration State {}'.format(m3gpp_operator, m3gpp_operator_code, m3gpp_registration_state))
    
    print('---- Sim')
    sim_proxy = bus.get_object("org.freedesktop.ModemManager1", sim)
    sim_prop_iface = dbus.Interface(sim_proxy, "org.freedesktop.DBus.Properties")
    
    sim_identifier = sim_prop_iface.Get("org.freedesktop.ModemManager1.Sim", "SimIdentifier")
    imsi = sim_prop_iface.Get("org.freedesktop.ModemManager1.Sim", "Imsi")
    operator_identifier = sim_prop_iface.Get("org.freedesktop.ModemManager1.Sim", "OperatorIdentifier")
    operator_name = sim_prop_iface.Get("org.freedesktop.ModemManager1.Sim", "OperatorName")
    
    print('SIM id {}, imsi {}, operator id {}, operator name ({})'.format(sim_identifier, imsi, operator_identifier, operator_name))

    if state == 11: # No SIM: -1, Inactive with SIM: 8, Active with SIM: 11
        # we should have a bearer, so let's query it
        bearer_proxy = bus.get_object("org.freedesktop.ModemManager1", bearers[0])
        bearer_prop_iface = dbus.Interface(bearer_proxy, "org.freedesktop.DBus.Properties")
        bearer_interface = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Interface")
        bearer_connected = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Connected")
        bearer_suspended = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Suspended")
        bearer_ip4_config = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Ip4Config")
        bearer_ip6_config = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Ip6Config")
        bearer_ip_timeout = bearer_prop_iface.Get("org.freedesktop.ModemManager1.Bearer", "Ip6Config")
        print('---- Bearer')
        print('Device Interface: {}'.format(bearer_interface))
        print('Connected {}'.format(bearer_connected))
        print('Suspended {}'.format(bearer_suspended))
        print('IP4 Config {}'.format(bearer_ip4_config))
        print('IP6 Config {}'.format(bearer_ip6_config))

