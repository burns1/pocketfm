#!/usr/bin/python
# -*- coding: utf-8-unix; python-indent: 4; -*-
"""
Display detailed information about currently active connections.
"""
from threading import Thread
import dbus.mainloop.glib; dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
from gi.repository import GObject
import NetworkManager
import socket
from pydispatch import dispatcher
import json

PFM = True
if PFM == True:
    import logging
    import pfm_signals
    import pfm_config

PFM_WIFI_MODE_OFF ='OFF'
PFM_WIFI_MODE_CLIENT = 'CLIENT MODE'
PFM_WIFI_MODE_AP = 'ACCESS POINT'
PFM_CONNECTIONMANAGER_WIFI_MODE_LIST = [PFM_WIFI_MODE_AP, PFM_WIFI_MODE_CLIENT, PFM_WIFI_MODE_OFF]

# default passwords mandated ... *sigh*
PFM_WIFI_AP_DEFAULT_PASSWORD = '12345678'
PFM_WIFI_CLIENT_DEFAULT_PASSWORD = '12345678'

PFM_WIFI_STATUS_ERROR = '(error)'
PFM_WIFI_STATUS_WAIT = '(wait...)'
PFM_WIFI_STATUS_OFF = '(off)'

# ui state messages for WIFI client mode
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'] =                       'No network selected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NET_NOT_FOUND'] =                         'Network not found'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi client connected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Connecting ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'WiFi disconnected'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_SECONDARIES]  = 'Checking ...'
PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS[NetworkManager.NM_DEVICE_STATE_NEED_AUTH]    = 'Password not accepted'

# ui state messages for AP mode
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS  = {}
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS['PFM_PLEASE_WAIT'] =                           'Please wait ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_ACTIVATED]    = 'WiFi AP active'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DISCONNECTED] = 'WiFi AP disconnected'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_CONFIG]       = 'Configuring ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CONFIG]    = 'Getting IP config ...'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_IP_CHECK]     = 'IP check ...'

PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_FAILED]       = 'AP Failed'
PFM_CONNECTIONMANAGER_WIFI_AP_STATUS[NetworkManager.NM_DEVICE_STATE_DEACTIVATING] = 'AP Deactivating'


# for dbus connect_to_signal()
d_args = ('sender', 'destination', 'interface', 'member', 'path')
d_args = dict([(x + '_keyword', 'd_' + x) for x in d_args])

class PFMWiFiDevice(Thread):
    def __init__(self, interface):

        super(PFMWiFiDevice, self).__init__()

        self.WiFiDevice = None
        self.WiFiDeviceState = None
        # Intialize hooks into NM DBus interface
        for dev in NetworkManager.NetworkManager.GetDevices():
            if dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_WIFI:
                print('Debug: Found WiFi Device {}'.format(NetworkManager.Device(dev).Interface))                
                if NetworkManager.Device(dev).Interface == interface:
                    self.WiFiDevice = interface
                    print('Debug: Found my WiFi device {}'.format(self.WiFiDevice))
                    res = dev.SpecificDevice().connect_to_signal('StateChanged', self.handle_device_state_change, **d_args)
                    print('Debug: Registered device {} for dbus NM state change. Current NM device state: {} ({})'.format(self.WiFiDevice, NetworkManager.const('device_state',NetworkManager.Device(dev).State), NetworkManager.Device(dev).State))
                    self.WiFiDeviceState = NetworkManager.Device(dev).State
                    res = dev.SpecificDevice().connect_to_signal('AccessPointAdded', self.handle_available_ap_list_change, **d_args)
                    print('Debug: Registered {} for dbus NM available AP added to list'.format(self.WiFiDevice))
                    res = dev.SpecificDevice().connect_to_signal('AccessPointRemoved', self.handle_available_ap_list_change, **d_args)
                    print('Debug: Registered {} for dbus NM available AP removed from list'.format(self.WiFiDevice))
        if self.WiFiDevice == None:
            print('Debug: Error!! PFMWiFiDevice {} not found!!'.format(interface))

        # WiFi Interface Mode: (currently Client Mode, AP Mode, or Off)
        if PFM == True:
            self.WiFiCurrentMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_MODE, PFM_WIFI_MODE_OFF)
        else:
            self.WiFiCurrentMode = PFM_WIFI_MODE_CLIENT
        self.WiFiAPActive = False
        self.WiFiAPDevice = interface
        # self.WiFiAPSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_NETWORK, socket.gethostname())
        self.WiFiAPSSID = socket.gethostname()

        self.WiFiClientActive = False
        self.WiFiClientDevice = interface
        #  self.WiFiClientSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, None)
        self.WiFiClientSSID = 'AndroidAP' #  TEST!!
        if self.WiFiClientSSID == None:
            dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'])
            self.WiFiClientPassword = PFM_WIFI_CLIENT_DEFAULT_PASSWORD


        if PFM == True:
            #  Initialize PFM configuration
            self.WiFiCurrentMode = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_MODE, PFM_WIFI_MODE_OFF)
            #  Access Point Mode
            self.WiFiAPActive = False
            self.WiFiAPDevice = 'wlan0'
            self.WiFiAPSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_NETWORK, socket.gethostname())
            self.WiFiAPPassword = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_AP_PASSWORD, PFM_WIFI_AP_DEFAULT_PASSWORD)
            #  Client Mode
            self.WiFiClientActive = False
            self.WiFiClientDevice = 'wlan0'
            self.WiFiClientSSID = pfm_signals.get_value_with_default(pfm_signals.SIG_SET_WIFI_CLIENT_NETWORK, None)
            print('Debug: WiFiClientSSID = {}'.format(self.WiFiClientSSID))
            if self.WiFiClientSSID == None:
                dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_CLIENT_STATUS, self, PFM_CONNECTIONMANAGER_WIFI_CLIENT_STATUS['PFM_NO_NET_SELECTED'])
                self.WiFiClientPassword = PFM_WIFI_CLIENT_DEFAULT_PASSWORD

        self.WiFiAvailableNetworks = []
        self.PFM_update_available_aps()



    def PFM_update_available_aps(self):
         new_ap_list = []
         for dev in NetworkManager.NetworkManager.GetDevices():
                 if dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_WIFI:
                     # print('Debug: Found WiFi Device {}'.format(NetworkManager.Device(dev).Interface))                
                     if NetworkManager.Device(dev).Interface == self.WiFiDevice:
                         # print('Debug: Found my WiFi device {}'.format(self.WiFiDevice))
                         ap_report = ''
                         for ap in dev.SpecificDevice().GetAccessPoints():
                             # selective addition of APs based on WPA or other criteria can occur here ...
                             new_ap_list.append(ap.Ssid)
                             ap_report += "{:>35}: {}MHz {}bits/sec Flags: {} WPA flags: {} RSN flags: {} Signal Strength {}% \n".format(ap.Ssid, ap.Frequency, ap.MaxBitrate, ap.Flags, ap.WpaFlags, ap.RsnFlags, ord(ap.Strength))
                         # print('unfiltered new ap list {}'.format(new_ap_list))
                         new_ap_list = list(set(new_ap_list))
                         if self.WiFiAvailableNetworks != new_ap_list:
                             self.WiFiAvailableNetworks = new_ap_list
                             # print("==UPDATE=RAW AP LIST")
                             # print(ap_report)
                             print('deduped new ap list self.WiFiAvailableNetworks {}'.format(self.WiFiAvailableNetworks))
                             print("===================")
                             if PFM == True:
                                 dispatcher.send(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, self, self.WiFiAvailableNetworks)


    def handle_device_state_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        newState = NetworkManager.const('device_state', args[0])
        oldState = NetworkManager.const('device_state', args[1])
        print('Debug: ******************** ENTERED DEVICE STATE CHANGE SIGNAL')
        print("Received signal: %s:%s" % (kwargs['d_interface'], kwargs['d_member']))
        print("Sender:          (%s)%s" % (kwargs['d_sender'], kwargs['d_path']))
        print("Network Device: {}".format(device.Interface))
        print("Arguments:       (%s)" % ", ".join([str(x) for x in args]))
        if device.Interface == self.WiFiDevice:
            print('*****************************Debug: {} interface state changed from {} to {}'.format(self.WiFiDevice, oldState, newState))
            self.WiFiDeviceState = newState

    def handle_available_ap_list_change(self, *args, **kwargs):
        msg  = kwargs['d_member']
        path = kwargs['d_path']
        device   = NetworkManager.Device(path)
        # print("Received signal: %s:%s" % (kwargs['d_interface'], kwargs['d_member']))
        # print("Sender:          (%s)%s" % (kwargs['d_sender'], kwargs['d_path']))
        # print("Network Device {}".format(device.Interface))
        # print("Arguments:       (%s)" % ", ".join([str(x) for x in args]))
        if msg == 'AccessPointRemoved':
            # idea: try proxy to see if this does not throw exception
            # print('An Available Access Point {} disappeared from {}'.format(device.Interface, args[0].Ssid))
            print('An Available Access Point disappeared from {}'.format(device.Interface))
            # print('Access Point SSID \'{}\' removed'.format(args[0].Ssid, args[0].HwAddress))
        if msg == 'AccessPointAdded':
            print('An Available Access Point SSID \'{}\' MAC {} found on {}'.format(args[0].Ssid, args[0].HwAddress, device.Interface))
            if self.WiFiCurrentMode == PFM_WIFI_MODE_CLIENT:
                if self.WiFiClientSSID == args[0].Ssid:
                    print('Debug: we are in client mode, current SSID has just been seen and added to AP list')
        self.PFM_update_available_aps()


class PFMConnections():
    def __init__(self):
        # Read in known, saved, NetworkManager connections, settings and secrets
        self.KnownNMConnections = {}
        for conn in NetworkManager.Settings.ListConnections():
            settings = conn.GetSettings()
            print('Retrieving known connection {}'.format(settings['connection']['id']))
            secrets = {}
            try:
                secrets = conn.GetScrets()
            except Exception, e:
                pass
            for key in secrets:
                settings[key].update(secrets[key])
            self.KnownNMConnections[settings['connection']['id']] = settings

        # self.print_known_connections(self.KnownNMConnections)

        # Read in all currently active network connections
        self.ActiveNMConnections = {}
        for conn in NetworkManager.NetworkManager.ActiveConnections:
            settings = conn.Connection.GetSettings()
            print('Retrieving active connection {}'.format(settings['connection']['id']))
            for s in list(settings.keys()):
                if 'data' in settings[s]:
                    settings[s + '-data'] = settings[s].pop('data')
            secrets = {}
            try:
                secrets = conn.Connection.GetSecrets()
                for key in secrets:
                    settings[key].update(secrets[key])
            except Exception, e:
                pass
            self.ActiveNMConnections[settings['connection']['id']] = settings

        # print self.ActiveNMConnections
        # self.print_active_connections(self.ActiveNMConnections)

    def handle_active_connection_change(self, *args, **kwargs):
        pass


    def print_known_connections(self,saved_connections):
        print ("# CURRENT SAVED NETWORK MANAGER PROFILES")
        print ("  Number of saved NM connections {}".format(len(saved_connections)))
        for conn in saved_connections:
            print("------------- ID    {}".format(saved_connections[conn]['connection'].get('id')))
            print("------------- TYPE  {}".format(saved_connections[conn]['connection'].get('type')))
            print("------------- IFACE {}".format(saved_connections[conn]['connection'].get('interface-name')))
            # for wifi connections
            if saved_connections[conn]['connection'].get('type') == '802-11-wireless':
                print("------------- SSID  {}".format(saved_connections[conn]['802-11-wireless'].get('ssid')))
                if saved_connections[conn]['802-11-wireless'].get('security') == '802-11-wireless-security':
                    print("------------- WSEC  {}".format(saved_connections[conn]['802-11-wireless'].get('security')))
                    # for wifi wpa-psk
                    if saved_connections[conn]['802-11-wireless-security'].get('key-mgmt') == 'wpa-psk':
                        print("------------- PSK   {}".format(saved_connections[conn]['802-11-wireless-security'].get('psk')))
            # for ethernet connections
            elif saved_connections[conn]['connection'].get('type') == '802-3-ethernet':
                    print("------------- MAC   {}".format(saved_connections[conn]['802-3-ethernet'].get('mac-address')))
            # for GSM/3G connections
            elif saved_connections[conn]['connection'].get('type') == 'gsm':
                    print("------------- APN   {}".format(saved_connections[conn]['gsm'].get('apn')))
                    print("------------- USER  {}".format(saved_connections[conn]['gsm'].get('user')))
                    print("------------- PASS  {}".format(saved_connections[conn]['gsm'].get('password')))
            else:
                print(json.dumps(saved_connections[conn], indent = 4))
            print

    def print_active_connections(self, active_connections):
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        print ('@ CURRENT ACTIVE NETWORK MANAGER PROFILES')
        print ('@ Number of ACTIVE NM connections {}'.format(len(active_connections)))
        for conn in active_connections:
            # print("------------- STRUCT    {}".format(active_connections[conn]))
            print("@ {:20} {:20} {:20} {:20}".format(active_connections[conn]['connection'].get('id'), 
                active_connections[conn]['connection'].get('type'), 
                active_connections[conn]['connection'].get('interface-name'),
                active_connections[conn]['connection'].get('uuid')))
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')


def test_print_active_profiles():
    print ("@@CURRENT ACTIVE NETWORK MANAGER PROFILES")
    print ("  Number of ACTIVE NM connections {}".format(len(NetworkManager.NetworkManager.ActiveConnections)))
    active_connections = {}
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        settings = conn.Connection.GetSettings()
        for s in list(settings.keys()):
            if 'data' in settings[s]:
                settings[s + '-data'] = settings[s].pop('data')
        secrets = {}
        try:
            secrets = conn.Connection.GetSecrets()
            for key in secrets:
                settings[key].update(secrets[key])
        except Exception, e:
            pass
        devices = ""
        if conn.Devices:
            devices = " (on %s)" % ", ".join([x.Interface for x in conn.Devices])
        print('Active connection: {}{}'.format(settings['connection']['id'], devices))
        size = max([max([len(y) for y in x.keys() + ['']]) for x in settings.values()])
        format = "      %%-%ds %%s" % (size + 5)
        for key, val in sorted(settings.items()):
            print("   %s" % key)
            for name, value in val.items():
                print(format % (name, value))
        for dev in conn.Devices:
            print("Device: %s" % dev.Interface)
            try:
                print('   Type             {}'.format(NetworkManager.const('device_type', dev.DeviceType)))
                devicedetail = dev.SpecificDevice()
                if not callable(devicedetail.HwAddress):
                    print('   MAC address      {}'.format(devicedetail.HwAddress))
                print('   IPv4 config')
                print('      Addresses')
                try:
                    for addr in dev.Ip4Config.Addresses:
                        print('         {}'.format(addr[0]))
                    print('      Routes')
                    for route in dev.Ip4Config.Routes:
                        print('         {}'.format(route))
                    print('      Nameservers')
                    for ns in dev.Ip4Config.Nameservers:
                        print('         {}'.format(ns))
                except AttributeError:
                    print('No IP Address details available for {}'.format(dev.Interface))
            except ValueError:
                print('   Type             {}'.format(dev.DeviceType))

def register_all_wifi_devices():
    # register each wifi device for available network list change signals
    for dev in NetworkManager.NetworkManager.GetDevices():
        if dev.DeviceType != NetworkManager.NM_DEVICE_TYPE_WIFI:
            continue
        res = dev.SpecificDevice().connect_to_signal('AccessPointAdded', accesspointlist_change, **d_args)
        print('res = {}'.format(res))
        res = dev.SpecificDevice().connect_to_signal('AccessPointRemoved', accesspointlist_change, **d_args)
        print('res = {}'.format(res))
        print("------------------{} - {}".format(dev.Interface, len(dev.SpecificDevice().GetAccessPoints())))
        for ap in dev.SpecificDevice().GetAccessPoints():
            print('{:>35}: {}MHz {}bits/sec Flags: {} WPA flags: {} RSN flags: {} Signal Strength {}%'.format(ap.Ssid, ap.Frequency, ap.MaxBitrate, ap.Flags, ap.WpaFlags, ap.RsnFlags, ord(ap.Strength)))

# register_all_wifi_devices()
testCon = PFMConnections()

testWiFi = PFMWiFiDevice('wlan0')

_loop = GObject.MainLoop()
t = Thread(target=_loop.run)
t.start()

testCon.print_active_connections(testCon.ActiveNMConnections)
test_print_active_profiles()

