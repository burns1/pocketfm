from functools import wraps
from flask import Flask, request, Response, jsonify
from flask_sslify import SSLify
import json
import time
import subprocess

sslcert    = 'ssl_cert.pem'
sslkey     = 'ssl_key.pem'
username   = 'testuser'
password   = 'testpassword'
ip         = "0.0.0.0"
port       = 8443
debug      = True

app = Flask(__name__)
#https://github.com/kennethreitz/flask-sslify
sslify = SSLify(app, age=100, subdomains=True)

def check_auth(u, p):
    """This function is called to check if a username /
    password combination is valid.
    """
    return u == username and p == password

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@app.route('/')
@requires_auth
def index():
    return "Index"

def error(reason='unknown'):
    print 'error', reason
    return json.dumps({
            'success': 'false',
            'reasons': reason
            })

@app.route('/setparameter', methods=['GET', 'POST'])
@requires_auth
def setparameter():
    print "request", request.form
    print dir(request)

    #https://stackoverflow.com/questions/13279399/
    id = request.values.get('id','')
    value = request.values.get('value','')

    if not id or not value:
        return error("missing at least id and value")

    time.sleep(2)
    print "set \"%s\" to \"%s\""%(id, value)
    return json.dumps({'success': 'success', 'authors': "123"})





def mstime():
    return int(round(time.time() * 1000))

def satinfo():
    """$ femon -H -c 1
    FE: Philips TDA10086 DVB-S (DVBS)
    Problem retrieving frontend information: Resource temporarily unavailable
    status       | signal  62% | snr  71% | ber 0 | unc -1096239540 |
    """
	#output = subprocess.check_output("femon -H -c 1", stderr=subprocess.STDOUT, shell=True)
    output = "status       | signal  62% | snr  71% | ber 0 | unc -1096239540 |"
    items = {}
    for i in output.split('|'):
        e = i.split()
        if (len(e) == 2):
            items[e[0]] = e[1]

    return items

# These
pollroute = {
    'time': mstime,
    'satinfo': satinfo
}
@app.route('/pollparameter', methods=['GET', 'POST'])
@requires_auth
def pollparameter():
    id = request.values.get('id','')

    if not id:
        return error("missing at least id")

    if id not in pollroute:
        return error("no poll by that id", id)

    value = pollroute[id]()
    print "poll \"%s\" to \"%s\""%(id, value)
    return json.dumps({'success': 'success', 'value': value})


if __name__ == '__main__':
    app.run(ip, port=port, debug=debug, ssl_context=(sslcert,sslkey))
