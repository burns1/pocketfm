from flask import Flask, request, Response, jsonify
import time, subprocess, shlex
import ConfigParser
import json
import os
import collections


# ip and port shouldn't change
ip		 = "127.0.0.1"
port	 = 10012
# gpio pins shouldn't change
RST_LINE = 24
LED_TX   = 12

configfile = "pfm.conf"

config = ConfigParser.ConfigParser().read(configfile)

debug      = config.getboolean('global', 'debug')
tx_freq    = config.getfloat('transmitter', 'freq')
tx_rds_id  = config.get('transmitter', 'station_name')
tx_input   = config.get('transmitter', 'input').upper()
tx_power   = config.getint('transmitter', 'power')




# If not arm then put in development mode (fake lcd, gpio, etc)
if os.uname()[4][:3] != 'arm':
	from mockpfm import MockAdafruit_Si4713 as Adafruit_Si4713, MockGPIO as _GPIO
	GPIO = _GPIO(dokeyboard=False)
else:
	from Adafruit_Si4713 import Adafruit_Si4713
	import RPi.GPIO as GPIO


src_tx_pairs = collections.OrderedDict([
	("AUX", 'analog'),
	("SAT", 'digital'),
	("USB", 'digital'),
	("INT", 'digital')
])

class RadioControl(Adafruit_Si4713):

	# match input sources to analog/digital modes
	# as specified in settings.py
	src_tx_pairs = dict(src_tx_pairs)

	def __init__(self, rst_pin, led_pin, src, fm_freq, fm_rds, fm_power):

		Adafruit_Si4713.__init__(self, resetpin=rst_pin)

		# derive corrent TX mode
		self._tx_mode = self.src_tx_pairs[src]
		self._tx_src  = src

		# get frequency in the right format
		self._tx_freq = int(fm_freq * 100)

		# TODO: truncate?
		self._tx_rds = fm_rds

		self._tx_power = fm_power

		# GPIO setup
		self._led = led_pin
		self.gpio_setup()
		self.startup()

		# start up radio in right mode
		self.startup()

	def gpio_setup(self):
		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)

		GPIO.setup(self._led, GPIO.OUT)

		# turn off by default
		GPIO.output(self._led, GPIO.LOW)

	def tx_led_on(self):
		GPIO.output(self._led, GPIO.HIGH)

	def tx_led_off(self):
		GPIO.output(self._led, GPIO.LOW)

	def set_tx_freq(self, new_freq):
		# get frequency in the right format
		self._tx_freq = int(new_freq * 100)

		config.set('transmitter', 'freq', str(new_freq))
		with open(configfile, 'wb') as fd:
			config.write(fd)

		self.tuneFM(self._tx_freq)

	def get_tx_freq(self):
		self.tx.readTuneStatus()
		return int(self.tx.currFreq)/100


	def startup(self):

		self.tx_led_off()

		# power up sequence
		if not self.begin(self._tx_mode):
			print "Si4713 start up failed."

		else:

			# turn on tx led
			self.tx_led_on()

			# adjust settings
			self.setTXpower(self._tx_power)
			self.tuneFM(self._tx_freq)

			self.beginRDS()

			self.setRDSstation(self._tx_rds)


	def status(self):
		"""Some functions to test the info and status features of the si chips
		   not fully integrated yet"""

		print """
		TX_TUNE_STATUS
		Queries the status of a previously sent TX Tune Freq, TX Tune
		Power, or TX Tune Measure command.
		"""

		self.tx.readTuneStatus()

		print "freq:", self.tx.currFreq,
		print "eg. 10010 = 100.10"

		print "power:", self.tx.currdBuV,
		print "eg. 118 dBuV"

		print "antenna capacitance:", self.tx.currAntCap,
		print "eg. 1. (0-191). Did not change we we disconnected antenna"

		print "Noise level:", self.tx.currNoiseLevel,
		print "eg. 0"

		print """
		TX_ASQ_STATUS
		Queries the TX status and input audio signal metrics.
		"""

		self.tx.readASQ()

		print "ASQ:", self.tx.currASQ,
		print "eg. 1 or 5. No idea why it changes when we change volume"

		print "inLevel:", self.tx.currInLevel,
		print "eg. -55 to -6. Changes with volume level"

		print """
		TX_TUNE_MEASURE
		Measure the received noise level at the specified frequency.
		I dont know what this is for actually
		"""

		self.tx.readTuneMeasure(20020)

	def get_active_input(self):
		return self._tx_src

	def get_active_mode(self):
		return self._tx_mode

	def adapt_to_input(self, new_src):
		# check if tx_mode has to be changed
		new_tx_mode = self.src_tx_pairs[ new_src.upper() ]
		if new_tx_mode != self.get_active_mode():

			# store new mode
			self._tx_mode = new_tx_mode
			self._tx_src = new_src.upper()

			config.set('transmitter', 'input', new_src.upper())
			with open(configfile, 'wb') as fd:
				config.write(fd)

			# start up in right mode
			self.startup()


tx = RadioControl(rst_pin=RST_LINE, led_pin=LED_TX, src=tx_input, fm_freq=tx_freq, fm_rds=tx_rds_id, fm_power=tx_power)

#
# API
#
# see tools/radio_test.sh for some test commends
# As agreed, in future we would not use JSON for control and instead interact
# through controll scripts/commends

app = Flask(__name__)

@app.errorhandler(404)
def not_found():
	return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/input/<action>', methods=['POST'])
def input(action='get'):
	"""input is SAT, AUX, USB, INT"""
	if action == 'set':
		if not request.json:
			return not_found()
		new_input = request.json.get('input','')
		print "set input", new_input
		tx.adapt_to_input(new_input)
	else:
		print "get input", tx.get_active_input()

	return json.dumps({'input': tx.get_active_input(),'success': 'success'})


@app.route('/frequency/<action>', methods=['POST'])
def frequency(action='get'):
	"""frequency (via json) is float"""
	if action == 'set':
		if not request.json:
			return not_found()
		frequency = request.json.get('frequency','')
		print "set frequency", frequency
		tx.set_tx_freq(int(frequency))
	else:
		print "get frequency", tx.get_tx_freq()

	return json.dumps({'frequency': tx.get_tx_freq(), 'success': 'success'})


if __name__ == '__main__':

	app.run(ip, port=port, debug=debug)
