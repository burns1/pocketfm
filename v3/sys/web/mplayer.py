# This attempts to replace the v2 MPlayerControl in PFMTools with a daemon
# that accepts commands via network (with JSON).

# Not sure if this is the best route. An attempt to seperate webui and other
# daemons.

from flask import Flask, request, Response, jsonify, make_response
import time, subprocess, shlex
import ConfigParser
import json
import os

# ip and port shouldn't change
ip         = "127.0.0.1"
port       = 10011
#
configfile = "pfm.conf"


config = ConfigParser.ConfigParser().read(configfile)

debug  = config.getboolean('global', 'debug')
mode   = config.get('mplayer', 'mode').upper()

basic_cmd = "mplayer -ao pulse -srate 48000 -really-quiet -vo null -nolirc -framedrop -loop 0 "

class MPlayerControl(object):

        def __init__(self, silence_file, usb_playlist, sat_playlist, int_playlist):
                self.silence_cmd = silence_file
                self.usb_cmd = '-playlist ' + usb_playlist
                self.sat_cmd = '-playlist ' + sat_playlist
                self.int_cmd = '-playlist ' + int_playlist

                # bring up the process and play back silence
                try:
                    if debug:
                        # FLASH debug mode restarts process, creating 2 mplayer proc
                        # this is a dirty way to avoid that. not required in production
                        subprocess.Popen(['killall', 'mplayer'])
                    print "cmd: "+basic_cmd + self.silence_cmd
                    self.p = subprocess.Popen( shlex.split( basic_cmd + self.silence_cmd ) )
                except:
                    print "can not start mplayer"

                self._mode = mode # 'AUX' # AUX = silence for digital inputs

        def play(self, mode):
                mode = mode.upper()
                if mode == self._mode:

                # do nothing when mode is not changed
                        return True

                else:

                # update flag
                    self._mode = mode
                    with open(configfile, 'wb') as fd:
                        config.write(fd)

                # kill existing processes
                if self.p.poll() is None:
                        self.p.kill()

                # bring up again
                if mode == 'SAT':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.sat_cmd ) )

                elif mode == 'USB':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.usb_cmd ) )
                elif mode == 'INT':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.int_cmd ) )

                # play back silence during aux for better audio transitions
                elif mode == 'AUX':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.silence_cmd ) )


        def mute(self):

                self.play('AUX')

cwd = os.path.abspath(os.path.dirname(__file__))
mpl = MPlayerControl(silence_file=cwd+'/silence.mp3', usb_playlist=cwd+'/usb.playlist', sat_playlist=cwd+'/sat.playlist', int_playlist=cwd+'/int.playlist')

#
# API
#
# see tools/mplayer_test.sh for some test commends
# As agreed, in future we would not use JSON for control and instead interact
# through controll scripts/commends
app = Flask(__name__)

@app.errorhandler(404)
def not_found():
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/mode/<action>', methods=['POST'])
def mode(action='get'):
    """mode (via json) is SAT, AUX, USB, INT"""
    if action == 'set':
        if not request.json:
            return not_found()
        mode = request.json.get('mode','')
        print "set mode", mode
        mpl.play(mode)
    else:
        print "get mode", mpl._mode

    return jsonify({'mode': mpl._mode,'success': 'success'})


@app.route('/mute', methods=['POST'])
def mute():
    mpl.mute()
    return jsonify({'success': 'success'})


if __name__ == '__main__':

    app.run(ip, port=port, debug=debug)
