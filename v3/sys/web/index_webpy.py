#
# NO LONGER USED.
#
# I switched to Flask. Just in generally it appears no one is contributing
# meaningful documentation for web.py any more and most suggest using Flask
# as a low memory low overhead replacement. (nathan)
#


import web
import json
import time
import base64
import re
from web.wsgiserver import CherryPyWSGIServer
from web.wsgiserver.ssl_builtin import BuiltinSSLAdapter
from functools import wraps

ssl_cert = 'ssl_cert.pem'
ssl_key = 'ssl_key.pem'
username = 'testuser'
password = 'testpassword'
listenip = "0.0.0.0"
listenport = 8443

CherryPyWSGIServer.ssl_adapter = BuiltinSSLAdapter(ssl_cert,ssl_key,None)


urls = (
    '/authors', 'authors',
    '/suggest/(.*)/(.*)', 'suggest',
    '/setparameter', 'setparameter',
    '/login', 'login',
    '/', 'index'
)
# when using web.py's internal web server the /static directory gets auto routed

app = web.application(urls, globals())

AUTHORS = ['Miguel de Cervantes', 'William Shakespeare', 'Francisco de Quevedo']



def force_ssl():
    if web.ctx.protocol == 'http':
        raise web.redirect('https://' + web.ctx.host + web.ctx.fullpath)

def force_login():
    raise web.redirect('https://' + web.ctx.host + 'login')

def check_auth(username, password):
    return username == 'username' and password == 'password'

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = web.ctx.env['HTTP_AUTHORIZATION'] if 'HTTP_AUTHORIZATION' in  web.ctx.env else None
        if auth:
            auth = re.sub('^Basic ', '', auth)
            username, password = base64.decodestring(auth).split(':')
        if not auth or not check_auth(username, password):
            web.header('WWW-Authenticate', 'Basic realm="admin"')
            web.ctx.status = '401 Unauthorized'
            return Unauthorized()
        return f(*args, **kwargs)
    return decorated

class Unauthorized():
    def GET(self):
        return "401 Unauthorized"
    def POST(self):
        return "401 Unauthorized"



@requires_auth
class index:
    def GET(self):
        force_ssl(self)
        return 'Hello'

# Define the actions when the URL's are called
class setparameter:
    def error(self, reason='unknown'):
        print 'error', reason
        return json.dumps({
                'success': 'false',
                'reasons': reason
                })

    def run(self):
        data = web.input()

        if 'id' not in data or 'value' not in data:
            return self.error("missing at least id and value")

        time.sleep(2)
        print "set \"%s\" to \"%s\""%(data.id, data.value)
        return json.dumps({'success': 'success', 'authors': AUTHORS})

    def GET(self):
        return self.run()

    def POST(self):
        return self.run()

class authors:
    def GET(self):
        return json.dumps({'authors': AUTHORS})


class suggest:
    def GET(self, author, words):
        if author in AUTHORS:
            suggestions = find_suggestions(author, words)
            return json.dumps(suggestions)
        else:
            return 'Error: author not found.'

if __name__ == "__main__":
    # Start the server
    #app.run()
    web.httpserver.runsimple(app.wsgifunc(), (listenip, listenport))
