#!/usr/bin/env bash
if [ $# -lt 1 ]; then
	echo "usage: $0 <mp3directory>"
	echo ""
	echo "scans directory and outputs list of mp3's compatible with mplayer -playlist option"
	exit 1
fi

find "$1" -type f -iregex ".*\.\(aac\|flac\|mp3\|ogg\|wav\)$" 
