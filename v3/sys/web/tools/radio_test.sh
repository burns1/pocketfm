#!/bin/bash -x

curl http://localhost:10012/input/get -H "Content-Type: application/json" -X POST \
     -d ''

curl http://localhost:10012/input/set -H "Content-Type: application/json" -X POST \
     -d '{"input": "AUX"}'

curl http://localhost:10012/frequency/get -H "Content-Type: application/json" -X POST \
     -d ''

curl http://localhost:10012/frequency/set -H "Content-Type: application/json" -X POST \
     -d '{"frequency": 99.1}'
