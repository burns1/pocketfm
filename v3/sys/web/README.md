index_webpy.py was an attempt to setup a server using web.py. But this framework
isn't really maintained any more so we switched to Flask.

Setup a python virtualenv and install Flash (or pip install -r requirements.txt):

    virtualenv pyenv
    source pyenv/bin/activate
    pip install -r requirements.txt

run with ``python index_flask.py``. There is a basic test of the AJAX'ish forms
at http://localhost:8443/static/index.html. This is a work in progress. To test
polling see the setupPolling() function from index.html
