#!/usr/bin/python

import serial
import time
import sys
import subprocess
import gsm
import wifi
from pydispatch import dispatcher
import pfm_signals
import usb_storage
import power

cnt = 0

while cnt == 0:

    cnt =+ 1


    port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)
    #check BOB, get version
    print("Info: Check breakout board")
    port.write("\x1b?G\x1b253P")
    time.sleep(3)
    port.flushInput()


    port.write("\x1b?V")
    time.sleep(3)
    rcv = port.read(19)
    if rcv == "\x1b7,2,10,36988V":
        print("Success: Software Version reported as "+repr(rcv))
    else:
        print("Failure: Software Version: "+repr(rcv)+"\n")
        print("Wrong software version or no BoB\n")
        sys.exit(-1)
    time.sleep(3)
        
    #switch everything on
    print("\nInfo: Power everything on")
    port.write("\r\nPower everything on")
    port.write("\x1b255,0P")
    time.sleep(3)

    #read the power mode back
    #port.write("\x1b?P")
    #rcv = port.read(35)
    #print("\nPower mode bit: "+repr(rcv))
    
    #set leds
    print('\nInfo: LED Test - visual check required')
    led_color = 2
    led_colors = {0,1,2,3,4,5,6,7}

    #led_value2 = ((1 | (3 << 3) | (5 << 6) | (6 << 9) | (7 << 12)) , 128)

    for lcolor in led_colors:
        #led_value2 = ((2 | (2 << 3) | (2 << 6) | (2 << 9) | (2 << 12)))
        led_value2 = ((lcolor | (lcolor << 3) | (lcolor << 6) | (lcolor << 9) | (lcolor << 12)))
        print("Info: LED value "+str(led_value2))
        #port.write("\x1bL")
        port.write("\x1b"+str(led_value2)+",200L")
        time.sleep(0.5)

    #activate and check GPS
    print("\nInfo: Activate and check GPS\n")
    port.write("\x1bG")
    rcv = port.read(40)
    if rcv != "":
        print("Success: GPS active: "+repr(rcv))
    else:
        print('Failure: GPS Output not detected')
        sys.exit(-1)
    port.write("\x1b?G")
    port.write("\x1b253P")
    port.flushInput()

    #read ADC
    print("\nInfo: Read ADC")
    port.write("\x1b\x3fI")
    rcv = port.read(128)
    print("Success: ADC values: "+repr(rcv))
    
    #read Encoder
    print("\nInfo: Read Encoder")
    port.write("\x1b\x3fE")
    rcv = port.read(128)
    print("Success: Encoder values: "+repr(rcv))
    
    #check maxpro
    #check for presence of bus
    print('\nInfo: Maxpro Board i2c detection')
    rcv = subprocess.check_output(['/usr/sbin/i2cdetect', '-l'])
    if rcv != "":
        # print("Some I2C bus present: "+repr(rcv))
        i2cdect = subprocess.Popen(('/usr/sbin/i2cdetect', '-y', '1'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', '40'), stdin=i2cdect.stdout)
        i2cdect.wait()
        # print("Result of i2cdetect for maxpro: "+repr(rcv))
        i2cadd = repr(rcv).split()
        if i2cadd[1] == "40":
            print('Success: Maxpro i2c address detected ({}) '.format(i2cadd[1]))
        else:
            print('Failure: Maxpro i2c address not found by Raspberry Pi')
            sys.exit(-1)
    else:
        print('Failure: Maxpro i2c detection failed')
        sys.exit(-1)

    port.close()
    time.sleep(3)
    print('\nInfo: USB Satellite device detection')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', 'TechnoTrend'), stdin=usbdect.stdout)
        usbdect.wait()
        usbadd = repr(rcv).split()
        if usbadd[6] == "TechnoTrend":
            print('Success: USB Satellite detected ({}) '.format(usbadd[6]))
        else:
            print('Failure: USB Satellite not detected ({}) '.format(usbadd[6]))
            sys.exit(-1)

    
    print('\nInfo: USB GSM device detection')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', 'Huawei'), stdin=usbdect.stdout)
        usbdect.wait()
        usbadd = repr(rcv).split()
        if usbadd[6] == "Huawei":
            print('Success: USB GSM detected ({}) '.format(usbadd[6]))
        else:
            print('Failure: USB GSM not detected ({}) '.format(usbadd[6]))
            sys.exit(-1)
    print('Info: GSM SIM card Network Detection ...')
    #gsm.main()
    g = gsm.GSM()
    # dispatcher.connect(handle_event, signal=pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, sender=dispatcher.Any)
    time.sleep(25)
    test = pfm_signals.get_value(pfm_signals.SIG_UPDATE_GSM_NETWORK_NAME, 'Failure: no GSM SIM Network Operator found')
    print test

    print('\nInfo: Detecting USB WiFi device')
    rcv = subprocess.check_output(['/usr/bin/lsusb'])
    if rcv != "":
        usbdect = subprocess.Popen(('/usr/bin/lsusb'), stdout=subprocess.PIPE)
        rcv = subprocess.check_output(('grep', 'Atheros'), stdin=usbdect.stdout)
        usbdect.wait()
        usbadd = repr(rcv).split()
        if usbadd[6] == "Atheros":
            print('Success: USB WiFi detected ({}) '.format(usbadd[6]))
        else:
            print('Failure: USB WiFi not detected ({}) '.format(usbadd[6]))
            sys.exit(-1)
    print('\nInfo: WiFi scanning for networks ...')
    w = wifi.WiFi()
    time.sleep(10)
    try: 
        dispatcher.send(pfm_signals.SIG_SET_WIFI_AVAILABLE_NETWORKS, 'hardware_test main()', [])
        time.sleep(20)
    except:
        raise
        pass

    test = pfm_signals.get_value(pfm_signals.SIG_UPDATE_WIFI_AVAILABLE_NETWORKS, ['Failure: no WiFi networks found'])
    print('Success: WiFi networks found: {}'.format(test))

    u = usb_storage.USBStorage()
    time.sleep(3)
    port.write("\x1b207P")
    time.sleep(3)
    port.write("\x1b255P")
    time.sleep(3)
    if u.Active == True:
        print('Success: USB Storage device detection state {}'.format(u.Active))
    else:
        print('Failure: USB Storage device detection state {}'.format(u.Active))

