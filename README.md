Radio transmitter for crisis regions and developing countries

Pocket FM is a portable FM radio transmitter that enables people in crisis
regions or developing countries to communicate without internet, mobile phones,
television or when electricity is unavailable.

Despite technological innovation, local radio continues to be a vital source
of information and entertainment in many regions. 
Throughout the developed world, people use television, newspapers and 
the internet to stay up-to-date but these sources are not an option
in rural areas of developing countries. At times of disaster, war, 
and population displacement, local radio can provide updates 
which help save lives. This is where Pocket FM comes in.

http://www.pocket-fm.com/

System software is mainly based on Python (2.7).
