#!/bin/bash
#
#
hash kpartx >/dev/null 2>&1 || { echo >&2 "I require kpartx but it's not installed.  Aborting."; exit 1; }
hash shasum >/dev/null 2>&1 || { echo >&2 "I require shasum but it's not installed.  Aborting."; exit 1; }


# Root file system path in repo relative to build script
DIST_ROOT_PATH=

DIST_BASE=jessie
DIST_NAME=raspbian
DIST_TYPE=
DIST_DATE=2015-11-24
DIST_VER=2015-11-21

if [ "${DIST_NAME}" == "raspbian" ] && [ "${DIST_TYPE}" == "lite" ]; then
    DIST_ROOT_URL=https://downloads.raspberrypi.org
    DIST_PATH=/raspbian_lite/images/raspbian_lite-2016-05-31
    DIST_IMAGE_ZIP_FILE=2016-05-27-raspbian-jessie-lite.zip
    DIST_IMAGE_ZIP_FILE_SHA1=${DIST_IMAGE_ZIP_FILE}.sha1
    WGET_PATH="${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE}"
fi

if [ "${DIST_NAME}" == "raspbian" ] && [ "${DIST_TYPE}" == "" ]; then
    DIST_ROOT_URL=https://downloads.raspberrypi.org
    DIST_PATH=/raspbian/images/raspbian-2016-05-31
    DIST_IMAGE_ZIP_FILE=2016-05-27-raspbian-jessie.zip
    DIST_IMAGE_ZIP_FILE_SHA1=${DIST_IMAGE_ZIP_FILE}.sha1
    WGET_PATH="${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE}"
fi

SCRIPT_UID=`id -u`
SCRIPT_USER=`id -un`
echo
echo "Looking for zipped RPi raw disk image ..."
if [ ! -f ${DIST_IMAGE_ZIP_FILE} ]; then
    echo "Downloading ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE}";
    wget ${WGET_PATH}
else
    echo
    echo "Found existing ${DIST_IMAGE_ZIP_FILE}"
fi

# Image Zip File SHA1
if [ ! -f ${DIST_IMAGE_ZIP_FILE_SHA1} ]; then
    echo
    echo "Downloading ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE_SHA1}"
    wget ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE_SHA1}
else
    echo
    echo "Found existing ${DIST_IMAGE_ZIP_FILE_SHA1}"
fi

# Verify Image
# TODO - this does NOT actually verify yet, just runs sha1sum - should exit if verify fails
# checksum should match that published on website
SHA1RESULT=`cat ${DIST_IMAGE_ZIP_FILE_SHA1} | cut -f1 -d' '`
SHA1FILE=`cat ${DIST_IMAGE_ZIP_FILE_SHA1} | cut -f3 -d' '`
echo "${SHA1RESULT}  ${SHA1FILE##*/}" >${DIST_IMAGE_ZIP_FILE_SHA1} # Delete full path from RPi SHA1 file
echo 
echo "Verifying SHA1 hash of zipped raw disk image ..."
shasum -c ${DIST_IMAGE_ZIP_FILE_SHA1}

# Unzip image
DIST_IMAGE_RAW=${DIST_IMAGE_ZIP_FILE%%.*}.img
if [ ! -f ${DIST_IMAGE_RAW} ]; then
    echo
    echo "Unzipping ${DIST_IMAGE_RAW} ..."
    unzip ${DIST_IMAGE_ZIP_FILE}
else
    echo
    echo "Found existing image ${DIST_IMAGE_RAW}"
fi

DIST_IMAGE_PART_1_NAME="${DIST_IMAGE_RAW}1"
DIST_IMAGE_PART_2_NAME="${DIST_IMAGE_RAW}2"


device=$(kpartx -lv ${DIST_IMAGE_RAW} | sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1)
device="/dev/mapper/${device}"

echo "using device $device"

sudo kpartx -av "${DIST_IMAGE_RAW}"

bootp=${device}p1
rootp=${device}p2
echo "--- bootp ${bootp}" 
echo "--- rootp ${rootp}" 
echo

mkdir -p ${DIST_IMAGE_PART_1_NAME}
mkdir -p ${DIST_IMAGE_PART_2_NAME}
sleep 2
sudo mount ${bootp} ${DIST_IMAGE_PART_1_NAME}
sudo mount ${rootp} ${DIST_IMAGE_PART_2_NAME}

# 
# Clone files in /DTB into partition 1
echo
echo "Copying files from ${PWD}/DTB tree into /boot partition ..."
echo "Created at `date -Iminutes` from rev `git rev-parse HEAD` by ${SCRIPT_USER}" >DTB/CREATED.pfm
sudo cp -vr DTB/. ${DIST_IMAGE_PART_1_NAME}/

# 
# Clone files in /root into partition 2
echo
echo "Copying files from ${PWD}/root tree into / partition ..."
echo "Created at `date -Iminutes` from rev `git rev-parse HEAD` by ${SCRIPT_USER}" >root/CREATED.pfm
sudo cp -vr root/etc/network/interfaces ${DIST_IMAGE_PART_2_NAME}/etc/network/
sudo cp -vr root/etc/rc.local* ${DIST_IMAGE_PART_2_NAME}/etc/
sudo cp -vr root/sbin/* ${DIST_IMAGE_PART_2_NAME}/sbin/
sudo mkdir -v ${DIST_IMAGE_PART_2_NAME}/overlay
sudo mkdir ${DIST_IMAGE_PART_2_NAME}/.firstboot.pfm.d
sudo cp -vr root/. ${DIST_IMAGE_PART_2_NAME}/.firstboot.pfm.d

echo
echo "Umounting all partitions ..."
eval "sudo umount ${DIST_IMAGE_PART_1_NAME}"
eval "sudo umount ${DIST_IMAGE_PART_2_NAME}"
rmdir -v ${DIST_IMAGE_PART_1_NAME} ${DIST_IMAGE_PART_2_NAME}

sudo kpartx -vd ${DIST_IMAGE_RAW}

echo
echo "Creating Pocket FM RPi image ..."
PFM_IMAGE_RAW="`date -I`-pocketfm-v3.img"
cp -v ${DIST_IMAGE_RAW} ${PFM_IMAGE_RAW}

echo "Creating raw image SHA1 hash ..."
shasum ${PFM_IMAGE_RAW} > "${PFM_IMAGE_RAW}.sha1"
