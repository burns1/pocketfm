#!/bin/bash
#
#
DIST_BASE=jessie
DIST_NAME=raspbian
DIST_TYPE=lite
DIST_DATE=2015-11-24
DIST_VER=2015-11-21

DIST_ROOT_URL=https://downloads.raspberrypi.org
DIST_PATH=/raspbian_lite/images/raspbian_lite-2015-11-24
DIST_IMAGE_ZIP_FILE=2015-11-21-raspbian-jessie-lite.zip
DIST_IMAGE_ZIP_FILE_SHA1=${DIST_IMAGE_ZIP_FILE}.sha1

SCRIPT_UID=`id -u`
SCRIPT_USER=`id -un`

echo
echo "Looking for zipped RPi raw disk image ..."
if [ ! -f ${DIST_IMAGE_ZIP_FILE} ]; then
    echo "Downloading ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE}";
    wget ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE}
else
    echo
    echo "Found existing ${DIST_IMAGE_ZIP_FILE}"
fi

# Image Zip File SHA1
if [ ! -f ${DIST_IMAGE_ZIP_FILE_SHA1} ]; then
    echo
    echo "Downloading ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE_SHA1}"
    wget ${DIST_ROOT_URL}/${DIST_PATH}/${DIST_IMAGE_ZIP_FILE_SHA1}
else
    echo
    echo "Found existing ${DIST_IMAGE_ZIP_FILE_SHA1}"
fi

# Verify Image
# TODO - this does NOT actually verify yet, just runs sha1sum - should exit if verify fails
# checksum should match that published on website
SHA1RESULT=`cat ${DIST_IMAGE_ZIP_FILE_SHA1} | cut -f1 -d' '`
SHA1FILE=`cat ${DIST_IMAGE_ZIP_FILE_SHA1} | cut -f3 -d' '`
echo "${SHA1RESULT}  ${SHA1FILE##*/}" >${DIST_IMAGE_ZIP_FILE_SHA1} # Delete full path from RPi SHA1 file
echo 
echo "Verifying SHA1 hash of zipped raw disk image ..."
#sha1sum -c ${DIST_IMAGE_ZIP_FILE_SHA1}
shasum -c ${DIST_IMAGE_ZIP_FILE_SHA1}

# Unzip image
DIST_IMAGE_RAW=${DIST_IMAGE_ZIP_FILE%%.*}.img
if [ ! -f ${DIST_IMAGE_RAW} ]; then
    echo
    echo "Unzipping ${DIST_IMAGE_RAW} ..."
    unzip ${DIST_IMAGE_ZIP_FILE}
else
    echo
    echo "Found existing image ${DIST_IMAGE_RAW}"
fi

# find offsets for file system partitions using fdisk within DIST_IMAGE_RAW
#FDISKCMD="fdisk -l ${DIST_IMAGE_RAW}"
FDISKRESULT=`${FDISKCMD}`

# find offsets using file
FILERESULT="`file ${DIST_IMAGE_RAW}`"
#echo $FILERESULT

DIST_IMAGE_PART_1_NAME="${DIST_IMAGE_RAW}1"
DIST_IMAGE_PART_2_NAME="${DIST_IMAGE_RAW}2"
DIST_IMAGE_PART_1_CHECK=`echo ${FILERESULT} | cut -f3  -d','`
DIST_IMAGE_PART_2_CHECK=`echo ${FILERESULT} | cut -f6  -d','`

# 
# Sanity Check for hack using cut to parse output from file command
#
if [ "${DIST_IMAGE_PART_1_CHECK%' '*}" != " startsector" ] || [ "${DIST_IMAGE_PART_2_CHECK%' '*}" != " startsector" ]; then
    echo 
    echo "Error: To find offsets and mount the 2 partitions in the raw img file, this build script relies on a very particular output from the UNIX 'file' command"
    echo 
    echo "Cannot parse the following command output to extract offsets"
    echo 
    echo "$ file ${DIST_IMAGE_RAW}"
    echo "${FILERESULT}"
    echo 
    echo 
    exit 1
fi

# cut the " startsector" from the results leaving the partition offsets for mounting
DIST_IMAGE_PART_1_OFFSET=`echo ${FILERESULT} | cut -f8  -d','  | cut -f3 -d' '`
DIST_IMAGE_PART_2_OFFSET=`echo ${FILERESULT} | cut -f16 -d ',' | cut -f3 -d' '`

echo
echo "Mounting /DTB ..."
echo "Partition Name: ${DIST_IMAGE_PART_1_NAME}"
echo "Partition Offset ${DIST_IMAGE_PART_1_OFFSET} * 512"
mkdir -p ${DIST_IMAGE_PART_1_NAME}
echo "sudo mount -o loop,uid=${SCRIPT_UID},offset="$(( DIST_IMAGE_PART_1_OFFSET * 512 ))" $DIST_IMAGE_RAW ${DIST_IMAGE_PART_1_NAME}"
eval "sudo mount -o loop,uid=${SCRIPT_UID},offset="$(( DIST_IMAGE_PART_1_OFFSET * 512 ))" $DIST_IMAGE_RAW ${DIST_IMAGE_PART_1_NAME}"
echo
echo "Mounting / ..."
echo "Partition Name: ${DIST_IMAGE_PART_2_NAME}"
echo "Partitiion Offset ${DIST_IMAGE_PART_2_OFFSET} * 512"
mkdir -p ${DIST_IMAGE_PART_2_NAME}
sudo mount -o loop,offset="$(( DIST_IMAGE_PART_2_OFFSET * 512 ))" $DIST_IMAGE_RAW ${DIST_IMAGE_PART_2_NAME}
sudo mount -o loop,offset="$(( DIST_IMAGE_PART_2_OFFSET * 512 ))" $DIST_IMAGE_RAW ${DIST_IMAGE_PART_2_NAME}

# At this point both partitions of the Raspbian image are mounted on the host system.
# For example, for a recent Raspbian Lite image, mount points are directories in CWD
# mirroring Device names reported by
#
# fdisk -l 2015-11-21-raspbian-jessie-lite.img
#
# Device                               Boot  Start     End Sectors  Size Id Type
# 2015-11-21-raspbian-jessie-lite.img1        8192  131071  122880   60M  c W95 FAT32 (LBA)
# 2015-11-21-raspbian-jessie-lite.img2      131072 2848767 2717696  1.3G 83 Linux

# 
# Clone files in /DTB into partition 1
echo
echo "Copying files from ${PWD}/DTB tree into /DTB partition ..."
echo "Created at `date -Iminutes` from rev `git rev-parse HEAD` by ${SCRIPT_USER}" >DTB/CREATED.pfm
sudo cp -vr DTB/. ${DIST_IMAGE_PART_1_NAME}/

# 
# Clone files in /root into partition 2
echo
echo "Copying files from ${PWD}/root tree into / partition ..."
echo "Created at `date -Iminutes` from rev `git rev-parse HEAD` by ${SCRIPT_USER}" >root/CREATED.pfm
sudo cp -vr root/etc/network/interfaces ${DIST_IMAGE_PART_2_NAME}/etc/network/
sudo cp -vr root/etc/rc.local* ${DIST_IMAGE_PART_2_NAME}/etc/
sudo mkdir ${DIST_IMAGE_PART_2_NAME}/.firstboot.pfm.d
sudo cp -vr root/. ${DIST_IMAGE_PART_2_NAME}/.firstboot.pfm.d

echo
echo "Umounting all partitions ..."
eval "sudo umount ${DIST_IMAGE_PART_1_NAME}"
eval "sudo umount ${DIST_IMAGE_PART_2_NAME}"

echo
echo "Creating Pocket FM RPi image ..."
PFM_IMAGE_RAW="`date -I`-pocketfm-v3.img"
mv -v ${DIST_IMAGE_RAW} ${PFM_IMAGE_RAW}

echo "Creating raw image SHA1 hash ..."
#sha1sum ${PFM_IMAGE_RAW} > "${PFM_IMAGE_RAW}.sha1"
shasum ${PFM_IMAGE_RAW} > "${PFM_IMAGE_RAW}.sha1"

echo
echo "Cleaning up ..."
rmdir -v ${DIST_IMAGE_PART_1_NAME} ${DIST_IMAGE_PART_2_NAME}
