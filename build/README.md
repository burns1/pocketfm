Pocket FM build system for v3

The build system hierachy for  a base Pocket FM image, consistent for all developers.

pfm-image-build.sh - This should be run on a Linux or MacOS host (not yet tested), not a RPi

- downloads, verifies and decompresses the baseline RPi OS image for v3 (currently Raspbian Lite)
- directly loopback mounts the file's DTB & root partitions as read-write
- recursively copies files under the DTB & root directories into the mounted partitions in the file
- generates a Pocket FM SD card image (and SHA1 hash) ready to be dd'ed onto a RPi!

root/

This directory contains all files that need to be updated from the vanilla distribution root partition.
The pfm-image-build.sh script does this automatically, but it can be done manually via git on a RPi itself.

DTB/

This directory contains all files that need to be updated from the vanilla distribution DTB partition.
The pfm-image-build.sh script does this automatically, but it can be done manually via git on a RPi itself.


Additional Notes on other files

/root/etc/rc.local - contains a first boot installer (installs the minimum necessary packages)

/root/etc/rc.local.packages-pfm - contains the minimum list of packages to be installed for Pocket FM

/root/etc/rc.local.services-pfm - contains the list of services to be activated for Pocket FM (eg dnsmasq, hostap for WiFi ...)

