#!/usr/bin/python
# -*- coding: utf-8 -*-

import smbus
import time

# I2C-Adresse des MCP23017
address = 0x40

# Erzeugen einer I2C-Instanz und Öffnen des Busses
pfmatmega = smbus.SMBus(1)

# this routine converts Strings to ASCII code
def StringToBytes(val):
        retVal = []
        for c in val:
                retVal.append(ord(c))
        return retVal

#Set power to 5, low/high frequency 100 kHz (100000/5)
#pfmatmega.write_byte(address, 0x00)
#valCmd = StringToBytes("RFboardAdd")
#print valCmd
#pfmatmega.write_block_data(address, 0, valCmd)
#pfmatmega.write_byte(address,0x01)
#valVal = StringToBytes("64")
#pfmatmega.write_byte_data(address, 1, 64)
#pfmatmega.write_byte(address,0x02)

#valVal = StringToBytes("Power")
valVal = [0x00, 0x10, 0x20, 0x4e, 0x05, 0x05]
#valVal = StringToBytes("AAAAPower")
#pfmatmega.write_byte(address, 5)
#pfmatmega.write_block_data(address, 0, valVal)
#valVal = StringToBytes("5")
pfmatmega.write_i2c_block_data(address, 42, valVal)
print ("writing: ", valVal)
pfmatmega.write_byte(address, 10)
#pfmatmega.write_byte(address, 255)

#pfmatmega.write_block_data(address, 0, [10,20000,20000])

#valVal = StringToBytes("FreqLo")
#pfmatmega.write_block_data(address, 0, valVal)
#pfmatmega.write_byte(address, 15600)

#valVal = StringToBytes("FreqHi")
#pfmatmega.write_block_data(address, 0, valVal)
#pfmatmega.write_byte(address, 15600)

#r = pfmatmega.read_byte(address)
#print ("r=", r)

#pfmatmega.write_byte(address, 0x00)
#valCmd = StringToBytes("Power")
#print valCmd
#pfmatmega.write_block_data(address, 2, valCmd)
#pfmatmega.write_byte(address,0x01)
#valVal = StringToBytes("5")
#pfmatmega.write_byte_data(address, 3, valVal)
#pfmatmega.write_byte(address,0x02)

#r = pfmatmega.read_byte(address)
#print ("r=", r)

#if __name__ == "__main__":
#   main()
