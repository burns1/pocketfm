#!/usr/bin/python

import serial

port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)

while True:
    # port.write("\x0c")
    port.write("\r\nPower Test")
    # set power using ESC FF 00 50 
    port.write("\x1b255,0P")
    port.write("\r\nSent: 1b 80 7F 50")
    # now send power query ESC ? P
    port.write("\x1b\x3f\x50")
    rcv = port.read(10)
    port.write("\r\nPower:" + repr(rcv))
    print("\nPower:" + repr(rcv))
    port.write("\r\nDate Test")
    port.write("\x1b\x3f\x54")
    rcv = port.read(30)
    port.write("\r\nDate:" + repr(rcv))
    print("\nDate:" + repr(rcv))
    # port.write("\x0c")
    port.write("\r\nGPS Test")
    port.write("\x1b\x3f\x47")
    rcv = port.read(30)
    port.write("\r\nGPS:" + repr(rcv))
    print("\nGPS:" + repr(rcv))
    port.write("\r\nEncoder Test")
    port.write("\x1b\x3fE")
    rcv = port.read(30)
    port.write("\r\nEncoder:" + repr(rcv))
    print("\nEncoder:" + repr(rcv))
    port.write("\r\nLED Test")
    port.write("\x1b\x8f\xffL")
    rcv = port.read(30)
    port.write("\r\nLED:" + repr(rcv))
    print("\nLED:" + repr(rcv))
    port.write("\x1b32767,200L")

