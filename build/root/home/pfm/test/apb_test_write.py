#!/usr/bin/python
# -*- coding: utf-8 -*-
# cat /sys/module/i2c_bcm2708/parameters/baudrate
import smbus
import time

# I2C-Adresse des MCP23017
address = 0x40

# Erzeugen einer I2C-Instanz und Öffnen des Busses
pfmatmega = smbus.SMBus(1)

# this routine converts Strings to ASCII code
def StringToBytes(val):
        retVal = []
        for c in val:
                retVal.append(ord(c))
        return retVal

#valVal = StringToBytes("Power")
valVal = StringToBytes("P")
#valVal = StringToBytes("P")

for cnt in range(72, 256, 8):
#   pfmatmega.write_block_data(address, 0, [cnt,0x20,0x4E,0x05,0x05])
    pfmatmega.write_block_data(address, 0, [cnt])
#   pfmatmega.write_byte(address, cnt)
    print ("writing: ", cnt, hex(cnt), bin(cnt))
    time.sleep(5)

#r = pfmatmega.read_byte(address)
#print ("r=", r)

#time.sleep(5)
valVal = StringToBytes("Q")
#pfmatmega.write_block_data(address, 0, valVal)

time.sleep(2)
valVal = StringToBytes("P")
#pfmatmega.write_block_data(address, 0, valVal)

#if __name__ == "__main__":
#   main()
