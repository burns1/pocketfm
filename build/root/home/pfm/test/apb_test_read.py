#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import smbus
import time

# I2C-Adresse des MCP23017
address = 0x40

# Erzeugen einer I2C-Instanz und Öffnen des Busses
pfmatmega = smbus.SMBus(1)

# this routine converts Strings to ASCII code
def StringToBytes(val):
        retVal = []
        for c in val:
                retVal.append(ord(c))
        return retVal

# this routine converts MaxPro double byte BCD values into strings
def BCDToString(dig01, dig23):
    dig = [dig01 >> 4, dig01 & 0xF, dig23 >> 4, dig23 & 0xF]
    string = ''
    numDigits = 0
    numDecPoints = 0
    invalid = 0
    for i in range(4):
        if dig[i] < 0xa:
            # normal BCD
            numDigits += 1
        elif dig[i]==0xb:
            # maxpro defines 0xb nibble as 'blank'
           dig[i]=' ' 
           if numDigits > 0:
                # invalid mBCD format, ie "1 100"
                invalid += 1
        elif dig[i]==0xc:
            # maxpro defines 0xc nibble as 'skip'
            dig[i]=' '
            if numDigits > 0:
                # invalid mBCD format, ie "1(skip)100"
                invalid += 1
        elif dig[i]==0xa:
            # maxpro defines 0xa as decimal point
            dig[i]='.'
            numDecPoints += 1
            if numDecPoints > 1:
                # invalid mBCD format, ie "1.1.1"
                invalid += 1
        else:
            # maxpro does not define nibble values 0xe or 0xf
            dig[i] = '#'
            invalid += 1
        string +=str(dig[i])
    return string

numTries = 0
numSuccesses = 0
numFailures = 0
while True:
    print 'Tries: {0} Successes: {1} Failures: {2}'.format(numTries, numSuccesses, numFailures)
    numTries += 1
    try:
        test = pfmatmega.read_i2c_block_data(address, 0)
        numSuccesses += 1
        print test 
        RFout   = BCDToString(test[0],  test[1])
        SWR     = BCDToString(test[2],  test[3])
        Temp    = BCDToString(test[4],  test[5])
        Volts   = BCDToString(test[6],  test[7])
        Vext    = BCDToString(test[8],  test[9])
        UpDays  = BCDToString(test[10], test[11])
        UpHours = BCDToString(0xCC,     test[12])
        UpMins  = BCDToString(0xCC,     test[13])
        ErrFlags = test[14]
        TempExt = BCDToString(test[15], test[16])
        DipSWMode = test[17]
        ExtAmp = BCDToString(test[18], test[19])
        print '{0} \t==\t {1}'.format('RFout', RFout)
        print '{0} \t==\t {1}'.format('SWR', SWR)
        print '{0} \t==\t {1}'.format('Temp', Temp)
        print '{0} \t==\t {1}'.format('Volts', Volts)
        print '{0} \t==\t {1}'.format('Vext', Vext)
        print '{0} \t==\t {1}'.format('Days', UpDays)
        print '{0} \t==\t {1}'.format('Hours', UpHours)
        print '{0} \t==\t {1}'.format('Mins', UpMins)
        print '{0} \t==\t {1}'.format('Error', hex(ErrFlags))
        print '{0} \t==\t {1}'.format('ExTemp', TempExt)
        print '{0} \t==\t {1}'.format('DipSW', hex(DipSWMode))
        print '{0} \t==\t {1}'.format('ExAmp', ExtAmp)
        print "---"
        # time.sleep(3)
    except IOError as e:
        numFailures += 1
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

#if __name__ == "__main__":
#   main()
