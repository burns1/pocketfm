#!/usr/bin/python

#http://stackoverflow.com/questions/17317317/using-python-smbus-on-a-raspberry-pi-confused-with-syntax

import smbus
import time

bus = smbus.SMBus(1)

address = 0x40

bus.write_i2c_block_data(address, 0, [0xF8, 0x43, 5, 5])

time.sleep(1)
