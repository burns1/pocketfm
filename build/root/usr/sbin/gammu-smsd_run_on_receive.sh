#!/bin/bash

from=$SMS_1_NUMBER
message=$SMS_1_TEXT
reply=""

source /usr/sbin/sms_command_parse_functions.sh

#if test "$message" = "Ping"; then
#    reply="Pong!"
#else
#    reply="Y U NO PLAY PING PONG?"
#fi

reply="$(parseSms ${PIN} ${message})"

logger "run-on-receive from=$from message=$message reply=$reply"
echo "$reply" | gammu-smsd-inject TEXT "$from"
