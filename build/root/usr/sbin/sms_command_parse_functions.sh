#!/bin/bash
verifySmsPin()
{
    local pfm_pin="$1"
    local sms_message="$2"
    local pfm_pin_length=${#pfm_pin}
    local regex="\([0-9]\{${pfm_pin_length}\}\)"
    local sms_pin=`expr "$sms_message" : "$regex"`
    if [ "${sms_pin}" == "${pfm_pin}" ]; then
        echo $sms_pin
    fi
}
getAuthorisedSmsCommand()
{
    local pfm_pin="$1"
    local sms_message="$2"
    if [ ! -z $(verifySmsPin "$pfm_pin" "$sms_message") ]; then
        echo "${sms_message#$pfm_pin}"
    fi
}
parseSms()
{
    local pfm_pin="$1"
    local sms_message="$2"
    local sms_commandline=""
    local sms_command=""
    local sms_parameters=""
    local sms_valid_command="false"
    local sms_valid_parameters="false"
    local valid_command=""

    # cleanse sms_message to alphanumeric removing leading & trailing whitespace
    sms_message="$(echo -e "${sms_message}" | sed -e 's/[^[:alnum:]=\ ]//g' -e 's/=/\ /g' -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

    # validate pin & return sms command line
    sms_commandline="$(getAuthorisedSmsCommand "$pfm_pin" "$sms_message")"

    # Incorrect PIN
    if [ -z "${sms_commandline}" ] && [ ${#sms_message} -gt ${#pfm_pin} ]; then
             echo "Failed: Invalid PIN for command: ${sms_message}"
             return
    fi

    # if authorisation correct with non-empty command given
    if [ -n "${sms_commandline}" ] && [ ${#sms_message} -gt ${#pfm_pin} ]; then

        sms_commandline="$(echo -e "${sms_commandline}" | sed -e 's/^[[:space:]]*//')"
        sms_command="${sms_commandline%%[[:space:]]*}"
        sms_parameters="$(echo -e "${sms_commandline}" | sed -e "s/^${sms_command}*//" -e 's/^[[:space:]]*//')"

        # DEFINED COMMANDS

        valid_command="STATUS"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"
             sms_valid_parameters="true"
             echo "Getting status"
        fi

        valid_command="PWR"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             sms_valid_command="true"

             # 0-25 integer
             if [ ! -z "${sms_parameters}" ]; then
                 sms_parameters=`expr ${sms_parameters}`
                 if [ ${sms_parameters} -ge 0 ] && [ ${sms_parameters} -le 25 ]; then
                     sms_valid_parameters="true"
                 fi
             fi
        fi

        valid_command="IN"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             # 
             valid_parameters="USB"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
             fi
             valid_parameters="AN"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
             fi
             valid_parameters="SAT"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
             fi
             valid_parameters="HD"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set audio
                 echo "Setting audio to $valid_parameters"
             fi
        fi

        valid_command="WIFI"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"

             #
             valid_parameters="ON"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set wifi
                 echo "Setting Wifi AP mode to $valid_parameters"
             fi
             valid_parameters="OFF"
             if [ "${sms_parameters:0:${#valid_parameters}}" == "${valid_parameters}" ]; then
                 sms_valid_parameters="true"
                 # set wifi
                 echo "Setting Wifi AP mode to $valid_parameters"
             fi
        fi

        valid_command="SHUTDOWN"
        if [ "${sms_command:0:${#valid_command}}" == "${valid_command}" ]; then
             if [ ${#sms_command} != ${#valid_command} ]; then
                 sms_parameters="${sms_command:${#valid_command}}"
             fi
             # echo ":${sms_command:0:${#valid_command}}:${sms_parameters}:"
             sms_valid_command="true"
             sms_valid_parameters="true"
             echo "Shutting down"
        fi
    fi

    # CATCH ALL INVALID COMMAND
    if [ "${sms_valid_command}" != "true" ]; then
         echo "Failed: Invalid Command \"${sms_command} ${sms_parameters}\""
    fi

    # CATCH ALL INVALID PARAMETERS
    if [ "${sms_valid_parameters}" != "true" ]; then
         echo "Failed: Invalid Parameters for Command \"${sms_command} ${sms_parameters}\""
    fi

    if [ "${sms_valid_command}" == "true" ] && [ "${sms_valid_parameters}" == "true" ]; then
        echo "Success: ${sms_command} ${sms_parameters}"
    fi

}    

if [ "$1" == "test" ]; then 
#   pin="123456"; sms="123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms="213456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms="12345 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms="123456HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms="123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms=" 123456 HELLO"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms=""; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   pin="123456"; sms="123456"; echo verifySmsPin \""$pin"\" \""$sms"\"  retval=:$(verifySmsPin "$pin" "$sms"):
#   echo " ---- "
    pin="123456"; sms="123456 STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="666666 STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="666666STATUS"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR 0"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR0"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR  1 "; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR 21"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR=26"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 PWR 26"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 IN SAT"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 IN USB"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 IN HD"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 IN AN"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 IN HORSE"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 WIFI ON"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 WIFI=OFF"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 WIFI=OFF extraparam"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 SHUTDOWN"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 SHUTDOWNextraending"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 INVALIDCOMMAND INVALID PARAMETER"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456"; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
    pin="123456"; sms="123456 "; echo parseSms \""$pin"\" \""$sms"\"  retval=:$(parseSms "$pin" "$sms"):
fi
