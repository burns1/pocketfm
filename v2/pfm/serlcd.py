#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#
# based on https://github.com/scogswell/ArduinoSerLCD/blob/master/SerLCD.h
# -*- coding: utf-8 -*-

import serial
from time import sleep
import timeit

# debug?
debug_active = False

SERLCD_SHORT_DELAY = 10/1000.0 # as seen in https://github.com/scogswell/ArduinoSerLCD/blob/master/SerLCD.h

# Init Commands
LCD_SEND_COMMAND = '\xFE'
LCD_SEND_SPECIAL = '\x7C'

# Commands
LCD_BACKLIGHT = '\x80'
LCD_CLEARDISPLAY = '\x01'
LCD_CURSORSHIFT = '\x10'
LCD_DISPLAYCONTROL = '\x08'
LCD_ENTRYMODESET = '\x04'
LCD_FUNCTIONSET = '\x20'
LCD_SETCGRAMADDR = '\x40'
LCD_SETDDRAMADDR = '\x80'
LCD_SETSPLASHSCREEN = '\x0A'
LCD_SPLASHTOGGLE = '\x09'
LCD_RETURNHOME = '\x02'

# Flags for display entry mode
LCD_ENTRYRIGHT = '\x00'
LCD_ENTRYLEFT = '\x02'

# Flags for display on/off control
LCD_BLINKON = '\x01'
LCD_CURSORON = '\x02'
LCD_DISPLAYON = '\x04'

# Flags for display size
LCD_2LINE = '\x02'
LCD_4LINE = '\x04'
LCD_16CHAR = '\x10'
LCD_20CHAR = '\x14'

#  Flags for setting display size
LCD_SET2LINE = '\x06'
LCD_SET4LINE = '\x05'
LCD_SET16CHAR = '\x04'
LCD_SET20CHAR = '\x03'

LCD_BAUD9600 = '\x0D'

def debug(msg):
    if debug_active:
        date = timeit.default_timer()
        print(str(date) + ": " + msg)


class SerLCD(object):

    def __init__(self):
        debug('Initializing Display')
        self.lcd = serial.Serial(port='/dev/ttyAMA0',baudrate=9600)

        # tell the display what it is...
        self.special(LCD_SET4LINE)
        self.special(LCD_SET20CHAR)
        sleep(10/1000.0)

        # set default baud
        self.special(LCD_BAUD9600)
        sleep(10/1000.0)

        self.clear()

    def write(self, *args):
        if len(args) == 1:
            msg, = args # need that comma!
            if self.lcd.isOpen():
                self.lcd.write(msg)
        elif len(args) == 3:
            row,col,msg = args
            self.cursor(row, col)
            if self.lcd.isOpen():
                self.lcd.write(msg)
        else:
            raise TypeError("Wrong number of arguments!")

    def command(self, value):
        if self.lcd.isOpen():
            self.lcd.write(LCD_SEND_COMMAND)
            self.lcd.write(value)
        #sleep(5/1000.0) # as seen in https://github.com/nemith/serLCD/blob/master/serLCD.cpp

    def special(self, value):
        if self.lcd.isOpen():
            self.lcd.write(LCD_SEND_SPECIAL)
            self.lcd.write(value)
        sleep(5/1000.0) # as seen in https://github.com/nemith/serLCD/blob/master/serLCD.cpp

    def clear(self):
        debug('clear()')
        self.command(LCD_CLEARDISPLAY)

    def cursor(self, row, col):
        line_offset = [0,64,20,84]

        # calculate command (human readable - 1)
        pos_cmd = 128 + line_offset[row-1] + (col-1)

        self.command(chr(pos_cmd))

    def show_cursor(self,on=True):
        if on:
            self.command('\x0E')
        else:
            self.command('\x0C')
    
    def splash(self):
        # print (will be centerd on 20x4)
        self.write('      Pocket FM     ')
        self.write('                    ')
        sleep(1)
        
        # save in EEPROM
        sleep(0.1)
        self.special(LCD_SETSPLA7SHSCREEN)
        sleep(0.1)

    def createChar(self, pos, charmap):

        cmd = LCD_SETCGRAMADDR | (pos << 3)
        

        self.command(chr(cmd))
        
        for i in range(8):
            self.lcd.write(chr(charmap[i]))

    def printCustomChar(self, num):
        self.lcd.write(chr(num))  
        print "out"     

    def __del__(self):
        debug('__del__()')
        self.lcd.close()
