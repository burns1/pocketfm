#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

from PFMstatics import *
from simplecrypt import decrypt
from binascii import hexlify, unhexlify
import shutil, hashlib

# get SHA1 hash of file
def sha1_of_file(filepath):
	if os.path.isfile(filepath):
		with open(filepath, 'rb') as f:
			return hashlib.sha1(f.read()).hexdigest()
	else:
		return False


# DEBUG MODE
if GPIO.input(BTN_UP) == 0:

	# display 
	lcd.clear()
	lcd.write(1,1, 'DEBUG MODE')
	lcd.write(3,1,'{:^20}'.format('CONNECT USB'))
	lcd.write(4,1,'{:^20}'.format('FLASH DRIVE...'))

	time.sleep(3) # minimum time to display splash

	# wait for USB to be mounted 
	while not os.path.isfile( SRC_FOLDER+TARGET_DEBUG_FILE ): time.sleep(0.200) 

	# run debug script and log output on usb stick
	os.system('python %s%s 2>&1 | tee %sdebug-log.txt' % (SRC_FOLDER, TARGET_DEBUG_FILE, SRC_FOLDER))

	# some delay befor proceeding
	time.sleep(3)

	lcd.clear()
	lcd.write(2,1,'{:^20}'.format('ALL DONE.'))
	time.sleep(2)
	lcd.write(3,1,'{:^20}'.format('PROCEEDING...'))
	time.sleep(2)


# UPDATE MODE
def download_config_file():

	# display 
	lcd.clear()
	lcd.write(2,1, '{:^20}'.format('FOUND PFM.CONF'))
	time.sleep(2)
	lcd.write(3,1,'{:^20}'.format('DOWNLOADING...'))
	time.sleep(2) # minimum time

	# make filesystem writable
	os.popen("mount -o remount,rw /")

	shutil.copy(SRC_FOLDER+TARGET_CONFIG_FILE, TARGET_FOLDER+TARGET_CONFIG_FILE)

	# lock filesystem read only again
	os.popen("mount -o remount,ro /")

	# success
	# TODO: make this more solid
	lcd.clear()
	lcd.write(2,1,'{:^20}'.format('PFM.CONF UPDATED'))
	time.sleep(2)  
	lcd.write(3,1,'{:^20}'.format('PROCEEDING...'))
	time.sleep(2)

def download_system_update():

	# display 
	lcd.clear()
	lcd.write(2,1, '{:^20}'.format('FOUND UPDATE.LICENSE'))
	time.sleep(2)
	
	# read licenses in array
	licenses = [line.strip() for line in open(SRC_FOLDER+TARGET_LICENSE_FILE)]

	# display number of updates or error
	if len(licenses) > 0:
		
		lcd.write(3,1,'{:^20}'.format('WITH %s UPDATES...' % str( len(licenses) ) ) )
		time.sleep(2)
		
	else:

		lcd.write(2,1,'{:^20}'.format('NO CONTENT FOUND'))
		time.sleep(2)
		lcd.write(3,1,'{:^20}'.format('ABORTING...'))
		time.sleep(2) # minimum time

		return False

	# make filesystem writable for upcoming updates
	os.popen("mount -o remount,rw /")

	# validate every row and download to device
	saved_files = 0
	for l in licenses:

		# display 
		lcd.clear()
		lcd.write(1,1, 'VERIFYING')

		license = decrypt(KEY, unhexlify(l))
		data = license.split('|')
		filename = data[0]
		sha1 = data[1]

		if filename == TARGET_CONFIG_FILE:
			lcd.write(2,1,'{:^20}'.format(filename))
			time.sleep(2)
			lcd.write(3,1,'{:^20}'.format('SKIPPING...'))
			time.sleep(2)
			lcd.clear()
			time.sleep(1)

			continue

		# display
		lcd.write(3,1,'{:^20}'.format(filename))
		time.sleep(2)

		# check if file has sha1 tag
		if sha1_of_file(SRC_FOLDER+filename) == sha1:

			# display success
			lcd.clear()
			lcd.write(2,1,'{:^20}'.format('FILE VALID'))
			time.sleep(2)
			lcd.write(3,1,'{:^20}'.format('DOWNLOADING...'))
			time.sleep(2)

			# copy file to device
			shutil.copy(SRC_FOLDER+filename, TARGET_FOLDER+filename)
			
			saved_files += 1

			# display success
			lcd.clear()
			lcd.write(2,1,'{:^20}'.format('FILE SAVED.'))
			time.sleep(2)
			lcd.write(3,1,'{:^20}'.format('PROCEEDING...'))
			time.sleep(2)

		else:

			# display unvalid entry
			lcd.clear()
			lcd.write(2,1,'{:^20}'.format('ERROR: FILE INVALID'))
			time.sleep(2)
			lcd.write(3,1,'{:^20}'.format('SKIPPING...'))
			time.sleep(2)

	
	# all done - lock filesystem read only again
	os.popen("mount -o remount,ro /")

	# display all done
	lcd.clear()
	lcd.write(2,1,'{:^20}'.format('ALL DONE'))
	time.sleep(1)
	lcd.write(3,1,'{:^20}'.format('%s FILES SAVED.' % saved_files))
	time.sleep(2)		

if GPIO.input(BTN_SET) == 0:

	# display 
	lcd.clear()
	lcd.write(1,1, 'UPDATE MODE')
	lcd.write(3,1,'{:^20}'.format('CONNECT USB'))
	lcd.write(4,1,'{:^20}'.format('FLASH DRIVE...'))

	time.sleep(3) # minimum time to display splash

	found_config_file = False
	found_license_file = False

	while True:

		found_config_file = os.path.isfile(SRC_FOLDER+TARGET_CONFIG_FILE)
		found_license_file = os.path.isfile(SRC_FOLDER+TARGET_LICENSE_FILE)

		# download just the config file
		if 		found_config_file and not found_license_file:
			# download and leave
			download_config_file()
			break

		elif 	found_license_file and not found_config_file:
			# download and leave
			download_system_update()
			break

		elif	found_config_file and found_license_file:
			# both - one after the other
			download_config_file()
			time.sleep(2)
			download_system_update()
			break

		# save some cpu
		time.sleep(0.250)

# say bye
lcd.clear()
GPIO.cleanup()