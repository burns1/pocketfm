#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

from serlcd import SerLCD
import time
from PFMstatics import *

class Screen(object):

	def __init__(self, contents=None, cursor=False, cursor_pos=0):
		self.contents = contents
		self.cursor = cursor
		self.cursor_pos = cursor_pos

		self.max_cursor_pos = None
		self.get_max_cursor_pos()

		self.first_appearance = True

	# loop through all contents that can have a
	# cursor and get the largest and store it
	def get_max_cursor_pos(self):
		for l, c in self.contents.iteritems():
			if hasattr(c, 'cursor'):
				if c.cursor > self.max_cursor_pos:
					self.max_cursor_pos = c.cursor

	def cursor_down(self, callback_dummy=None):
		if self.cursor_pos < self.max_cursor_pos:
			self.cursor_pos += 1

		self.update_cursor()

	def cursor_up(self, callback_dummy=None):
		if self.cursor_pos > 0:
			self.cursor_pos -= 1

		self.update_cursor()

	def get_cursor_pos(self):
		return self.cursor_pos

	def draw(self):
		lcd.clear()

		for l, c in self.contents.iteritems():

			lcd.cursor(c.position[0], c.position[1])
			lcd.write(c.formatted())

		self.update_cursor()

	def update(self):

		for l, c in self.contents.iteritems():
			if c.update is True:
				lcd.cursor(c.position[0], c.position[1])
				lcd.write(c.formatted())
				c.update = False

		self.update_cursor()

	def update_cursor(self):
		for l, c in self.contents.iteritems():
			# check if cursor needs to be drawn
			if hasattr(c, 'cursor'):
				lcd.cursor(c.position[0], c.position[1]+4)
				if c.cursor == self.cursor_pos:
					lcd.write('\x7E')
				elif c.cursor is not None:
					lcd.write(' ')

class ScreenContent(object):

	def __init__(self, value=None, position=None, display=None, cursor=None, array=None):
		self.value = value
		self.position = position
		self.display = display
		self.cursor = cursor
		self.array = array

		# flag for update lcd
		self.update = True

		# buffer for show hide
		self._display = self.display

	def show(self):
		# restore buffered
		self.display = self._display
		self.update = True

	def hide(self):
		# buffer
		self._display = self.display

		# over write exactly old string
		self.display = ' '*len( self.formatted(True) )

		self.update = True

	def update_value(self, new_value):

		self.value = new_value

		self.update = True

	def formatted(self, buffered=False):
		output = self.value

		if self.array is not None:
			output = self.array[self.value]

		if buffered is not False:
			return self.display.format(output)
		else:
			return self._display.format(output)
