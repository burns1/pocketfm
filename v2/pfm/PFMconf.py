#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT
# zsolt@softmonsters.com
#

#from PFMstatics import *
import os
import ConfigParser
debug = 0

class ConfHandle():

        def __init__(self, conf_label_path, rw_conf_file_path, ro_conf_file_path):
                self.configit=ConfigParser.ConfigParser()
                self.conf_label = conf_label_path
                self.rw_conf_file = ""
                if debug > 0:
                        print "label: "+self.conf_label
                        print "rw_conf: "+rw_conf_file_path
                        print "ro_conf: "+ro_conf_file_path
                if (os.path.islink(self.conf_label) and os.path.isfile(rw_conf_file_path)):
                    self.conf_file = rw_conf_file_path
                    #set the path here for the read/writable
                    #configuration file
                    self.rw_conf_file = rw_conf_file_path
                else:
                    self.conf_file = ro_conf_file_path
                if debug > 0:
                        print "conf: "+self.conf_file

        def exists_rw(self):
        #check again for existence of the read/write-able
        #configuration file
        #doesnt makes much sense since the check happend
        #already during init?
                if (os.path.islink(self.conf_label) and os.path.isfile(self.rw_conf_file)):
                    if debug > 0:
                        print "rw exists"
                    return True
                else:
                    if debug > 0:
                        print "no rw"
                    return False

        def conf_read(self, getval):
                if (os.path.islink(self.conf_label) and os.path.isfile(self.conf_file)):
                    self.configit.read(self.conf_file)

                if getval == "locked":
                    try:
                        retval = self.configit.getboolean('keylock', 'active')
                    except:
                        print "Can not read config value: locked, return to default"
                        retval = False
                return retval
    
        def conf_write(self, conf_group, conf_param, conf_val):
                if self.exists_rw():
                    if debug > 0:
                        print "conf vals to set: "+conf_group+" "+conf_param+" "+str(conf_val)
                    self.configit.read(self.conf_file)
                    self.configit.set(conf_group, conf_param, conf_val)
                    with open(self.conf_file, "wb") as file2write:
                        self.configit.write(file2write)
                    file2write.close()
                    #try:
                        #self.configit.set(conf_group, conf_param, conf_val)
                        #self.configit.remove('transmitter','input')
                        #self.configit.set('transmitter','input','AUX')
                        #self.configit.write(self.conf_file)
                    #except:
                     #   print "Cant set conf value"
                else:
                    print "Writable configuration file does not exists"
