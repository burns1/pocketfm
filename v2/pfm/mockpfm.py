# classes help emulate the pocket FM for local testing


class MockLCD(object):

    def __init__(self, cols=20, rows=4):
        self.cols = cols
        self.rows = rows
        self.canvas = [' '*cols]*rows
        self.y = 0
        self.x = 0

        self.show_curso = False

        self.draw(self.canvas)

    def draw(self, args):
        i=0;
        print '-'*(self.cols+2)
        while(i<self.rows):
            print '|'+args[i][:self.cols]+'|'
            i+=1
        print '-'*(self.cols+2)


    def clear(self):
        self.canvas = [' '*self.cols]*self.rows
        self.draw(self.canvas)

    def cursor(self, row, col):
        self.y = row-1
        self.x = col-1

    def write(self, *args):
        # print "canvas", self.canvas
        # print "args", args, len(args[0])
        # print "y", self.y, "x", self.x
        if len(args) == 1:
            string, = args
        elif len(args) == 3:
            self.y = args[0]
            self.x = args[1]
            string = args[2]
        # always allow canvas to be larger than LCD, so that should we include
        # scrolling menu support ever
        if self.y > len(self.canvas)-1:
            print self.canvas
            self.canvas += [' '*self.cols]*(self.y-(len(self.canvas)-1))
            print self.canvas

        left = self.canvas[self.y][:self.x]
        right = self.canvas[self.y][self.x+len(string):]
        self.canvas[self.y] = left + string + right
        self.draw(self.canvas)

    def show_cursor(self,boolean=False):
        self.show_curso = boolean


import time
import sys
import termios
import fcntl
import os
class MockGPIO(object):

    def __init__(self):
        from threading import Thread

        # attributes:
        self.BCM = None
        self.IN = None
        self.PUD_UP = None
        self.FALLING = None
        self.OUT = None
        self.LOW = None
        self.HIGH = None


        self.callbacks = {}

        self.poller = Thread(target=self.poll_keyboard)
        self.poller.daemon = True
        self.poller.start()

        # map GPIO pins to a keyboard key
        self.pin_to_key = [ "0","1","2","3","4","5","6","7","8","9",
                            "a","b","c","d","e","f","g","h","i","j",
                            "e","f","g","h","i","j","k","l","m","n",
                            "o","p","q","r","s","t","u","v","w","x", ]


    def add_event_detect(self,pin,edge,callback,bouncetime):
        key = self.pin_to_key[pin]
        print '## ADDED CALLBACK FOR PIN: %s KEY: "%s"', pin, key
        self.callbacks[key] = callback

    def poll_keyboard(self):
        print "## KEYBOARD POLLING THREAD STARTED"

        while True:
            time.sleep(0.1)

            c = None
            c = self.getch()
            if c in self.callbacks.keys():
                print "callback", self.callbacks[c]
                pin = self.pin_to_key.index(c)
                self.callbacks[c](pin)
            elif c:
                print "valid keys: ", self.callbacks.keys()

    def getch(self):
        # yeah, find another way to get just one char from stdin without
        # line buffer
        fd = sys.stdin.fileno()
        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
        try:
            while 1:
                try:
                    c = sys.stdin.read(1)
                    break
                except IOError: pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
            fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
        return c



    def setwarnings(self,boolean):
        return
        # self.warnings = boolean

    def setmode(self,pin_naming_convinsion):
        return

    def setup(self,pin,in_or_out,pull_up_down=None):
        return

    def output(self, led, highlow):
        return

    def cleanup(self):
        return

    def input(self, channel):
        return 1

class MockAdafruit_Si4713(object):

    def __init__(self,resetpin):
        return

    def begin(self, mode):
        return

    def tuneFM(sef, tx_freq):
        return
