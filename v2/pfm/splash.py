#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

from PFMstatics import *
import sys

# catching errors from rc.local
errors = int(sys.argv[1])
error_list = []

# decode error integer form rc.local
# 1: DVB issue
# 2: RTC issue
# 4: INA issue (TODO)
# 8: SI4713 issue (TODO)
if (errors & (1<<0)):
    error_list.append('DVB Device failed.')
if (errors & (1<<1)):
    error_list.append('RTC init failed.')    


# display possible errors
if len(error_list) > 0:

    lcd.clear()

    # blink message
    for i in range(3):
        time.sleep(0.6)
        lcd.write(1,1,'{:<20}'.format('            '))
        time.sleep(0.4)
        lcd.write(1,1,'{:<20}'.format('BOOT ERRORS:')) # last to make it stay
    
    time.sleep(1) # just rythm

    # list errors on after another
    for e in error_list:
        lcd.write(3,1,'                    ') # clear line again
        lcd.write(3,1,'{:^20}'.format(e))
        time.sleep(3)

    # clear error screen
    lcd.clear()
    time.sleep(1)
        
    # display revision to user for better debugging
    lcd.write(2,1,'{:^20}'.format('REV ' + SOFTWARE_REVISION))
    time.sleep(2)
    lcd.clear()

# exit and move on in rc.local (next: updater.py)
exit()