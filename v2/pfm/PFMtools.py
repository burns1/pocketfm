#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

from PFMstatics import *
import os

# If not arm then put in development mode (fake lcd, gpio, etc)
if os.uname()[4][:3] != 'arm':
	from mockpfm import MockAdafruit_Si4713 as Adafruit_Si4713
else:
	from Adafruit_Si4713 import Adafruit_Si4713



import time, subprocess, shlex
import ConfigParser

class MPlayerControl(object):

        def __init__(self, silence_file, usb_playlist, sat_playlist, int_playlist):
                self.silence_cmd = silence_file
                self.usb_cmd = '-playlist ' + usb_playlist
                self.sat_cmd = '-playlist ' + sat_playlist
                self.int_cmd = '-playlist ' + int_playlist

                # bring up the process and play back silence
                try:
                    print "cmd: "+basic_cmd + self.silence_cmd
                    self.p = subprocess.Popen( shlex.split( basic_cmd + self.silence_cmd ) )
                except:
                    print "can not start mplayer"
                self._mode = 'AUX' # AUX = silence for digital inputs

        def play(self, mode):

                if mode == self._mode:

                # do nothing when mode is not changed
                        return True

                else:

                # update flag
                    self._mode = mode

                # kill existing processes
                if self.p.poll() is None:
                        self.p.kill()

                # bring up again
                if mode == 'SAT':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.sat_cmd ) )

                elif mode == 'USB':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.usb_cmd ) )
                elif mode == 'INT':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.int_cmd ) )
                        
                # play back silence during aux for better audio transitions
                elif mode == 'AUX':
                        self.p = subprocess.Popen( shlex.split( basic_cmd + self.silence_cmd ) )


        def mute(self):

                self.play('AUX')



class RadioControl(Adafruit_Si4713):

	# match input sources to analog/digital modes
	# as specified in settings.py
	src_tx_pairs = dict(src_tx_pairs)

	def __init__(self, rst_pin, led_pin, src, fm_freq, fm_rds, fm_power):

		Adafruit_Si4713.__init__(self, resetpin=rst_pin)

		# derive corrent TX mode
		self._tx_mode = self.src_tx_pairs[src]

		# get frequency in the right format
		self._tx_freq = int(fm_freq * 100)

		# TODO: truncate?
		self._tx_rds = fm_rds

		self._tx_power = fm_power

		# GPIO setup
		self._led = led_pin
		self.gpio_setup()
		self.startup()

        #conf space
		self.conf_label_path = "/dev/disk/by-label/pfm_conf"
		self.conf_file_path = "/var/run/usbmount/pfm_conf/pfm.conf"

		# start up radio in right mode
		self.startup()

	def gpio_setup(self):
		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)

		GPIO.setup(self._led, GPIO.OUT)

		# turn off by default
		GPIO.output(self._led, GPIO.LOW)

	def tx_led_on(self):
		GPIO.output(self._led, GPIO.HIGH)

	def tx_led_off(self):
		GPIO.output(self._led, GPIO.LOW)

	def set_tx_freq(self, new_freq):
		# get frequency in the right format
		self._tx_freq = int(new_freq * 100)

		self.tuneFM(self._tx_freq)

	def startup(self):

		self.tx_led_off()

		# power up sequence
		if not self.begin(self._tx_mode):
			print "Si4713 start up failed."

		else:

			# turn on tx led
			self.tx_led_on()

			# adjust settings
			self.setTXpower(self._tx_power)
			self.tuneFM(self._tx_freq)

			self.beginRDS()

			self.setRDSstation(self._tx_rds)


	def status(self):
		"""Some functions to test the info and status features of the si chips
		   not fully integrated yet"""

		print """
		TX_TUNE_STATUS
		Queries the status of a previously sent TX Tune Freq, TX Tune
		Power, or TX Tune Measure command.
		"""

		self.tx.readTuneStatus()

		print "freq:", self.tx.currFreq,
		print "eg. 10010 = 100.10"

		print "power:", self.tx.currdBuV,
		print "eg. 118 dBuV"

		print "antenna capacitance:", self.tx.currAntCap,
		print "eg. 1. (0-191). Did not change we we disconnected antenna"

		print "Noise level:", self.tx.currNoiseLevel,
		print "eg. 0"

		print """
		TX_ASQ_STATUS
		Queries the TX status and input audio signal metrics.
		"""

		self.tx.readASQ()

		print "ASQ:", self.tx.currASQ,
		print "eg. 1 or 5. No idea why it changes when we change volume"

		print "inLevel:", self.tx.currInLevel,
		print "eg. -55 to -6. Changes with volume level"

		print """
		TX_TUNE_MEASURE
		Measure the received noise level at the specified frequency.
		I dont know what this is for actually
		"""

		self.tx.readTuneMeasure(20020)

	def get_active_mode(self):
		return self._tx_mode

	def adapt_to_input(self, new_src):
		# check if tx_mode has to be changed
		new_tx_mode = self.src_tx_pairs[ new_src ]
		if new_tx_mode != self.get_active_mode():

			# store new mode
			self._tx_mode = new_tx_mode

			# start up in right mode
			self.startup()

class USBDaemon(object):

	def __init__(self, mount_point='/media/usb0/', file_types_accepted=['.mp3', '.wav', '.m4a'], playlist_file='/media/usb0/usb.playlist'):

		self.mount_point = mount_point
		self.playlist_file = playlist_file
		self.file_types_accepted = self.extend_extension_tolerance(file_types_accepted)
		self.usb_present = self.is_mounted()
		self.ready_for_playback = False

	###
	### DETECT USB DRIVE
	###

	def is_mounted(self):
		return os.path.ismount(self.mount_point)

	###
	### MEDIA FUNCTIONS
	###

	def extend_extension_tolerance(self, filetypes):
		output = []

		#
		for ft in filetypes:
			output.append(ft.lower())
			output.append(ft.upper())

		return output


	def list_audio_files(self):

		found_audio_files = []

		# recursively walk through usb thumb drive
		for ROOT,DIR,FILES in os.walk(self.mount_point):
			for file in FILES:
				# restrict results to specific filetypes and miss out hidden files (.*)
				if file.endswith(tuple(self.file_types_accepted)) and not file.startswith(('.')):
					found_audio_files.append(file)

		return found_audio_files


	def add_to_playlist(self, files_array):

		pl = open(self.playlist_file, 'w')

		for row in files_array:
			pl.write("%s/%s\n" % (self.mount_point, row))

		pl.close()

		return self.playlist_file

class SATQuality(object):

	# get one human readable sample, then exit
	femon_cmd = "femon -H -c 1"

	# watch for fail if SAT device is not initialised properly
	femon_fail_msg = "failed"

	# get channel names of current transponder
	# scan takes a few seconds so should only call sparingly
	scan_cmd = "scan -c -q"

	# TODO: perhaps this should not be here:
	channelsconf = PFM_DIR+"/channels.conf"

	def __init__(self):
		self._signal = False
		self._output = None
		self._status = False
		self._lock   = False

		self._scan   = None
		self._name   = False
		self._apid   = self.wanted_apid()

	def wanted_apid(self):
		# only need run once on startup
		name = open(sat_playlist).readline()[6:].rstrip() # after dvb://
		for channel in open(self.channelsconf).readlines():
			if channel.find(name) == 0:
				return int(channel.split(':')[6])
		return None

	def parse_femon_output(self, femon_row, value, expected_chars):
			try:
				start_pos = femon_row.index(value) + len(value) + 1 # +1 because of space between label and value
				end_pos = start_pos + expected_chars
				return femon_row[start_pos:end_pos]
			except:
				return " "*expected_chars

	def is_signal(self):
		# if there is no output, generate it first
		self.query([])

		self._status = self.parse_femon_output(self._output, 'status', 5)

		# if status is blank, then no signal is received at all
		# dish is not connected or not pointing in right direction
		# or hardware error
		if self._status and self._status.strip():
			self._signal = True
		else:
			self._signal = False

		return self._signal

	def is_lock(self):
		if not self._output:
			self.query([])
		if not self._status:
			self._status = self.parse_femon_output(self._output, 'status', 5)

		if self._status[4] == 'L':
			self._lock = True
		else:
			self._lock = False

		return self._lock

	def get_status(self):
		if self._status:
			return self._status
		else:
			return ""

	def query(self, parameters_array):

		# prepare return
		output = []

		# get one sample from femon command
		# pass stderr to output, to check if it failed
		self._output = subprocess.check_output(self.femon_cmd, stderr=subprocess.STDOUT, shell=True)

		# not the most elegant way to check for a failed SAT device, but it works (TODO: try, except)
		if not self.femon_fail_msg in self._output:

			# get isolated values for queried parameters
			for p in parameters_array:
				# only two-digit values allowed right now
				# have to append a space - quick fix
				# (TODO: make it more solid)
				output.append( self.parse_femon_output(self._output, p+' ', 2) )

		else:

			# some error occured
			# return empty results so we do not cause errors
			output = [""]*len(parameters_array)


		return output

	def query_name(self):
		self._name = False
		if not self._signal:
			return self._name

		self._scan = subprocess.check_output(self.scan_cmd, stderr=subprocess.STDOUT, shell=True)
		for channel in self._scan.split("\n"):
			if channel.find("%x"%self._apid) >= 0:
				self._name = channel.split('(')[0].rstrip()
				break

		return self._name

	def get_name(self):
		if self._name:
			return self._name
		else:
			return ""
