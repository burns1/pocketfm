#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

import sys
from PFMstatics import *

# pass errors
errors = sys.argv[1]

# user input
pin_input_index = 0
pin_input = [0,0,0,0]
pin_input_index_offset = 7 # xoffset to center pin digits on screen -->1 2 3 4<--

def pinlock():
    global locked

    init_pin_screen()

    # initialise keys
    GPIO.add_event_detect(BTN_UP,  GPIO.FALLING, callback = button_callback, bouncetime = BUTTON_BOUNCE)
    GPIO.add_event_detect(BTN_DWN, GPIO.FALLING, callback = button_callback, bouncetime = BUTTON_BOUNCE)
    GPIO.add_event_detect(BTN_SET, GPIO.FALLING, callback = button_callback, bouncetime = BUTTON_BOUNCE)

    while locked == True:
        # save some cpu cycles
        time.sleep(1)

    # hide cursor when done
    lcd.show_cursor(False)

def button_callback(pin):

    if   pin == BTN_UP:     increase_digit()
    elif pin == BTN_DWN:    decrease_digit()
    elif pin == BTN_SET:    enter_digit()

def init_pin_screen():

    lcd.clear()

    lcd.write(1,1,'ENTER PIN')

    draw_digits()

    # place & enable cursor
    lcd.cursor(3, pin_input_index_offset)
    lcd.show_cursor()


def draw_digits():

    for digit_no in range(4):
        digit_pos = pin_input_index_offset + digit_no*2
        lcd.write(3, digit_pos, str( pin_input[digit_no] ))


def update_pin_screen():

    draw_digits()

    # update cursor position
    pos = pin_input_index_offset + pin_input_index*2
    lcd.cursor(3,pos)


def increase_digit():
    global pin_input

    # loop at borders
    if pin_input[pin_input_index] < 9:
        pin_input[pin_input_index] += 1
    else:
        pin_input[pin_input_index] = 0

    update_pin_screen()


def decrease_digit():
    global pin_input

    # loop at borders
    if pin_input[pin_input_index] > 0:
        pin_input[pin_input_index] -= 1
    else:
        pin_input[pin_input_index] = 9

    update_pin_screen()


def enter_digit():

    global pin_input_index

    # check for last index position
    if pin_input_index == 3:

        # verify input
        pin_str = "".join( map(str, pin_input) )

        if pin_str == pin_target:
            pin_success()
        else:
            pin_wrong()

    else:

        # shift cursor to next digit
        pin_input_index += 1

        update_pin_screen()


def pin_success():
    global locked

    lcd.clear()
    lcd.show_cursor(False)

    lcd.write(2,1, '{:^20}'.format('PIN VERIFIED'))
    time.sleep(1)
    lcd.write(3,1, '{:^20}'.format('UNLOCKING...'))
    time.sleep(1)

    locked = False

    start_pfm()

def pin_wrong():
    global pin_input, pin_input_index

    lcd.clear()
    lcd.show_cursor(False)

    lcd.write(2,1, '{:^20}'.format('INVALID PIN'))
    time.sleep(1)

    lcd.write(3,1, '{:^20}'.format('TRY AGAIN...'))
    time.sleep(1)

    # reset user input
    pin_input_index = 0
    pin_input = [0,0,0,0]

    # start over
    init_pin_screen()

def start_pfm():
	os.system('python /home/pfm/pfmd.py %s' % errors)



# MAIN PROGRAM HERE

if locked == False:
	print "No pinlock - just start pfmd.py"
	start_pfm()
else:
	print "Device locked - enter pinlock"
	pinlock()

exit()
