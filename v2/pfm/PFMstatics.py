#!/usr/bin/python2

#
# POCKET FM (c) 2015 by MICT & IXDS
# Visit: http://pocket-fm.com
# Developer: hans.kadel@ixds.com
#
# Modifications: Zsolt & Nathan


############################
SOFTWARE_REVISION = "0.2.1"
############################
# ...as rendered in splash

import os
import ConfigParser
import time
import collections

# If not arm then put in development mode (fake lcd, gpio, etc)
#FIXME: Should probably go into a configuration value since you could
#use an arm machine for simulation too
if os.uname()[4][:3] != 'arm':
    PFM_DIR = './'
    from mockpfm import MockLCD as SerLCD, MockGPIO as _GPIO
    GPIO=_GPIO()
    basic_cmd = "./dummyplayer.sh "
    rw_conf_file_path = PFM_DIR+"pfm_conf/pfm.conf"
    conf_label_path = PFM_DIR+"pfm_conf/label_dummy"    
else:
    PFM_DIR = '/home/pfm/'
    import RPi.GPIO as GPIO
    from serlcd import SerLCD
    basic_cmd = "mplayer -ao pulse -srate 48000 -really-quiet -vo null -nolirc -framedrop -loop 0 "
    #for usbmount but this doesnt works in boot/coldplug
    #mode due to unionfs
    #conf_file_path = "/var/run/usbmount/pfm_conf/pfm.conf"
    #path with mount via fstab
    rw_conf_file_path = PFM_DIR + "pfm_conf/pfm.conf"
    conf_label_path = "/dev/disk/by-label/pfm_conf"

# init display
lcd = SerLCD()
lcd.clear()

# software debounce of buttons
BUTTON_BOUNCE = 200

# GPIO SETTINGS
BTN_UP  = 13
BTN_DWN = 26
BTN_SET = 5

LED_TX = 12
RST_LINE = 24

GPIO.setwarnings(False)
#setmode accepts 2 values here: GPIO.BOARD and GPIO.BCM
#BCM enables the Broadcom SOC channel naming scheme
#BOARD specifies a more general naming scheme
#This is not available in the Adafruit_BBIO lib
GPIO.setmode(GPIO.BCM)

GPIO.setup(BTN_UP, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(BTN_DWN, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(BTN_SET, GPIO.IN, pull_up_down = GPIO.PUD_UP)

# basic MPlayer command for gapless I2S audio at 48kHz, zero output to user, video disabled and looping
#basic_cmd = "mplayer -ao pulse -srate 48000 -really-quiet -vo null -nolirc -framedrop -loop 0 "

# USB settings: mount point, playlist location, extensions
# If we have writeable configuration and storage space
# mount point changes to: /media/usb2
# For writeable storage this would be: /home/pfm/pfm_store
if (os.path.islink(conf_label_path)):
    mount_point = '/media/usb2'
else:
    mount_point = '/media/usb0'

#For the time beeing assume that we have only 2 locations
#1. Removable automatically mounted and the playlist
#automatically generated
#2. A fixed one. Playlist generation needs to be implemented.
#playlist_file is for removable storage
playlist_file = '/media/usb0/usb.playlist'
#playlist_file for media on internal storage
playlist_int = '/home/pfm/pfm_store/int.playlist'

usb_filetypes = ['.mp3', '.wav', '.m4a']
ro_conf_file_path = PFM_DIR+"pfm.conf"

# updater settings
TARGET_CONFIG_FILE = 'pfm.conf'
TARGET_LICENSE_FILE = 'update.license'
TARGET_DEBUG_FILE = 'debug.py'
SRC_FOLDER = '/media/usb/'
TARGET_FOLDER = PFM_DIR
KEY = 'powerplay'

# SAT settings
sat_playlist = PFM_DIR+'sat.playlist'

# load config file
config = ConfigParser.ConfigParser()
#check here if our config space is loaded and load this one
#FIXME: we need a genral class for this. Outsource this whole checking and
#config parsing to a module
if (os.path.islink(conf_label_path) and os.path.isfile(rw_conf_file_path)):
    config.read(rw_conf_file_path)
else:
    #if not go for the default one
    config.read(ro_conf_file_path)

# input sources and their specific tx modes
src_tx_pairs = collections.OrderedDict([
    ("AUX", 'analog'),
    ("SAT", 'digital'),
    ("USB", 'digital'),
    ("INT", 'digital')
])

try:
	# pin lock settings from config
	locked     = config.getboolean('keylock', 'active')
	pin_target = config.get('keylock', 'pin')

	# radio constants from config
	tx_freq    = float( config.get('transmitter', 'freq') )
	tx_rds_id  = config.get('transmitter', 'station_name')
	tx_input   = config.get('transmitter', 'input')
	tx_power   = int(config.get('transmitter', 'power'))
except:
	print "no or outdated pfm.conf - revert to default"

	locked     = False
	pin_target = "0000"
	tx_freq    = 100.0
	tx_rds_id  = "NO PFM.CONF"
	tx_input   = 0
	tx_power   = 0

	pass


# helper function to calculate time offsets (used in FFWD)
def millis(): return int(round(time.time() * 1000))

FFWD_STEP_SPEED = 0.060
