#!/usr/bin/python2

#
# POCKET FM (c) 2015, MICT & IXDS
# pocket-fm.com / hans.kadel@ixds.com
#

import time, os, sys
from PFMstatics import *
from PFMscreens import Screen, ScreenContent
from PFMtools import MPlayerControl, RadioControl, USBDaemon, SATQuality
from PFMconf import ConfHandle

# catching errors from rc.local
try:
	errors = int(sys.argv[1])
except:
	errors = 0
	pass

sat_disabled = False
# decode error integer form rc.local
if (errors & (1<<0)):
    sat_disabled = True

if not sat_disabled: sat_quality = SATQuality()
usb_daemon = USBDaemon(mount_point, usb_filetypes, playlist_file)
mpl = MPlayerControl(silence_file='/home/pfm/silence.mp3', usb_playlist=playlist_file, sat_playlist='/home/pfm/sat.playlist', int_playlist=playlist_int)
tx = RadioControl(rst_pin=RST_LINE, led_pin=LED_TX, src=tx_input, fm_freq=tx_freq, fm_rds=tx_rds_id, fm_power=tx_power)
config = ConfHandle(conf_label_path, rw_conf_file_path, ro_conf_file_path)
config.conf_write('transmitter','input',"AUX")

# get initial state
is_usb_present = usb_daemon.is_mounted()

# prepare sat playlist in background
if tx_input == 'SAT':
	mpl.play('SAT')

# helpers for conversion of int and str sources
def input_sources_array():
	return list(src_tx_pairs)

def get_source_index(source_string):
	return input_sources_array().index(source_string)

def get_source_str(source_index):
	return input_sources_array()[source_index]

# user interface screens
MainScreen = Screen(
	{
	'rds_id': 	ScreenContent(tx_rds_id, [1,1], "RDS: {:<20}"),
	'freq': 	ScreenContent(tx_freq, [2,1], "FRQ: {:5.2f} ", cursor=0), # note the extra space. needed to properly overwrite 106.30 with 88.00 without having 88.000 on the LCD
	'src': 		ScreenContent(get_source_index(tx_input), [3,1], "SRC: {:<10}", cursor=1, array=input_sources_array()),
	'quality': 	ScreenContent("", [4,1], "QLT: {:<15}", cursor=2)
	}, cursor=True)

FreqScreen = Screen({
	'title': 	ScreenContent("SET FREQUENCY:", [1,1], "{:<20}"),
	'freq': 	ScreenContent(tx_freq, [3,8], "{:6.2f} Mhz")
})

SrcScreen = Screen({
	'title': 	ScreenContent("SELECT INPUT SOURCE:", [1,1], "{:<20}"),
	'src': 		ScreenContent(get_source_index(tx_input), [3,1], "{:^20} ", array=input_sources_array())
})

QualScreen = Screen({
	# Layout 1:
	# 01234567890123456789
	# AGC: ##%  SNR: ##%
    # SIG: #
	# CAR: #    SYNC: #
	# FEC: #    LOCK: #
	# 'agc': ScreenContent("", [1,1], "AGC: {:<3}"),
	# 'snr': ScreenContent("", [1,11], "SNR: {:<3}"),
	# 'signal': 	ScreenContent("", [2,1], "SIG: {:<1}"),
	# 'carrier': 	ScreenContent("", [3,1], "CAR: {:<1}"),
	# 'viterbi': 	ScreenContent("", [4,1], "FEC: {:<1}"),
	# 'sync': 	ScreenContent("", [3,11], "SYNC: {:<1}"),
	# 'lock': 	ScreenContent("", [4,11], "LOCK: {:<1}"),

	# Layout 2:
	# 01234567890123456789
	# AGC:################
	# SNR:################
	# SIG: # CAR: # FEC: #
	# LCK: # SYN: #
	# 'agc': ScreenContent("", [1,1], "AGC:{:<16}"),
	# 'snr': ScreenContent("", [2,1], "SNR:{:<16}"),
	# 'signal': 	ScreenContent("", [3,1], "SIG: {:<1}"),
	# 'carrier': 	ScreenContent("", [3,7], "CAR: {:<1}"),
	# 'viterbi': 	ScreenContent("", [3,14], "FEC: {:<1}"),
	# 'sync': 	ScreenContent("", [4,1], "SYN: {:<1}"),
	# 'lock': 	ScreenContent("", [4,7], "LCK: {:<1}"),

	# Layout 3:
	# 01234567890123456789
	# NAME:###############
	# AGC: ##%  SNR: ##%
	# LCK: # SYN: #
	# SIG: # CAR: # FEC: #
	'name': ScreenContent("", [1,1], "NAME:{:<15}"),
	'agc': ScreenContent("", [2,1], "AGC: {:<3}"),
	'snr': ScreenContent("", [2,11], "SNR: {:<3}"),
	'sync': 	ScreenContent("", [3,1], "SYN: {:<1}"),
	'lock': 	ScreenContent("", [3,8], "LCK: {:<1}"),
	'signal': 	ScreenContent("", [4,1], "SIG: {:<1}"),
	'carrier': 	ScreenContent("", [4,8], "CAR: {:<1}"),
	'viterbi': 	ScreenContent("", [4,15], "FEC: {:<1}"),
})

context = 'main'
screens = {'main': MainScreen, 'freq': FreqScreen, 'src': SrcScreen, 'qual': QualScreen }

# print to lcd
screens[context].draw()

def switch_screen(new_context):
	global context

	context = new_context
	screens[context].draw()

def callback_button(button=None):

	if context == 'main':
		buttons_main(button)

	elif context == 'freq':
		buttons_freq(button)

	elif context == 'src':
		buttons_src(button)

	elif context == 'qual':
		buttons_qual(button)


def buttons_main(button):

	if button == BTN_UP:
		screens[context].cursor_up()

	if button == BTN_DWN:
		screens[context].cursor_down()

	if button == BTN_SET:
		cursor = screens[context].get_cursor_pos()

		if cursor == 0:
			switch_screen('freq')
		if cursor == 1:
			switch_screen('src')
		if cursor == 2 and tx_input == 'SAT':
			switch_screen('qual')

def buttons_freq(button):
        if button == BTN_UP:
                tune_fm_freq(FreqScreen.contents['freq'].value, 0.1, button, ffwd=True)

        if button == BTN_DWN:
                tune_fm_freq(FreqScreen.contents['freq'].value, 0.1, button, ffwd=True)

        if button == BTN_SET:
                # check if selected frequency differs from what is transmitting
                if FreqScreen.contents['freq'].value != MainScreen.contents['freq'].value:
                    MainScreen.contents['freq'].update_value(FreqScreen.contents['freq'].value)
                    #conf update
                    config.conf_write('transmitter', 'freq', FreqScreen.contents['freq'].value)

                    # adjust fm transmitter
                    freq = FreqScreen.contents['freq'].value
                    tx.set_tx_freq(freq)

        # switch screen
		switch_screen('main')

def buttons_src(button):
        global tx_input, mplayer_process
        if button == BTN_UP:
            select_src(SrcScreen.contents['src'].value, -1, min=0, max=3)

        if button == BTN_DWN:
            select_src(SrcScreen.contents['src'].value, 1, min=0, max=3)

        if button == BTN_SET:
            # prepare data to compare and work with: index (int) & label (str)
            new_src = SrcScreen.contents['src'].value
            old_src = MainScreen.contents['src'].value
            new_src_str = get_source_str(new_src)

            # check if selected source differs from what is transmitting
            if new_src != old_src:
                MainScreen.contents['src'].update_value( new_src )
                #conf update for input source
                config.conf_write('transmitter', 'input', get_source_str(new_src))

                # adjust fm transmitter - if needed
                tx.adapt_to_input(new_src_str)
                tx_input = new_src_str

                # prepare playlist for sat and other inputs
                if new_src_str == 'SAT':
                    mpl.play('SAT')
                elif new_src_str == 'USB':
                    mpl.play('USB')
                elif new_src_str == 'INT':
                    mpl.play('INT')
                    
                # uncomment if you wish to kill SAT mplayer on change to AUX
                # this has the disadvantage of adding delay between switch
                # but may improve power?
                # TODO: test if there are any power benifits
                #elif new_src_str == 'AUX':
                #	mpl.play('AUX')

                # blank out QLT: line if not SAT input
                # TODO: this is ugly, solve anders
            if new_src_str != 'SAT':
                MainScreen.contents['quality'].update_value('               ')

            # switch screen
            switch_screen('main')

def buttons_qual(button):
    #FIXME: set PIN on/off here
	if button == BTN_SET:
		# switch screen
		switch_screen('main')

def select_src(value, incr, ffwd=False, min=0, max=2, cycle=True):
	# calculate new value upfront
	new_value = value + incr

	# check if within borders
	if new_value <= max and new_value >= min:
		value = new_value

	# loop if cycle is True
	elif cycle == True:
		if new_value < min:
			value = max
		elif new_value > max:
			value = min

	# update lcd
	SrcScreen.contents['src'].update_value(value)
	screens[context].update()


def tune_fm_freq(value, incr, channel, ffwd=False, min=76.0, max=108.0, cycle=False):

		# calculate direction from button
		# (channel is needed further down for
		# FFWD processing)
		if channel == BTN_UP:
			direction = 1
		elif channel == BTN_DWN:
			direction = -1


		# calculate new value upfront
		new_value = value + ( incr * direction )

		# check if within borders
		if new_value <= max and new_value >= min:
			value = new_value

		# loop if cycle is True
		elif cycle == True:
			if new_value < min:
				value = max
			elif new_value > max:
				value = min

		# update lcd
		FreqScreen.contents['freq'].update_value(value)
		screens[context].update()

		# single touch
		offset = millis()

		# do fast forward if desirable
		if ffwd is True:

			while(GPIO.input(channel)==0):

				elapsed_time = millis() - offset

				if elapsed_time > 500:

					# wunschkind
					new_value = value + ( incr * direction * 1 )

					# check if within borders
					if new_value <= max and new_value >= min:
						value = new_value
						FreqScreen.contents['freq'].update_value(value)
						screens[context].update()

					time.sleep(FFWD_STEP_SPEED)

			# instantly leave the function
			return None


GPIO.add_event_detect(BTN_UP,	GPIO.FALLING, callback = callback_button,	bouncetime=BUTTON_BOUNCE)
GPIO.add_event_detect(BTN_DWN,	GPIO.FALLING, callback = callback_button,	bouncetime=BUTTON_BOUNCE)
GPIO.add_event_detect(BTN_SET,	GPIO.FALLING, callback = callback_button,	bouncetime=BUTTON_BOUNCE)


if tx_input == 'SAT':
	sat_active = True
else:
	sat_active = False
	MainScreen.contents['quality'].hide()

sig_chars = 16

def monitor_sat_name():
	if tx_input == 'SAT':
		sat_quality.query_name()
		name = sat_quality.get_name()[:15]
		QualScreen.contents['name'].update_value(name)

		if context == 'qual':
			QualScreen.update()

def monitor_sat_signal():

	if tx_input == 'SAT':
		quality = sat_quality.query(['signal', 'snr'])
		name = sat_quality.get_name()

		if sat_quality.is_signal():
			status  = sat_quality.get_status()
			QualScreen.contents['name'].update_value(name[:15])
			QualScreen.contents['agc'].update_value(quality[0]+'%')
			QualScreen.contents['snr'].update_value(quality[1]+'%')
			QualScreen.contents['signal'].update_value(status[0])
			QualScreen.contents['carrier'].update_value(status[1])
			QualScreen.contents['viterbi'].update_value(status[2])
			QualScreen.contents['sync'].update_value(status[3])
			QualScreen.contents['lock'].update_value(status[4])

			if sat_quality.is_lock():
				if name:
					MainScreen.contents['quality'].update_value( name[:15])
				else:
					MainScreen.contents['quality'].update_value( quality[0] + '/' + quality[1] + " \xBC") #\xBC jap smiley
			else:
				MainScreen.contents['quality'].update_value( quality[0] + '/' + quality[1] + ' ' + status)

		else:
			MainScreen.contents['quality'].update_value('NO SIGNAL')
			QualScreen.contents['name'].update_value('NO SIGNAL')
			QualScreen.contents['agc'].update_value(quality[0]+'%')
			QualScreen.contents['snr'].update_value(quality[1]+'%')
			QualScreen.contents['signal'].update_value('')
			QualScreen.contents['carrier'].update_value('')
			QualScreen.contents['viterbi'].update_value('')
			QualScreen.contents['sync'].update_value('')
			QualScreen.contents['lock'].update_value('')

	else:
		MainScreen.contents['quality'].update_value('               ')
		MainScreen.contents['quality'].display = '                   '

	# TODO: REMOVE
	# This was a quick hack to get the Devices DHCP assigned IP
	# import subprocess
	# cmd = "ifconfig eth1 | grep 'inet addr' | awk '{print $2}' | sed 's/addr://'"
	# ip = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
	# MainScreen.contents['quality'].update_value(ip)
	# cmd2 = "ifconfig wlan0 | grep 'inet addr' | awk '{print $2}' | sed 's/addr://'"
	# ip2 = subprocess.check_output(cmd2, stderr=subprocess.STDOUT, shell=True)
	# MainScreen.contents['rds_id'].update_value(ip2)

	# finally - update LCD
	# TODO: fix this "bug"
	if context == 'main':
		MainScreen.update()
	elif context == 'qual':
		QualScreen.update()

tx_input_previous = tx_input
def usb_attached():
	global is_usb_present, tx_input_previous

	print "USB connected"

	MainScreen.contents['src'].update_value( get_source_index('USB') )

	mpl.mute()

	is_usb_present = True
	tx_input_previous = tx_input

	# gather all audio files
	audio_files = usb_daemon.list_audio_files()

	# set up playlist
	usb_daemon.add_to_playlist(audio_files)

	# send to mplayer
    #dont play media on usb automatically. Since there
    #are 2 types of media storage right now
	#mpl.play('USB')

	# switch tx mode
	#tx.adapt_to_input('USB')


def usb_removed():
	global is_usb_present, tx_input

	print "USB disconnected"

	is_usb_present = False
	tx_input = tx_input_previous

	mpl.mute()

	tx.adapt_to_input( tx_input )

	mpl.play( tx_input )

	# update user interface
	MainScreen.contents['src'].update_value( get_source_index(tx_input) )

###
### MAIN LOOP
###

#TODO: ugly hack run monitor_sat_name() slower than main loop
scannameon = 10
scantick = scannameon

try:

	while True:

		# check if usb drive is connected
		if usb_daemon.is_mounted() and not is_usb_present: 		usb_attached()

		# check if usb drive is disconnected
		elif is_usb_present and not usb_daemon.is_mounted():	usb_removed()


		if not sat_disabled:

			if (context == 'main' or context == 'qual'):

				# check for sat signal quality
				monitor_sat_signal()

				# Get channel name
				if scantick == scannameon:
					scantick = 0
					monitor_sat_name()
				else:
					scantick += 1


		time.sleep(0.5)

except KeyboardInterrupt:
	os.system('pkill mplayer') # kill mplayer so it gets restarted to simulate rc.local
	os.system('pkill python') # kill any existing python thats around
	print "Bye! :)"

except:
	# COMMENT OUT FOR DEVELOPMENT
	pass

finally:
	GPIO.cleanup() # this ensures a clean exit
