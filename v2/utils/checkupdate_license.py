#!/usr/bin/python
#
#reads license file and shows value
#

from simplecrypt import decrypt
from binascii import hexlify, unhexlify
import shutil, hashlib

LICENSE_FILE = "update.license"
KEY = "powerplay"

licenses = [line.strip() for line in open(LICENSE_FILE)]

for l in licenses:
    license = decrypt(KEY, unhexlify(l))
    data = license.split('|')
    filename = data[0]
    sha1 = data[1]

    print "filename: "+filename
    print "sha1: "+sha1

