#!/usr/bin/env python

# This will make an update
# supply files as arguments
# files should be intended for copy into pfm home dir

# pip install simple-crypt
from simplecrypt import decrypt, encrypt

import sys

if len(sys.argv) < 2:
    print "Usage: %s <KEY> <file1> [<fileN> ...]"%sys.argv[0]
    print """
    KEY = the key used to encrypt update/license entries
          grep KEY PFMstatics.py
    """
    sys.exit(1)


import time
from binascii import hexlify, unhexlify
import shutil, hashlib


KEY=sys.argv[1]

updatefile = ""
for file in sys.argv[2:]:
    hash=hashlib.sha1(open(file).read()).hexdigest()
    entry="%s|%s"%(file,hash)
    sys.stderr.write(entry+"\n")
    licensed=hexlify(encrypt(KEY, entry))
    updatefile += licensed+"\n"

sys.stderr.write("\nThe update file should match the following contents:\n")
print updatefile,
