#!/bin/bash
#

#in ap mode the ip of the box is always the same
IPOFTHEBOX="192.168.86.1"
TARBALLSYS="pfm_sys_prod_080816_2.tar.gz"
TARBALLCONF=""
TARBALLAPP="app_update.tar.gz"

#get current date on local machine
#
CURDATE=`date +%m%d%H%M%y`

#set current date first
ssh root@$IPOFTHEBOX "date $CURDATE"

#mount the production partition
ssh root@$IPOFTHEBOX "/tmp/sys_prod"
ssh root@$IPOFTHEBOX "mount /dev/mmcblk0p2 /tmp/sys_prod"

#clean it up
ssh root@$IPOFTHEBOX "rm -fr /tmp/sys_prod/bin /tmp/sys_prod/etc /tmp/sys_prod/home /tmp/sys_prod/lib /tmp/sys_prod/opt /tmp/sys_prod/root /tmp/sys_prod/sbin /tmp/sys_prod/srv /tmp/sys_prod/static /tmp/sys_prod/usr /tmp/sys_prod/var"

#copy sys image over
scp provision/$TARBALLSYS pfm@$IPOFTHEBOX:/home/pfm/pfm_store/
#untar it over the partition
ssh root@$IPOFTHEBOX "tar -xvzpf /home/pfm/pfm_store/$TARBALLSYS -C /tmp/sys_prod/"

ssh root@$IPOFTHEBOX "rm -fr /tmp/sys_prod/boot/*"
ssh root@$IPOFTHEBOX "rm -fr /tmp/sys_prod/var/tmp/*"

#copy over changed fstab
scp provision/fstab root@$IPOFTHEBOX:/tmp/sys_prod/etc/

ssh root@$IPOFTHEBOX "mkdir /tmp/sys_prod/home/pfm/pfm_conf"
ssh root@$IPOFTHEBOX "chown pfm:pfm /tmp/sys_prod/home/pfm/pfm_conf"
ssh root@$IPOFTHEBOX "mkdir /tmp/sys_prod/home/pfm/pfm_store"
ssh root@$IPOFTHEBOX "chown pfm:pfm /tmp/sys_prod/home/pfm/pfm_store"

#copy over the app tarball
scp provision/ pfm@$IPOFTHEBOX:/home/pfm/pfm_store/

#symbolic links are already in the tarball
ssh pfm@$IPOFTHEBOX "tar -xvzpf app_update.tar.gz -C /tmp/sys_prod/home/pfm/"


##DD

#dd localy on mac. get a local copy 
#sudo dd if=/dev/rdisk3s2 of=/Volumes/BLACKBOX/pfm_images/pfm_sys_part2_150916.img bs=4m

#copy conf
#sudo dd if=/dev/disk2s3 of=pfm_conf_160916.img bs=4m

#copy over image
scp pfm_sys_part2_150916.img pfm@pfm_dev2:~/pfm_store/images/

#copy over conf_image
scp pfm_conf_160916.img.bz2 pfm@pfm_dev3:~/pfm_store/images/

#dd it to the other partition
dd if=pfm_sys_part2_150916.img of=/dev/mmcblk0p3 bs=4M

#check sys fs
fsck -f /dev/mmcblk0p3

#resize to fit partition
resize2fs /dev/mmcblk0p3

#check sys again
fsck -f /dev/mmcblk0p3

#dd conf to the other partition
dd if=pfm_conf_160916.img of=/dev/mmcblk0p5 bs=4M

#check conf fs
e2fsck -f /dev/mmcblk0p5

#resize conf
resize2fs /dev/mmcblk0p5

#check conf fs again
e2fsck -f /dev/mmcblk0p5

#set RTC according to remote device (laptop) time


#copy over app or resync git over sshfs
scp app_160916.tar.gz pfm@pfm_dev3:/home/pfm/pfm_store/tarballs/
tar xvzf app_160916.tar.gz -C /home/pfm/pfm_store/
chown -R pfm:pfm /home/pfm/pfm_store/app

#get the serial nr.
#from the user as base for hostname, SSID and revssh port
echo "Please type in the serial number and the press [Enter]:"
read SERIALNR

MYHOSTNAME="PFM${SERIALNR}"
echo "Host name will be: $MYHOSTNAME"

REVSSHPORT="101${SERIALNR}"
echo "revssh port will be: $REVSSHPORT"

#SSID?

#mount / to rw
mount -o rw,remount /

#setting the hostname on the machine
#should be done as root
hostnamectl set-hostname $MYHOSTNAME
#hostnamectl seems not to be entire complete
#/etc/hostname isnt set?

#write the sn somewhere
#revssh gets it from there and constructs th port
#out of it
#echo ${SERIALNR} > /home/pfm/pfm_conf/sn.txt

#generate ssh-key on the box
#-b 4096

rm /home/pfm/.ssh/id_rsa /home/pfm/.ssh/id_rsa.pub
ssh pfm@$IPOFTHEBOX 'ssh-keygen -b 2048 -f /home/pfm/.ssh/id_rsa -t rsa -q -N ""'

#change PIN in master conf derived from cpuserial on pi
#change boot flag

