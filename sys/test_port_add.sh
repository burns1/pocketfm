#!/bin/bash

show_port()
{
        # write revssh port configuration file
	#remove leading 0
	PORTNR=$(echo $SERIALNR | sed 's/^0*//')
        PFM_REVSSH_PORT=$((11000 + ${PORTNR}))
	
	echo "Serial: ${SERIALNR}"
	echo "Nr. to add: ${PORTNR}"
	echo "Rev-SSH port: ${PFM_REVSSH_PORT}"

}

echo "Please type in serial number and press [Enter]:"
read SERIALNR

show_port
