#!/bin/bash

# itest values for/from provisioning.sh
IPOFTHEBOX="192.168.153.20"
SERIALNR="666"


# assumes MICT pocketfm NAS mount point is mounted already
NAS_MNT_POINT="/mnt/mict/pocketfm"

REVSSH_SERVER="dam.mict-international.org"
REVSSH_USERNAME="mict"
REVSSH_PORT="2323"

RELEASE="current"
PROV_KEY_FILENAME="id_rsa_pfm_prov"
TARGET_HOSTNAME="pfm${SERIALNR}"
TARGET_PUB_KEY_FILENAME="id_rsa_${TARGET_HOSTNAME}.pub"

make_revsshkey()
{
    # copy open key for uploading provisioning details
    echo "cp -p ${NAS_MNT_POINT}/release/${RELEASE}/${PROV_KEY_FILENAME} /tmp"
    cp -p ${NAS_MNT_POINT}/release/${RELEASE}/${PROV_KEY_FILENAME} /tmp
    # tighten permissions
    chmod u+rwx,go-rwx /tmp/${PROV_KEY_FILENAME}
    
    # generate PocketFM ssh key for SSH server
    echo "ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX 'ssh-keygen -b 2048 -f /home/pfm/.ssh/id_rsa -t rsa -q -N \"\"'"
    ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX 'ssh-keygen -b 2048 -f /home/pfm/.ssh/id_rsa -t rsa -q -N ""'
    
    # copy pub key local
    scp -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX:.ssh/id_rsa.pub /tmp/${TARGET_PUB_KEY_FILENAME}
    
    KEY_EXIST=`ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} "ls ~/.ssh/${TARGET_PUB_KEY_FILENAME} 2>/dev/null"`
    
    if [ -z "${KEY_EXIST}" ]; then
    
        # copy public key to SSH server
        echo "scp -P ${REVSSH_PORT} -i /tmp/${TARGET_PUB_KEY_FILENAME} -o 'StrictHostKeyChecking no' ${REVSSH_USERNAME}@${REVSSH_SERVER}:.ssh/${TARGET_PUB_KEY_FILENAME}"
        scp -P ${REVSSH_PORT} -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} /tmp/${TARGET_PUB_KEY_FILENAME} ${REVSSH_USERNAME}@${REVSSH_SERVER}:.ssh/${TARGET_PUB_KEY_FILENAME}
        
        # add new public key to server's authorized_keys 
        echo "ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} \"cat ~/.ssh/${TARGET_PUB_KEY_FILENAME}\" >> ~/.ssh/authorized_keys"
        ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} "cat ~/.ssh/${TARGET_PUB_KEY_FILENAME} >> ~/.ssh/authorized_keys"

        # write revssh port configuration file
        PFM_REVSSH_PORT=$((10101 + ${SERIALNR}))
        echo "ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX \"cat ${PFM_REVSSH_PORT} >~/revsshport.txt\""
        ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX "cat ${PFM_REVSSH_PORT} >~/revsshport.txt"
    else
        echo "*** WARNING: ${TARGET_HOSTNAME} appears to be provsioned already, as there is an existing SSH key ${TARGET_PUB_KEY_FILENAME}. Exiting for safety."
        exit
    fi
}

make_revsshkey

