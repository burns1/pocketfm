#!/bin/bash
#
set -x

#in ap mode the ip of the box is always the same
IPOFTHEBOX="192.168.86.1"
#IPOFTHEBOX="192.168.153.20"
RELEASEDIR="/Volumes/pocketfm/release/current"
PFMSYSDIR="/home/pfm/pfm_store/sys"
PFMIMAGEDIR="/home/pfm/pfm_store/images"
RELEASESYSIMG="pfm_root_201609281920.img"
RELEASECONFIMG="pfm_conf_201609281845.img"
RELEASEAPP="pfm_app_0710161423.tar.gz"
RELEASECONFTAR="pfm_conf.tar.gz"
MEDIADIR="/Volumes/pocketfm/media"
#BETADIR="/Volumes/pocketfm/release/beta"
INFDIR="/Volumes/pocketfm/provisioned"

REVSSH_SERVER="dam.mict-international.org"
REVSSH_USERNAME="mict"
REVSSH_PORT="2323"

RELEASE="current"
PROV_KEY_FILENAME="id_rsa_pfm_prov"
TARGET_HOSTNAME="pfm${SERIALNR}"
TARGET_PUB_KEY_FILENAME="id_rsa_${TARGET_HOSTNAME}.pub"
TARGET_ROOT_MOUNT="/tmp/pfm_root"
TARGET_PFM_HOME="$TARGET_ROOT_MOUNT/home/pfm"
NAS_MNT_POINT="/Volumes/pocketfm"

#get current date on local machine
#
CURDATE=`date -u +%m%d%H%M%y`

make_revsshkey()
{
    # copy open key for uploading provisioning details
    #echo "cp -p ${NAS_MNT_POINT}/release/${RELEASE}/${PROV_KEY_FILENAME} /tmp/"
    cp -p ${NAS_MNT_POINT}/release/${RELEASE}/${PROV_KEY_FILENAME} /tmp/
    # tighten permissions
    chmod u+rwx,go-rwx /tmp/${PROV_KEY_FILENAME}
    
    # generate PocketFM ssh key for SSH server
    echo "ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX 'ssh-keygen -b 2048 -f ${TARGET_ROOT_MOUNT}/home/pfm/.ssh/id_rsa -t rsa -q -N \"\"'"
    ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX "echo -e  'y\n' | ssh-keygen -b 2048 -f ${TARGET_ROOT_MOUNT}/home/pfm/.ssh/id_rsa -t rsa -q -N ''"
    
    # copy pub key local
    scp -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX:${TARGET_PFM_HOME}/.ssh/id_rsa.pub /tmp/${TARGET_PUB_KEY_FILENAME}
    
    KEY_EXIST=`ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} "ls ~/.ssh/${TARGET_PUB_KEY_FILENAME} 2>/dev/null"`
    
    if [ -z "${KEY_EXIST}" ]; then
    
        # copy public key to SSH server
        echo "scp -P ${REVSSH_PORT} -i /tmp/${TARGET_PUB_KEY_FILENAME} -o 'StrictHostKeyChecking no' ${REVSSH_USERNAME}@${REVSSH_SERVER}:.ssh/${TARGET_PUB_KEY_FILENAME}"
        scp -P ${REVSSH_PORT} -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} /tmp/${TARGET_PUB_KEY_FILENAME} ${REVSSH_USERNAME}@${REVSSH_SERVER}:.ssh/${TARGET_PUB_KEY_FILENAME}
        
        # add new public key to server's authorized_keys 
        #echo "ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} \"cat ~/.ssh/${TARGET_PUB_KEY_FILENAME}\" >> ~/.ssh/authorized_keys"

	#leave out polluting authorized_key file for testing
	ssh -o 'StrictHostKeyChecking no' -i /tmp/${PROV_KEY_FILENAME} -p ${REVSSH_PORT} ${REVSSH_USERNAME}@${REVSSH_SERVER} "cat ~/.ssh/${TARGET_PUB_KEY_FILENAME} >> ~/.ssh/authorized_keys"

        # write revssh port configuration file
        PFM_REVSSH_PORT=$((10101 + ${SERIALNR}))
        #echo "ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX \"cat ${PFM_REVSSH_PORT} >${TARGET_ROOT_MOUNT}/home/pfm/revsshport.txt\""
        ssh -o 'StrictHostKeyChecking no' pfm@$IPOFTHEBOX "echo ${PFM_REVSSH_PORT} >${TARGET_ROOT_MOUNT}/home/pfm/revsshport.txt"
    else
        echo "*** WARNING: ${TARGET_HOSTNAME} appears to be provsioned already, as there is an existing SSH key ${TARGET_PUB_KEY_FILENAME}. Exiting for safety."
        exit
    fi
}

#set philipps sshkey in root and pfm
#sshpass -p "kultur" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $RELEASEDIR/id_rsa_philipp.pub root@${IPOFTHEBOX}:/root/.ssh/
#sshpass -p "kultur" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $RELEASEDIR/id_rsa_philipp.pub root@${IPOFTHEBOX}:/home/pfm/.ssh/
#sshpass -p "kultur" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$IPOFTHEBOX "cat /root/.ssh/id_rsa_philipp.pub root /root/.ssh/authorized_keys"
#sshpass -p "kultur" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$IPOFTHEBOX "cat /home/pfm/.ssh/id_rsa_philipp.pub root /home/pfm/.ssh/authorized_keys"

ssh root@$IPOFTHEBOX "mount -o rw,remount /"
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $RELEASEDIR/id_rsa_philipp.pub root@${IPOFTHEBOX}:/root/.ssh/
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$IPOFTHEBOX "cat /root/.ssh/id_rsa_philipp.pub >>/root/.ssh/authorized_keys"
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $RELEASEDIR/id_rsa_philipp.pub root@${IPOFTHEBOX}:/home/pfm/.ssh/
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$IPOFTHEBOX "cat /root/.ssh/id_rsa_philipp.pub >> /home/pfm/.ssh/authorized_keys"


#get the serial nr.
#from the user as base for hostname, SSID and revssh port
#TODO: would need some additional check/confirmation 
echo "Please type in serial number and press [Enter]:"
read SERIALNR


ssh pfm@$IPOFTHEBOX "mkdir -p /home/pfm/pfm_store/sys"
scp $RELEASEDIR/check_rtc_time.py pfm@${IPOFTHEBOX}:${PFMSYSDIR}/

#set current date first
ssh root@$IPOFTHEBOX "date -u $CURDATE"

#call the set rtc
ssh root@$IPOFTHEBOX "$PFMSYSDIR/check_rtc_time.py"

# make images store
ssh pfm@$IPOFTHEBOX "mkdir -p /home/pfm/pfm_store/images"

#copy over image
scp $RELEASEDIR/$RELEASESYSIMG pfm@$IPOFTHEBOX:${PFMIMAGEDIR}

#copy over conf_image
scp $RELEASEDIR/$RELEASECONFIMG pfm@$IPOFTHEBOX:${PFMIMAGEDIR}
#scp $RELEASEDIR/$RELEASECONFTAR pfm@$IPOFTHEBOX:${PFMIMAGEDIR}

#dd root to the other partition
ssh root@$IPOFTHEBOX "dd if=${PFMIMAGEDIR}/${RELEASESYSIMG} of=/dev/mmcblk0p3 bs=4M"

#check sys fs
ssh root@$IPOFTHEBOX "e2fsck -f -y /dev/mmcblk0p3"

#resize to fit partition
ssh root@$IPOFTHEBOX "resize2fs /dev/mmcblk0p3"

#check sys again
ssh root@$IPOFTHEBOX "e2fsck -f -y /dev/mmcblk0p3"

# umount the pfm_conf partition
ssh root@$IPOFTHEBOX "umount /home/pfm/pfm_conf"

#tar/untar pfm_conf
#clean pfm_conf
#ssh root@$IPOFTHEBOX "rm -fr ${TARGET_PFM_HOME}/pfm_conf/mplayer ${TARGET_PFM_HOME}/pfm_conf/NetworkManager ${TARGET_PFM_HOME}/pfm_conf/*.ini ${TARGET_PFM_HOME}/pfm_conf/vsftpd"
#ssh pfm@$IPOFTHEBOX "tar -xvzf $RELEASEDIR/$RELEASECONFTAR -C ${TARGET_PFM_HOME}/pfm_conf/"

#dd conf to the other partition
ssh root@$IPOFTHEBOX "dd if=${PFMIMAGEDIR}/${RELEASECONFIMG} of=/dev/mmcblk0p5 bs=4M"

#check conf fs
ssh root@$IPOFTHEBOX "e2fsck -f -y /dev/mmcblk0p5"

#resize conf
ssh root@$IPOFTHEBOX "resize2fs /dev/mmcblk0p5"

#check conf fs again
ssh root@$IPOFTHEBOX "e2fsck -f -y /dev/mmcblk0p5"

#mount production partition
ssh root@$IPOFTHEBOX "mkdir -p ${TARGET_ROOT_MOUNT}"
ssh root@$IPOFTHEBOX "mount /dev/mmcblk0p3 ${TARGET_ROOT_MOUNT}"

#copy over app
scp $RELEASEDIR/$RELEASEAPP pfm@$IPOFTHEBOX:${PFMIMAGEDIR}
ssh root@$IPOFTHEBOX "rm -fr ${TARGET_ROOT_MOUNT}/home/pfm/app"
ssh root@$IPOFTHEBOX "cd ${TARGET_ROOT_MOUNT}/home/pfm; tar xvzf ${PFMIMAGEDIR}/$RELEASEAPP"
ssh root@$IPOFTHEBOX "chown -R pfm ${TARGET_ROOT_MOUNT}/home/pfm/app"

MYHOSTNAME="pfm${SERIALNR}"
echo "Host name will be: $MYHOSTNAME"
TARGET_HOSTNAME="pfm${SERIALNR}"
TARGET_PUB_KEY_FILENAME="id_rsa_${TARGET_HOSTNAME}.pub"

#ssh root@$IPOFTHEBOX "mount -o rw,remount /"
#ssh root@$IPOFTHEBOX "hostnamectl set-hostname $MYHOSTNAME"
#chroot to set hostname? or reboot
ssh root@$IPOFTHEBOX "echo $MYHOSTNAME > ${TARGET_ROOT_MOUNT}/etc/hostname"

#call ssh-key distribution here
make_revsshkey

#copy over master conf
scp $RELEASEDIR/pfm_master.ini pfm@${IPOFTHEBOX}:${TARGET_PFM_HOME}/

#change owner of pfm_store for ftp 113/122
ssh root@$IPOFTHEBOX "chown 113:122 ${TARGET_PFM_HOME}/pfm_store"

#get the pi cpu serial nr for the pin
PISERIALHEX=`ssh pfm@$IPOFTHEBOX "cat /proc/cpuinfo | grep Serial"`
PISERIALHEX=${PISERIALHEX#'Serial		: '}
#echo "Serial in hex:$PISERIALHEX"

PISERIALDEC=$((16#$PISERIALHEX))
#echo "Serial in dec:$PISERIALDEC"

#get only the last digits
PISERIALDEC=${PISERIALDEC:(-6)}
echo "Serial in dec: $PISERIALDEC"

CONFPIN="PFM Support PIN = $PISERIALDEC"
CONFHOST="Pocket FM Host Identifier = ${MYHOSTNAME}"

#writing it to the master.ini
ssh pfm@$IPOFTHEBOX "echo $CONFPIN >> ${TARGET_PFM_HOME}/pfm_master.ini"
ssh pfm@$IPOFTHEBOX "echo $CONFHOST >> ${TARGET_PFM_HOME}/pfm_master.ini"

#change hostname
#SEDSTR="\'2!b;s/pfm_dev2_rep/${MYHOSTNAME}/\'"
#ssh pfm@$IPOFTHEBOX "sed -i ${SEDSTR} ${TARGET_PFM_HOME}/pfm_master.ini"

#create media dir and copy media files over
ssh pfm@$IPOFTHEBOX "mkdir -p ${TARGET_ROOT_MOUNT}/home/pfm/media"
scp -r $MEDIADIR/* pfm@$IPOFTHEBOX:${TARGET_ROOT_MOUNT}/home/pfm/media

scp -r $MEDIADIR/* pfm@$IPOFTHEBOX:/home/pfm/pfm_store

#copy over sys files
#needs to be part of the image
ssh pfm@$IPOFTHEBOX "mkdir -p ${TARGET_PFM_HOME}/bin"
scp $RELEASEDIR/revssh.sh pfm@${IPOFTHEBOX}:${TARGET_PFM_HOME}/bin
scp $RELEASEDIR/set_os_date2rtc.py pfm@${IPOFTHEBOX}:${TARGET_PFM_HOME}/bin

scp $RELEASEDIR/rc.local root@${IPOFTHEBOX}:${TARGET_ROOT_MOUNT}/etc

#vsftp
ssh root@$IPOFTHEBOX "mount /home/pfm/pfm_conf"
ssh pfm@$IPOFTHEBOX "mkdir -p /home/pfm/pfm_conf/vsftpd"
scp $RELEASEDIR/vsftpd-virtual-user.db pfm@${IPOFTHEBOX}:/home/pfm/pfm_conf/vsftpd/
scp $RELEASEDIR/vusers.txt pfm@${IPOFTHEBOX}:/home/pfm/pfm_conf/vsftpd/

#swap boot flag
ssh root@$IPOFTHEBOX "mount /dev/mmcblk0p1 /boot"
ssh root@$IPOFTHEBOX "echo 'dwc_otg.lpm_enable=0 console=tty1 elevator=deadline root=/dev/mmcblk0p3 rootfstype=ext4 fsck.repair=yes rootwait' > /boot/cmdline.txt"
ssh root@$IPOFTHEBOX "umount /boot"

echo "Serial number: ${SERIALNR}"
echo "Host name: ${MYHOSTNAME}"
echo "Support PIN: ${PISERIALDEC}"

if [ ! -d "${INFDIR}/${MYHOSTNAME}" ]; then
    mkdir -p ${INFDIR}/${MYHOSTNAME}
fi
echo "${SERIALNR}" > ${INFDIR}/${MYHOSTNAME}/${MYHOSTNAME}_sn.txt
#echo "${MYHOSTNAME}" > ${INFDIR}/${MYHOSTNAME}/${MYHOSTNAME}_hostname.txt
echo "${PISERIALDEC}" > ${INFDIR}/${MYHOSTNAME}/${MYHOSTNAME}_spin.txt
#copy over public ssh key
scp pfm@${IPOFTHEBOX}:${TARGET_PFM_HOME}/.ssh/id_rsa.pub ${INFDIR}/${MYHOSTNAME}/id_rsa_${MYHOSTNAME}.pub

ssh root@$IPOFTHEBOX "umount ${TARGET_ROOT_MOUNT}"

echo "Provisioning finished!"
#should reboot into new system
#ssh root@$IPOFTHEBOX "reboot"

